1             Starting year
1847          Last year
eq_enfarm.100      Site file name
0             Labeling type
-1            Labeling year
-1            Microcosm
-1            CO2 Systems
-1            pH shift
-1            Soil warming
-1            N input scalar option
-1            OMAD scalar option
-1            Climate scalar option
1             Initial system
G3            Initial crop
              Initial tree

Year Month Option
1             Block #   w/eq-specific <site.100>
1846          Last year
4             Repeats # years
1             Output starting year
12            Output month
1.000         Output interval
F             Weather choice
enfarm_1980-2017.wth         
   1   16 CROP TMC4 
   1   16 FRST 
   1  136 GRAZ GM_V 
   1  166 GRAZ GM_V 
   1  196 GRAZ GM_V 
   1  320 SENM
   1  350 LAST
   2   16 CROP TMC4 
   2   16 FRST
   2  136 GRAZ GM_V 
   2  166 GRAZ GM_V 
   2  196 GRAZ GM_V 
   2  320 SENM
   2  350 LAST
   3   16 CROP TMC4 
   3   16 FRST
   3  136 GRAZ GM_V 
   3  166 GRAZ GM_V 
   3  196 GRAZ GM_V 
   3  320 SENM
   3  350 LAST
   4   16 CROP TMC4 
   4   16 FRST 
   4  106 FIRE M    
   4  320 SENM
   4  350 LAST
-999 -999 X
2             Block #   Kill_block
1847          Last year
1             Repeats # years
1847          Output starting year
12            Output month
1.000         Output interval
C             Weather choice
   1    16 CROP TMC4 
   1    16 FRST
   1   228 FIRE H
   1   320 SENM
   1   350 LAST
-999 -999 X