2008          Starting year
2030          Last year
cnt_enfarm.100     Site file name
0             Labeling type
-1            Labeling year
-1            Microcosm
-1            CO2 Systems
-1            pH effect
-1            Soil warming
-1            N input scalar option
-1            OMAD scalar option
-1            Climate scalar option
1             Initial system
C10           Initial crop
              Initial tree

Year Month Option
1            Block #   @enfarm
2015         Last year
8            Repeats # years
2008         Output starting year
1            Output month
0.0833       Output interval
F            Weather choice
enfarm_2008-2019.wth
   1   127 CULT CSG
   1   127 CROP C10     #2008 Unknown, using corn
   1   127 PLTM
   1   127 FERT UAN17
   1   302 HARV G90S
   1   302 LAST
   2   132 CULT CSG
   2   132 CROP C10     #2009 Unknown, using corn
   2   132 PLTM
   2   132 FERT UAN20
   2   307 HARV G90S
   2   307 LAST
   3   145 CULT CSG
   3   145 CROP SYBN2   #2010 Soybean
   3   145 PLTM
   3   285 HARV G   
   3   285 LAST
   4   55 CULT CUT
   4   65 CULT JT
   4  100 CULT H1
   4  150 CROP MISC1    #2011 Miscanthus trials
   4  152 FRST
   4  200 CULT H1
   4  290 SENM
   4  365 LAST
   5   65 CULT CSG
   5   67 CROP MISC2    #2012 Miscanthus trials
   5   68 FRST
   5   68 FERT MISCt
   5  290 SENM
   5  320 SENM
   5  350 HARV MISC
   5  365 LAST
   6   65 CULT CSG
   6   67 CROP MISC3    #2013 Miscanthus trials
   6   68 FRST
   6   68 FERT MISCt
   6  290 SENM
   6  320 SENM
   6  350 HARV MISC   
   6  365 LAST
   7   65 CULT CSG
   7   67 CROP MISC4    #2014 Miscanthus trials  
   7   68 FERT MISCt 
   7   68 FRST
   7  290 SENM
   7  320 SENM
   7  350 HARV MISC
   7  365 LAST
   8   65 CULT CSG
   8   67 CROP MISC5    #2015 Miscanthus trials  
   8   68 FERT MISCt
   8   68 FRST
   8  290 SENM
   8  320 SENM
   8  350 HARV MISC
   8  365 LAST
-999 -999 X
2            Block #   @enfarm--C10-SYBN
2017         Last year
2            Repeats # years
2016         Output starting year
1            Output month
0.0833       Output interval
C            Weather choice
   1   130 CULT CSG
   1   130 CROP C10   
   1   130 PLTM
   1   130 FERT UAN20      #202_kg_N_ha
   1   296 HARV G90S
   1   296 LAST
   2   145 CULT CSG
   2   145 CROP SYBN2   
   2   145 PLTM
   2   285 HARV G   
   2   285 LAST
-999 -999 X
3            Block #   @enfarm--sorghum-150-lbsN-per-acre-BNI-soybean
2030         Last year
2            Repeats # years
2018         Output starting year
1            Output month
0.0833       Output interval
C            Weather choice
   1   138 CULT CSG
   1   138 CROP SORG1   
   1   138 PLTM            # Plant 2018-05-18
   1   142 FERT UAN8       # Fert 2018-05-22
   1   168 CULT CSG2
   1   170 FERT UAN8
   1   178 FERT BNI03      # BNI 3% 2018-06-27
   1   192 FERT BNI12
   1   198 CULT CSG2
   1   206 FERT BNI20      # BNI 20% 2018-07-25
   1   220 FERT BNI12
   1   228 CULT CSG2
   1   234 FERT BNI05      # BNI 5% 2018-08-22
   1   258 CULT CSG2
   1   288 HARV G95S       # Harv 2018-10-15
   1   288 LAST
   2   139 CULT CSG
   2   139 CROP SYBN2   
   2   139 PLTM            # Planted 2019-05-19
   2   285 HARV G   
   2   285 LAST
-999 -999 X

