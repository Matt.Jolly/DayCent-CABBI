
Name of the dataset:
  DayCent-CABBI_energy_farm_miscanthus_model_repository

Link to website, if applicable:
  Not Applicable

Version of dataset:

Date uploaded:
  June 8, 2020

Who uploaded the files:
  Melannie Hartman
  melannie.hartman@colostate.edu

Description of dataset: 

  Example of DayCent-CABBI simulation for miscanthus at the energy farm.
  Model results, including miscanthus biomass production for both above- 
  and below-ground biomass, are compared to measured data.
  The observed data are stored in the Data folder.
  Model documentation is stored in the Documentation folder.

Temporal resolution: 
  DayCent simulations produced montly and mean annual output years 1 to 2007.
  DayCent simulations produced daily and mean annual output years 2008 to 2019.
  Observed biomass data (Data folder) are mean annual values from 2008-2016.

Spatial resolution:
  These are single point-level simulations.

File naming system and File format:
  *.exe Windows 64-bit executables for the DayCent and list100 programs.
  *.R are R scripts set up to run the DayCent model, text format.
  *.100 are DayCent parameters files in text format.
  *.sch are DayCent schedule files in text format.
  *.wth are weather files read by the DayCent model, space delimented text format.
  *.in are other DayCent input files in text format.
  *.bin are binary files (not human readible) produced by the DayCent model.
  *.lis are annual to monthly output from the DayCent model in space-delimited text format.
  *.csv are annual to daily output from the DayCent model.
  *.log are log files containing DayCent screen output, saved by the R script.
  *.pl are perl scripts that Melannie uses, but aren't needed to run DayCent.


