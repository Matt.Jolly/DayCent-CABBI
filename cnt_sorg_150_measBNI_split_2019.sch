2008          Starting year
2030          Last year
cnt_enfarm.100     Site file name
0             Labeling type
-1            Labeling year
-1            Microcosm
-1            CO2 Systems
-1            pH effect
-1            Soil warming
-1            N input scalar option
-1            OMAD scalar option
-1            Climate scalar option
1             Initial system
C10           Initial crop
              Initial tree

Year Month Option
1            Block #   @enfarm
2015         Last year
8            Repeats # years
2008         Output starting year
1            Output month
0.0833       Output interval
F            Weather choice
enfarm_2008-2020.wth
   1   127 CULT CSG
   1   127 CROP C10      #2008 Unknown, using corn
   1   127 PLTM
   1   127 FERT UAN17
   1   302 HARV G90S
   1   302 LAST
   2   132 CULT CSG
   2   132 CROP C10      #2009 Unknown, using corn
   2   132 PLTM
   2   132 FERT UAN20
   2   307 HARV G90S
   2   307 LAST
   3   145 CULT CSG
   3   145 CROP SYBN2    #2010 Unknown, using soybean
   3   145 PLTM
   3   285 HARV G   
   3   285 LAST
   4   132 CULT CSG
   4   132 CROP SORG1     #2011 Sorghum
   4   132 PLTM
   4   132 FERT UAN11
   4   307 HARV G90S
   4   307 LAST
   5   145 CULT CSG
   5   145 CROP SYBN2     #2012 Soy
   5   145 PLTM
   5   285 HARV G   
   5   285 LAST
   6   132 CULT CSG
   6   132 CROP SORG1     #2013 Sorghum
   6   132 PLTM
   6   132 FERT UAN11
   6   307 HARV G90S
   6   307 LAST
   7   145 CULT CSG
   7   145 CROP SYBN2     #2014 Soy
   7   145 PLTM
   7   285 HARV G   
   7   285 LAST
   8   132 CULT CSG
   8   132 CROP SORG1     #2015 Sorghum
   8   132 PLTM
   8   132 FERT UAN11
   8   307 HARV G90S
   8   307 LAST
-999 -999 X
2            Block #   @enfarm--SYBN-C10
2017         Last year
2            Repeats # years
2016         Output starting year
1            Output month
0.0833       Output interval
C            Weather choice
   1   145 CULT CSG
   1   145 CROP SYBN2      #2016 Soy
   1   145 PLTM
   1   285 HARV G   
   1   285 LAST
   2   130 CULT CSG
   2   130 CROP C10        #2017 corn
   2   130 PLTM
   2   130 FERT UAN20      #202_kg_N_ha
   2   296 HARV G90S
   2   296 LAST
-999 -999 X
3            Block #   @enfarm--soybean-sorghum-150-lbsN-per-acre-BNI
2030         Last year
2            Repeats # years
2018         Output starting year
1            Output month
0.0833       Output interval
C            Weather choice
   1   139 CULT CSG
   1   139 CROP SYBN2   
   1   139 PLTM            # Planted 2019-05-19
   1   285 HARV G   
   1   285 LAST
   2   152 CULT CSG
   2   152 CROP SORG1   
   2   152 PLTM            # Plant 2019-06-01
   2   158 FERT UAN8       # Fert 2019-06-07
   2   182 CULT CSG2
   2   186 FERT UAN8
   2   189 FERT BNI14      # BNI 14% 2019-07-08
   2   203 FERT BNI36
   2   212 CULT CSG2
   2   218 FERT BNI58      # BNI 58% 2019-08-05
   2   232 FERT BNI40
   2   242 CULT CSG2
   2   246 FERT BNI22      # BNI 22% 2019-09-03
   2   272 CULT CSG2 
   2   283 HARV G95S       # Harv 2019-10-09
   2   283 LAST
-999 -999 X

