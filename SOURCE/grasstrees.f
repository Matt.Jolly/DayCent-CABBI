
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      grasstrees.f
c
c  FUNCTION:  subroutine grasstrees()
c
c  PURPOSE:   Compute daily production potential for a grasstree
c
c  AUTHOR:    New function derived from crop.
c             Melannie Hartman Jan. 24, 2014
c
c  FUNCTION ARGUMENTS:
c    time 	- simulation time (years)
c    bgwfunc 	- soil moisture effect on decomposition (0-1) (See calcdefac) 
c    tfrac	- time fraction (1/# days in current month)  
c    tavedly	- average daily temperature (C)
c    curday	- current day of year (1-366)
c    avgstemp	- weighted average soil temperature from the 2nd and 3rd
c          	  soil layers (deg C) (See calcdefac)
c    gtGrossPsn	- grasstree photosynthesis, with water stress
c
c  CALLED BY: simsom
c
c  CALLS: addomad, grasstreegrowth, falstdgt, drootgt, dshootgt, cultivgt
c 
c***********************************************************************

      subroutine grasstrees(time, curday, bgwfunc, tfrac, tavedly,
     &                      avgstemp, gtGrossPsn)

      implicit none
      include 'const.inc'
      include 'dovars.inc'
      include 'fertil.inc'
      include 'ligvar.inc'
      include 'param.inc'
c     include 'parcp.inc'
      include 'pargt.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot4.inc'
      include 'timvar.inc'

c ... Argument declarations
      real             time
      real             bgwfunc
      real             tfrac
      real             tavedly
      real             avgstemp
      double precision gtGrossPsn
      integer          curday

c ... Driver for calling all of grasstree code.

c ... Organic Matter additions
      if (doomad .and. (omadday .eq. curday)) then
        call addomad(time)
      endif

c ... If microcosm selected, skip the rest of the crop code
      if (micosm .eq. 1) then
        goto 999
      endif

c ... Update flows so direct absorption will be accounted for
c ... before plant uptake.
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar

c ... grasstreegrowth checks grasstreegrw computes actual growth 
c ... along with maintenance and growth respiration
      call grasstreegrowth(tfrac, tavedly, gtGrossPsn)
 
c ... Fall of standing dead
      call falstdgt(gtlig, tfrac)

c ... Death of roots
      call drootgt(gtlig, tfrac, avgstemp)

c ... Death of shoots
      call dshootgt(curday, bgwfunc, tfrac)

c ... Cultivation
      if (docult .and. (cultday .eq. curday)) then
        call cultivgt(gtlig)
      endif

c ... Update state variables and accumulators and sum carbon isotopes.

c     write(*,*) 'grasstrees Before flowup: gtlvcis(1)=', gtlvcis(1)
c     write(*,*) 'grasstrees Before flowup: gtlvcis(2)=', gtlvcis(2)
c     write(*,*) 'grasstrees Before flowup: gtstmcis(1)=', gtstmcis(1)
c     write(*,*) 'grasstrees Before flowup: gtstmcis(2)=', gtstmcis(2)
c     write(*,*) 'grasstrees Before flowup: gtdlvcis(1)=', gtdlvcis(1)
c     write(*,*) 'grasstrees Before flowup: gtdlvcis(2)=', gtdlvcis(2)
c     write(*,*) 'grasstrees Before flowup: gtdstmcis(1)=', gtdstmcis(1)
c     write(*,*) 'grasstrees Before flowup: gtdstmcis(2)=', gtdstmcis(2)
c     write(*,*) 'grasstrees Before flowup: gtfrtcisj(1)=', gtfrtcisj(1)
c     write(*,*) 'grasstrees Before flowup: gtfrtcisj(2)=', gtfrtcisj(2)
c     write(*,*) 'grasstrees Before flowup: gtfrtcism(1)=', gtfrtcism(1)
c     write(*,*) 'grasstrees Before flowup: gtfrtcism(2)=', gtfrtcism(2)
c     write(*,*) 'grasstrees Before flowup: gtcrtcis(1)=', gtcrtcis(1)
c     write(*,*) 'grasstrees Before flowup: gtcrtcis(2)=', gtcrtcis(2)
c     write(*,*) 'grasstrees Before flowup: gtleave(1)=', gtleave(1)
c     write(*,*) 'grasstrees Before flowup: gtsteme(1)=', gtsteme(1)
c     write(*,*) 'grasstrees Before flowup: gtfrootej(1)=', gtfrootej(1)
c     write(*,*) 'grasstrees Before flowup: gtfrootem(1)=', gtfrootem(1)
c     write(*,*) 'grasstrees Before flowup: gtcroote(1)=', gtcroote(1)
c     write(*,*) 'grasstrees Before flowup: gtdleave(1)=', gtdleave(1)
c     write(*,*) 'grasstrees Before flowup: gtdsteme(1)=', gtdsteme(1)

      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar

c     write(*,*) 'grasstrees After flowup: gtlvcis(1)=', gtlvcis(1)
c     write(*,*) 'grasstrees After flowup: gtlvcis(2)=', gtlvcis(2)
c     write(*,*) 'grasstrees After flowup: gtstmcis(1)=', gtstmcis(1)
c     write(*,*) 'grasstrees After flowup: gtstmcis(2)=', gtstmcis(2)
c     write(*,*) 'grasstrees After flowup: gtdlvcis(1)=', gtdlvcis(1)
c     write(*,*) 'grasstrees After flowup: gtdlvcis(2)=', gtdlvcis(2)
c     write(*,*) 'grasstrees After flowup: gtdstmcis(1)=', gtdstmcis(1)
c     write(*,*) 'grasstrees After flowup: gtdstmcis(2)=', gtdstmcis(2)
c     write(*,*) 'grasstrees After flowup: gtfrtcisj(1)=', gtfrtcisj(1)
c     write(*,*) 'grasstrees After flowup: gtfrtcisj(2)=', gtfrtcisj(2)
c     write(*,*) 'grasstrees After flowup: gtfrtcism(1)=', gtfrtcism(1)
c     write(*,*) 'grasstrees After flowup: gtfrtcism(2)=', gtfrtcism(2)
c     write(*,*) 'grasstrees After flowup: gtcrtcis(1)=', gtcrtcis(1)
c     write(*,*) 'grasstrees After flowup: gtcrtcis(2)=', gtcrtcis(2)
c     write(*,*) 'grasstrees After flowup: gtleave(1)=', gtleave(1)
c     write(*,*) 'grasstrees After flowup: gtsteme(1)=', gtsteme(1)
c     write(*,*) 'grasstrees After flowup: gtfrootej(1)=', gtfrootej(1)
c     write(*,*) 'grasstrees After flowup: gtfrootem(1)=', gtfrootem(1)
c     write(*,*) 'grasstrees After flowup: gtcroote(1)=', gtcroote(1)
c     write(*,*) 'grasstrees After flowup: gtdleave(1)=', gtdleave(1)
c     write(*,*) 'grasstrees After flowup: gtdsteme(1)=', gtdsteme(1)

999   continue

      return
      end
