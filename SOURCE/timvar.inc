
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


c ... Time variables not declared by Time-0
c ... blktnd    = ending time of the block
c ... daylength = amount of daylight hours (1..24)
c ... decodt    = time step used for the decomposition model
c ... month     = current month
c ... strtyr    = starting year of the block
c ... tplt      = next time (years) when variables should be written to
c ...             the plot file
c ... co2year   = most recent year read from co2conc.dat (when co2sys=2)

      common/timvar/blktnd, decodt, month, strtyr, tplt, strplt,
     &              daylength(366), co2year
      integer month, strtyr, co2year
      real blktnd, decodt, tplt, strplt, daylength

      save /timvar/
