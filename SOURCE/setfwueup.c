
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      setfwueup.c
**
**  FUNCTION:  void setfwueup()
**
**  PURPOSE:   Set fwueup[] using the value layers->ftransp[] or layers->tcoeff[]
**             (daily soil model). The value of layers->ftransp[] is reset daily 
**             in function soiltransp. The value of layers->tcoeff[] is set once
**             when the soils.in file is read in function initlyrs.
**
**  INPUTS:
**    nlyrpg   - number of rooting layers in Century soil profile
**    dofwueup = 0 when E uptake is based on relative abundance of E in each layer
**             = 1 when E uptake is based on amount of transpiration in each layer
**             = 2 when E uptake is based root fractions of each layer (soils.in)
**
**  GLOBAL VARIABLES:
**    CMXLYR   - maximum number of Century soil layers (10)
**    MXSWLYR  - maximum number of soil water model layers (21)
**    layers->lbnd[] - index of the lower soil water model layer which
**                     corresponds to clyr in Century
**    layers->ubnd[] - index of the upper soil water model layer which
**                     corresponds to clyr in Century
**    layers->ftransp[] - fraction of transpiration from DayCent soil layers
**    layers->tcoeff[]  - fraction of roots in each DayCent soil layers
**
**  LOCAL VARIALBES:
**    clyr -  Century soil layer (1..nlyrpg)
**
**  OUTPUTS:
**    fwueup[clyr] - fraction of transpiration or root density from Century
**                   soil layers, to be used to calculate mineral E uptake. The 
**                   sum of all the fractions will equal 1.0 exactly.
**
**  CALLED BY:
**    growth()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "soilwater.h"
#include "swconst.h"

    void setfwueup(int *nlyrpg, int *dofwueup, float fwueup[CMXLYR])
    {
      int clyr, ilyr;
      int debug = 0;
      float fwueupSum = 0.0;
      extern LAYERPAR_SPT layers;

      if (*dofwueup !=1 && *dofwueup !=2)
      {
         fprintf(stderr,"Unexpected value of dofwueup in setfwueup: %1d\n", *dofwueup);
         fprintf(stderr,"Expecting a value of 1 or 2.\n");
         exit(1);
      }

      if (debug) printf("\n");
      for(clyr = 0; clyr < *nlyrpg; clyr++) 
      {
          fwueup[clyr] = 0.0;
          for (ilyr = layers->ubnd[clyr]; ilyr <= layers->lbnd[clyr]; ilyr++)
          {
              if (*dofwueup == 1) 
                  fwueup[clyr] += layers->ftransp[ilyr];
              else
                  fwueup[clyr] += layers->tcoeff[ilyr];
          }
          if (debug) 
          {
              /* Show FORTRAN index (clyr+1) in output. */
              printf("fwueup[%1d] = %6.4f\n", clyr+1, fwueup[clyr]);
          }
          fwueupSum += fwueup[clyr];
      }
      
      if (debug) 
      {
          printf("fwueupSum = %6.4f\n", fwueupSum);
      }

      /* Normalize fwueup[*] so sum of fractions == 1.0 exactly */
      for(clyr = 0; clyr < *nlyrpg; clyr++) 
      {
          fwueup[clyr] /= fwueupSum;
      }

      return;
    }
