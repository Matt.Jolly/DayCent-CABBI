
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      initsite_tg.c
**
**  FUNCTION:  void initsite()
**
**  PURPOSE:   Read in site specific parameters (from sitepar.in)
**
**  AUTHOR:    Susan Chaffee    March 10, 1992
**
**  REWRITE:   Melannie Hartman  9/9/93 - 9/23/93
**
**  HISTORY:
**    8/13/92 (SLC) - Change the way the parameter for minimum soil water
**                    content is used.  No longer a function of wilting point,
**                    now it simply gives the percent of water at each layer.
**    5/08/08 (CAK) - Add checks for end of file, if the end of the file is
**                    reached before all of the variables have been read and
**                    initialized this will result in a fatal error.
**
**  INPUTS:
**    flags          - structure containing debugging flags
**    flags->debug   - flag to set debugging mode, 0 = off, 1 = on
**    flags->verbose - flag to set verbose debugging mode, 0 = off, 1 = on
**    sitename       - data file name containing site parameters
**    sitepar        - site specific parameters structure for soil water model
**    layers         - soil water soil layer structure
**
**  GLOBAL VARIABLES:
**    INPTSTRLEN - maximum length of input file line (120)
**    NTDEPTHS   - maximum number of soil regions (4)
**
**  LOCAL VARIABLES:
**    errmsg[]   - string containing error message
**    fp_in      - pointer to input file
**    imo        - current month (0..11)
**    inptline[] - line read from input file
**    m          - month
**
**  OUTPUTS:
**    Read from file sitename:
**    bioabsorp              - litter biomass at full absorption of radiation
**                             (grams biomass)
**    maxphoto               - maximum carbon loss due to photodecomposition
**                             (ug C/KJ srad)
**    sitepar->albedo        - fraction of light reflected by snow
**    sitepar->aspect        - site aspect (degrees)
**    sitepar->Aeh           - differential coefficient for calculation of
**                             soil Eh
**    sitepar->Beh_drain     - upper-limit value of Eh during drainage
**                             course (mv)
**    sitepar->Beh_flood     - low-limit value for Eh during flooding
**                             course (mv)
**    sitepar->C6H12O6_to_CH4 - reaction of anaerobic carbohydrate
**                              fermentation with methanogenesis
**                              (mole weight)
**    sitepar->cldcov[]      - average cloud cover for the month (%, 1..100)
**    sitepar->CO2_to_CH4    - fraction of CO2 from heterotrophic soil
**                             respiration that is used to produce CH4
**                             (0.0 - 1.0)
**    sitepar->Deh           - differential coefficient for calculation of
**                             soil Eh
**    sitepar->dmp           - damping factor for calculating soil temperature
**                             by layer
**    sitepar->dmpflux       - damping factor for soil water flux (in h2oflux)
**    dofDpH                 - compute pH effect on gross denitrification (1=yes, 0=no) 
**                             (added -mdh 2/7/2019)
**    dofRpH                 - compute pH effect on Ratio N2:N2O during denitrification 
**                             (1=yes, 0=no) (added -mdh 2/7/2019)
**    dofwueup               = 0 when E uptake is based on relative abundance of E in each layer (default)
**                           = 1 when E uptake is based on amount of transpiration in each layer
**                           = 2 when E uptake is based root fractions of each layer (soils.in)
**                             The default is to take up E in proportion to N amount in each soil layer.
**    sitepar->drainlag      - number of days that soil drainage should lag
**                             behind rain/irrigation/melt events (1-5)
**    sitepar->ehoriz        - site east horizon, degrees
**    sitepar->elevation     - site elevation (meters)
**    sitepar->frac_to_exudates - fraction of fine root production that is
**                                root exudates
**    sitepar->fswcinit      - initial soil water content, fraction of field
**                             capacity (0.0 - 1.0)
**    sitepar->hours_rain    - the duration of the rainfall/snowmelt event
**                             (hours)
**    sitepar->hpotdeep      - hydraulic water potential of deep storage
**                             layer, the more negative the number the dryer
**                             the soil layer (units?)
**    sitepar->jdayEnd       - the day of the year to end the turning off of
**                             the restriction of the CO2 effect on
**                             denitrification
**    sitepar->jdayStart     - the day of the year to start the turning off of
**                             the restriction of the CO2 effect on
**                             denitrification
**    sitepar->ksatdeep      - saturated hydraulic conductivity of deep
**                             storage layer (cm/sec)
**    sitepar->MaxNitAmt     - maximum daily nitrification amount (gN/m^2)
**    sitepar->N2N2Oadj      - multiplier on N2/N2O ratio
**    sitepar->NO3_N2O_slope - slope, controls how fast the percentage of NO3
**                             from mineralization that is lost as N2O changes
**                             as a function of water filled pore space
**    sitepar->NO3_N2O_step  - step between minimum and maximum percentage of
**                             NO3 from mineralization that is lost as N2O
**    sitepar->NO3_N2O_x     - x-inflection, water filled pore space where 1/2
**                             of maximum percentage of NO3 from
**                             mineralization is lost as N2O
**    sitepar->NO3_N2O_y     - y-inflection, percentage of NO3 from
**                             mineralization that is lost as N2O at
**                             x-inflection water filled pore space
**    sitepar->Ncoeff        - minimum water/temperature limitation
**                             coefficient for nitrification
**    sitepar->netmn_to_no3  - fraction of new net mineralization that goes to
**                             NO3 (0.0-1.0)
**    sitepar->reflec        - fraction of light reflected by vegetation
**    sitepar->slope         - site slope (degrees)
**    sitepar->SnowFlag      - effect of snow on soil surface temperature calc
**                             1 = insulating effect, 0 = no insulating effect
**    sitepar->sublimscale   - multiplier to scale sublimation
**    sitepar->tbotmn        - minimum temperature for bottom soil layer
**                             (degrees C)
**    sitepar->tbotmx        - maximum temperature for bottom soil layer
**                             (degrees C)
**    sitepar->texture       - texture classification for trace gas model
**                             (1 = coarse, 2 = medium, 3 = fine)
**    sitepar->th2ocoef1     - relative soil water content required for 50% of
**                             maximum transpiration
**    sitepar->th2ocoef2     - 4 times the slope at relative soil water
**                             content required for 50% of maximum
**                             transpiration
**    sitepar->timlag        - days from Jan 1 to coolest temp at bottom of
**                             soil (days)
**    sitepar->usexdrvrs     - 0 = use air temperature to drive PET rates
**                             1 = use extra weather drivers (solrad, rhumid,
**                                 windsp) for PET calculation
**                             2 = use extra weather drivers (srad, vpd)
**                                 for photosynthesis calculation
**                             3 = use extra drivers for both PET and
**                                 photosynthesis calculations
**    sitepar->wfpsdnitadj   - adjustment on inflection point for water filled
**                             pore space effect on denitrification curve
**    sitepar->whoriz        - site west horizon, degrees
**    sitepar->zero_root_frac - fraction of CH4 emitted via bubbles when there
**                              is zero root biomass (0.0-1.0)
**    sradadj[]              - solar radiation adjustment for cloud cover and
**                             transmission coeffient
**    tminintercept          - intercept used to adjust minimum temperature
**                             for calculating VPD at dewpoint
**    tminslope              - slope used to adjust minimum temperature
**                             for calculating VPD at dewpoint
**
**  EXAMPLE INPUT FILE:
**  0         / usexdrvrs: 0 = no extra drivers, 1 = PET drivers, 2 = psyn drivers, 3 = both
**  1.0       / sublimscale: multiplier on sublimation (1.0 no effect)
**  0.18      / reflec: vegetation reflectivity (frac)
**  0.65      / albedo of snow (frac) 
**  0.90      / fswcinit: initial swc, fraction of field capacity
**  0.0000008 / dmpflux: in h2oflux routine (0.000001 = original value)
**  10        / hours_rain: duration of each rain event (hours)
**  0        / drainlag: # of days between rainfall event and drainage of soil (-1=computed)
**  -200     / hpotdeep: hydraulic water potential of deep storage layer (units?)
**  0.0003   / ksatdeep: saturated hydraulic conductivity of deep storage layer (cm/sec)
**  0        / cldcov[1]: cloud cover month 1 (%). For PET and sublim calcs when usexdrivers= 1 or 3
**  0        / cldcov[2]
**  0        / cldcov[3]
**  0        / cldcov[4]
**  0        / cldcov[5]
**  0        / cldcov[6]
**  0        / cldcov[7]
**  0        / cldcov[8]
**  0        / cldcov[9]
**  0        / cldcov[10]
**  0        / cldcov[11]
**  0        / cldcov[12]
**  0.0      / tbotmn: min and max temperature for bottom soil layer (degrees C)
**  12.4     / tbotmx: min and max temperature for bottom soil layer (degrees C)
**  0.003    / dmpsoiltemp: damping factor for calculating soil temperature by layer
**  30.0     / timlag: days from Jan 1 to coolest temp at bottom of soil (days)
**  0.03     / Ncoeff: min water/temperature limitation coefficient for nitrify
**  0        / jdayStart: turn off respiration restraint on denit from jdyStart to jdyEnd
**  0        / jdayEnd: turn off respiration restraint on denit from jdyStart to jdyEnd
**  0.7      / NO3_N2O_x: x-inflection, WFPS where 1/2 of max % of NO3 is lost as N2O
**  2.0      / NO3_N2O_y: y-inflection, percentage of NO3 lost as N2O at x-inflection WFPS
**  4.0      / NO3_N2O_step: step between minimum and maximum percentage of NO3 lost as N2O
**  5.0      / NO3_N2O_slope: slope, controls how fast the percent changes as a function of WFPS
**  0.4      / MaxNitAmt: maximum daily nitrification amount (gN/m^2)
**  1        / SnowFlag: snow effect on soil surface temp: 0 = not insulating, 1 = insulating
**  0.2      / netmn_to_no3: fraction of new net mineralization that goes to NO3 (0.0-1.0)
**  1.0      / wfpsadjdnit: adjustment multiplier on inflection point for WFPS effect on denit
**  1.0      / n2n2oadj: adjustment multiplier on N2/N2O ratio
**  0.378    / th2ocoef1: relative swc required for 50% of maximum transpiration
**  9.0      / th2ocoef2: 4 times the slope at relative swc required for 50% of max transp
**  2.0      / flood_N2toN2O: N2/N2O ratio for flooded state
**  0.5      / CO2_to_CH4: fraction of CO2 from soil respiration used to produce CH4
**  0.27     / C6H12O6_to_CH4:reaction anaerob. carbohydrate ferment. w/ methanogen.(mole weight C6H12O6:CH4)
**  0.45     / frac_to_exudates: fraction of root production that is root exudates
**  0.23     / Aeh: differential coefficient (Aeh)
**  0.16     / Deh: differential coefficient (Deh)
**  -250.0   / Beh_flood: low-limit value for Eh during flooding course (mv)
**  300.0    / Beh_drain: upper-limit value of Eh during drainage course (mv)
**  7.0      / zero_root_frac: fraction CH4 emitted via bubbles when zero root biomass (0.0-1.0)
**  1646.0   / elevation: (meters)
**  0.0      / siteslope: site slope, degrees
**  0.0      / aspect: site aspect, degrees
**  0.0      / ehoriz: site east horizon, degrees
**  0.0      / whoriz: site west horizon, degrees
**  1.39     / sradadj[1]: solar radiation adjustment for cloud cover & transmission coeffient month=1
**  1.33     / sradadj[2]
**  1.24     / sradadj[3]
**  1.22     / sradadj[4]
**  1.25     / sradadj[5]
**  1.28     / sradadj[6]
**  1.28     / sradadj[7]
**  1.27     / sradadj[8]
**  1.28     / sradadj[9]
**  1.31     / sradadj[10]
**  1.38     / sradadj[11]
**  1.39     / sradadj[12]
**  1.0      / tminslope: slope for adjusting minimum temperature for VPD dewpoint calc
**  0.0      / tminintercept: intercept for adjusting minimum temperature for VPD dewpoint calc
**  1.50     / maxphoto: maximum carbon loss due to photodecomposition (ug C/KJ srad)
**  200.0    / bioabsorp: litter biomass for full absorption of solar radiation (g biomass)
**  1        / dofDpH: compute pH effect on total denitrification? (1=yes, 0=no)
**  1        / dofRpH: compute pH effect on Ratio N2:N2O during denitrification? (1=yes, 0=no)
**  2        / dofwueup: plant N,P,S uptake by layer based on (0=N,P,S distribution,1=transpiration,2=root density)
**
**  CALLED BY:
**    initsw()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include "soilwater.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SITEPARMISSINGVAL -9999

    void initsite(char *sitename, SITEPAR_SPT sitepar, LAYERPAR_SPT layers,
                  FLAG_SPT flags, float sradadj[NMONTH], float *tminslope,
                  float *tminintercept, float *maxphoto, float *bioabsorp,
                  int *dofDpH, int *dofRpH, int *dofwueup)
    {

      int  imo, fcnt, linecnt;
      char inptline[INPTSTRLEN];
      char errmsg[INPTSTRLEN];
      float fval;
      FILE *fp_in;

      if (flags->debug) {
        printf("Entering function initsite\n");
      }

      if ((fp_in = fopen(sitename, "r")) == NULL) {
        sprintf(errmsg, "Cannot open file %s\n", sitename);
        perror(errmsg);
        exit(1);
      }

      /* Give all parameters missing values so we can determine */
      /* if they were specified in the sitepar.in file. -mdh 5/10/2019 */

      sitepar->usexdrvrs = SITEPARMISSINGVAL-1;
      sitepar->sublimscale = SITEPARMISSINGVAL-1;
      sitepar->reflec = SITEPARMISSINGVAL-1;
      sitepar->albedo = SITEPARMISSINGVAL-1;
      sitepar->fswcinit = SITEPARMISSINGVAL-1;
      sitepar->dmpflux = SITEPARMISSINGVAL-1;
      sitepar->hours_rain = SITEPARMISSINGVAL-1;
      sitepar->drainlag = SITEPARMISSINGVAL-1;
      sitepar->hpotdeep = SITEPARMISSINGVAL-1;
      sitepar->ksatdeep = SITEPARMISSINGVAL-1;
      sitepar->cldcov[0] = SITEPARMISSINGVAL-1;
      for(imo=1; imo<=11; imo++) 
      {
          sitepar->cldcov[imo] = 0.0;
      }
      sitepar->tbotmn = SITEPARMISSINGVAL-1;
      sitepar->tbotmx = SITEPARMISSINGVAL-1;
      sitepar->dmp = SITEPARMISSINGVAL-1;
      sitepar->timlag = SITEPARMISSINGVAL-1;
      sitepar->Ncoeff = SITEPARMISSINGVAL-1;
      sitepar->jdayStart = SITEPARMISSINGVAL-1;
      sitepar->jdayEnd = SITEPARMISSINGVAL-1;
      sitepar->NO3_N2O_x = SITEPARMISSINGVAL-1;
      sitepar->NO3_N2O_y = SITEPARMISSINGVAL-1;
      sitepar->NO3_N2O_step = SITEPARMISSINGVAL-1;
      sitepar->NO3_N2O_slope = SITEPARMISSINGVAL-1;
      sitepar->MaxNitAmt = SITEPARMISSINGVAL-1;
      sitepar->SnowFlag = SITEPARMISSINGVAL-1;
      sitepar->netmn_to_no3 = SITEPARMISSINGVAL-1;
      sitepar->wfpsdnitadj = SITEPARMISSINGVAL-1;
      sitepar->n2n2oadj = SITEPARMISSINGVAL-1;
      sitepar->th2ocoef1 = SITEPARMISSINGVAL-1;
      sitepar->th2ocoef2 = SITEPARMISSINGVAL-1;
      sitepar->flood_N2toN2O = SITEPARMISSINGVAL-1;
      sitepar->CO2_to_CH4 = SITEPARMISSINGVAL-1;
      sitepar->C6H12O6_to_CH4 = SITEPARMISSINGVAL-1;
      sitepar->frac_to_exudates = SITEPARMISSINGVAL-1;
      sitepar->Aeh = SITEPARMISSINGVAL-1;
      sitepar->Deh = SITEPARMISSINGVAL-1;
      sitepar->Beh_flood = SITEPARMISSINGVAL-1;
      sitepar->Beh_drain = SITEPARMISSINGVAL-1;
      sitepar->zero_root_frac = SITEPARMISSINGVAL-1;
      sitepar->elevation = SITEPARMISSINGVAL-1;
      sitepar->slope = SITEPARMISSINGVAL-1;
      sitepar->aspect = SITEPARMISSINGVAL-1;
      sitepar->ehoriz = SITEPARMISSINGVAL-1;
      sitepar->whoriz = SITEPARMISSINGVAL-1;
      sradadj[0] = SITEPARMISSINGVAL-1;
      for(imo=0; imo<=11; imo++) 
      {
          sradadj[imo] = 1.0;
      }
      *tminslope = SITEPARMISSINGVAL-1;
      *tminintercept = SITEPARMISSINGVAL-1;
      *maxphoto = SITEPARMISSINGVAL-1;
      *bioabsorp = SITEPARMISSINGVAL-1;
      *dofDpH = SITEPARMISSINGVAL-1;
      *dofRpH = SITEPARMISSINGVAL-1;
      *dofwueup = SITEPARMISSINGVAL-1;
      
      linecnt = 0;

      /*****************************************************************************************/
      /* Code from Kendrick to check sitepar.in format 
       */

      flags->verbose = 1;

      while (fgets(inptline, INPTSTRLEN, fp_in) != NULL) {
        fcnt = sscanf(inptline, "%f", &fval);
        linecnt++;
        if(fcnt != 1) {
          printf("WARNING: unable to parse sitein: %s",inptline);
          continue;
        }

        if(strstr(inptline,"usexdrvrs") != NULL) {
          sscanf(inptline, "%d", &sitepar->usexdrvrs);
          if (flags->verbose) {printf("sitepar; usexdrvrs: %1d\n", sitepar->usexdrvrs);}
          if (sitepar->usexdrvrs < 0 || sitepar->usexdrvrs > 3) {
            printf("ERROR:  Invalid usexdrvrs in sitepar.in: %1d\n", sitepar->usexdrvrs);
          }
        }

        else if(strstr(inptline,"sublimscale") != NULL) {
          sitepar->sublimscale = fval;
          if (flags->verbose) {
            printf("sitepar; sublimscale, sublimation scalar: %f\n", sitepar->sublimscale);
          }
        }

        else if(strstr(inptline,"reflec") != NULL) {
          sitepar->reflec = fval;
          if (flags->verbose) {
            printf("sitepar; reflec, vegetation reflectivity, reflec: %f\n", sitepar->reflec);
          }
        }

        else if(strstr(inptline,"albedo") != NULL) {
          sitepar->albedo = fval;
          if (flags->verbose) {
            printf("sitepar; albedo: snow albedo: %f\n", sitepar->albedo);
          }
        }
 
        else if(strstr(inptline,"fswcinit") != NULL) {
          sitepar->fswcinit = fval;
          if (flags->verbose) {
            printf("sitepar; fswcinit, initial soil water content: %f\n", sitepar->fswcinit);
          }
        }

        else if(strstr(inptline,"dmpflux") != NULL) {
          sitepar->dmpflux = fval;
          if (flags->verbose) {
            printf("sitepar; dmpflux, soil water flux damping factor: %10.8f\n", sitepar->dmpflux);
          }
        }

        else if(strstr(inptline,"hours_rain") != NULL) {
          sitepar->hours_rain = fval;
          if (flags->verbose) {
            printf("sitepar; hours_rain, rainfall duration: %f\n", sitepar->hours_rain);
          }
        }
 
        /* Allow user to set number of days between rainfall event and */
        /* drainage of soil profile.  If a value of -1 is entered set the */
        /* number of days to drainage based in the soil texture.  Constrain */
        /* the number of days to drainage to be <=5 to prevent numerical */
        /* instabilities in the h2oflux subroutine.  cak - 02/13/04 */

        else if(strstr(inptline,"drainlag") != NULL) {
          sitepar->drainlag = fval;
          if (sitepar->drainlag < 0) {
            sitepar->drainlag = sitepar->texture - 1;
          }
          if (sitepar->drainlag > 5) {
            printf("sitepar; drainlag, lag period for drainage too long, setting to max value of 5\n");
            sitepar->drainlag = 5;
          }
          if (flags->verbose) {
            printf("sitepar; drainlag: %d\n", sitepar->drainlag);
          }
        }

        else if(strstr(inptline,"hpotdeep") != NULL) {
          sitepar->hpotdeep = fval;
          if (flags->verbose) {
            printf("sitepar; hpotdeep, deep storage hydraulic potential: %f\n", sitepar->hpotdeep);
          }
        }

        else if(strstr(inptline,"ksatdeep") != NULL) {
          sitepar->ksatdeep = fval;
          if (flags->verbose) {
            printf("sitepar; ksatdeep, deep storage saturated hydraulic conductivity: %f\n", sitepar->ksatdeep);
          }
        }

        else if(strstr(inptline,"cldcov") != NULL) {
          imo = 0;
          sscanf(inptline, "%f", &sitepar->cldcov[imo]);
          if (flags->verbose) {
            printf("sitepar; cldcov[%1d]: %f\n", imo+1, sitepar->cldcov[imo]);
          }
          for(imo=1; imo<=11; imo++) 
          {
            if (fgets(inptline, INPTSTRLEN, fp_in) != NULL) 
            {
              linecnt++;
              if(strstr(inptline,"cldcov") != NULL) 
              {
                  sscanf(inptline, "%f", &sitepar->cldcov[imo]);
                  if (flags->verbose) {
                    printf("sitepar; cldcov[%1d]: %f\n", imo+1, sitepar->cldcov[imo]);
                  }
              }
              else
              {
                printf("ERROR:  Missing average cloud cover (cldcov) for month %1d\n", imo+1);
                printf("in sitepar.in file.  Ending simulation!\n");
                exit(1);
              }
            }
            else 
            {
              printf("ERROR:  Unexpected end of file in sitepar.in.\n");
              printf("Ending simulation!\n");
              exit(1);
            }
          }
        }

        /* The texture parameter is being set in the initlyrs subroutine  CAK - 05/31/01 */
        /* The texture parameter has been replaced by the minimum and */
        /* maximum temperature values for the bottom soil layer */
        else if(strstr(inptline,"tbotmn") != NULL) {
          sscanf(inptline, "%f", &sitepar->tbotmn);
          if (flags->verbose) {
            printf("sitepar; tbotmn: min temperature for bottom soil layer, tbotmn: %f\n", sitepar->tbotmn);
          }
        }

        else if(strstr(inptline,"tbotmx") != NULL) {
          sscanf(inptline, "%f", &sitepar->tbotmx);
          if (flags->verbose) {
            printf("sitepar; tbotmx: max temperature for bottom soil layer, tbotmx: %f\n", sitepar->tbotmx);
          }
          if (sitepar->tbotmn > sitepar->tbotmx) {
            printf("ERROR:  base layer minimum temperature is greater than maximum, tbotmn>tbotmx, in sitepar.in\n");
          }
        }

      /* Added dmp parameter to the sitepar.in file, cak - 12/16/02 */
        else if(strstr(inptline,"dmpsoiltemp") != NULL) {
          sitepar->dmp = fval;
          if (flags->verbose) {
            printf("sitepar; soil temperature damping factor, dmp: %f\n", sitepar->dmp);
          }
        }

      /* Added timlag parameter to the sitepar.in file, cak - 04/24/03 */
        else if(strstr(inptline,"timlag") != NULL) {
          sitepar->timlag = fval;
          if (flags->verbose) {
            printf("sitepar; timlag, days from Jan 1 to coolest temp at bottom of soil: %f\n", sitepar->timlag);
          }
        }

      /* Added Ncoeff parameter to the sitepar.in file, cak - 04/08/03 */
        else if(strstr(inptline,"Ncoeff") != NULL) {
          if(fval > 1.0f) {fval = 1.0f;} else if (fval < -1.0f) {fval = -1.0f;}
          sitepar->Ncoeff = fval;
          if (flags->verbose) {
            printf("sitepar; Ncoeff, coefficient for nitrification: %f\n", sitepar->Ncoeff);
          }
        }

        else if(strstr(inptline,"jdayStart") != NULL) {
          sscanf(inptline, "%d", &sitepar->jdayStart);
          if (flags->verbose) {
            printf("sitepar; jdayStart: denitrification respiration restraint on: %d\n",
                   sitepar->jdayStart);
          }
        }

        else if(strstr(inptline,"jdayEnd") != NULL) {
          sscanf(inptline, "%d", &sitepar->jdayEnd);
          if (flags->verbose) {
            printf("sitepar; jdayEnd: denitrification respiration restraint off: %d\n",
                   sitepar->jdayEnd);
          }
        }


        /* Added 4 coefficients to parameterize the arctangent function that */
        /* used to control the amount of NO3 from mineralization is lost as */
        /* N2O, cak - 08/28/2014 */

        else if(strstr(inptline,"NO3_N2O_x") != NULL) {
          sitepar->NO3_N2O_x = fval;
          if (flags->verbose) {
            printf("sitepar; NO3_N2O_x, x-inflection, WFPS where 1/2 of max NO3 is lost as N2O: ");
            printf("sitepar->NO3_N2O_x: %f\n", sitepar->NO3_N2O_x);
          }
        }

        else if(strstr(inptline,"NO3_N2O_y") != NULL) {
          sitepar->NO3_N2O_y = fval;
          if (flags->verbose) {
            printf("sitepar; NO3_N2O_y, y-inflection, Percent NO3 lost as N2O at x-inflection: ");
            printf("sitepar->NO3_N2O_y: %f\n", sitepar->NO3_N2O_y);
          }
        }

        else if(strstr(inptline,"NO3_N2O_step") != NULL) {
          sitepar->NO3_N2O_step = fval;
          if (flags->verbose) {
            printf("sitepar; NO3_N2O_step, Step between min and max NO3 lost as N2O: ");
            printf("sitepar->NO3_N2O_step: %f\n", sitepar->NO3_N2O_step);
          }
        }

        else if(strstr(inptline,"NO3_N2O_slope") != NULL) {
          sitepar->NO3_N2O_slope = fval;
          if (flags->verbose) {
            printf("sitepar; NO3_N2O_slope, Slope of NO3/N2O WFPS funtion: ");
            printf("sitepar->NO3_N2O_slope: %f\n", sitepar->NO3_N2O_slope);
          }
        }

        /* coefficient to control the maximum daily nitrification amount - cak 09/21/2011 */
        else if(strstr(inptline,"MaxNitAmt") != NULL) {
          sitepar->MaxNitAmt = fval;
          if (flags->verbose) {
            printf("sitepar; MaxNitAmt, maximum daily nitrification (gN/m2): %f\n", sitepar->MaxNitAmt);
          }
        }

        /* flag to turn off the insulating effect of snow on soil surface temperature - cak 10/07/2011 */
        else if(strstr(inptline,"SnowFlag") != NULL) {
          sscanf(inptline, "%d", &sitepar->SnowFlag);
          if (flags->verbose) {
            printf("sitepar; SnowFlag, Snow insulating flag: %d\n", sitepar->SnowFlag);
          }
        }

        /* coefficient to control the fraction of new net mineralization goesing to NO3 - cak 01/09/2012 */
        else if(strstr(inptline,"netmn_to_no3") != NULL) {
          sitepar->netmn_to_no3 = fval;
          if (flags->verbose) {
            printf("sitepar; netmn_to_no3, Fraction of net mineralization to NO3: %f\n", sitepar->netmn_to_no3);
          }
        }

        /* inflection point for the water filled pore space effect on denitrification curve - cak 01/06/2013 */
        else if(strstr(inptline,"wfpsdnitadj") != NULL) {
          sitepar->wfpsdnitadj = fval;
          if (flags->verbose) {
            printf("sitepar; wfpsdnitadj, WFPS inflection point adjustment: %f\n", sitepar->wfpsdnitadj);
          }
        }

        /* Added N2/N2O ratio adjustment coefficient - cak 01/06/2013 */
        else if(strstr(inptline,"n2n2oadj") != NULL) {
          sitepar->n2n2oadj = fval;
          if (flags->verbose) {
            printf("sitepar; n2n2oadj, multiplier on N2/N2O ratio: %f\n", sitepar->n2n2oadj);
          }
        }

        else if(strstr(inptline,"th2ocoef1") != NULL) {
          sitepar->th2ocoef1 = fval;
          if (flags->verbose) {
            printf("sitepar; th2ocoef1, relative swc required for half of max. transpiration: %f\n", sitepar->th2ocoef1);
          }
        }

        else if(strstr(inptline,"th2ocoef2") != NULL) {
          sitepar->th2ocoef2 = fval;
          if (flags->verbose) {
            printf("sitepar; th2ocoef2, 4 times the slope at relative swc required for half max. transp: %f\n", sitepar->th2ocoef2);
          }
        }

        else if(strstr(inptline,"flood_N2toN2O") != NULL) {
          sitepar->flood_N2toN2O = fval;
          if (flags->verbose) {
            printf("sitepar; flood_N2toN2O, N2/N2O ratio for flooded state: %f\n", sitepar->flood_N2toN2O);
          }
        }
  
        else if(strstr(inptline,"CO2_to_CH4") != NULL) {
          sitepar->CO2_to_CH4 = fval;
          if (flags->verbose) {
            printf("sitepar; CO2_to_CH4, fraction of CO2 from soil respiration used to produce CH4: %f\n", sitepar->CO2_to_CH4);
          }
        }

        else if(strstr(inptline,"C6H12O6_to_CH4") != NULL) {
          sitepar->C6H12O6_to_CH4 = fval;
          if (flags->verbose) {
            printf("sitepar; C6H12O6_to_CH4, reaction anaerobic carbohydrate fermentation w/ methanogenesis (mole weight C6H12O6:CH4): %f\n", sitepar->C6H12O6_to_CH4);
          }
        }

        else if(strstr(inptline,"frac_to_exudates") != NULL) {
          sitepar->frac_to_exudates = fval;
          if (flags->verbose) {
            printf("sitepar; frac_to_exudates, fraction of root production that is root exudates: %f\n", sitepar->frac_to_exudates);
          }
        }

        else if(strstr(inptline,"Aeh") != NULL) {
          sitepar->Aeh = fval;
          if (flags->verbose) {
            printf("sitepar; Aeh, differential coefficient: %f\n", sitepar->Aeh);
          }
        }

        else if(strstr(inptline,"Deh") != NULL) {
          sitepar->Deh = fval;
          if (flags->verbose) {
            printf("sitepar; Deh, differential coefficient: %f\n", sitepar->Deh);
          }
        }

        else if(strstr(inptline,"Beh_flood") != NULL) {
          sitepar->Beh_flood = fval;
          if (flags->verbose) {
            printf("sitepar; Beh_flood, low-limit value for Eh during flooding course (mv): %f\n", sitepar->Beh_flood);
          }
        }

        else if(strstr(inptline,"Beh_drain") != NULL) {
          sitepar->Beh_drain = fval;
          if (flags->verbose) {
            printf("sitepar; Beh_drain, upper-limit value of Eh during drainage course (mv): %f\n", sitepar->Beh_drain);
          }
        }

        else if(strstr(inptline,"zero_root_frac") != NULL) {
          sitepar->zero_root_frac = fval;
          if (flags->verbose) {
            printf("sitepar; zero_root_frac, fraction CH4 emitted via bubbles when zero root biomass (0.0-1.0): %f\n", sitepar->zero_root_frac);
          }
        }

        else if(strstr(inptline,"elevation") != NULL) {
          sitepar->elevation = fval;
          if (flags->verbose) {
            printf("sitepar; elevation, site elevation (meters): %f\n", sitepar->elevation);
          }
        }

        else if(strstr(inptline,"siteslope") != NULL) {
          sitepar->slope = fval;
          if (flags->verbose) {
            printf("sitepar; slope, site slope (degrees): %f\n", sitepar->slope);
          }
        }

        else if(strstr(inptline,"aspect") != NULL) {
          sitepar->aspect = fval;
          if (flags->verbose) {
            printf("sitepar; aspect, site aspect (degrees): %f\n", sitepar->aspect);
          }
        }

        else if(strstr(inptline,"ehoriz") != NULL) {
          sitepar->ehoriz = fval;
          if (flags->verbose) {
            printf("sitepar; ehoriz, site east horizon (degrees): %f\n", sitepar->ehoriz);
          }
        }

        else if(strstr(inptline,"whoriz") != NULL) {
          sitepar->whoriz = fval;
          if (flags->verbose) {
            printf("sitepar; whoriz, site west horizon (degrees): %f\n", sitepar->whoriz);
          }
        }

        else if(strstr(inptline,"sradadj") != NULL) {
          imo = 0;
          sscanf(inptline, "%f", &sradadj[imo]);
          if (flags->verbose) {
            printf("sitepar; sradadj[%1d]: %f\n", imo+1, sradadj[imo]);
          }
          for(imo=1; imo<=11; imo++) 
          {
            if (fgets(inptline, INPTSTRLEN, fp_in) != NULL) 
            {
              linecnt++;
              if(strstr(inptline,"sradadj") != NULL) 
              {
                  sscanf(inptline, "%f", &sradadj[imo]);
                  if (flags->verbose) {
                    printf("sitepar; sradadj[%1d]: %f\n", imo+1, sradadj[imo]);
                  }
              }
              else
              {
                printf("ERROR: Missing solar radiation adjustment factor (srad) for month %1d\n", imo+1);
                printf("in sitepar.in file.  Ending simulation!\n");
                exit(1);
              }
            }
            else 
            {
              printf("ERROR:  Unexpected end of file in sitepar.in.\n");
              printf("Ending simulation!\n");
              exit(1);
            }
          }
        }


        else if(strstr(inptline,"tminslope") != NULL) {
          *tminslope = fval;
          if (flags->verbose) {
            printf("sitepar; tminslope, slope for adjusting minimum temperature for VPD dewpoint calc: %f\n", *tminslope);
          }
        }

        else if(strstr(inptline,"tminintercept") != NULL) {
          *tminintercept = fval;
          if (flags->verbose) {
            printf("sitepar; tminintercept, intercept for adjusting minimum temperature for VPD dewpoint calc: %f\n", *tminintercept);
          }
        }

        else if(strstr(inptline,"maxphoto") != NULL) {
          *maxphoto = fval;
          if (flags->verbose) {
            printf("sitepar; maxphoto, maximum carbon loss due to photodecomposition (ug C/KJ srad): %f\n", *maxphoto);
          }
        }

        else if(strstr(inptline,"bioabsorp") != NULL) {
          *bioabsorp = fval;
          if (flags->verbose) {
            printf("sitepar; bioabsorp, litter biomass for full absorption of solar radiation (g biom/m2): %f\n", *bioabsorp);
          }
        }

        else if(strstr(inptline,"dofDpH") != NULL) {
          *dofDpH = (int)fval;
          if (flags->verbose) {
            printf("sitepar; dofDpH, compute pH effect on total denitrification? (1=yes, 0=no): %d\n", *dofDpH);
          }
        }

        else if(strstr(inptline,"dofRpH") != NULL) {
          *dofRpH = (int)fval;
          if (flags->verbose) {
            printf("sitepar; dofRpH, compute pH effect on Ratio N2:N2O during denitrification? (1=yes, 0=no): %d\n", *dofRpH);
          }
        }

        else if(strstr(inptline,"dofwueup") != NULL) {
          *dofwueup = (int)fval;
          if (flags->verbose) {
            printf("sitepar; dofwueup, plant N,P,S uptake by layer based on (0=N,P,S distribution,1=transpiration,2=root density): %d\n", *dofwueup);
          }
        }

        /* unable to find a variable name */
        else {
          printf("ERROR: Unknown sitepar record: %s", inptline);
          exit(1);
        }
      }

      /*****************************************************************************************/

      fclose(fp_in);

      if (linecnt != 73) {
        printf("sitepar.in: Incorrect line count.  Expecting %1d lines, found %1d\n", 72, linecnt);
        /* exit(1); */
      }

      /* If any expected parameters were not specified, set them */
      /* to their default and notify the user. -mdh 5/10/2019 */

      if (sitepar->usexdrvrs <= SITEPARMISSINGVAL)
      {
          sitepar->usexdrvrs = 0;
          printf("Using default value for sitepar.in parameter usexdrvrs: %1d\n", sitepar->usexdrvrs);
      }
      if (sitepar->sublimscale <= SITEPARMISSINGVAL)
      {
          sitepar->sublimscale = 1.0;
          printf("Using default value for sitepar.in parameter sublimscale: %3.1f\n", sitepar->sublimscale);
      }
      if (sitepar->reflec <= SITEPARMISSINGVAL)
      {
          sitepar->reflec = 0.18;
          printf("Using default value for sitepar.in parameter reflec: %4.2f\n", sitepar->reflec);
      }
      if (sitepar->albedo <= SITEPARMISSINGVAL)
      {
          sitepar->albedo = 0.65;
          printf("Using default value for sitepar.in parameter albedo: %4.2f\n", sitepar->albedo);
      }
      if (sitepar->fswcinit <= SITEPARMISSINGVAL)
      {
          sitepar->fswcinit = 0.90;
          printf("Using default value for sitepar.in parameter fswcinit: %4.2f\n", sitepar->fswcinit);
      }
      if (sitepar->dmpflux <= SITEPARMISSINGVAL)
      {
          sitepar->dmpflux = 0.0000001;
          printf("Using default value for sitepar.in parameter dmpflux: %10.8f\n", sitepar->dmpflux);
      }
      if (sitepar->hours_rain <= SITEPARMISSINGVAL)
      {
          sitepar->hours_rain = 10.0;
          printf("Using default value for sitepar.in parameter hours_rain: %4.1f\n", sitepar->hours_rain);
      }
      if (sitepar->drainlag <= SITEPARMISSINGVAL)
      {
          sitepar->drainlag = 0;
          printf("Using default value for sitepar.in parameter drainlag: %1d\n", sitepar->drainlag);
      }
      if (sitepar->hpotdeep <= SITEPARMISSINGVAL)
      {
          sitepar->hpotdeep = -200.0;
          printf("Using default value for sitepar.in parameter hpotdeep: %6.1f\n", sitepar->hpotdeep);
      }
      if (sitepar->ksatdeep <= SITEPARMISSINGVAL)
      {
          sitepar->ksatdeep = 0.0003;
          printf("Using default value for sitepar.in parameter ksatdeep: %6.4f\n", sitepar->ksatdeep);
      }
      if (sitepar->cldcov[0] <= SITEPARMISSINGVAL)
      {
          for(imo=0; imo<=11; imo++) 
          {
              sitepar->cldcov[imo] = 0.0;
          }
          printf("Using default value for sitepar.in parameter cldcov[1-12]: %3.1f\n", sitepar->cldcov[0]);
      }
      if (sitepar->tbotmn <= SITEPARMISSINGVAL)
      {
          sitepar->tbotmn = 0.0;
          printf("Using default value for sitepar.in parameter tbotmn: %3.1f\n", sitepar->tbotmn);
      }
      if (sitepar->tbotmx <= SITEPARMISSINGVAL)
      {
          sitepar->tbotmx = 12.4;
          printf("Using default value for sitepar.in parameter tbotmx: %4.1f\n", sitepar->tbotmx);
      }
      if (sitepar->dmp <= SITEPARMISSINGVAL)
      {
          sitepar->dmp = 0.003;
          printf("Using default value for sitepar.in parameter dmpsoiltemp: %5.3f\n", sitepar->dmp);
      }
      if (sitepar->timlag <= SITEPARMISSINGVAL)
      {
          sitepar->timlag = 30.0;
          printf("Using default value for sitepar.in parameter timlag: %4.1f\n", sitepar->timlag);
      }
      if (sitepar->Ncoeff <= SITEPARMISSINGVAL)
      {
          sitepar->Ncoeff = 0.03;
          printf("Using default value for sitepar.in parameter Ncoeff: %4.2f\n", sitepar->Ncoeff);
      }
      if (sitepar->jdayStart <= SITEPARMISSINGVAL || sitepar->jdayEnd <= SITEPARMISSINGVAL)
      {
          sitepar->jdayStart = 0;
          sitepar->jdayEnd = 0;
          printf("Using default value for sitepar.in parameters jdayStart, jdayEnd: %1d, %1d\n", 0,0);
      }
      if (sitepar->NO3_N2O_x <= SITEPARMISSINGVAL)
      {
          sitepar->NO3_N2O_x = 0.5;
          printf("Using default value for sitepar.in parameter NO3_N2O_x: %3.1f\n", sitepar->NO3_N2O_x);
      }
      if (sitepar->NO3_N2O_y <= SITEPARMISSINGVAL)
      {
          sitepar->NO3_N2O_y = 2.0;
          printf("Using default value for sitepar.in parameter NO3_N2O_y: %3.1f\n", sitepar->NO3_N2O_y);
      }
      if (sitepar->NO3_N2O_step <= SITEPARMISSINGVAL)
      {
          sitepar->NO3_N2O_step = 2.0;
          printf("Using default value for sitepar.in parameter NO3_N2O_step: %3.1f\n", sitepar->NO3_N2O_step);
      }
      if (sitepar->NO3_N2O_slope <= SITEPARMISSINGVAL)
      {
          sitepar->NO3_N2O_slope = 6.0;
          printf("Using default value for sitepar.in parameter NO3_N2O_slope: %3.1f\n", sitepar->NO3_N2O_slope);
      }
      if (sitepar->MaxNitAmt <= SITEPARMISSINGVAL)
      {
          sitepar->MaxNitAmt = 0.4;
          printf("Using default value for sitepar.in parameter MaxNitAmt: %3.1f\n", sitepar->MaxNitAmt);
      }
      if (sitepar->SnowFlag <= SITEPARMISSINGVAL)
      {
          sitepar->SnowFlag = 1;
          printf("Using default value for sitepar.in parameter SnowFlag: %1d\n", sitepar->SnowFlag);
      }
      if (sitepar->netmn_to_no3 <= SITEPARMISSINGVAL)
      {
          sitepar->netmn_to_no3 = 0.2;
          printf("Using default value for sitepar.in parameter netmn_to_no3: %3.1f\n", sitepar->netmn_to_no3);
      }
      if (sitepar->wfpsdnitadj <= SITEPARMISSINGVAL)
      {
          sitepar->wfpsdnitadj = 1.0;
          printf("Using default value for sitepar.in parameter wfpsdnitadj: %3.1f\n", sitepar->wfpsdnitadj);
      }
      if (sitepar->n2n2oadj <= SITEPARMISSINGVAL)
      {
          sitepar->n2n2oadj = 1.0;
          printf("Using default value for sitepar.in parameter n2n2oadj: %3.1f\n", sitepar->n2n2oadj);
      }
      if (sitepar->th2ocoef1 <= SITEPARMISSINGVAL)
      {
          sitepar->th2ocoef1 = 0.378;
          printf("Using default value for sitepar.in parameter th2ocoef1: %5.3f\n", sitepar->th2ocoef1);
      }
      if (sitepar->th2ocoef2 <= SITEPARMISSINGVAL)
      {
          sitepar->th2ocoef2 = 9.0;
          printf("Using default value for sitepar.in parameter th2ocoef2: %3.1f\n", sitepar->th2ocoef1);
      }
      if (sitepar->flood_N2toN2O <= SITEPARMISSINGVAL)
      {
          sitepar->flood_N2toN2O = 2.0;
          printf("Using default value for sitepar.in parameter flood_N2toN2O: %3.1f\n", sitepar->flood_N2toN2O);
      }
      if (sitepar->CO2_to_CH4 <= SITEPARMISSINGVAL)
      {
          sitepar->CO2_to_CH4 = 0.5;
          printf("Using default value for sitepar.in parameter CO2_to_CH4: %3.1f\n", sitepar->CO2_to_CH4);
      }
      if (sitepar->C6H12O6_to_CH4 <= SITEPARMISSINGVAL)
      {
          sitepar->C6H12O6_to_CH4 = 0.27;
          printf("Using default value for sitepar.in parameter C6H12O6_to_CH4: %4.2f\n", sitepar->C6H12O6_to_CH4);
      }
      if (sitepar->frac_to_exudates <= SITEPARMISSINGVAL)
      {
          sitepar->frac_to_exudates = 0.45;
          printf("Using default value for sitepar.in parameter frac_to_exudates: %4.2f\n", sitepar->frac_to_exudates);
      }
      if (sitepar->Aeh <= SITEPARMISSINGVAL)
      {
          sitepar->Aeh = 0.23;
          printf("Using default value for sitepar.in parameter Aeh: %4.2f\n", sitepar->Aeh);
      }
      if (sitepar->Deh <= SITEPARMISSINGVAL)
      {
          sitepar->Deh = 0.16;
          printf("Using default value for sitepar.in parameter Deh: %4.2f\n", sitepar->Deh);
      }
      if (sitepar->Beh_flood <= SITEPARMISSINGVAL)
      {
          sitepar->Beh_flood = -250.0;
          printf("Using default value for sitepar.in parameter Beh_flood: %6.1f\n", sitepar->Beh_flood);
      }
      if (sitepar->Beh_drain <= SITEPARMISSINGVAL)
      {
          sitepar->Beh_drain = 300.0;
          printf("Using default value for sitepar.in parameter Beh_drain: %5.1f\n", sitepar->Beh_drain);
      }
      if (sitepar->zero_root_frac <= SITEPARMISSINGVAL)
      {
          sitepar->zero_root_frac = 7.0;
          printf("Using default value for sitepar.in parameter zero_root_frac: %3.1f\n", sitepar->zero_root_frac);
      }
      if (sitepar->elevation <= SITEPARMISSINGVAL)
      {
          sitepar->elevation = 100.0;
          printf("Using default value for sitepar.in parameter elevation: %5.1f\n", sitepar->elevation);
      }
      if (sitepar->slope <= SITEPARMISSINGVAL)
      {
          sitepar->slope = 0.0;
          printf("Using default value for sitepar.in parameter siteslope: %3.1f\n", sitepar->slope);
      }
      if (sitepar->aspect <= SITEPARMISSINGVAL)
      {
          sitepar->aspect = 0.0;
          printf("Using default value for sitepar.in parameter aspect: %3.1f\n", sitepar->aspect);
      }
      if (sitepar->ehoriz <= SITEPARMISSINGVAL)
      {
          sitepar->ehoriz = 0.0;
          printf("Using default value for sitepar.in parameter ehoriz: %3.1f\n", sitepar->ehoriz);
      }
      if (sitepar->whoriz <= SITEPARMISSINGVAL)
      {
          sitepar->whoriz = 0.0;
          printf("Using default value for sitepar.in parameter whoriz: %3.1f\n", sitepar->whoriz);
      }
      if (sradadj[0] <= SITEPARMISSINGVAL)
      {
          for(imo=0; imo<=11; imo++) 
          {
              sradadj[imo] = 1.0;
          }
          printf("Using default value for sitepar.in parameter sradadj[1-12]: %3.1f\n", sradadj[0]);
      }
      if (*tminslope <= SITEPARMISSINGVAL)
      {
          *tminslope = 1.0;
          printf("Using default value for sitepar.in parameter tminslope: %3.1f\n", *tminslope);
      }
      if (*tminintercept <= SITEPARMISSINGVAL)
      {
          *tminintercept = 0.0;
          printf("Using default value for sitepar.in parameter tminintercept: %3.1f\n", *tminintercept);
      }
      if (*maxphoto <= SITEPARMISSINGVAL)
      {
          *maxphoto = 1.5;
          printf("Using default value for sitepar.in parameter maxphoto: %3.1f\n", *maxphoto);
      }
      if (*bioabsorp <= SITEPARMISSINGVAL)
      {
          *bioabsorp = 200.0;
          printf("Using default value for sitepar.in parameter bioabsorp: %5.1f\n", *bioabsorp);
      }
      if (*dofDpH <= SITEPARMISSINGVAL)
      {
          *dofDpH = 0;
          printf("Using default value for sitepar.in parameter dofDpH: %1d\n", *dofDpH);
      }
      if (*dofRpH <= SITEPARMISSINGVAL)
      {
          *dofRpH = 0;
          printf("Using default value for sitepar.in parameter dofRpH: %1d\n", *dofRpH);
      }

      if (*dofwueup <= SITEPARMISSINGVAL)
      {
          *dofwueup = 0;
          printf("Using default value for sitepar.in parameter dofwueup: %1d\n", *dofwueup);
      }


      if (flags->debug) {
        printf("Exiting function initsite\n");
      }

      return;
    }
