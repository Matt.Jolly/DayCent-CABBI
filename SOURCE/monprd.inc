
c               Copyright 1996 Colorado State University
c                       All Rights Reserved


c ... Monthly production variables. 

c...............................................................................
c     MODIFICATIONS FOR GRASSTREE (-mdh, January 2019):
c ... mrspTempEffect(2,2) increased to mrspTempEffect(3,2)
c ... mrspWaterEffect(2) increased to mrspWaterEffect(3)
c ... mrspdyflux(2), grspdyflux(2) increased to mrspdyflux(3), grspdyflux(3)
c ... Added gtwstress - water stress term for grasstree
c ...   Note: gwstress - water stress term for grain filling plants
c ... Added mgtprd(5)
c ... Added gtmrspdyflux(5), gtgrspdyflux(5)  (GTLIVPARTS=5)
c...............................................................................

      common/monprd/nit_amt_year, nit_amt_month,
     &              mcprd(3), mfprd(6), mgtprd(5),
     &              mrspdyflux(3), cmrspdyflux(3), fmrspdyflux(6),
     &              grspdyflux(3), cgrspdyflux(3), fgrspdyflux(6),
     &              gtmrspdyflux(5), gtgrspdyflux(5),
     &              mrspTempEffect(3,2), mrspWaterEffect(3),
     &              N2O_year, NO_year, N2_year,
     &              ch4_oxid_year, stempmth, annppt,
     &              N2O_month, NO_month, N2_month, ch4_oxid_month,
     &              pptmonth, cwstress, gwstress, gtwstress,
     &              twstress

      double precision nit_amt_year, nit_amt_month

      real mcprd, mfprd, mgtprd,
     &     mrspdyflux, cmrspdyflux, fmrspdyflux,
     &     grspdyflux, cgrspdyflux, fgrspdyflux,
     &     gtmrspdyflux, gtgrspdyflux,
     &     mrspTempEffect, mrspWaterEffect,
     &     N2O_year, NO_year, N2_year, ch4_oxid_year, stempmth, annppt,
     &     N2O_month, NO_month, N2_month, ch4_oxid_month,
     &     pptmonth, cwstress, gtwstress, gwstress, twstress

      save /monprd/
