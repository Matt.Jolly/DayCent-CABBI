
c               Copyright 1993 Colorado State University
c                       All Rights Reserved

c ... File: harvgt.inc
c ... GrassTree harvest output variables
c ... These variables are set in gtlivsdrem, gtcutrtn, and gtkillrt
c ... -mdh 9/5/2019

      common/harvgt/
     &  gtlvcrem, gtdlvcrem, gtstmcrem, gtdstmcrem, gtlv2dlvc,
     &  gtstm2dstmc, gtlverem(3), gtdlverem(3), gtstmerem(3),
     &  gtdstmerem(3), gtlv2dlve(3), gtstm2dstme(3),
     &  carbostgloss, srfclittrj, srfclittrm, srfclittrcrt,
     &  soillittrcrt, soillittrj, soillittrm, gtstgloss(3),
     &  esrfclittrj(3), esrfclittrm(3), esrfclittrcrt(3),
     &  esoillittrj(3), esoillittrm(3), esoillittrcrt(3),
     &  gtlvcret, gtdlvcret, gtstmcret, gtdstmcret,
     &  gtlveret(3), gtdlveret(3), gtstmeret(3), gtdstmeret(3),
     &  fertapp(3), irrapp, omadapp, omaeapp(3)

      real gtlvcrem, gtdlvcrem, gtstmcrem, gtdstmcrem, gtlv2dlvc,
     &  gtstm2dstmc, gtlverem, gtdlverem, gtstmerem,
     &  gtdstmerem, gtlv2dlve, gtstm2dstme,
     &  carbostgloss, srfclittrj, srfclittrm, srfclittrcrt,
     &  soillittrcrt, soillittrj, soillittrm, gtstgloss,
     &  esrfclittrj, esrfclittrm, esrfclittrcrt,
     &  esoillittrj, esoillittrm, esoillittrcrt,
     &  gtlvcret, gtdlvcret, gtstmcret, gtdstmcret,
     &  gtlveret, gtdlveret, gtstmeret, gtdstmeret,
     &  fertapp, irrapp, omadapp, omaeapp

      save /harvgt/

