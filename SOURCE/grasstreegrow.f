
c               Copyright 1993 Colorado State University
c                       All Rights Reserved

c**********************************************************************
c
c  FILE:      grasstreegrowth.f
c
c  FUNCTION:  subroutine grasstreegrowth()
c
c  PURPOSE:   Compute grasstree production 
c            (GTLEAF, GTSTEM, GTFROOTJ, GTFROOTM, and GTCROOT)
c
c  AUTHOR:    New function derived from treegrow.
c             Melannie Hartman Jan. 23, 2014
c
c  Comments:
c     Note: GTFROOT=3, GTFROOTJ=3, GTCROOT=4, GTFROOTM=5
c     Be careful about the way grasstree_cfrac(GTFROOT) is used, here
c       and in other subroutines. grasstree_cfrac(GTFROOT) is the total
c       C fraction allocated to fine roots, and 
c       grasstree_cfrac(GTFROOTM) = 0.
c     eup(ipart) is used in nutrlm with grasstree_cfrac(GTFROOT).  
c       Here initialize it for GTFROOT and not for GTFROOTJ and 
c       GTFROOTM separately. 
c     eupgtprt(ipart) is an output variable only.  Here it is set for   
c       both GTFROOTJ and GTFROOTM. 
c     We will not implement late season growth restriction (Bill Parton).
c       All code that uses gtlsgres was removed (-mdh 3/6/2014). 
c
c  FUNCTION ARGUMENTS:
c    tfrac	- time fraction (1/# days in current month)  
c    tavedly	- average daily temperature (C)
c    gtGrossPsn	- grasstree photosynthesis, with water stress
c
c  CALLED FROM: grasstrees
c
c  CALLS: lacalc, restrp,  
c
c**********************************************************************

      subroutine grasstreegrowth (tfrac, tavedly, gtGrossPsn)

      implicit none
      include 'const.inc'
      include 'comput.inc'
      include 'dovars.inc'
      include 'dynam.inc'
      include 'fertil.inc'
      include 'isovar.inc'
      include 'monprd.inc'
      include 'npool.inc'
      include 'param.inc'
      include 'parfx.inc'
      include 'pargt.inc'
      include 'pheno.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot3.inc'
      include 'plot4.inc'
      include 'potent.inc'
      include 'zztim.inc'

c ... Argument declarations
      real             tfrac, tavedly
      double precision gtGrossPsn

c ... Fortran to C prototype
      INTERFACE

        SUBROUTINE cmpnfrac(clyr, ammonium, nitrate, minerl,
     &                      frac_nh4, frac_no3, no3pref)
          !MS$ATTRIBUTES ALIAS:'_cmpnfrac' :: cmpnfrac
          INTEGER          clyr
          DOUBLE PRECISION ammonium
          DOUBLE PRECISION nitrate(*)
          REAL             minerl(*)
          DOUBLE PRECISION frac_nh4
          DOUBLE PRECISION frac_no3
          REAL             no3pref
        END SUBROUTINE cmpnfrac

        SUBROUTINE flow(from, to, when, howmuch)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          REAL from
          REAL to
          REAL when
          REAL howmuch
        END SUBROUTINE flow

        SUBROUTINE update_npool(clyr, amt, frac_nh4, frac_no3, 
     &             ammonium, nitrate, subname)
          !MS$ATTRIBUTES ALIAS:'_update_npool' :: update_npool
          INTEGER          clyr
          REAL             amt
          DOUBLE PRECISION frac_nh4
          DOUBLE PRECISION frac_no3
          DOUBLE PRECISION ammonium
          DOUBLE PRECISION nitrate(*)
          CHARACTER        subname*10
        END SUBROUTINE update_npool

      END INTERFACE

c ... Simulate grasstree production associated with growth.
c ...   tfrac   - fraction of month over which current production event
c ...             occurs (0-1)
c ...   tavedly - mean air temperature over production period (deg C)

c ... Function declarations
      real      gtleafa, line, maxswpot, rtimp
      external  gtleafa, line, maxswpot, rtimp

c ... Local variables
      integer   i, iel, ipart, lyr
      real      accum(ISOS), availm(MAXIEL), amt
      real      calcup, cprodgtLeft
      real      eugt(GTLIVPARTS), fsol
      real      namt
      real      remCfrac, rimpct
      real      sum_cfrac, toler, totCup
      real      uptake(4,MAXIEL)
      real      gtNfix
      double precision frac_nh4, frac_no3, totmrspdyflux
      real      cprodgtdy, eprodgtdy(MAXIEL)
      real      lai, leavc_opt, mrspReduce, fraclblstg
      real      lgwoodc
      real      cerat(2,2,MAXIEL), cfrac(2)
      character subname*10

      if (tfrac .lt. 0.0 .or. tfrac .gt. 1.0) then
        write(*,*) 'Error in grasstreegrow, tfrac = ', tfrac
        STOP
      endif
      if (tavedly .le. -999.0) then
        write(*,*) 'Error in grasstreegrow, tavedly = ', tavedly
        STOP
      endif

      subname = 'grstreegrw'
      toler = 1.0E-30

      accum(UNLABL) = 0.0
      accum(LABELD) = 0.0

      if (isnan(carbostg(GTARYINDX,UNLABL))) then
        write(*,*) 'grasstreegrowth 0: carbostg(3,UNLABL) is NaN'
        STOP
      endif

c ... Use grasstree specific favail(1) value
      favail(1) = sfavail(3)

      do 15 iel = 1, nelem
        uptake(ESTOR,iel) = 0.0
        uptake(ESOIL,iel) = 0.0
        uptake(ENFIX,iel) = 0.0
        uptake(EFERT,iel) = 0.0
c ..... New daily output variables. -mdh 5/29/2019
        do 6 ipart = 1,GTLIVPARTS
          eupstg(ipart,iel) = 0.0
          eupsoil(ipart,iel) = 0.0
          eupnfix(ipart,iel) = 0.0
          eupaufert(ipart,iel) = 0.0
          cercrpnew(ipart,iel) = 0.0
6       continue
15    continue

c ... Compute fraction of labeled material in the grasstree 
c ... carbohydrate storage pool
c ... When carbostg(GTARYINDX,*) goes to zero, set the labeled fraction 
c ... for the new material entering this pool to the labeled fraction
c ... of the current grasstree, cisofr. -cak 5/8/2014
      if (carbostg(GTARYINDX,UNLABL) + carbostg(GTARYINDX,LABELD) .gt.
     &        0.0) then
        fraclblstg = carbostg(GTARYINDX,LABELD) /
     &              (carbostg(GTARYINDX,UNLABL) 
     &               + carbostg(GTARYINDX,LABELD))
      else
        fraclblstg = cisofr
      endif

c ... Previous flowup, in trees, should have updated mineral pools.
c ... Determine nutrients available to plants for growth.
      do 20 iel = 1, nelem
        availm(iel) = 0.0
c ..... Nutrients available to trees are in the top gtlaypg layers,
        do 30 lyr = 1, gtlaypg
          if (minerl(lyr,iel) .gt. toler) then
            availm(iel) = availm(iel) + minerl(lyr, iel)
          endif
30      continue
20    continue

c ... Temperature effect on maintenance respiration for aboveground
c ... components

      mrspTempEffect(GTARYINDX,SRFC) = 0.1 * exp(0.07 * tavedly)

c ... Bound maintenance respiration temperature effect between 0.0 and 1.0

      mrspTempEffect(GTARYINDX,SRFC) =
     &  min(1.0, mrspTempEffect(GTARYINDX,SRFC))
      mrspTempEffect(GTARYINDX,SRFC) =
     &  max(0.0, mrspTempEffect(GTARYINDX,SRFC))

c ... Temperature effect on maintenance respiration for belowground
c ... components

      mrspTempEffect(GTARYINDX,SOIL) = 0.1 * exp(0.07 * tavedly)

c ... Bound maintenance respiration temperature effect between 0.0 and 1.0

      mrspTempEffect(GTARYINDX,SOIL) =
     &  min(1.0, mrspTempEffect(GTARYINDX,SOIL))
      mrspTempEffect(GTARYINDX,SOIL) =
     &  max(0.0, mrspTempEffect(GTARYINDX,SOIL))

c ... Add a soil water term to the fine root maintenance respiration
c ... equation, cak - 06/27/2007
c ... Calculate the soil water potential of the wettest soil layer
c ... in the tree rooting zone

      mrspWaterEffect(GTARYINDX) = maxswpot(gtlaypg)
      if (mrspWaterEffect(GTARYINDX) .le. 76.0) then
        mrspWaterEffect(GTARYINDX) =
     &    (80.0 - mrspWaterEffect(GTARYINDX)) / 80.0
      else if (mrspWaterEffect(GTARYINDX) .gt. 76.0) then
        mrspWaterEffect(GTARYINDX) = 0.05
      endif
      mrspWaterEffect(GTARYINDX) = min(1.0, mrspWaterEffect(GTARYINDX))
      mrspWaterEffect(GTARYINDX) = max(0.0, mrspWaterEffect(GTARYINDX))

c ... Linearly decrease maintenance respiration as the amount of
c ... carbohydrate stored in the carbohydrate storage pool gets
c ... smaller based on the carbon level that is required for optimal
c ... leaf area, cak - 10/20/2006
c ... We will be using two line functions for this calculation
c ... depending on the optimal leaf carbon value, cak - 08/13/2009
c ... Calculate the amount of carbon in the leaves if the grasstree 
c ... is at its optimal LAI, cak - 10/20/2006

      lgwoodc = 0.0
      call lacalc(lai, gtstemc, lgwoodc, gtmaxlai, gtklai)
      if ((gtbiomc * gtbtolai) .gt. 0.0001) then
        leavc_opt = lai / (gtbiomc * gtbtolai)
      else
        leavc_opt = 0.0
      endif
c     write(*,*) 'grasstreegrow: lai, leavc_opt =', lai, leavc_opt

      if (leavc_opt > 0.000000001) then
        if (carbostg(GTARYINDX,UNLABL)+carbostg(GTARYINDX,LABELD) .lt.
     &      gtmrsplai(3) * leavc_opt) then

c ....... mrspReduce (and maintenance resp) increases with carbohydrate storage.
c         X = carbostg(*,*) (< gtmrsplai(3)*leavc_opt)
c         Y = mrspReduce
c         x1 = gtmrsplai(1) * leavc_opt (usually 0.0)
c         y1 = gtmrsplai(2) (usually 0.0)
c         x2 = gtmrsplai(3) * leavc_opt
c         y2 = gtmrsplai(4) (usually 1.0)

          mrspReduce = 
     &      line(carbostg(GTARYINDX,UNLABL)+carbostg(GTARYINDX,LABELD),
     &           gtmrsplai(1) * leavc_opt, gtmrsplai(2),
     &           gtmrsplai(3) * leavc_opt, gtmrsplai(4))

        elseif (carbostg(GTARYINDX,UNLABL)+carbostg(GTARYINDX,LABELD) 
     &          .gt. gtmrsplai(5) * leavc_opt) then
          mrspReduce = gtmrsplai(6)

        else
c ....... mrspReduce (and maintenance resp) increases with carbohydrate storage.
c         X = carbostg(*,*) (gtmrsplai(3)*leavc_opt < carbostg(*,*) < gtmrsplai(5)*leavc_opt)
c         Y = mrspReduce
c         x1 = gtmrsplai(3) * leavc_opt
c         y1 = gtmrsplai(4) (usually 1.0)
c         x2 = gtmrsplai(5) * leavc_opt
c         y2 = gtmrsplai(6) (usually 2.0)

          mrspReduce =
     &      line(carbostg(GTARYINDX,UNLABL)+carbostg(GTARYINDX,LABELD),
     &           gtmrsplai(3) * leavc_opt, gtmrsplai(4),
     &           gtmrsplai(5) * leavc_opt, gtmrsplai(6))
        endif
      else
        mrspReduce = 0.0
      endif

c ... Maintenance respiration flux is proportional to live carbon
c ... and increases with tavedly.  For roots, maintenance respiration
c ... is reduced by dry soils (CORRECT?).
 
      gtmrspdyflux(GTLEAF)   = gtkmrspmx(GTLEAF) * 
     &                         mrspTempEffect(GTARYINDX,SRFC) * 
     &                         gtleavc * tfrac * mrspReduce

      gtmrspdyflux(GTSTEM)   = gtkmrspmx(GTSTEM) *
     &                         mrspTempEffect(GTARYINDX,SRFC) * 
     &                         gtstemc * tfrac * mrspReduce

      gtmrspdyflux(GTFROOTJ) = gtkmrspmx(GTFROOTJ) *
     &                         mrspTempEffect(GTARYINDX,SOIL) * 
     &                         mrspWaterEffect(GTARYINDX) * 
     &                         gtfrootcj * tfrac * mrspReduce

      gtmrspdyflux(GTFROOTM) = gtkmrspmx(GTFROOTM) *
     &                         mrspTempEffect(GTARYINDX,SOIL) *
     &                         mrspWaterEffect(GTARYINDX) * 
     &                         gtfrootcm * tfrac * mrspReduce

      gtmrspdyflux(GTCROOT)  = gtkmrspmx(GTCROOT) *
     &                         mrspTempEffect(GTARYINDX,SOIL) * 
     &                         mrspWaterEffect(GTARYINDX) *
     &                         gtcrootc * tfrac * mrspReduce

c ... When photosynthesis occurs adjust the maintenance respiration
c ... based on photosynthate for the day and a weighted averaging of
c  .. the maintenance respiration values calculated above.
      if (gtGrossPsn .gt. 0.0) then
        totmrspdyflux = 0.0
        do 40 ipart = 1, GTLIVPARTS
          totmrspdyflux = totmrspdyflux + gtmrspdyflux(ipart)
40      continue
        do 50 ipart = 1, GTLIVPARTS
          if (totmrspdyflux .gt. 0.0) then
            gtmrspdyflux(ipart) =
     &        (gtmrspdyflux(ipart) / totmrspdyflux) *
     &        (mrspReduce * ps2mrsp(GTARYINDX) * gtGrossPsn)
          else
            gtmrspdyflux(ipart) = 0.0
          endif
50      continue
      endif

c ... Maintenance respiration is subtracted from the carbohydrate
c ... storage pool, cak - 08/12/2009
      mrspdyflux(GTARYINDX) = 0.0
      do 55 ipart = 1, GTLIVPARTS
        mrspdyflux(GTARYINDX) = mrspdyflux(GTARYINDX)
     &                           + gtmrspdyflux(ipart)
55    continue
      if(isnan(mrspdyflux(GTARYINDX))) then
        write(*,*) 'grasstreegrow 1 mrspdyflux(3) is NaN'
        STOP
      endif
      call csched(mrspdyflux(GTARYINDX),fraclblstg,1.0,
     &            carbostg(GTARYINDX,UNLABL),csrsnk(UNLABL),
     &            carbostg(GTARYINDX,LABELD),csrsnk(LABELD),
     &            1.0,gtautoresp)

c     write(*,*)
c     write(*,*) 'Subroutine grasstreegrow:'
c     write(*,*) 'time = ', time
c     write(*,*) 'grasstreegrw = ', grasstreegrw
c     write(*,*) 'pgrasstreec =', pgrasstreec
c     write(*,*) 'senecnt =', senecnt

c ... If growth can occur
      if (grasstreegrw .eq. 1 .and. pgrasstreec .gt. 0.0 .and.
     &    .not. (senecnt .gt. 0)) then

c ..... Calculate actual production values
c ..... Calculate impact of root biomass on available nutrients
        rimpct = rtimp(riint, rictrl, gtfrootcj+gtfrootcm)

c       write(*,*) 'grasstreegrow1: grasstree_cfrac(GTLEAF) =', 
c    &              grasstree_cfrac(GTLEAF)
c       write(*,*) 'grasstreegrow1: grasstree_cfrac(GTSTEM) =', 
c    &              grasstree_cfrac(GTSTEM)
c       write(*,*) 'grasstreegrow1: grasstree_cfrac(GTFROOTJ) =', 
c    &              grasstree_cfrac(GTFROOTJ)
c       write(*,*) 'grasstreegrow1: grasstree_cfrac(GTFROOTM) =', 
c    &              grasstree_cfrac(GTFROOTM)
c       write(*,*) 'grasstreegrow1: grasstree_cfrac(GTCROOT) =', 
c    &              grasstree_cfrac(GTCROOT)
 
c ..... Determine actual production values, restricting the C/E ratios
c ..... When calling restrp we are only looking at allocation to 
c ..... fine roots and leaves, cak - 07/02/02 
c ..... Grasstree parts 1 & 2 are leaves and stems, not leaves and fine 
c ..... roots, so cergrasstree and grasstree_cfrac can not be passed to 
c ..... restrp here. Copy leaf and fine root values into local arrays 
c ..... (cerat and cfrac) to pass to restrp. Neither cerat nor cfrac
c ..... are updated by the call to restrp. -mdh 3/21/2014
        do 57 iel = 1, MAXIEL
          cerat(IMIN,1,iel) = cergrasstree(IMIN,GTLEAF,iel) 
          cerat(IMAX,1,iel) = cergrasstree(IMAX,GTLEAF,iel) 
          cerat(IMIN,2,iel) = cergrasstree(IMIN,GTFROOTJ,iel) 
          cerat(IMAX,2,iel) = cergrasstree(IMAX,GTFROOTJ,iel) 
57      continue
        cfrac(1) = grasstree_cfrac(GTLEAF)
        cfrac(2) = grasstree_cfrac(GTFROOTJ)

c       write(*,*) 'Calling restrp...'
c       write(*,*) 'cfrac(1) =', cfrac(1)
c       write(*,*) 'cfrac(2) =', cfrac(2)
c       write(*,*) 'availm(1) =', availm(1)
c       write(*,*) 'rimpct =', rimpct
c       write(*,*) 'gtstg(1) =', gtstg(1)
c       write(*,*) 'snfxmx(GTARYINDX) =', snfxmx(GTARYINDX)

        call restrp(elimit, nelem, availm, cerat, 2, 
     &      cfrac, pgrasstreec, rimpct, gtstg, 
     &      snfxmx(GTARYINDX), cprodgtdy, eprodgtdy, uptake, 
     &      grasstree_a2drat, gtNfix, relyld)
      else
        cprodgtdy = 0.0
        do 58 iel = 1, MAXIEL
          eprodgtdy(iel) = 0.0
58      continue
      endif

c     write(*,*) 'cprodgtdy =', cprodgtdy

c ... If growth occurs...
      if (cprodgtdy .gt. 0.) then

c ..... If the carbohydrate storage pool falls below a critical value
c ..... add a minimal amount of carbon from the csrsnk to allow plant
c ..... growth.  This should only occur when the tree is small.

        if (carbostg(GTARYINDX,UNLABL)+carbostg(GTARYINDX,LABELD) 
     &    .lt. 15.0) then
          if (gtfrtcindx .eq. 1) then
          write(*,*) 'Warning, carbostg pool < minimal in grasstreegrow'
          write(*,*) 'time carbostg = ', time,
     &        carbostg(GTARYINDX,UNLABL)+carbostg(GTARYINDX,LABELD)
          endif
          carbostg(GTARYINDX,UNLABL) = carbostg(GTARYINDX,UNLABL) +
     &                              (15.0 * (1.0 - fraclblstg))
          carbostg(GTARYINDX,LABELD) = carbostg(GTARYINDX,LABELD) +
     &                              (15.0 * fraclblstg)
          csrsnk(UNLABL) = csrsnk(UNLABL) - (15.0 * (1.0 - fraclblstg))
          csrsnk(LABELD) = csrsnk(LABELD) - (15.0 * fraclblstg)
          if (gtfrtcindx .eq. 1) then
            write(*,*) 'grasstreegrow: reset carbostg =', carbostg(3,1)
          endif
        endif

c ..... Increment the counter that is tracking the number of days to
c ..... allow unrestricted stem and coarse root growth
        gtgrwdys = gtgrwdys + 1

c ..... Compute carbon allocation fractions for each tree part
c ..... Calculate how much of the carbon the roots use
        cprodgtLeft = cprodgtdy * (1.0 - grasstree_cfrac(GTFROOT))

c ..... Calculate how much of the carbon the leaves use, allocate leaves
c ..... up to a optimal LAI
        grasstree_cfrac(GTLEAF) = gtleafa(gtleavc, gtstemc, 
     &                                    cprodgtLeft, cprodgtdy)
        remCfrac = 1.0-grasstree_cfrac(GTFROOT)-grasstree_cfrac(GTLEAF)
c       write(*,*) 'grasstreegrow: gtleafa =', grasstree_cfrac(GTLEAF)
c       write(*,*) 'grasstreegrow: froota =', grasstree_cfrac(GTFROOT)
c       write(*,*) 'grasstreegrow: remCfrac =', remCfrac

c ..... If we have leftover carbon allocate it to the stems and coarse 
c ..... roots using a weighted average
        if (remCfrac .lt. 1.0E-05) then
          grasstree_cfrac(GTSTEM) = 0.0
          grasstree_cfrac(GTCROOT) = 0.0
        else
          totCup = 0.0
          grasstree_cfrac(GTSTEM) = gtcfrac(GTSTEM)
          grasstree_cfrac(GTCROOT) = gtcfrac(GTCROOT)
          totCup = grasstree_cfrac(GTSTEM) + grasstree_cfrac(GTCROOT)
          if (totCup .gt. 0.0) then
            grasstree_cfrac(GTSTEM) = grasstree_cfrac(GTSTEM) 
     &                                / totCup * remCfrac
            grasstree_cfrac(GTCROOT) = grasstree_cfrac(GTCROOT) 
     &                               / totCup * remCfrac
          else
            write(*,*) 'Error in grasstreegrow'
            write(*,*) 'gtcfrac(GTSTEM)+gtcfrac(GTCROOT) <= 0'
            STOP
          endif
        endif

c ... Error checking
        sum_cfrac = 0.0
        do 90 ipart = 1, GTLIVPARTS
          if (grasstree_cfrac(ipart) .lt. 0.0) then
            write(*,*) 'Error in grasstreegrow, cfrac(ipart) < 0'
            STOP
          else if (grasstree_cfrac(ipart) .gt. 1.0) then
            write(*,*) 'Error in grasstreegrow, cfrac(ipart) > 1'
            STOP
          else
            if (ipart .ne. GTFROOTM) then
              sum_cfrac = sum_cfrac + grasstree_cfrac(ipart)
            endif
          endif
90      continue
        if (abs(1.0 - sum_cfrac) .gt. 0.001) then
          write(*,*) "Error in tree carbon allocation fractions!"
          write(*,*) "sum_cfrac = ", sum_cfrac
c          STOP
        endif

c ..... Recalculate actual production values with updated C-allocation
c ..... fractions, restricting the C/E ratios -mdh 5/11/01
c ..... Calculate impact of root biomass on available nutrients
        rimpct = rtimp(riint, rictrl, gtfrootcj+gtfrootcm)

c       write(*,*) 'grasstreegrow2: grasstree_cfrac(GTLEAF) =', 
c    &              grasstree_cfrac(GTLEAF)
c       write(*,*) 'grasstreegrow2: grasstree_cfrac(GTSTEM) =', 
c    &              grasstree_cfrac(GTSTEM)
c       write(*,*) 'grasstreegrow2: grasstree_cfrac(GTFROOTJ) =', 
c    &              grasstree_cfrac(GTFROOTJ)
c       write(*,*) 'grasstreegrow2: grasstree_cfrac(GTFROOTM) =', 
c    &              grasstree_cfrac(GTFROOTM)
c       write(*,*) 'grasstreegrow2: grasstree_cfrac(GTCROOT) =', 
c    &              grasstree_cfrac(GTCROOT)
c       write(*,*) 'grasstreegrow: cergrasstree(IMIN,GTLEAF,N)=', 
c    &              cergrasstree(IMIN,GTLEAF,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMAx,GTLEAF,N)=', 
c    &              cergrasstree(IMAX,GTLEAF,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMIN,GTSTEM,N)=', 
c    &              cergrasstree(IMIN,GTSTEM,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMAX,GTSTEM,N)=', 
c    &              cergrasstree(IMAX,GTSTEM,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMIN,GTFROOTJ,N)=', 
c    &              cergrasstree(IMIN,GTFROOTJ,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMAX,GTFROOTJ,N)=', 
c    &              cergrasstree(IMAX,GTFROOTJ,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMIN,GTFROOTM,N)=', 
c    &              cergrasstree(IMIN,GTFROOTM,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMAX,GTFROOTM,N)=', 
c    &              cergrasstree(IMAX,GTFROOTM,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMIN,GTCROOT,N)=', 
c    &              cergrasstree(IMIN,GTCROOT,N)
c       write(*,*) 'grasstreegrow: cergrasstree(IMAX,GTCROOT,N)=', 
c    &              cergrasstree(IMAX,GTCROOT,N)
 
c ..... Determine actual production values, restricting the C/E ratios
c ..... To be consistent with treegrow, do not include mature fine roots
c ..... -mdh 2/22/2019
        call restrp(elimit, nelem, availm, cergrasstree, GTLIVPARTS-1,
     &              grasstree_cfrac, pgrasstreec, rimpct, gtstg, 
     &              snfxmx(GTARYINDX), cprodgtdy, eprodgtdy, uptake, 
     &              grasstree_a2drat, gtNfix, relyld)

c ..... Calculations for symbiotic N fixation accumulators moved from
c ..... nutrlm subroutine, cak - 10/17/02
c ..... Compute N fixation which actually occurs and add to the
c ..... N fixation accumulator.
        nfix = nfix + gtNfix
        snfxac(GTARYINDX) = snfxac(GTARYINDX) + gtNfix
c ..... Add computation for nfixac -mdh 1/16/02
        nfixac = nfixac + gtNfix

c ..... C/N ratio for production
        if (eprodgtdy(N) .eq. 0.0) then
          write(*,*) 'Error in grasstreegrow, eprodgtdy(N) = 0.0)'
          STOP
        endif
        tcnpro = cprodgtdy/eprodgtdy(N)

c ..... Calculate production for each grasstree part
        do 95 ipart = 1, GTLIVPARTS
          if (ipart .eq. GTLEAF .or. ipart .eq. GTSTEM .or.
     &        ipart .eq. GTCROOT) then
            mgtprd(ipart) = grasstree_cfrac(ipart) * cprodgtdy
          else if (ipart .eq. GTFROOTJ) then
            mgtprd(ipart) = grasstree_cfrac(GTFROOTJ) * cprodgtdy
     &                      * (1.0 - gtmrtfrac)
          else if (ipart .eq. GTFROOTM) then
            mgtprd(ipart) = grasstree_cfrac(GTFROOTJ) * cprodgtdy 
     &                      * gtmrtfrac
          else
            write(*,*) 'Error in grasstreegrow, ipart out of bounds = ',
     &                  ipart
            STOP
          endif
95      continue

c ..... GrassTree Growth
c ..... All growth comes from the carbohydrate pool, cak - 08/12/2009

        if (isnan(carbostg(GTARYINDX,UNLABL))) then
          write(*,*) 'grasstreegrowth 1: carbostg(3,UNLABL) is NaN'
          STOP
        endif

c ..... Growth of leaves split into labeled & unlabeled parts
      if(isnan(mgtprd(GTLEAF))) then
        write(*,*) 'grasstreegrow mgtprd(GTLEAF) is NaN'
        STOP
      endif
        call csched(mgtprd(GTLEAF),fraclblstg,1.0,
     &              carbostg(GTARYINDX,UNLABL),gtlvcis(UNLABL),
     &              carbostg(GTARYINDX,LABELD),gtlvcis(LABELD),
     &              1.0,agtlvcis)

c ..... Growth juvenile fine roots; split into labeled & unlabeled 
c ..... parts
      if(isnan(mgtprd(GTFROOTJ))) then
        write(*,*) 'grasstreegrow mgtprd(GTFROOTJ) is NaN'
        STOP
      endif
        call csched(mgtprd(GTFROOTJ),fraclblstg,1.0,
     &              carbostg(GTARYINDX,UNLABL),gtfrtcisj(UNLABL),
     &              carbostg(GTARYINDX,LABELD),gtfrtcisj(LABELD),
     &              1.0,agtfrcisj)

c ..... Growth mature fine roots; split into labeled & unlabeled parts
      if(isnan(mgtprd(GTFROOTM))) then
        write(*,*) 'grasstreegrow mgtprd(GTFROOTM) is NaN'
        STOP
      endif
        call csched(mgtprd(GTFROOTM),fraclblstg,1.0,
     &              carbostg(GTARYINDX,UNLABL),gtfrtcism(UNLABL),
     &              carbostg(GTARYINDX,LABELD),gtfrtcism(LABELD),
     &              1.0,agtfrcism)

c ..... Growth of stems; split into labeled & unlabeled parts
        if (isnan(carbostg(GTARYINDX,UNLABL))) then
          write(*,*) 'grasstreegrowth 2: carbostg(3,UNLABL) is NaN'
          STOP
        endif
        if (isnan(gtstmcis(UNLABL))) then
          write(*,*) 'grasstreegrowth gtstmcis(UNLABL) is NaN'
          STOP
        endif

c       write(*,*) 'Before growth: gtstmcis(UNLABL) =', gtstmcis(UNLABL)
c       write(*,*) 'Before growth: gtstmcis(LABELD) =', gtstmcis(LABELD)
c       write(*,*) 'Before growth: fraclblstg =', fraclblstg
c       write(*,*) 'Before growth: mgtprd(GTSTEM) =', mgtprd(GTSTEM)
c       write(*,*) 'Before growth: carbostg(3,1)=',carbostg(3,1)
c       write(*,*) 'Before growth: carbostg(3,2)=',carbostg(3,2)

        call csched(mgtprd(GTSTEM),fraclblstg,1.0,
     &              carbostg(GTARYINDX,UNLABL),gtstmcis(UNLABL),
     &              carbostg(GTARYINDX,LABELD),gtstmcis(LABELD),
     &              1.0,agtstmcis)

c ..... Growth of coarse roots; split into labeled & unlabeled parts
        if(isnan(mgtprd(GTCROOT))) then
          write(*,*) 'grasstreegrow mgtprd(GTCROOT) is NaN'
          STOP
        endif
        call csched(mgtprd(GTCROOT),fraclblstg,1.0,
     &              carbostg(GTARYINDX,UNLABL),gtcrtcis(UNLABL),
     &              carbostg(GTARYINDX,LABELD),gtcrtcis(LABELD),
     &              1.0,agtcrtcis)

c       write(*,*) 'grasstreegrow: mgtprd(GTLEAF) =', mgtprd(GTLEAF)
c       write(*,*) 'grasstreegrow: mgtprd(GTSTEM) =', mgtprd(GTSTEM)
c       write(*,*) 'grasstreegrow: mgtprd(GTFROOTJ) =', mgtprd(GTFROOTJ)
c       write(*,*) 'grasstreegrow: mgtprd(GTFROOTM) =', mgtprd(GTFROOTM)
c       write(*,*) 'grasstreegrow: mgtprd(GTCROOT) =', mgtprd(GTCROOT)

c ..... GROWTH RESPIRATION
c ..... Growth respiration is a fixed fraction of production
        gtgrspdyflux(GTLEAF) = mgtprd(GTLEAF) * gtgresp(GTLEAF)
        gtgrspdyflux(GTFROOTJ) = mgtprd(GTFROOTJ) * gtgresp(GTFROOTJ)
        gtgrspdyflux(GTFROOTM) = mgtprd(GTFROOTM) * gtgresp(GTFROOTM)
        gtgrspdyflux(GTSTEM)  = mgtprd(GTSTEM) * gtgresp(GTSTEM)
        gtgrspdyflux(GTCROOT) = mgtprd(GTCROOT) * gtgresp(GTCROOT)

c       endif

c ..... Growth respiration is subtracted from the carbohydrate
c ..... storage pool.
        grspdyflux(GTARYINDX) = 0.0
        do 105 ipart = 1, GTLIVPARTS
          grspdyflux(GTARYINDX) = grspdyflux(GTARYINDX) 
     &                            + gtgrspdyflux(ipart)
105     continue
        if(isnan(grspdyflux(GTARYINDX))) then
          write(*,*) 'grasstreegrow grspdyflux(3) is NaN'
          STOP
        endif
        call csched(grspdyflux(GTARYINDX),fraclblstg,1.0,
     &              carbostg(GTARYINDX,UNLABL),csrsnk(UNLABL),
     &              carbostg(GTARYINDX,LABELD),csrsnk(LABELD),
     &              1.0,gtautoresp)

c ..... Actual Uptake
        do 110 iel = 1, nelem
          if (eprodgtdy(iel) .le. 0.0) then
            print *, 'Divide by zero in grasstreegrow: eprodgtdy(i)=', 
     &                eprodgtdy(iel)
            STOP
          endif
          eugt(GTLEAF)   = eup(GTLEAF,iel) / eprodgtdy(iel)
          eugt(GTFROOTJ) = (eup(GTFROOT,iel) * (1.0 - gtmrtfrac))
     &                     / eprodgtdy(iel)
          eugt(GTFROOTM) = (eup(GTFROOT,iel)*gtmrtfrac) / eprodgtdy(iel)
          eugt(GTSTEM)   = eup(GTSTEM,iel) / eprodgtdy(iel)
          eugt(GTCROOT)  = eup(GTCROOT,iel) / eprodgtdy(iel)

c         write(*,*) 'grasstreegrow: eugt(GTLEAF) =', eugt(GTLEAF)
c         write(*,*) 'grasstreegrow: eugt(GTSTEM) =', eugt(GTSTEM)
c         write(*,*) 'grasstreegrow: eugt(GTFROOTJ) =', eugt(GTFROOTJ)
c         write(*,*) 'grasstreegrow: eugt(GTFROOTM) =', eugt(GTFROOTM)
c         write(*,*) 'grasstreegrow: eugt(GTCROOT) =', eugt(GTCROOT)

c ....... Reset eprodcdy(iel) to track the actual uptake which can be
c ....... restricted late in the growing season
          eprodgtdy(iel) = 0.0

c ....... Take up nutrients from internal storage pool
c ....... Don't allow uptake from storage if gtstg is negative

          if (gtstg(iel) .gt. 0.0) then
            amt = uptake(ESTOR,iel) * eugt(GTLEAF)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 1 is NaN'
              STOP
            endif
            call flow(gtstg(iel),gtleave(iel),time,amt)
            eupgtprt(GTLEAF,iel) = eupgtprt(GTLEAF,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupstg(GTLEAF,iel) = eupstg(GTLEAF,iel) + amt

            amt = uptake(ESTOR,iel) * eugt(GTFROOTJ)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 2 is NaN'
              STOP
            endif
            call flow(gtstg(iel),gtfrootej(iel),time,amt)
            eupgtprt(GTFROOTJ,iel) = eupgtprt(GTFROOTJ,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupstg(GTFROOTJ,iel) = eupstg(GTFROOTJ,iel) + amt

            amt = uptake(ESTOR,iel) * eugt(GTFROOTM)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 3 is NaN'
              STOP
            endif
            call flow(gtstg(iel),gtfrootem(iel),time,amt)
            eupgtprt(GTFROOTM,iel) = eupgtprt(GTFROOTM,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupstg(GTFROOTM,iel) = eupstg(GTFROOTM,iel) + amt

            amt = uptake(ESTOR,iel) * eugt(GTSTEM)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 4 is NaN'
              STOP
            endif
            call flow(gtstg(iel),gtsteme(iel),time,amt)
            eupgtprt(GTSTEM,iel) = eupgtprt(GTSTEM,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupstg(GTSTEM,iel) = eupstg(GTSTEM,iel) + amt

            amt = uptake(ESTOR,iel) * eugt(GTCROOT)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 5 is NaN'
              STOP
            endif
            if (amt .lt. 0.0) then
              write(*,*) 'grasstreegrow neg amt: gtstg->gtcroote', amt
            endif
            call flow(gtstg(iel),gtcroote(iel),time,amt)
            eupgtprt(GTCROOT,iel) = eupgtprt(GTCROOT,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupstg(GTCROOT,iel) = eupstg(GTCROOT,iel) + amt

          endif

c ....... Take up nutrients from soil
c ....... Nutrients for uptake are available in the top gtlaypg layers.

          do 100 lyr = 1, gtlaypg
            if (minerl(lyr,iel) .gt. toler) then
              fsol = 1.0
c ........... The fsol calculation for P is not needed here to compute
c ........... the weighted average, cak - 04/05/02
c              if (iel .eq. P) then
c                fsol = fsfunc(minerl(SRFC,P), pslsrb, sorpmx)
c              endif

              call cmpnfrac(lyr,ammonium,nitrate,minerl,
     &                      frac_nh4,frac_no3,no3pref(GTARYINDX))
              calcup = uptake(ESOIL,iel)*minerl(lyr,iel)*
     &                 fsol/availm(iel)

c ........... Leaves
c ........... minerl -> gtleave
              amt = calcup * eugt(GTLEAF)
              if (iel .eq. N) then
                namt = -1.0*amt
                call update_npool(lyr, namt, frac_nh4, frac_no3, 
     &                            ammonium, nitrate, subname)
              endif
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 6 is NaN'
                STOP
              endif
              call flow(minerl(lyr,iel),gtleave(iel),time,amt)
              eupgtprt(GTLEAF,iel) = eupgtprt(GTLEAF,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupsoil(GTLEAF,iel) = eupsoil(GTLEAF,iel) + amt

c ........... Juvenile fine roots
c ........... minerl -> gtfrootej
              amt = calcup * eugt(GTFROOTJ)
              if (iel .eq. N) then
                namt = -1.0*amt
                call update_npool(lyr, namt, frac_nh4, frac_no3, 
     &                            ammonium, nitrate, subname)
              endif
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 7 is NaN'
                STOP
              endif
              call flow(minerl(lyr,iel),gtfrootej(iel),time,amt)
              eupgtprt(GTFROOTJ,iel) = eupgtprt(GTFROOTJ,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupsoil(GTFROOTJ,iel) = eupsoil(GTFROOTJ,iel) + amt

c ........... Mature fine roots
c ........... minerl -> gtfrootem
              amt = calcup * eugt(GTFROOTM)
              if (iel .eq. N) then
                namt = -1.0*amt
                call update_npool(lyr, namt, frac_nh4, frac_no3, 
     &                            ammonium, nitrate, subname)
              endif
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 8 is NaN'
                STOP
              endif
              call flow(minerl(lyr,iel),gtfrootem(iel),time,amt)
              eupgtprt(GTFROOTM,iel) = eupgtprt(GTFROOTM,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupsoil(GTFROOTM,iel) = eupsoil(GTFROOTM,iel) + amt

c ........... Stems
c ........... minerl -> gtsteme
              amt = calcup * eugt(GTSTEM)
              if (iel .eq. N) then
                namt = -1.0*amt
                call update_npool(lyr, namt, frac_nh4, frac_no3, 
     &                            ammonium, nitrate, subname)
              endif
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 9 is NaN'
                STOP
              endif
              call flow(minerl(lyr,iel),gtsteme(iel),time,amt)
              eupgtprt(GTSTEM,iel) = eupgtprt(GTSTEM,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupsoil(GTSTEM,iel) = eupsoil(GTSTEM,iel) + amt

c ........... Coarse roots
c ........... minerl -> gtcroote
              amt = calcup * eugt(GTCROOT)
c             write(*,*) 'Lyr ', lyr, ': calcup * eugt(GTCROOT)=', amt
              if (iel .eq. N) then
                namt = -1.0*amt
                call update_npool(lyr, namt, frac_nh4, frac_no3, 
     &                            ammonium, nitrate, subname)
              endif
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 10 is NaN'
                STOP
              endif
              if (amt .lt. 0.0) then
                write(*,*) 'grasstreegrow neg amt: minerl->gtcroote',amt
              endif
              call flow(minerl(lyr,iel),gtcroote(iel),time,amt)
              eupgtprt(GTCROOT,iel) = eupgtprt(GTCROOT,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupsoil(GTCROOT,iel) = eupsoil(GTCROOT,iel) + amt

            endif
100       continue

c ....... Take up nutrients from nitrogen fixation

          if (iel .eq. N .and. gtNfix .gt. 0) then

c ......... Leaves
c ......... fixation -> gtleave
            amt = uptake(ENFIX,iel) * eugt(GTLEAF)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 11 is NaN'
              STOP
            endif
            call flow(esrsnk(iel),gtleave(iel),time,amt)
            eupgtprt(GTLEAF,iel) = eupgtprt(GTLEAF,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupnfix(GTLEAF,iel) = eupnfix(GTLEAF,iel) + amt

c ......... Juvenile fine roots
c ......... fixation -> gtfrootej
            amt = uptake(ENFIX,iel) * eugt(GTFROOTJ)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 12 is NaN'
              STOP
            endif
            call flow(esrsnk(iel),gtfrootej(iel),time,amt)
            eupgtprt(GTFROOTJ,iel) = eupgtprt(GTFROOTJ,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupnfix(GTFROOTJ,iel) = eupnfix(GTFROOTJ,iel) + amt

c ......... Mature fine roots
c ......... fixation -> gtfrootem
            amt = uptake(ENFIX,iel) * eugt(GTFROOTM)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 13 is NaN'
              STOP
            endif
            call flow(esrsnk(iel),gtfrootem(iel),time,amt)
            eupgtprt(GTFROOTM,iel) = eupgtprt(GTFROOTM,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupnfix(GTFROOTM,iel) = eupnfix(GTFROOTM,iel) + amt

c ......... Stems
c ......... fixation -> gtsteme
            amt = uptake(ENFIX,iel) * eugt(GTSTEM)
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 14 is NaN'
              STOP
            endif
            call flow(esrsnk(iel),gtsteme(iel),time,amt)
            eupgtprt(GTSTEM,iel) = eupgtprt(GTSTEM,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupnfix(GTSTEM,iel) = eupnfix(GTSTEM,iel) + amt

c ......... Coarse roots
c ......... fixation -> gtcroote
            amt = uptake(ENFIX,iel) * eugt(GTCROOT)
c           write(*,*) 'uptake(ENFIX,iel) * eugt(GTCROOT)=', amt
            if(isnan(amt)) then
              write(*,*) 'grasstreegrow amt 15 is NaN'
              STOP
            endif
            if (amt .lt. 0.0) then
              write(*,*) 'grasstreegrow neg amt: Nfix->gtcroote', amt
            endif
            call flow(esrsnk(iel),gtcroote(iel),time,amt)
            eupgtprt(GTCROOT,iel) = eupgtprt(GTCROOT,iel) + amt
            eupacc(iel) = eupacc(iel) + amt
            eprodgtdy(iel) = eprodgtdy(iel) + amt
            eupnfix(GTCROOT,iel) = eupnfix(GTCROOT,iel) + amt

          endif

c ....... Take up nutrients from automatic fertilizer.

          if (aufert .ne. 0) then
            if (uptake(EFERT,iel) .gt. 0.) then
c ........... Automatic fertilizer added to plant pools

c ........... Leaves
c ........... Autofert -> gtleave
              amt = uptake(EFERT,iel) * eugt(GTLEAF)
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 16 is NaN'
                STOP
              endif
              call flow(esrsnk(iel),gtleave(iel),time,amt)
              eupgtprt(GTLEAF,iel) = eupgtprt(GTLEAF,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupaufert(GTLEAF,iel) = eupaufert(GTLEAF,iel) + amt

c ........... Juvenile fine roots
c ........... Autofert -> gtfrootej
              amt = uptake(EFERT,iel) * eugt(GTFROOTJ)
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 17 is NaN'
                STOP
              endif
              call flow(esrsnk(iel),gtfrootej(iel),time,amt)
              eupgtprt(GTFROOTJ,iel) = eupgtprt(GTFROOTJ,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupaufert(GTFROOTJ,iel) = eupaufert(GTFROOTJ,iel) + amt

c ........... Mature fine roots
c ........... Autofert -> gtfrootem
              amt = uptake(EFERT,iel) * eugt(GTFROOTM)
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 18 is NaN'
                STOP
              endif
              call flow(esrsnk(iel),gtfrootem(iel),time,amt)
              eupgtprt(GTFROOTM,iel) = eupgtprt(GTFROOTM,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupaufert(GTFROOTM,iel) = eupaufert(GTFROOTM,iel) + amt

c ........... Stems
c ........... Autofert -> gtsteme
              amt = uptake(EFERT,iel) * eugt(GTSTEM)
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 19 is NaN'
                STOP
              endif
              call flow(esrsnk(iel),gtsteme(iel),time,amt)
              eupgtprt(GTSTEM,iel) = eupgtprt(GTSTEM,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupaufert(GTSTEM,iel) = eupaufert(GTSTEM,iel) + amt

c ........... Coarse roots
c ........... Autofert -> gtcroote
              amt = uptake(EFERT,iel) * eugt(GTCROOT)
c             write(*,*) 'uptake(EFERT,iel) * eugt(GTCROOT)=', amt
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 20 is NaN'
                STOP
              endif
              if (amt .lt. 0.0) then
                write(*,*) 'grasstreegrow neg amt: afert->gtcroote', amt
              endif
              call flow(esrsnk(iel),gtcroote(iel),time,amt)
              eupgtprt(GTCROOT,iel) = eupgtprt(GTCROOT,iel) + amt
              eupacc(iel) = eupacc(iel) + amt
              eprodgtdy(iel) = eprodgtdy(iel) + amt
              eupaufert(GTCROOT,iel) = eupaufert(GTCROOT,iel) + amt

c ........... Automatic fertilizer added to mineral pool
              if (favail(iel) .eq. 0.0) then
                write(*,*) 'Error in grasstreegrow, favail(iel) = 0'
                STOP
              endif

              amt = uptake(EFERT,iel) * (1.0/favail(iel) - 1.0)
              fertot(iel) = fertot(iel) + uptake(EFERT,iel) + amt
              fertac(iel) = fertac(iel) + uptake(EFERT,iel) + amt
              if (iel .eq. N) then
                lyr = SRFC
                call update_npool(lyr, amt, frac_nh4_fert, 
     &                            frac_no3_fert, ammonium, nitrate,
     &                            subname)
              endif
              if(isnan(amt)) then
                write(*,*) 'grasstreegrow amt 21 is NaN'
                STOP
              endif
              call flow(esrsnk(iel),minerl(SRFC,iel),time,amt)
            endif
          endif
110     continue

c ... Else there is no production this time step due to nutrient
c ... limitation
      else  
        cprodgtdy = 0.0
        do 140 iel = 1, MAXIEL
          eprodgtdy(iel) = 0.0
          do 130 ipart = 1, GTLIVPARTS
            eup(ipart,iel) = 0.0
130       continue
140     continue
      endif

c ... Accumulate monthly output
      mrspflux(GTARYINDX) = mrspflux(GTARYINDX)+mrspdyflux(GTARYINDX)
      grspflux(GTARYINDX) = grspflux(GTARYINDX)+grspdyflux(GTARYINDX)
      do 150 ipart = 1, GTLIVPARTS
        sumrsp = sumrsp + gtmrspdyflux(ipart)
        gtmrspflux(ipart) = gtmrspflux(ipart) + gtmrspdyflux(ipart)
        mrspmth(GTARYINDX) = mrspmth(GTARYINDX) + gtmrspdyflux(ipart)
        gtgrspflux(ipart) = gtgrspflux(ipart) + gtgrspdyflux(ipart)
        grspmth(GTARYINDX) = grspmth(GTARYINDX) + gtgrspdyflux(ipart)
c ..... Accumulate annual output
        mrspann(GTARYINDX) = mrspann(GTARYINDX) + gtmrspdyflux(ipart)
        grspann(GTARYINDX) = grspann(GTARYINDX) + gtgrspdyflux(ipart)
150   continue

c ... Accumulate monthly and annual output for soil respiration
      srspmth(GTARYINDX) = srspmth(GTARYINDX) 
     &    + gtmrspdyflux(GTFROOTJ) + gtmrspdyflux(GTFROOTM) 
     &    + gtmrspdyflux(GTCROOT) + gtgrspdyflux(GTFROOTJ) 
     &    + gtgrspdyflux(GTFROOTM) + gtgrspdyflux(GTCROOT)
      srspann(GTARYINDX) = srspann(GTARYINDX) 
     &    + gtmrspdyflux(GTFROOTJ) + gtmrspdyflux(GTFROOTM) 
     &    + gtmrspdyflux(GTCROOT) + gtgrspdyflux(GTFROOTJ) 
     &    + gtgrspdyflux(GTFROOTM) + gtgrspdyflux(GTCROOT)

c ... Sum the daily production variables for output to the monthly *.bin file
      cprodgt = cprodgt + cprodgtdy
      do 160 i = 1, nelem
        eprodgt(i) = eprodgt(i) + eprodgtdy(i)
c ..... Compute C:E of new growth, daily output only. -mdh 5/29/2019
        cercrpnew(GTLEAF,i) = mgtprd(GTLEAF)/(eupsoil(GTLEAF,i)
     &    + eupstg(GTLEAF,i) + eupnfix(GTLEAF,i) 
     &    + eupaufert(GTLEAF,i) + 1.0e-10)
        cercrpnew(GTSTEM,i) = mgtprd(GTSTEM)/(eupsoil(GTSTEM,i)
     &    + eupstg(GTSTEM,i) + eupnfix(GTSTEM,i)
     &    + eupaufert(GTSTEM,i) + 1.0e-10)
        cercrpnew(GTFROOTJ,i) = mgtprd(GTFROOTJ)/(eupsoil(GTFROOTJ,i)
     &    + eupstg(GTFROOTJ,i) + eupnfix(GTFROOTJ,i)
     &    + eupaufert(GTFROOTJ,i) + 1.0e-10)
        cercrpnew(GTFROOTM,i) = mgtprd(GTFROOTM)/(eupsoil(GTFROOTM,i)
     &    + eupstg(GTFROOTM,i) + eupnfix(GTFROOTM,i)
     &    + eupaufert(GTFROOTM,i) + 1.0e-10)
        cercrpnew(GTCROOT,i) = mgtprd(GTCROOT)/(eupsoil(GTCROOT,i)
     &    + eupstg(GTCROOT,i) + eupnfix(GTCROOT,i)
     &    + eupaufert(GTCROOT,i) + 1.0e-10)
160   continue

      return
      end
