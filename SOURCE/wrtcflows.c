/*****************************************************************************
**
**  FILE:      wrtcflows.c
**
**  FUNCTION:  void wrtcflows()
**
**  PURPOSE:   This function writes daily values for tracking C & N flows
**             from litter inputs and decomposition.
**
**  INPUTS:
**    curday         - the day of the year (1..366)
**    inputmetabc1   - C inputs to surface metabolic pool (gC/m^2/day)
**    inputmetabc2   - C inputs to soil metabolic pool (gC/m^2/day)
**    inputstrucc1   - C inputs to surface structural pool (gC/m^2/day)
**    inputstrucc2   - C inputs to soil structural pool (gC/m^2/day)
**    inputmetabn1   - N inputs to surface metabolic pool (gN/m^2/day)
**    inputmetabn2   - N inputs to soil metabolic pool (gN/m^2/day)
**    inputstrucn1   - N inputs to surface structural pool (gN/m^2/day)
**    inputstrucn2   - N inputs to soil structural pool (gN/m^2/day)
**    metc1tosom11   - carbon flow from surface metabolic pool to fast
**                     surface organic matter pool (gC/m^2/day)
**    metc2tosom12   - carbon flow from soil metabolic pool to fast soil
**                     organic matter pool (gC/m^2/day)
**    som11tosom21   - carbon flow from fast surface organic matter pool
**                     to intermediate surface organic matter pool (gC/m^2/day)
**    som12tosom22   - carbon flow from fast soil organic matter pool
**                     to intermediate soil organic matter pool (gC/m^2/day)
**    som12tosom3    - carbon flow from fast soil organic matter pool to
**                     slow soil organic matter pool (gC/m^2/day)
**    som21tosom11   - carbon flow from intermediate surface organic
**                     matter pool to fast surface organic matter pool (gC/m^2/day)
**    som21tosom22   - carbon flow from intermediate surface organic
**                     matter pool to intermediate soil organic matter
**                     pool (gC/m^2/day)
**    som22tosom12   - carbon flow from intermediate soil organic matter
**                     pool to fast soil organic matter pool (gC/m^2/day)
**    som22tosom3    - carbon flow from intermediate soil organic matter
**                     pool to slow soil organic matter pool (gC/m^2/day)
**    som3tosom12    - carbon flow from slow soil organic matter pool to
**                     fast soil organic matter pool (gC/m^2/day)
**    struc1tosom11  - carbon flow from surface structural pool to fast
**                    surface organic matter pool (gC/m^2/day)
**    struc1tosom21  - carbon flow from surface structural pool to
**                     intermediate surface organic matter pool (gC/m^2/day)
**    struc2tosom12  - carbon flow from soil structural pool to fast
**                     soil organic matter pool (gC/m^2/day)
**    struc2tosom22  - carbon flow from soil structural pool to
**                     intermediate soil organic matter pool (gC/m^2/day)
**    time           - current simulation time (years)
**    wood1tosom11   - carbon flow from dead fine branch pool to fast
**                     surface organic matter pool (gC/m^2/day)
**    wood1tosom21   - carbon flow from dead fine branch pool to
**                     intermediate surface organic matter pool (gC/m^2/day)
**    wood2tosom11   - carbon flow from dead large wood pool to fast
**                     surface organic matter pool(gC/m^2/day) 
**    wood2tosom21   - carbon flow from dead large wood pool pool to
**                     intermediate surface organic matter pool (gC/m^2/day)
**    wood3tosom12   - carbon flow from dead coarse root pool to fast
**                     soil organic matter pool (gC/m^2/day)
**    wood3tosom22   - carbon flow from dead coarse root pool to
**                     intermediate soil organic matter pool (gC/m^2/day)
**    dwood1tosom11  - carbon flow from dead attached fine branch pool to fast
**                     surface organic matter pool (gC/m^2/day)
**    dwood1tosom21  - carbon flow from dead attached fine branch pool to
**                     intermediate surface organic matter pool (gC/m^2/day)
**    dwood2tosom11  - carbon flow from standing dead large wood pool to fast
**                     surface organic matter pool (gC/m^2/day)
**    dwood2tosom21  - carbon flow from standing dead large wood pool pool to
**                     intermediate surface organic matter pool (gC/m^2/day)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files->fp_cflows    - file pointer to cflows.out output file
**    files->write_cflows - flag to indicate if cflows.out output file should
**                          be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**    None
**
**  CALLED BY:
**    dailymoist()
**
*****************************************************************************/

#include <math.h>
#include <stdio.h>
#include "soilwater.h"

    void wrtcflows(float *time, int *curday, float *som11tosom21,
                   float *som12tosom22, float *som12tosom3,
                   float *som21tosom11, float *som21tosom22,
                   float *som22tosom12, float *som22tosom3,
                   float *som3tosom12, float *metc1tosom11,
                   float *metc2tosom12, float *struc1tosom11,
                   float *struc1tosom21, float *struc2tosom12,
                   float *struc2tosom22, float *wood1tosom11,
                   float *wood1tosom21, float *wood2tosom11,
                   float *wood2tosom21, float *wood3tosom12,
                   float *wood3tosom22, float *dwood1tosom11,
                   float *dwood1tosom21, float *dwood2tosom11,
                   float *dwood2tosom21,
                   float *inputmetabc1, float *inputmetabc2,
                   float *inputstrucc1, float *inputstrucc2,
                   float *inputmetabn1, float *inputmetabn2,
                   float *inputstrucn1, float *inputstrucn2) 
    {

      extern FILES_SPT files;

      if (!files->write_cflows) {
        return;
      }

      fprintf(files->fp_cflows, "%8.2f,%4d,", *time, *curday);
      fprintf(files->fp_cflows, "%12.6f,", *som11tosom21);
      fprintf(files->fp_cflows, "%12.6f,", *som12tosom22);
      fprintf(files->fp_cflows, "%12.6f,", *som12tosom3);
      fprintf(files->fp_cflows, "%12.6f,", *som21tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *som21tosom22);
      fprintf(files->fp_cflows, "%12.6f,", *som22tosom12);
      fprintf(files->fp_cflows, "%12.6f,", *som22tosom3);
      fprintf(files->fp_cflows, "%12.6f,", *som3tosom12);
      fprintf(files->fp_cflows, "%12.6f,", *metc1tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *metc2tosom12);
      fprintf(files->fp_cflows, "%12.6f,", *struc1tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *struc1tosom21);
      fprintf(files->fp_cflows, "%12.6f,", *struc2tosom12);
      fprintf(files->fp_cflows, "%12.6f,", *struc2tosom22);
      fprintf(files->fp_cflows, "%12.6f,", *wood1tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *wood1tosom21);
      fprintf(files->fp_cflows, "%12.6f,", *wood2tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *wood2tosom21);
      fprintf(files->fp_cflows, "%12.6f,", *wood3tosom12);
      fprintf(files->fp_cflows, "%12.6f,", *wood3tosom22);
      fprintf(files->fp_cflows, "%12.6f,", *dwood1tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *dwood1tosom21);
      fprintf(files->fp_cflows, "%12.6f,", *dwood2tosom11);
      fprintf(files->fp_cflows, "%12.6f,", *dwood2tosom21);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabc1);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabc2);
      fprintf(files->fp_cflows, "%12.8f,", *inputstrucc1);
      fprintf(files->fp_cflows, "%12.8f,", *inputstrucc2);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabn1);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabn2);
      fprintf(files->fp_cflows, "%12.8f,", *inputstrucn1);
      fprintf(files->fp_cflows, "%12.8f",  *inputstrucn2);
      fprintf(files->fp_cflows, "\n");

      return;
    }
