/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      wrtdcsip.c
**
**  FUNCTION:  void wrtdcsip()
**
**  PURPOSE:   Write out the values for the SIPNET model. 
**
**  AUTHOR:    Cindy Keough  04/09
** 
**  INPUTS:
**    accum     - the amount of snow added to the snowpack (cm H2O)
**    CO2resp   - heterotrophic CO2 respiration (g/m^2)
**    crootc    - coarse root live carbon for forest (g/m^2)
**    dayofyr   - current simulation day of the year (1..366)
**    evapdly   - water evaporated from soil (cm H2O)
**    intrcpt   - amount of precipitation intercepted by the standing crop and
**                litter (cm H2O)
**    mcprd1    - daily NPP for shoots for grass/crop system (gC/m^2)
**    mcprd2    - daily NPP for juvenile roots for grass/crop system (gC/m^2)
**    mcprd3    - daily NPP for mature roots for grass/crop system (gC/m^2)
**    melt      - the amount of snow melted from the snowpack, if 
**                daily air temperature is warm enough (cm H2O)
**    mfprd1    - daily NPP for live leaves for tree system (gC/m^2)
**    mfprd2    - daily NPP for live juvenile fine roots for tree system (gC/m^2)
**    mfprd6    - daily NPP for live mature fine roots for tree system (gC/m^2)
**    mfprd3    - daily NPP for live fine branches for tree system (gC/m^2)
**    mfprd4    - daily NPP for live large wood for tree system (gC/m^2)
**    mfprd5    - daily NPP for live coarse roots for tree system (gC/m^2)
**    mgtprd1   - daily NPP for live leaves for grasstree system (gC/m^2)
**    mgtprd2   - daily NPP for live stems for grasstree system (gC/m^2)
**    mgtprd3   - daily NPP for live juvenile fine roots for grasstree system (gC/m^2)
**    mgtprd4   - daily NPP for live coarse roots for grasstree system (gC/m^2)
**    mgtprd5   - daily NPP for live mature fine roots for grasstree system (gC/m^2)
**    nee       - net ecosystem exchange (npptot - CO2resp)
**    npptot    - summation of all production values (gC/m^2)
**    outflow   - water that runs off, or drains out of the profile (cm H2O)
**    petdly    - potential evapotranspiration rate for day (cm H2O)
**    ppt       - precipitation for the day (cm)
**    rleavc    - leaf live carbon for forest (g/m^2)
**    rlwodc    - large wood live carbon for forest (g/m^2)
**    runoffdly - amount of water (rain or snowmelt) which did not infiltrate
**                soil profile (cm H2O)
**    snlq      - the liquid water in the snowpack (cm H2O)
**    snow      - current snowpack (equiv. cm H2O)
**    stemp     - soil surface temperature (Celsius) 
**    sublim    - amount of water sublimated from the snowpack (cm H2O)
**    time      - simulation time (years)
**    tlai      - LAI of the tree leaves
**    trandly   - water transpired from soil (cm H2O)
**    wc_2cm    - water holding capacity of a 2 cm soil layer (cm H2O)
**    wc_3cm    - water holding capacity of a 3 cm soil layer (cm H2O)
**    wc_5cm    - water holding capacity of a 5 cm soil layer (cm H2O)
**    wc_10cm   - water holding capacity of a 10 cm soil layer (cm H2O)
**    wc_15cm   - water holding capacity of a 15 cm soil layer (cm H2O)
**    wc_30cm   - water holding capacity of a 30 cm soil layer (cm H2O)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files              - structure containing information about output files
**    files->fp_dcsip    - file pointer to dc_sip.out output file
**    files->write_dcsip - flag to indicate if dc_sip.out output file should
**                         be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**     None
**
**  CALLED BY:
**     dailymoist()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include "soilwater.h"

    void wrtdcsip(float *time, int *dayofyr, float *trandly, float *evapdly,
                  float *intrcpt, float *sublim, float *outflow,
                  float *runoffdly, float *ppt, float *accum, float *melt,
                  float *snow, float *snlq, float *petdly, float *stemp,
                  float *wc_2cm, float *wc_3cm, float *wc_5cm, float *wc_10cm,
                  float *wc_15cm, float *wc_30cm, float *CO2resp,
                  float *mcprd1, float *mcprd2, float *mcprd3, float *mfprd1,
                  float *mfprd2, float *mfprd6, float *mfprd3, float *mfprd4, float *mfprd5, 
                  float *mgtprd1, float *mgtprd2, float *mgtprd3, float *mgtprd4, float *mgtprd5, 
                  float *npptot, float *nee, float *tlai)
    {
      extern FILES_SPT files;

      if (!files->write_dcsip) {
        goto ex;
      }

      fprintf(files->fp_dcsip, "%.2f,%d,", *time, *dayofyr);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,",
              *trandly, *evapdly, *intrcpt, *sublim, *outflow, *runoffdly);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,",
              *ppt, *accum, *melt, *snow, *snlq, *petdly, *stemp);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f,%.4f,%.4f,",
              *wc_2cm,  *wc_3cm, *wc_5cm, *wc_10cm, *wc_15cm);

      fprintf(files->fp_dcsip, "%.4f,%.4f,", *wc_30cm, *CO2resp);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f,%.4f,%.4f,", 
              *mcprd1, *mcprd2, *mcprd3, *mfprd1, *mfprd2);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f,%.4f,", 
              *mfprd6, *mfprd3, *mfprd4, *mfprd5);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f,%.4f,%.4f,", 
              *mgtprd1, *mgtprd2, *mgtprd3, *mgtprd4, *mgtprd5);

      fprintf(files->fp_dcsip, "%.4f,%.4f,%.4f\n", 
              *npptot, *nee, *tlai);
     
ex:   return;
    }
