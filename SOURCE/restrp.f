
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


c ... RESTRP.F
c ... Restrict the actual production based on C/E ratios.  Calculate
c ... minimum, and maximum whole plant nutrient concentrations.

      subroutine restrp(elimit, nelem, availm, cerat, nparts, cfrac,
     &                  potenc, rimpct, storage, snfxmx, cprodl,
     &                  eprodl, uptake, a2drat, plantNfix, relyld)

      implicit none
      include 'const.inc'
      include 'fertil.inc'
      include 'parfx.inc'
      include 'potent.inc'

c ... Argument declarations
      integer   nelem, nparts
      real      a2drat(MAXIEL), availm(MAXIEL), cerat(2,nparts,MAXIEL),
     &          cfrac(nparts), cprodl, elimit, eprodl(MAXIEL),
     &          plantNfix, potenc, relyld, rimpct, snfxmx,
     &          storage(MAXIEL), uptake(4,MAXIEL)

c ... Definitions of subroutine arguments
c ... Input
c ...   nelem - number of elements being simulated (1=N; 2=N,P; 3=N,P,S).
c ...   availm(iel) - total soil mineral E in root zone, calculated 
c ...     in subroutines growth, treegrow, and grasstreegrow.
c ...   nparts - number of plant parts (3-1=crop, 5-1=grasstree, 6-1=tree).
c ...   cerat(IMIN/IMAX,ipart,iel) - min and max C/E ratio for new growth 
c ...     by plant part.
c ...   cfrac(nparts) - fraction of total plant C in each plant part.
c ...   potentc - potential total plant production (gC/m2/day)
c ...   rimpct - impact of root biomass on plant E uptake potential (0.0-1.0)
c ...   storage - internal E storage pool for the plant (gE/m2)
c ...   snfxmx - maximum rate of N fixtation (gN/gC production) as
c ...     specified in the crop.100, tree.100, or grasstree.100 file.
c ... Output
c ...   elimit - element most limiting to production (1=N, 2=P, 3=S).
c ...   cprodl - potential total plant production, downregulated by
c ...     nutrient limitation (gC/m2/day)
c ...   eprodl(iel) - total plant E uptake accounting for nutrient 
c ...     limitation (gE/m2/day)
c ...   uptake(source, iel) - uptake of E by source=ESTOR,ESOIL,EFERT,ENFIX (gE/m2/day)
c ...   a2drat(iel) - fraction of available E to demand for E, 
c ...     bounded 0.0-1.0.
c ...   plantNfix - symbiotic N fixation which actually occurs (gN/m2/day)

c ... Local variables
c ... NOTE:  Local variables cannot have adjustable array size.  MINECI
c ...        and MAXECI are set to the largest array size which may occur.
      integer   iel, ipart
      real      afert(MAXIEL), ctob, eavail(MAXIEL), maxec(MAXIEL),
     &          maxeci(FPARTS-1,MAXIEL), minec(MAXIEL),
     &          mineci(FPARTS-1,MAXIEL), ustorg(MAXIEL)

c ... Definitions of Local variables
c ...   afert(iel) - amount of automatic fertilization that occurs (gE/m2/day)
c ...   eavail(iel) - E available to plants from soil E and storage E (gE/m2).
c ...   ctob - weighted average carbon to biomass conversion factor
c ...   maxec(iel) - Average maximum E/C ratio for the whole plant, 
c ...     calculated in the calling subroutine (restrp).
c ...   minec(iel) - Average minimum E/C ratio for the whole plant, 
c ...     calculated in the calling subroutine (restrp).
c ...   maxeci(ipart,iel) - Maximum E/C ratio for the plant part,
c ...     calculated in the calling subroutine (restrp).
c ...   mineci(ipart,iel) - Minimum E/C ratio for the plant part,
c ...     calculated in the calling subroutine (restrp).
c ...   ustorg(iel) - E uptake from internal E plant storage (gE/m2/day)
c ...   aufert - automatic fertilzation option, fraction of nutrient
c ...     stress alleviated by automatic fertilization (0.0 - 2.0?),
c ...     from fertil.inc.

      if ((nelem .le. 0) .or. (nelem .gt. 3)) then
        write(*,*) 'Error in restrp, nelem out of bounds = ', nelem
        write(*,*) 'Check <site>.100 file'
        STOP
      endif
      if (nparts .gt. FPARTS-1) then
        write(*,*) 'Error in restrp, nparts out of bounds = ', nparts
        STOP
      endif

c ... Reset variables to zero
      cprodl = 0.0
      do 30 iel = 1, nelem
        eprodl(iel) = 0.0
        afert(iel) = 0.0
        do 20 ipart = 1, nparts
          eup(ipart,iel) = 0.0
20      continue
30    continue

c ... There is no production if one of the mineral elements is not
c ... available.
      do 40 iel = 1, nelem
        if ((availm(iel) .le. 1E-4) .and. (snfxmx .le. 0.0) .and.
     &      (aufert .le. 0.0)) then
          goto 999
        endif
40    continue

c ... Initialize cprodl
      cprodl = potenc

c ... Calculate soil available nutrients, based on a maximum fraction
c ... (favail) and the impact of root biomass (rimpct), adding storage.
      do 45 iel = 1, nelem
        eavail(iel) = (availm(iel) * favail(iel) * rimpct) + 
     &                 storage(iel)
45    continue

c ... Compute weighted average carbon to biomass conversion factor
      ctob = 0.0
      do 70 ipart = 1, nparts
        if (ipart .lt. 3) then
          ctob = ctob + (cfrac(ipart) * 2.5)
        else
          ctob = ctob + (cfrac(ipart) * 2.0)
        endif
70    continue

c ... Calculate average E/C of whole plant (crop, tree, or grasstree)
      do 50 iel = 1, nelem
        minec(iel) = 0.0
        maxec(iel) = 0.0
        do 60 ipart = 1, nparts
          if (cerat(IMAX,ipart,iel) .le. 0.0) then
            write(*,51) 'Error in restrp, cerat(IMAX,',
     &        ipart,',',iel,')=', cerat(IMAX,ipart,iel)
            write(*,*) 'nparts = ', nparts
            STOP
          endif
          if (cerat(IMIN,ipart,iel) .le. 0.0) then
            write(*,51) 'Error in restrp, cerat(IMIN,',
     &        ipart,',',iel,')=', cerat(IMIN,ipart,iel)
            write(*,*) 'nparts = ', nparts
            STOP
51        format(a28,i1,a1,i1,a2,f10.6)
          endif
          mineci(ipart,iel) = 1.0 / cerat(IMAX,ipart,iel)
          maxeci(ipart,iel) = 1.0 / cerat(IMIN,ipart,iel)
          minec(iel) = minec(iel) + cfrac(ipart) * mineci(ipart,iel)
60      continue
c ..... Calculate average nutrient content based on roots and shoots only,
c ..... cak - 07/25/02
        maxec(iel) = maxec(iel) +
     &               ((cfrac(FROOT) * maxeci(FROOT,iel)) / 2.5)
        maxec(iel) = maxec(iel) +
     &               (((1.0 - cfrac(FROOT)) * maxeci(LEAF,iel)) / 2.5)
c ..... Calculate average E/C
        maxec(iel) = maxec(iel) * ctob
50    continue

c ... Compute the limitation
      call nutrlm(elimit, nelem, nparts, cfrac, eavail, maxec, minec,
     &            maxeci, mineci, snfxmx, cprodl, eprodl, a2drat,
     &            plantNfix, afert, aufert, ctob)

c ... Calculate relative yield - reimplemented by AKM 18/8/2000
      relyld = cprodl/potenc

c ... Calculate uptakes from all sources: storage, soil, plantNfix, and
c ... automatic fertilizer
      do 200 iel = 1, nelem
        ustorg(iel) = min(storage(iel), eprodl(iel))
c ..... If storage pool contains all needed for uptake
        if (eprodl(iel) .le. ustorg(iel)) then
          uptake(ESTOR,iel) = eprodl(iel)
          uptake(ESOIL,iel) = 0.0
c ..... Otherwise, extra necessary from the soil pool
        elseif (eprodl(iel) .gt. ustorg(iel)) then
          uptake(ESTOR,iel) = storage(iel)
c          uptake(ESOIL,iel) = eprodl(iel) - storage(iel)
c ....... subtract plantNfix -mdh 3/8/99
c ....... soil N uptake should not include monthly symbiotic N
c ....... fixation amount
          if (iel .eq. N) then
c            uptake(ESOIL,iel)=min(eprodl(iel)-storage(iel)-plantNfix,
c     &                            eavail(iel)-storage(iel)-plantNfix)
c          else
c            uptake(ESOIL,iel)=min(eprodl(iel)-storage(iel),
c     &                            eavail(iel)-storage(iel))
            uptake(ESOIL,iel)=eprodl(iel)-storage(iel)-plantNfix
          else
            uptake(ESOIL,iel)=eprodl(iel)-storage(iel)
          endif
        endif
c ..... If (eprodl .gt. (uptake(ESTOR,iel) + uptake(ESOIL,iel) + plantNfix)
c ..... then pull from afert; afert should be > 0 only in this case, so
c ..... don't actually test eprodl, AKM 18/8/2000
        uptake(EFERT,iel) = afert(iel)
200   continue
c ... N fixation uptake was computed in the limitation routines
      uptake(ENFIX,N) = plantNfix

999   continue

      return
      end
