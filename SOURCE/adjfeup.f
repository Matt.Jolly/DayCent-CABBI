
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      subroutine adjfeup(nlyrpg, iel, time, toteup, gnfrac, 
     &                   rimpct, fwueup, feup)

      implicit none
c     include 'comput.inc'
      include 'const.inc'
      include 'param.inc'
c     include 'parcp.inc'
      include 'parfx.inc'
      include 'plot1.inc'
c     include 'plot2.inc'

c ... Argument declarations
      integer nlyrpg
      integer iel
      real time
      real toteup
      real gnfrac, rimpct
      real fwueup(CMXLYR)
      real feup(CMXLYR)
      real euptmp(CMXLYR)
      real evailm(CMXLYR)
      real feupsum

c ... Local variables
      integer lyr
      real fsol
      real toler
      real evailmsum, eupsum, edeficit
      logical usefwueup

c ... Functions
      real fsfunc
      external fsfunc

      toler = 1.0E-30
      evailmsum = 0.0
      eupsum = 0.0
      usefwueup = .TRUE.
     
      do 10 lyr = 1, nlyrpg
        evailm(lyr) = 0.0
        if (minerl(lyr,iel) .gt. toler) then
          fsol = 1.0
          if (iel .eq. P) then
            fsol = fsfunc(minerl(SRFC,P), pslsrb, sorpmx)
          endif
          evailm(lyr) = minerl(lyr,iel) * fsol 
     &                  * min(1.0,favail(iel))
     &                  * gnfrac * rimpct
          evailmsum = evailmsum + evailm(lyr)
        endif
10    continue

      if (toteup - evailmsum .gt. 0.00001) then
        write(*,*) 'Error in adjfeup: total E demand > E available'
        write(*,'(a7,f9.4)') 'time = ', time
        write(*,'(a10,i1)') 'Element = ', iel
        write(*,'(a18,f10.6)') 'total E demand = ', toteup
        write(*,'(a15,f10.6)') 'E available = ', evailmsum
        STOP
      endif

      evailmsum = 0.0
      do 20 lyr = 1, nlyrpg
        if (fwueup(lyr)*toteup > evailm(lyr)) then
          write(*,*) 'Required E uptake based on fwueup > E available.'
          write(*,'(a7,f9.4)') 'time = ', time
          write(*,'(a10,i1)') 'Element = ', iel
          write(*,'(a9,f9.4)') 'rimpct = ', rimpct
          write(*,'(a7,i1,a4,f6.4)') 'fwueup(',lyr,') = ', fwueup(lyr)
          write(*,'(a7,i1,a11,f6.4)') 'fwueup(',lyr,')*toteup = ', 
     &                                fwueup(lyr)*toteup
          write(*,'(a7,i1,a4,f6.4)') 'evailm(',lyr,') = ', evailm(lyr)
          usefwueup = .FALSE.
c ....... For this layer, uptake all evailm(lyr)
c ....... and evailm(lyr) becomes 0.0 for the next calculation
          euptmp(lyr) = evailm(lyr)
          evailm(lyr) = 0.0
        else
c ....... Reduce evailm(lyr) for the next calculation
c ....... which determines how to distribute the deficit.
          euptmp(lyr) = fwueup(lyr)*toteup
          evailm(lyr) = evailm(lyr) - euptmp(lyr)
          evailmsum = evailmsum + evailm(lyr)
        endif
c ..... eupsum is the amount of E uptake so far.
        eupsum = eupsum + euptmp(lyr)
20    continue

c ... Determine if there is a deficit of E uptake based on the
c ... calculation above.  
      edeficit = toteup - eupsum
      if ((edeficit .gt. 0.0) .and. (usefwueup .eqv. .FALSE.)) then
        write(*,'(a20,f9.6)') 'Profile E deficit = ', edeficit
c ..... edeficit must be split among layers with surplus evailm
c ..... in proportion to the amount of evailm(lyr) that remains 
c ..... after the initial uptake calculation above.
        do 25 lyr = 1, nlyrpg
          if (evailm(lyr) .gt. 0.0) then
            euptmp(lyr) = euptmp(lyr)+edeficit*(evailm(lyr)/evailmsum)
          endif
25      continue
      endif

      if (usefwueup) then
        do 30 lyr = 1, nlyrpg
          feup(lyr) = fwueup(lyr)
30      continue
      else
        write(*,*) 'Readjusted uptake fractions:'
        feupsum = 0.0
        eupsum = 0.0
        do 40 lyr = 1, nlyrpg
          feup(lyr) = euptmp(lyr)/toteup
          feupsum = feupsum + feup(lyr)
          eupsum = eupsum + euptmp(lyr)
          write(*,'(a5,i1,a4,f6.4)') 'feup(',lyr,') = ', feup(lyr)
40      continue
        write(*,*) 'Checking E uptake balance:'
        write(*,'(a9,f6.4)')  'toteup = ', toteup
        write(*,'(a9,f6.4)')  'eupsum = ', eupsum
        write(*,'(a10,f6.4)') 'feupsum = ', feupsum
        if (abs(1.0-feupsum) .gt. 0.00001) then
          write(*,*) 'Error in adjfeup'
          stop
        endif
      endif

      return
      end


