
c              Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c**********************************************************************
c
c  FILE:      fltcegt.f
c
c  FUNCTION:  subroutine fltcegrasstree()
c
c  PURPOSE:   Compute minimum and maximum C/E ratios for grasstree parts.
c             Above-ground parts (leaves and stems) have floating minimum 
c             and maximum C/E ratios for that are a function of 
c             above-ground biomass. Minimum and maximum C/E ratios for 
c             fine roots and coarse roots are defined in grasstree.100.
c
c  AUTHOR:    New function derived from fltce (for crops/grasses).
c             Melannie Hartman Jan. 24, 2014
c
c**********************************************************************

      subroutine fltcegrasstree(nelem, gtleavc, gtstemc, co2cce)

      implicit none
      include 'comput.inc'
      include 'const.inc'
      include 'dovars.inc'
      include 'pargt.inc'

c ... Argument declarations
      integer nelem
      real    gtleavc, gtstemc, co2cce(3,2,MAXIEL)

c ... Compute the minimum and maximum C/N, C/P, and C/S ratios allowed
c ... in plants.
c ... Changed grwprc to 2.5*aglivc in calculation of cercrp

c ... Inputs
c ...   co2cce(3,IMIN,iel) - CO2 effect on minimum C/E ratios
c ...   co2cce(3,IMAX,iel) - CO2 effect on maximum C/E ratios
c ...
c ... Outputs
c ...   cergrasstree(IMIN,GTLEAF,iel) - leaf minimum C/E ratio of the current grasstree
c ...   cergrasstree(IMAX,GTLEAF,iel) - leaf maximum C/E ratio of the current grasstree
c ...   cergrasstree(IMIN,GTSTEM,iel) - stem minimum C/E ratio of the current grasstree
c ...   cergrasstree(IMAX,GTSTEM,iel) - stem maximum C/E ratio of the current grasstree
c
c ...   cergrasstree(IMIN,GTFROOTJ,iel) - juevenile fine root minimum C/E ratio of the current grasstree
c ...   cergrasstree(IMAX,GTFROOTJ,iel) - juevenile fine root maximum C/E ratio of the current grasstree
c ...   cergrasstree(IMIN,GTFROOTM,iel) - mature fine root minimum C/E ratio of the current grasstree
c ...   cergrasstree(IMAX,GTFROOTM,iel) - mature fine root maximum C/E ratio of the current grasstree
c ...   cergrasstree(IMIN,GTCROOT,iel)  - coarse root minimum C/E ratio of the current grasstree
c ...   cergrasstree(IMAX,GTCROOT,iel)  - coarse root maximum C/E ratio of the current grasstree

c ... Local variables
      integer iel, ipart

c ... This was the calculation for crops in fltce, for reference.
c     do 20 iel=1,nelem
c       cercrp(IMIN,ABOVE,iel) =
c    &         min(pramn(iel,1)+(pramn(iel,2)-pramn(iel,1)) *
c    &         2.5 * aglivc / biomax,pramn(iel,2))
c       cercrp(IMAX,ABOVE,iel) = 
c    &         min(pramx(iel,1)+(pramx(iel,2)-pramx(iel,1)) *
c    &         2.5 * aglivc / biomax,pramx(iel,2))
c
c       cercrp(IMIN,BELOW,iel) = prbmn(iel,1)+prbmn(iel,2)*grwprc
c       cercrp(IMAX,BELOW,iel) = prbmx(iel,1)+prbmx(iel,2)*grwprc
c20   continue

c ... For grasstree there is a separate "biomax" for leaves and stems.
c ... -mdh 2/19/2014

      do 20 iel=1,nelem
        cergrasstree(IMIN,GTLEAF,iel) =
     &         min(cergtamn1(GTLEAF,iel)
     &             +(cergtamn2(GTLEAF,iel)-cergtamn1(GTLEAF,iel)) *
     &         gtbiomc * (gtleavc/gtlvbiomax), cergtamn2(GTLEAF,iel))
        cergrasstree(IMAX,GTLEAF,iel) = 
     &         min(cergtamx1(GTLEAF,iel)
     &             +(cergtamx2(GTLEAF,iel)-cergtamx1(GTLEAF,iel)) *
     &         gtbiomc * (gtleavc/gtlvbiomax), cergtamx2(GTLEAF,iel))

        cergrasstree(IMIN,GTSTEM,iel) =
     &         min(cergtamn1(GTSTEM,iel)
     &             +(cergtamn2(GTSTEM,iel)-cergtamn1(GTSTEM,iel)) *
     &         gtbiomc * (gtstemc/gtstmbiomax), cergtamn2(GTSTEM,iel))
        cergrasstree(IMAX,GTSTEM,iel) = 
     &         min(cergtamx1(GTSTEM,iel)
     &             +(cergtamx2(GTSTEM,iel)-cergtamx1(GTSTEM,iel)) *
     &         gtbiomc * (gtstemc/gtstmbiomax), cergtamx2(GTSTEM,iel))

        cergrasstree(IMIN,GTFROOTJ,iel) = cergtbmn(GTFROOTJ,iel)
        cergrasstree(IMAX,GTFROOTJ,iel) = cergtbmx(GTFROOTJ,iel)
        cergrasstree(IMIN,GTFROOTM,iel) = cergtbmn(GTFROOTM,iel)
        cergrasstree(IMAX,GTFROOTM,iel) = cergtbmx(GTFROOTM,iel)
        cergrasstree(IMIN,GTCROOT,iel) = cergtbmn(GTCROOT,iel)
        cergrasstree(IMAX,GTCROOT,iel) = cergtbmx(GTCROOT,iel)

c       write(*,*) 'fltcegt1: cergrasstree(IMIN,GTCROOT,N)=', 
c    &              cergrasstree(IMIN,GTCROOT,iel)
c       write(*,*) 'fltcegt1: cergrasstree(IMAX,GTCROOT,N)=', 
c    &              cergrasstree(IMAX,GTCROOT,iel)

20    continue

c ... Added effect of co2
      do 30 iel = 1, nelem
        do 40 ipart = 1, GTLIVPARTS


          cergrasstree(IMIN,ipart,iel) = cergrasstree(IMIN,ipart,iel) 
     &                                   * co2cce(GTARYINDX,IMIN,iel)
          cergrasstree(IMAX,ipart,iel) = cergrasstree(IMAX,ipart,iel)
     &                                   * co2cce(GTARYINDX,IMAX,iel)

c         write(*,*) 'fltcegt2: co2cce(3,IMIN,N) =', 
c    &                co2cce(GTARYINDX,IMIN,N)
c         write(*,*) 'fltcegt2: co2cce(3,IMAX,N) =', 
c    &                co2cce(GTARYINDX,IMAX,N)
c         write(*,*) 'fltcegt2: cergrasstree(IMIN,ipart,N)=', 
c    &                cergrasstree(IMIN,ipart,N)
c         write(*,*) 'fltcegt2: cergrasstree(IMAX,ipart,N)=', 
c    &                cergrasstree(IMAX,ipart,N)

          if (cergrasstree(IMIN,ipart,iel) .le. 0.0) then
            write(*,*) 'fltcegt: cergrasstree(IMIN,ipart,iel)=', 
     &                  cergrasstree(IMIN,ipart,iel)
            write(*,*) 'fltcegt: co2cce(GTARYINDX,IMIN,iel)=', 
     &                  co2cce(GTARYINDX,IMIN,iel)
            STOP
          endif

          if (cergrasstree(IMAX,ipart,iel) .le. 0.0) then
            write(*,*) 'fltcegt: cergrasstree(IMAX,ipart,iel)=', 
     &                  cergrasstree(IMAX,ipart,iel)
            write(*,*) 'fltcegt: co2cce(GTARYINDX,IMAX,iel)=', 
     &                  co2cce(GTARYINDX,IMAX,iel)
            STOP
          endif


40      continue
30    continue




      return
      end
