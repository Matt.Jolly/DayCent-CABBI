
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      addomad.f
c
c  FUNCTION:  subroutine addomad()
c
c  PURPOSE:   Organic Matter additions to crop, grasstree, and forest systems.
c             This subroutine should only be called 
c               if (doomad .and. (omadday .eq. curday)).
c
c  AUTHOR:    New function to consolidate repetitive code formerly in 
c             subroutines crop() and trees().
c             Melannie Hartman Jan. 24, 2014
c
c  FUNCTION ARGUMENTS:
c    time  - simulation time (years)
c
c  REPLACE CODE IN CROP.F AND TREES.F WITH A CALL TO THIS FUNCTION!
c 
c*****************************************************************************      

      subroutine addomad(time)

c ATTENTION: are all these includes needed?  Try to reduce the list.
      implicit none
      include 'const.inc'
      include 'dovars.inc'
      include 'fertil.inc'
      include 'ligvar.inc'
      include 'param.inc'
      include 'parcp.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'timvar.inc'
      include 'seq.inc'
     
c ... Argument declarations
      real             time

c ... Local variables
      real    accum(ISOS)
      real    fraclabl, k2
      integer iel, ii

      accum(LABELD) = 0.0
      accum(UNLABL) = 0.0
      fraclabl = 0.0


c ..... For C13/C14 labeling simulations astlbl is the concentration of
c ..... C13/C14 in the labeled C
c ..... Calculate fraction of labeled C as 14C if 14C labeling
        if (labtyp .eq. 1) then
c          fraclabl = ((astlbl / 1000.0) + 1.0) / 1000.0
          k2 = FRAC_C14 * (1.0 + (astlbl / 1000.0))
          fraclabl = k2 / (1.0 + k2)
        endif
c ..... Calculate fraction of labeled C as 13C if 13C labeling
        if (labtyp .eq. 2) then
          fraclabl = astlbl * PEEDEE * 1.0e-03 + PEEDEE
          fraclabl = 1 / (1/fraclabl + 1)
        endif
c ..... Allow for two types of organic matter addition.  Litter, for
c ..... example wheat straw, is added to the structural and metabolic
c ..... pools by the partit subroutine.  Partially decomposed organic
c ..... matter, for example compost, is added directly into the surface
c ..... slow pool (som2c(1)).
        if (omadtyp .eq. 1) then

c         write(*,*)
c         write(*,*) 'addomad: astgc=', astgc*OMADscalar(month)
c         write(*,*) 'addomad: astrec=', astrec
c         write(*,*) 'addomad: astlig=', astlig
c         write(*,*) 'addomad: fraclabl=', fraclabl
 
          call partit(astgc*OMADscalar(month),astrec,1,csrsnk,esrsnk,
     &                astlig,fraclabl)

        elseif (omadtyp .eq. 2) then
          call csched(astgc*OMADscalar(month), fraclabl, 1.0,
     &                csrsnk(UNLABL), som2ci(SRFC,UNLABL),
     &                csrsnk(LABELD), som2ci(SRFC,LABELD),
     &                1.0, accum)
          do 10 iel = 1, nelem
            call flow(esrsnk(iel), som2e(SRFC,iel), time,
     &                astgc*astrec(iel))
10        continue
c ..... For omadtyp values of 3 and 4 astlbl is the fraction of material that
c ..... is labeled rather than the concentration
        elseif (omadtyp .eq. 3) then
          call partit(astgc*OMADscalar(month),astrec,1,csrsnk,esrsnk,
     &                astlig,astlbl)
        elseif (omadtyp .eq. 4) then
          call csched(astgc*OMADscalar(month), astlbl, 1.0,
     &                csrsnk(UNLABL), som2ci(SRFC,UNLABL),
     &                csrsnk(LABELD), som2ci(SRFC,LABELD),
     &                1.0, accum)
          do 15 iel = 1, nelem
            call flow(esrsnk(iel), som2e(SRFC,iel), time,
     &                astgc*astrec(iel))
15        continue
        else
          write(*,*) 'omadtyp out of range omad.100.'
          write(*,*) 'Must be equal to 1, 2, 3, or 4'
          write(*,*) 'omadtyp currently set to: ', omadtyp
          STOP
        endif
c ..... Update OMAD accumulator output variables, cak - 07/13/2006
        omadac = omadac + astgc*OMADscalar(month)
        omadmth(month) = omadmth(month)  + astgc*OMADscalar(month)
        omadtot = omadtot + astgc*OMADscalar(month)
        if (cursys .eq. GRASSTREESYS) then
c ....... Track separate growing season omad accumulator for grasstree.
c ....... -mdh 11/6/2019
          gtomadtot = gtomadtot + astgc*OMADscalar(month)
        else
          gomadtot = gomadtot + astgc*OMADscalar(month)
        endif
        do 20 ii = 1, nelem
          omadae(ii) = omadae(ii) +
     &                 (astgc*OMADscalar(month) * astrec(ii))
          omadmte(month, ii) = omadmte(month, ii) +
     &                         (astgc*OMADscalar(month) * astrec(ii))
          omaetot(ii) = omaetot(ii) +
     &                  (astgc*OMADscalar(month) * astrec(ii))

          if (cursys .eq. GRASSTREESYS) then
c ......... Track separate growing season omad accumulator for grasstree.
c ......... -mdh 11/6/2019
            gtomaetot(ii) = gtomaetot(ii) +
     &                   (astgc*OMADscalar(month) * astrec(ii))
          else
            gomaetot(ii) = gomaetot(ii) +
     &                   (astgc*OMADscalar(month) * astrec(ii))
          endif
20      continue
c ..... don't let organic matter be added twice in savana
        doomad = .FALSE.

      return
      end

