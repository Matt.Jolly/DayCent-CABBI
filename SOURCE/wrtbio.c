
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      wrtbio.c
**
**  FUNCTION:  void wrtbio()
**
**  PURPOSE:   Write out carbon and nitrogen in live biomass. 
**
**  AUTHOR:    Melannie Hartman  6/93
** 
**  INPUTS:
**    aglivc  - above ground live crop/grass carbon (gC/m2)
**    aglivn  - amount of nitrogen in above ground live crop/grass (gN/m2)
**    bglivcj - below ground live crop/grass juvenile fine root carbon (gC/m2)
**    bglivnj - amount of nitrogen in live crop/grass junvenile fine roots (gN/m2)
**    bglivcm - below ground live crop/grass mature fine root carbon (gC/m2)
**    bglivnm - amount of nitrogen in live crop/grass mature fine roots (gN/m2)
**    crootc  - forest live coarse root carbon (gC/m2)
**    crootn  - forest live coarse root nitrogen (gN/m2)
**    curday  - the day of the year (1..366)
**    fbrchc  - forest live fine branch carbon (gC/m2)
**    fbrchn  - forest live fine branch nitrogen (gN/m2)
**    frootcj - forest live juvenile fine root carbon (gC/m2)
**    frootnj - forest live juvenile fine root nitrogen (gN/m2)
**    frootcm - forest live mature fine root carbon (gC/m2)
**    frootnm - forest live mature fine root nitrogen (gN/m2)
**    gtcrootc  - grasstree live coarse root carbon (gC/m2)
**    gtcrootn  - grasstree live coarse root nitrogen (gN/m2)
**    gtfrootcj - grasstree live juvenile fine root carbon (gC/m2)
**    gtfrootnj - grasstree live juvenile fine root nitrogen (gN/m2)
**    gtfrootcm - grasstree live mature fine root carbon (gC/m2)
**    gtfrootnm - grasstree live mature fine root nitrogen (gN/m2)
**    gtleavc  - grasstree live leaf carbon (gC/m2)
**    gtleavn  - grasstree live leaf nitrogen (gN/m2)
**    gtstemc  - grasstree live stem carbon (gC/m2)
**    gtstemn  - grasstree live stem nitrogen (gN/m2)
**    h2ogef1 - water effect on crop/grass production (0.0-1.0)
**    h2ogef2 - water effect on forest production (0.0-1.0)
**    h2ogef3 - water effect on grasstree production (0.0-1.0)
**    rleavc  - forest live leaf carbon (gC/m2)
**    rleavn  - forest live leaf nitrogen (gN/m2)
**    rlwodc  - forest live large wood carbon (gC/m2)
**    rlwodn  - forest live large wood nitrogen (gN/m2)
**    time    - simulation time (years)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files            - structure containing information about output files
**    files->fp_bio    - file pointer to bio.out output file
**    files->write_bio - flag to indicate if bio.out output file should be
**                       created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**     None
**
**  CALLED BY:
**     simsom()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include "soilwater.h"

    void wrtbio(float *time, int *curday, float *aglivc, float *bglivcj,
                float *bglivcm, float *aglivn, float *bglivnj, float *bglivnm,
                float *rleavc, float *fbrchc, float *rlwodc, float *frootcj, float *frootcm, float *crootc, 
                float *rleavn, float *fbrchn, float *rlwodn, float *frootnj, float *frootnm, float *crootn, 
                float *gtleavc, float *gtstemc, float *gtfrootcj, float *gtfrootcm, float *gtcrootc, 
                float *gtleavn, float *gtstemn, float *gtfrootnj, float *gtfrootnm, float *gtcrootn, 
                float *h2ogef1, float *h2ogef2, float *h2ogef3)
    {
      extern FILES_SPT files;

      if (!files->write_bio) {
        return;
      }

      fprintf(files->fp_bio, "%6.2f,%2d,%10.4f,%10.4f,%10.4f,",
              *time, *curday, *aglivc, *bglivcj, *bglivcm);
      fprintf(files->fp_bio, "%10.4f,%10.4f,%10.4f,",
                              *aglivn, *bglivnj, *bglivnm);
      fprintf(files->fp_bio, "%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,", 
                *rleavc, *fbrchc, *rlwodc, *frootcj, *frootcm, *crootc);
      fprintf(files->fp_bio, "%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,", 
                *rleavn, *fbrchn, *rlwodn, *frootnj, *frootnm, *crootn);
      fprintf(files->fp_bio, "%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,", 
                *gtleavc, *gtstemc, *gtfrootcj, *gtfrootcm, *gtcrootc);
      fprintf(files->fp_bio, "%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,", 
                *gtleavn, *gtstemn, *gtfrootnj, *gtfrootnm, *gtcrootn);
      fprintf(files->fp_bio, "%10.6f,%10.6f,%10.6f\n", 
              *h2ogef1, *h2ogef2, *h2ogef3);

      return;
    }
