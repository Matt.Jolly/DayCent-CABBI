
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      real function fcrpval(curcrp)
c ... Function that returns a numeric value for the current crop (curcrp).
c ... This is code pulled from cropin.f. -mdh 11/7/2019

      implicit none

c ... Argument declarations
      character*8 curcrp

c ... Local variables
      integer ii
      real crpval

c ... Determine the 'numerical value' of the curcrp, 
c ... for use as an output variable
      crpval = 0
      do 10 ii = 1, 5
        if (curcrp(ii:ii) .ne. ' ') then
          if (curcrp(ii:ii) .ge. '0' .and. curcrp(ii:ii) .le. '9') then
            crpval = crpval +
     &               ((ichar(curcrp(ii:ii)) - ichar('0')) / 10.0)
          else
            crpval = crpval + (ichar(curcrp(ii:ii)) - ichar('A')) + 1
          endif
        endif
10    continue

      fcrpval = crpval

      return 
      end
