
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c
c*****************************************************************************
c
c  FILE:      gtfirrtn.f
c
c  FUNCTION:  subroutine gtfirrtn()
c
c  PURPOSE:   TREM event (harvest) for GrassTrees. 
c             Elemental return from a fire event.
c
c  AUTHOR:    New function derived from firrtn.
c             Melannie Hartman Feb. 5, 2014
c             Updated to use new snag TREM parameters. -mdh 2/23/2019
c
c  Called from:  grasstreerem
c
c*****************************************************************************

      subroutine gtfirrtn()

      implicit none
      include 'const.inc'
      include 'forrem.inc'
      include 'npool.inc'
      include 'parfx.inc'
      include 'param.inc'
      include 'plot1.inc'
      include 'plot2.inc'
c     include 'plot3.inc'
      include 'plot4.inc'
      include 'zztim.inc'

c ... Fortran to C prototype
      INTERFACE

        SUBROUTINE flow(from, to, when, howmuch)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          REAL from
          REAL to
          REAL when
          REAL howmuch
        END SUBROUTINE flow

        SUBROUTINE update_npool(clyr, amt, frac_nh4, frac_no3, 
     &                          ammonium, nitrate, subname)
          !MS$ATTRIBUTES ALIAS:'_update_npool' :: update_npool
          INTEGER          clyr
          REAL             amt
          DOUBLE PRECISION frac_nh4
          DOUBLE PRECISION frac_no3
          DOUBLE PRECISION ammonium
          DOUBLE PRECISION nitrate(*)
          CHARACTER subname*10
        END SUBROUTINE update_npool

      END INTERFACE

c ... Local Variables
      integer   iel, clyr
      real      accum(ISOS)
      real      cpass, cret
      real      eret(MAXIEL), epass(MAXIEL)
      character subname*10
      double precision frac_nh4, frac_no3

      subname = 'gtfirrtn  '

c ... Only burn litter if a grasstree system.  Litter is burned
c ... in grem.f

      cpass = 0.0
      do 10 iel = 1, nelem
        epass(iel) = 0.0
        eret(iel) = 0.0
10    continue

c ... Carbon return from GRASSTREE compartments as charcoal

c ... Return carbon from burnt live leaves as charcoal
c ... to the passive SOM pool
      if (gtleavc .gt. 0.0) then
        cret = remf(1) * (1.0 - lv2std(1)) * retf(1,1) * gtleavc
        gtcreta = gtcreta + cret
        cpass = cpass + cret
        call csched(cret,gtlvcis(LABELD),gtleavc,
     &              csrsnk(UNLABL),som3ci(UNLABL),
     &              csrsnk(LABELD),som3ci(LABELD),
     &              1.0,accum)
      endif

c ... Return carbon from burnt live stems as charcoal
c ... to the passive SOM pool
      if (gtstemc .gt. 0.) then
        cret = remf(2) * (1.0 - lv2std(2)) * retf(2,1) * gtstemc
        gtcreta = gtcreta + cret
        cpass = cpass + cret
        call csched(cret,gtstmcis(LABELD),gtstemc,
     &              csrsnk(UNLABL),som3ci(UNLABL),
     &              csrsnk(LABELD),som3ci(LABELD),
     &              1.0,accum)
      endif

c ... Return carbon from burnt attached dead leaves as charcoal
c ... to the passive SOM pool
      if (gtdleavc .gt. 0.) then
        cret = remf(6) * retf(4,1) * gtdleavc
        gtcreta = gtcreta + cret
        cpass = cpass + cret
        call csched(cret,gtdlvcis(LABELD),gtdleavc,
     &              csrsnk(UNLABL),som3ci(UNLABL),
     &              csrsnk(LABELD),som3ci(LABELD),
     &              1.0,accum)
      endif

c ... Return carbon from burnt dead stems as charcoal
c ... to the passive SOM pool
      if (dfbrchc .gt. 0.) then
        cret = remf(7) * retf(5,1) * gtdstemc
        gtcreta = gtcreta + cret
        cpass = cpass + cret
        call csched(cret,gtdstmcis(LABELD),gtdstemc,
     &              csrsnk(UNLABL),som3ci(UNLABL),
     &              csrsnk(LABELD),som3ci(LABELD),
     &              1.0,accum)
      endif

c ... Associated elements to passive pool based on max C/E ratio
      do 100 iel = 1, nelem
        epass(iel) = cpass / varat3(1,iel)
        gtereta(iel) = gtereta(iel) + epass(iel)
        call flow(esrsnk(iel),som3e(iel),time,epass(iel))
100   continue

c ---------------------------------------------------------------------


c ... N, P, and S returns go to the top layer of minerl.  EGAIN will 
c ... contain the total returns for N, P, and S across above-ground pools.

c ... ATTENTION: burned live parts should have charcoal return also.

      do 20 iel = 1, nelem
        eret(iel) = eret(iel) +
     &    remf(1) * retf(1,iel+1) * (1.0 - lv2std(1)) * gtleave(iel) +
     &    remf(2) * retf(2,iel+1) * (1.0 - lv2std(2)) * gtsteme(iel) +
     &    remf(6) * retf(4,iel+1) * gtdleave(iel) +
     &    remf(7) * retf(5,iel+1) * gtdsteme(iel) 

        frac_nh4 = 0.5
        frac_no3 = 0.5
        if (iel .eq. N) then 
          clyr = 1
          call update_npool(clyr, eret(iel), frac_nh4, frac_no3, 
     &                      ammonium, nitrate, subname)
        endif
        call flow(esrsnk(iel),minerl(1,iel),time,eret(iel))
20    continue

      return
      end
