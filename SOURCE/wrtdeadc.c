
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      wrtdeadc.c
**
**  FUNCTION:  void wrtdeadc()
**
**  PURPOSE:   Write out the dead carbon values. 
**
**  AUTHOR:    Cindy Keough  10/02
** 
**  INPUTS:
**    curday    - the day of the year (1..366)
**    dleavc    - carbon in attached dead leaf component for forest (g C m-2)
**    dfbrchc   - carbon in attached dead fine branch component for forest (g C m-2)
**    dlwodc    - carbon in standing dead large wood component for forest (g C m-2)
**    gtdleavc  - carbon in attached dead leaf component for grasstree (g C m-2)
**    gtdstemc  - carbon in standing dead stem component for grasstree (g C m-2)
**    metabc(1) - carbon in metabolic component of surface litter (g C m-2)
**    metabc(2) - carbon in metabolic component of soil litter (g C m-2)
**    stdedc    - standing dead carbon (g C m-2)
**    strucc(1) - carbon in structural component of surface litter (g C m-2)
**    strucc(2) - carbon in structural component of soil litter (g C m-2)
**    time      - simulation time (years)
**    wood1c    - carbon in downed dead fine branch component for forest (g C m-2)
**    wood2c    - carbon in downed dead large wood component for forest (g C m-2)
**    wood3c    - carbon in dead coarse root component for forest (g C m-2)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files              - structure containing information about output files
**    files->fp_deadc    - file pointer to deadc.out output file
**    files->write_deadc - flag to indicate if deadc.out output file should
**                         be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**     None
**
**  CALLED BY:
**     simsom()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include "soilwater.h"

    void wrtdeadc(float *time, int *curday, float *stdedc, 
                  float *dleavc, float *dfrbchc, float *dlwodc,
                  float *wood1c, float *wood2c, float *wood3c,
                  float *gtdleavc, float *gtdstemc,
                  float *metabc1, float *metabc2, float *strucc1, float *strucc2) 
    {
      extern FILES_SPT files;

      if (!files->write_deadc) {
        goto ex;
      }

      fprintf(files->fp_deadc, "%6.2f,%2d,%10.4f,%10.4f,%10.4f,%10.4f,",
              *time, *curday, *stdedc, *dleavc, *dfrbchc, *dlwodc);
      fprintf(files->fp_deadc, "%10.4f,%10.4f,%10.4f,", 
              *wood1c, *wood2c, *wood3c);
      fprintf(files->fp_deadc, "%10.4f,%10.4f,", 
              *gtdleavc, *gtdstemc);
      fprintf(files->fp_deadc, "%10.4f,%10.4f,%10.4f,%10.4f\n", 
              *metabc1, *metabc2, *strucc1, *strucc2);

ex:   return;
    }
