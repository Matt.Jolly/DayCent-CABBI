
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c ..............................................................................
c MODIFICATIONS FOR GRASSTREE
c   Added pgrasstreec
c   Added grasstree_a2drat(3)
c   Note: eup(6,3) does not need to change because the 1st index is maximum 
c     dimension used (CPARTS=3, FPARTS=6, and GTLIVPARTS=5).
c     
c Daily N uptake variables added 5/15/2019 -mdh
c   eupsoil(FPARTS,MAXIEL) - soil N uptake (gN/m2/day)
c   eupstg(FPARTS,MAXIEL) - internal N uptake (gN/m2/day)
c   eupnfix(FPARTS,MAXIEL) - N uptake from symbiotic N fixation (gN/m2/day)
c   eupaufert(FPARTS,MAXIEL) - N uptake from auto fertilization (gN/m2/day)
c   cercrpnew(CPARTS,MAXIEL) - C/E of new growth
c ..............................................................................
c SWC variables for computing flooding effect on potential production. -mdh 6/10/2019
c   swcpg    - amount of water in the rooting zone (1..nlaypg) (cm H2O)
c   swcfcpg  - amount of water when at field capacity in the rooting zone (1..nlaypg) (cm H2O)
c   swcsatpg - amount of water when at saturation in the rooting zone (1..nlaypg) (cm H2O)
c ..............................................................................

      common/potent/agp,tgprod,pcropc,pforc,eup(6,3),
     &              crop_a2drat(3),tree_a2drat(3),
     &              pgrasstreec,grasstree_a2drat(3),
     &              eupsoil(6,3),eupstg(6,3),
     &              eupnfix(6,3),eupaufert(6,3),
     &              cercrpnew(6,3),swcpg,swcfcpg,swcsatpg

      real agp,tgprod,pcropc,pforc,eup,
     &              crop_a2drat,tree_a2drat,
     &              pgrasstreec,grasstree_a2drat,
     &              eupsoil,eupstg,eupnfix,eupaufert,
     &              cercrpnew,swcpg,swcfcpg,swcsatpg

      save /potent/
