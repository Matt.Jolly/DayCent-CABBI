
c               Copyright 1993 Colorado State University
c                       All Rights Reserved

c ... Added separate growing season accumulators for grasstree 
c ... (-mdh 11/6/2019): gtfertot, gtirrtot, gtomadtot, gtomaetot

      common/fertil/aufert, feramt(3), gfertot(3), girrtot, gomadtot,
     &              gomaetot(3), gtfertot(3), gtirrtot, gtomadtot,
     &              gtomaetot(3), ninhib, ninhtm,
     &              Nscalar(12), nreduce, OMADscalar(12)
      real aufert, feramt, gfertot, girrtot, gomadtot, gomaetot,
     &     gtfertot, gtirrtot, gtomadtot, gtomaetot,
     &     ninhib, Nscalar, nreduce, OMADscalar
      integer ninhtm

      save/fertil/
