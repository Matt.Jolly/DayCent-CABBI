
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c
c*****************************************************************************
c
c  FILE:      grasstreerem.f
c
c  FUNCTION:  subroutine grasstreerem()
c
c  PURPOSE:   TREM event (harvest) for GrassTrees. Includes removal of
c             above-ground live and standing dead material, death of roots,
c             return of C,N,P,S from cutting event, and return of N,P,S
c             from burning event. Litter burning is handled by grem.
c
c  AUTHOR:    New function derived from frem.
c             Melannie Hartman Feb. 5, 2014
c
c             Write grasstree harvest variables in harvgt.inc to 
c             harvest_gt.csv. -mdh 9/4/2019
c               evntyp = 0: Grasstree cutting event without grain harvest
c               evntyp = 1: Grasstree fire event
c               evntyp = 2: Grasstree cutting event with grain harvest
c
c  Called from:  simsom
c
c*****************************************************************************

      subroutine grasstreerem(time, curday)

      implicit none
      include 'const.inc'
      include 'fertil.inc'
      include 'forrem.inc'
      include 'harvgt.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot4.inc'

c ... Function arguments
      real    time
      integer curday

c ... Fortran to C prototype
      INTERFACE

        SUBROUTINE wrtharvestgt(time, curday, crpval, 
     &    gtlvacc, gtstemacc, gtfrtjacc,
     &    gtfrtmacc, gtcrtacc, potgtacc, 
     &    cgrain, egrain1, egrain2, egrain3, hi,
     &    gtlvcrem, gtlverem1, gtlverem2, gtlverem3,
     &    gtdlvcrem, gtdlverem1, gtdlverem2, gtdlverem3,
     &    gtstmcrem, gtstmerem1, gtstmerem2, gtstmerem3,
     &    gtdstmcrem, gtdstmerem1, gtdstmerem2, gtdstmerem3,
     &    gtstm2dstmc, gtstm2dstme1, gtstm2dstme2, gtstm2dstme3,
     &    gtlv2dlvc, gtlv2dlve1, gtlv2dlve2, gtlv2dlve3,
     &    gtlvcret, gtlveret1, gtlveret2, gtlveret3,
     &    gtdlvcret, gtdlveret1, gtdlveret2, gtdlveret3,
     &    gtstmcret, gtstmeret1, gtstmeret2, gtstmeret3,
     &    gtdstmcret, gtdstmeret1, gtdstmeret2, gtdstmeret3,
     &    irrapp, fertapp1, fertapp2, fertapp3,
     &    omadapp, omaeapp1, omaeapp2, omaeapp3, 
     &    strmac1, strmac2, strmac3, strmac4, 
     &    strmac5, strmac6, strmac7, strmac8,
     &    carbostgloss, gtstgloss1, gtstgloss2, gtstgloss3,
     &    cgracc, egracc1, egracc2, egracc3,
     &    srfclittrj, esrfclittrj1, esrfclittrj2, esrfclittrj3,
     &    soillittrj, esoillittrj1, esoillittrj2, esoillittrj3,
     &    srfclittrm, esrfclittrm1, esrfclittrm2, esrfclittrm3,
     &    soillittrm, esoillittrm1, esoillittrm2, esoillittrm3,
     &    srfclittrcrt, esrfclittrcrt1, esrfclittrcrt2,
     &    esrfclittrcrt3, soillittrcrt, esoillittrcrt1,
     &    esoillittrcrt2, esoillittrcrt3)
         !MS$ATTRIBUTES ALIAS:'_wrtharvest_gt' :: wrtharvest_gt
          REAL    time
          INTEGER curday
          REAL    crpval
          REAL    gtlvacc, gtstemacc, gtfrtjacc
          REAL    gtfrtmacc, gtcrtacc, potgtacc
          REAL    cgrain, egrain1, egrain2, egrain3, hi
          REAL    gtlvcrem, gtlverem1, gtlverem2, gtlverem3
          REAL    gtdlvcrem, gtdlverem1, gtdlverem2, gtdlverem3
          REAL    gtstmcrem, gtstmerem1, gtstmerem2, gtstmerem3
          REAL    gtdstmcrem, gtdstmerem1, gtdstmerem2, gtdstmerem3
          REAL    gtstm2dstmc, gtstm2dstme1, gtstm2dstme2, gtstm2dstme3
          REAL    gtlv2dlvc, gtlv2dlve1, gtlv2dlve2, gtlv2dlve3
          REAL    gtlvcret, gtlveret1, gtlveret2, gtlveret3
          REAL    gtdlvcret, gtdlveret1, gtdlveret2, gtdlveret3
          REAL    gtstmcret, gtstmeret1, gtstmeret2, gtstmeret3
          REAL    gtdstmcret, gtdstmeret1, gtdstmeret2, gtdstmeret3
          REAL    irrapp, fertapp1, fertapp2, fertapp3
          REAL    omadapp, omaeapp1, omaeapp2, omaeapp3
          REAL    strmac1, strmac2, strmac3, strmac4
          REAL    strmac5, strmac6, strmac7, strmac8
          REAL    carbostgloss, gtstgloss1, gtstgloss2, gtstgloss3
          REAL    cgracc, egracc1, egracc2, egracc3
          REAL    srfclittrj, esrfclittrj1, esrfclittrj2, esrfclittrj3
          REAL    soillittrj, esoillittrj1, esoillittrj2, esoillittrj3
          REAL    srfclittrm, esrfclittrm1, esrfclittrm2, esrfclittrm3
          REAL    soillittrm, esoillittrm1, esoillittrm2, esoillittrm3
          REAL    srfclittcrtr, esrfclittrcrt1, esrfclittrcrt2 
          REAL    esrfclittrcrt3, soillittcrtr, esoillittrcrt1
          REAL    esoillittrcrt2, esoillittrcrt3
        END SUBROUTINE wrtharvestgt

      END INTERFACE

c ... Local variables
      real     accum(ISOS)

      accum(LABELD) = 0.0
      accum(UNLABL) = 0.0

c ... Removal for both CUT and FIRE events
      if ((evntyp .eq. 0) .or. (evntyp .eq. 1) 
     &     .or. (evntyp .eq. 2)) then

c ..... Live Removal
        call gtlivsdrem(accum)

c ..... Death of Roots
        call gtkillroot(accum)

      endif

      if ((evntyp .eq. 0) .or. (evntyp .eq. 2)) then

c ..... C, N, P, S returns from cutting event
        call gtcutrtn(accum)

      else if (evntyp .eq. 1) then

c ..... N, P, S returns from fire event
        call gtfirrtn()

      endif

      if ((evntyp .eq. 0) .or. (evntyp .eq. 2)) then
        call wrtharvestgt(time, curday, crpval, 
     &    gtlvacc, gtstemacc, gtfrtjacc,
     &    gtfrtmacc, gtcrtacc, potgtacc,
     &    cgrain, egrain(1), egrain(2), egrain(3), hi,
     &    gtlvcrem, gtlverem(1), gtlverem(2), gtlverem(3),
     &    gtdlvcrem, gtdlverem(1), gtdlverem(2), gtdlverem(3),
     &    gtstmcrem, gtstmerem(1), gtstmerem(2), gtstmerem(3),
     &    gtdstmcrem, gtdstmerem(1), gtdstmerem(2), gtdstmerem(3),
     &    gtstm2dstmc, gtstm2dstme(1), gtstm2dstme(2), gtstm2dstme(3),
     &    gtlv2dlvc, gtlv2dlve(1), gtlv2dlve(2), gtlv2dlve(3),
     &    gtlvcret, gtlveret(1), gtlveret(2), gtlveret(3),
     &    gtdlvcret, gtdlveret(1), gtdlveret(2), gtdlveret(3),
     &    gtstmcret, gtstmeret(1), gtstmeret(2), gtstmeret(3),
     &    gtdstmcret, gtdstmeret(1), gtdstmeret(2), gtdstmeret(3),
     &    irrapp, fertapp(1), fertapp(2), fertapp(3),
     &    omadapp, omaeapp(1), omaeapp(2), omaeapp(3), 
     &    strmac(1), strmac(2), strmac(3), strmac(4), 
     &    strmac(5), strmac(6), strmac(7), strmac(8),
     &    carbostgloss, gtstgloss(1), gtstgloss(2), gtstgloss(3),
     &    cgracc, egracc(1), egracc(2), egracc(3), 
     &    srfclittrj, esrfclittrj(1), esrfclittrj(2), esrfclittrj(3), 
     &    soillittrj, esoillittrj(1), esoillittrj(2), esoillittrj(3), 
     &    srfclittrm, esrfclittrm(1), esrfclittrm(2), esrfclittrm(3), 
     &    soillittrm, esoillittrm(1), esoillittrm(2), esoillittrm(3),
     &    srfclittrcrt, esrfclittrcrt(1), esrfclittrcrt(2), 
     &    esrfclittrcrt(3), soillittrcrt, esoillittrcrt(1), 
     &    esoillittrcrt(2), esoillittrcrt(3))
      endif

      return
      end
