
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c
c*****************************************************************************
c
c  FILE:      gtlivsdrem.f
c
c  FUNCTION:  subroutine gtlivsdrem()
c
c  PURPOSE:   TREM event (harvest) for GrassTrees. 
c             Removal of live biomass and standing dead material due to 
c             cutting or fire.
c
c  AUTHOR:    New function derived from livrem.
c             Melannie Hartman Feb. 5, 2014
c 
c             Updated to use new definitions of TREM parameters
c             that were implemented for the snag model. -mdh 2/20/2019
c
c             Track grasstree harvest variables in harvgt.inc. -mdh 9/4/2019
c
c  Called from:  grasstreerem
c
c*****************************************************************************

      subroutine gtlivsdrem(accum)

      implicit none
      include 'const.inc'
      include 'fertil.inc'
      include 'forrem.inc'
      include 'harvgt.inc'
      include 'param.inc'
      include 'pargt.inc'
      include 'plot1.inc'
      include 'plot2.inc'
c     include 'plot3.inc'
      include 'plot4.inc'
      include 'timvar.inc'
      include 'zztim.inc'

c ... Argument declarations
      real      accum(ISOS)

c ... Fortran to C prototype
      INTERFACE
        SUBROUTINE flow(from, to, when, howmuch)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          REAL from
          REAL to
          REAL when
          REAL howmuch
        END SUBROUTINE flow
      END INTERFACE

c ... Local variables
      integer iel, mm, hmonth(2)
      real closs, eloss(MAXIEL)
      real sfertot(MAXIEL), sirrtot, somadtot, somaetot(MAXIEL)
      real sumpttr, sumtran

      data sfertot  /3*0.0/
      data sirrtot  /0.0/
      data somadtot /0.0/
      data somaetot /3*0.0/
      save sfertot, sirrtot, somadtot, somaetot

c ... Reset harvest output variables
      hi = 0.0
      cgrain = 0.0
      omadapp = 0.0
      do 5 iel = 1, MAXIEL
        egrain(iel) = 0.0
        fertapp(iel) = 0.0
        omaeapp(iel) = 0.0
5     continue

c ... Remove live LEAVES and GRAIN

      if (evntyp .eq. 2) then
c ..... Grain harvest
        sumtran = 0
        sumpttr = 0
        hmonth(1) = month - himongt(1)
        hmonth(2) = month - himongt(2)
        if (hmonth(1) .lt. 1) then
          hmonth(1) = hmonth(1) + MONTHS
        endif
        if (hmonth(2) .lt. 1)  then
          hmonth(2) = hmonth(2) + MONTHS
        endif
        if (hmonth(2) .ge. hmonth(1)) then
          do 10 mm = hmonth(1), hmonth(2)
            sumtran = sumtran + htran(mm)
            sumpttr = sumpttr + hpttr(mm)
10        continue
        else
          do 15 mm = hmonth(1), MONTHS
            sumtran = sumtran + htran(mm)
            sumpttr = sumpttr + hpttr(mm)
15        continue
          do 20 mm = 1, hmonth(2)
            sumtran = sumtran + htran(mm)
            sumpttr = sumpttr + hpttr(mm)
20        continue
        endif
         if (sumpttr .le. 0.0001) then
           sumpttr = 0.0001
         endif
        hi = himaxgt * (1.0 - hiwsfgt * (1.0 - (sumtran / sumpttr)))

        if (gtleavc .gt. 0.0) then
          cgrain = hi * remf(1) * (1.0 - lv2std(1)) * gtleavc
          cgracc = cgracc + cgrain
          call csched(cgrain,gtlvcis(LABELD),gtleavc,
     &                gtlvcis(UNLABL),csrsnk(UNLABL),
     &                gtlvcis(LABELD),csrsnk(LABELD),
     &                1.0,accum)

          closs = (1.0 - hi) * remf(1) * (1.0 - lv2std(1)) * gtleavc
          gtlvcrem = closs
          gtcrem = gtcrem + closs
          call csched(closs,gtlvcis(LABELD),gtleavc,
     &                gtlvcis(UNLABL),csrsnk(UNLABL),
     &                gtlvcis(LABELD),csrsnk(LABELD),
     &                1.0,accum)

          do 30 iel = 1, nelem
            egrain(iel) = efgrngt(iel) * remf(1) * (1.0-lv2std(1)) 
     &                    * gtleave(iel) * sqrt(hi/himaxgt)
            egracc(iel) = egracc(iel) + egrain(iel)
            call flow(gtleave(iel),esrsnk(iel),time,egrain(iel))
c           eloss(iel) = closs * (gtleave(iel) / gtleavc)
            eloss(iel) = (1.0-efgrngt(iel)) * remf(1) * (1.0-lv2std(1)) 
     &                   * gtleave(iel) * sqrt(hi/himaxgt)
            gtlverem(iel) = eloss(iel)
            gterem(iel) = gterem(iel) + eloss(iel)
            call flow(gtleave(iel),esrsnk(iel),time,eloss(iel))
30        continue
        endif

      else
c ..... Non-grain harvest
        if (gtleavc .gt. 0.0) then
          closs = remf(1) * (1.0 - lv2std(1)) * gtleavc
          gtlvcrem = closs
          gtcrem = gtcrem + closs
          call csched(closs,gtlvcis(LABELD),gtleavc,
     &                gtlvcis(UNLABL),csrsnk(UNLABL),
     &                gtlvcis(LABELD),csrsnk(LABELD),
     &                1.0,accum)

          do 40 iel = 1, nelem
            eloss(iel) = closs * (gtleave(iel) / gtleavc)
            gtlverem(iel) = eloss(iel)
            gterem(iel) = gterem(iel) + eloss(iel)
            call flow(gtleave(iel),esrsnk(iel),time,eloss(iel))
40        continue
        endif
      endif 

c ... Remove dead attached LEAVES

      if (gtdleavc .gt. 0.0) then
        closs = remf(6) * gtdleavc
        gtdlvcrem = closs
        gtcrem = gtcrem + closs
        call csched(closs,gtdlvcis(LABELD),gtdleavc,
     &              gtdlvcis(UNLABL),csrsnk(UNLABL),
     &              gtdlvcis(LABELD),csrsnk(LABELD),
     &              1.0,accum)

        do 45 iel = 1, nelem
          eloss(iel) = closs * (gtdleave(iel) / gtdleavc)
          gtdlverem(iel) = eloss(iel)
          gterem(iel) = gterem(iel) + eloss(iel)
          call flow(gtdleave(iel),esrsnk(iel),time,eloss(iel))
45      continue
      endif


c ... Remove live STEMS

      if (gtstemc .gt. 0.0) then
        closs = remf(2) * (1.0 - lv2std(2)) * gtstemc
        gtstmcrem = closs
        gtcrem = gtcrem + closs
c       write(*,*) 'Before removal: gtstmcis(UNLABL)=',gtstmcis(UNLABL)
c       write(*,*) 'Before removal: gtstmcis(LABELD)=',gtstmcis(LABELD)
c       write(*,*) 'Before removal: closs=',closs
        call csched(closs,gtstmcis(LABELD),gtstemc,
     &              gtstmcis(UNLABL),csrsnk(UNLABL),
     &              gtstmcis(LABELD),csrsnk(LABELD),
     &              1.0,accum)

        do 50 iel = 1, nelem
          eloss(iel) = closs * (gtsteme(iel) / gtstemc)
          gtstmerem(iel) = eloss(iel)
          gterem(iel) = gterem(iel) + eloss(iel)
          call flow(gtsteme(iel),esrsnk(iel),time,eloss(iel))
50      continue
      endif

c ... Remove dead standing STEMS

      if (gtdstemc .gt. 0.0) then
        closs = remf(7) * gtdstemc
        gtdstmcrem = closs
        gtcrem = gtcrem + closs
        call csched(closs,gtdstmcis(LABELD),gtdstemc,
     &              gtdstmcis(UNLABL),csrsnk(UNLABL),
     &              gtdstmcis(LABELD),csrsnk(LABELD),
     &              1.0,accum)

        do 55 iel = 1, nelem
          eloss(iel) = closs * (gtdsteme(iel) / gtdstemc)
          gtdstmerem(iel) = eloss(iel)
          gterem(iel) = gterem(iel) + eloss(iel)
          call flow(gtdsteme(iel),esrsnk(iel),time,eloss(iel))
55      continue
      endif

c ... NOTE: Removal of carbohydrate storage and gtstg occurs
c ... in subroutine gtkillroot.

c ... Transfer above ground live grasstree parts to their 
c ... standing dead counterparts when grasstrees die during 
c ... a TREM fire or cutting event. -mdh 2/20/2019

c ... Live leaves are transferred to dead attached leaves
      if (gtleavc .gt. 0.001) then
        closs = remf(1) * lv2std(1) * gtleavc
        gtlv2dlvc = closs
        call csched(closs,gtlvcis(LABELD),gtleavc,
     &              gtlvcis(UNLABL),gtdlvcis(UNLABL),
     &              gtlvcis(LABELD),gtdlvcis(LABELD),
     &              1.0,accum)

        do 60 iel = 1, nelem
          eloss(iel) = closs * (gtleave(iel) / gtleavc)
          gtlv2dlve(iel) = eloss(iel)
          call flow(gtleave(iel),gtdleave(iel),time,eloss(iel))
60      continue
      endif

c ... Live stems are transferred to standing dead stems
      if (gtstemc .gt. 0.001) then
        closs = remf(2) * lv2std(2) * gtstemc
        gtstm2dstmc = closs
        call csched(closs,gtstmcis(LABELD),gtstemc,
     &              gtstmcis(UNLABL),gtdstmcis(UNLABL),
     &              gtstmcis(LABELD),gtdstmcis(LABELD),
     &              1.0,accum)

        do 65 iel = 1, nelem
          eloss(iel) = closs * (gtsteme(iel) / gtstemc)
          gtstm2dstme(iel) = eloss(iel)
          call flow(gtsteme(iel),gtdsteme(iel),time,eloss(iel))
65      continue
      endif

c ... gtfertot(iel) are simulation accumulators updated in dailymoist.
c ... gtomadtot and gtomaetot(iel) are simulation accumulators
c ...   updated in addomad.
c ... gtirrtot is a simulation accumulator updated in irrigt.
      do 70 iel = 1, nelem
        fertapp(iel) = gtfertot(iel) - sfertot(iel)
        omaeapp(iel) = gtomaetot(iel) - somaetot(iel)
        sfertot(iel) = gtfertot(iel)
        somaetot(iel) = gtomaetot(iel)
70    continue
      irrapp = gtirrtot - sirrtot
      omadapp = gtomadtot - somadtot
      sirrtot = gtirrtot
      somadtot = gtomadtot

      return
      end
