
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      subroutine inprac(system)

      implicit none
      include 'const.inc'
      include 'dovars.inc'
      include 'parcp.inc'
      include 'parfs.inc'
      include 'pargt.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot3.inc'
      include 'plot4.inc'

c ... Argument declarations
      integer system

c ... Initialize annual production accumulators.
c ... Called from initialize() and simsom()
c...............................................................................
c    MODIFICATIONS FOR GRASSTREE (-mdh, January 2019):
c    Added section for GRASSTREESYS accumulators
c...............................................................................

c ... Local variables
      integer ii, jj

c ... In the CROP system or SAVANNA, if it is the last month
c ... of the growing season reset the accumulators for the grasses.
      if (dolast .and. crpgrw .eq. 0 .and. system .eq. CRPSYS) then
c ..... Aboveground carbon production
        agcisa(UNLABL) = 0.0
        agcisa(LABELD) = 0.0
        agcprd = agcacc
        agcacc = 0.0
        ptagc = 0.0
c ..... Belowground carbon production
        bgcisja(UNLABL) = 0.0
        bgcisja(LABELD) = 0.0
        bgcjprd = bgcjacc
        bgcjacc = 0.0
        bgcisma(UNLABL) = 0.0
        bgcisma(LABELD) = 0.0
        bgcmprd = bgcmacc
        bgcmacc = 0.0
        ptbgc = 0.0
        do 10 ii = 1, MAXIEL
c ....... N, P, and S uptake by plants
          eupaga(ii) = 0.0
          eupbga(ii) = 0.0
10      continue
      endif

      if ((dolast .and. crpgrw .eq. 0 .and. system .eq. CRPSYS)
     &  .or. (dogtlast .and. grasstreegrw .eq. 0 .and. 
     &        system .eq. GRASSTREESYS)) then
c ..... Add code to reset the growing season accumulators for fertilizer
c ..... and organic matter addition and N2O flux, cak - 06/06/2008
c ..... Fertilizer addition
        do 20 ii = 1, MAXIEL
          fertprd(ii) = fertac(ii)
          fertac(ii) = 0.0
          do 30 jj = 1, MONTHS
            fertmth(jj,ii) = 0.0
30        continue
20      continue
c ..... Organic matter addition
        omadprd = omadac
        omadac = 0.0
        do 40 ii = 1, MAXIEL
          omadpre(ii) = omadae(ii)
          omadae(ii) = 0.0
          do 50 jj = 1, MONTHS
            omadmth(jj) = 0.0
            omadmte(jj,ii) = 0.0
50        continue
40      continue
c ..... N2O flux
        n2oprd = n2oacc
        n2oacc = 0.0
        do 60 ii = 1, MONTHS
          n2omth(ii) = 0.0
60      continue
      endif

c ... In the FOREST system or SAVANNA, if it is the last month
c ... of the growing season reset the accumulators for the trees.
      if (doflst .and. forgrw .eq. 0 .and. system .eq. FORSYS) then
c ..... Total forest carbon
        fcprd = fcacc
        fcacc = 0
c ..... Leaf carbon production
        alvcis(UNLABL) = 0.0
        alvcis(LABELD) = 0.0
        rlvprd = rlvacc
        rlvacc = 0.0
c ..... Fine root carbon production
        afrcisj(UNLABL) = 0.0
        afrcisj(LABELD) = 0.0
        frtjprd = frtjacc
        frtjacc = 0.0
        afrcism(UNLABL) = 0.0
        afrcism(LABELD) = 0.0
        frtmprd = frtmacc
        frtmacc = 0.0
c ..... Fine branch carbon production
        afbcis(UNLABL) = 0.0
        afbcis(LABELD) = 0.0
        fbrprd = fbracc
        fbracc = 0.0
c ..... Large wood carbon production
        alwcis(UNLABL) = 0.0
        alwcis(LABELD) = 0.0
        rlwprd = rlwacc
        rlwacc = 0.0
c ..... Coarse root carbon production
        acrcis(UNLABL) = 0.0
        acrcis(LABELD) = 0.0
        crtprd = crtacc
        crtacc = 0.0
c ..... N, P, and S uptake by plants
        do 70 ii = 1, MAXIEL
          do 80 jj = 1, FPARTS-1
            eupprt(jj,ii) = 0.0
80        continue
70      continue
      endif

c................................................................................
c ... In the GRASSTREE system, if it is the last month of the 
c ... growing season reset the accumulators for the grasses.

      if (dogtlast .and. grasstreegrw .eq. 0 .and. 
     &    system .eq. GRASSTREESYS) then

c...... LIVE LEAVES
c...... agtlvcis(2) = annual accumulator
c...... gtlvacc = annual accumulator
c...... gtlvprd = gtlvacc in the last month of the growing season

        agtlvcis(UNLABL) = 0.0
        agtlvcis(LABELD) = 0.0
        gtlvprd = gtlvacc
        gtlvacc = 0.0

c...... LIVE STEMS
c...... agtstmcis(2) = annual accumulator
c...... gtstemacc = annual accumulator
c...... gtstemprd = gtstemacc in the last month of the growing season

        agtstmcis(UNLABL) = 0.0
        agtstmcis(LABELD) = 0.0
        gtstemprd = gtstemacc
        gtstemacc = 0.0

c...... LIVE JUVENILE AND MATURE FINE ROOTS
c...... agtfrcisj(2) = annual accumulator
c...... agtfrcism(2) = annual accumulator
c...... gtfrtjacc = annual accumulator
c...... gtfrtmacc = annual accumulator
c...... gtfrtjprd = gtfrtjacc in the last month of the growing season
c...... gtfrtmprd = gtfrtmacc in the last month of the growing season

        agtfrcisj(UNLABL) = 0.0
        agtfrcisj(LABELD) = 0.0
        agtfrcism(UNLABL) = 0.0
        agtfrcism(LABELD) = 0.0
        gtfrtjprd = gtfrtjacc
        gtfrtmprd = gtfrtmacc
        gtfrtjacc = 0.0
        gtfrtmacc = 0.0

c...... LIVE COARSE ROOTS
c...... agtcrtcis(2) = annual accumulator
c...... gtcrtacc = annual accumulator
c...... gtcrtprd = gtcrtacc in the last month of the growing season

        agtcrtcis(UNLABL) = 0.0
        agtcrtcis(LABELD) = 0.0
        gtcrtprd = gtcrtacc
        gtcrtacc = 0.0

        gtcprd = gtcacc
        gtcacc = 0.0
        potgtacc = 0.0

c...... eupgtprt(5,3) = annual accumulator of E uptake by plant part
        do 90 ii = 1, MAXIEL
          do 95 jj = 1, GTLIVPARTS
c ......... N, P, and S uptake by plants
            eupgtprt(jj,ii) = 0.0
95        continue
90      continue
      endif
c................................................................................

c ... N, P, and S uptake by plants, reset if no growth is occurring
      if (crpgrw .eq. 0 .and. forgrw .eq. 0 
     &    .and. grasstreegrw .eq. 0) then
        do 100 ii = 1, MAXIEL
          eupprd(ii) = eupacc(ii)
          eupacc(ii) = 0.0
100     continue
      endif

      return
      end
