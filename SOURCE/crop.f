
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      subroutine crop(time, bgwfunc, tfrac, tavedly, curday, avgstemp,
     &                crpGrossPsn)

      implicit none
      include 'const.inc'
      include 'dovars.inc'
      include 'fertil.inc'
      include 'ligvar.inc'
      include 'param.inc'
      include 'parcp.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'timvar.inc'

c ... Argument declarations
      real             time
      real             bgwfunc
      real             tfrac
      real             tavedly
      real             avgstemp
      double precision crpGrossPsn
      integer          curday

c ... Driver for calling all of crop code.

c ... Fortran to C prototype
      INTERFACE

        SUBROUTINE flow(from, to, when, howmuch)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          REAL from
          REAL to
          REAL when
          REAL howmuch
        END SUBROUTINE flow

        SUBROUTINE flowup(time)
          !MS$ATTRIBUTES ALIAS:'_flowup' :: flowup
          REAL time
        END SUBROUTINE flowup

        SUBROUTINE flowup_double(time)
          !MS$ATTRIBUTES ALIAS:'_flowup_double' :: flowup_double
          REAL time
        END SUBROUTINE flowup_double

        SUBROUTINE flowup_double_in(time)
          !MS$ATTRIBUTES ALIAS:'_flowup_double_in' :: flowup_double_in
          REAL time
        END SUBROUTINE flowup_double_in

        SUBROUTINE flowup_double_out(time)
          !MS$ATTRIBUTES ALIAS:'_flowup_double_out' :: flowup_double_out
          REAL time
        END SUBROUTINE flowup_double_out

      END INTERFACE

c ... Local variables
      real    accum(ISOS)
      real    fraclabl, k2
      integer iel, ii

      accum(LABELD) = 0.0
      accum(UNLABL) = 0.0
      fraclabl = 0.0

c ... Organic matter addition
      if (doomad .and. (omadday .eq. curday)) then
        call addomad(time)
      endif

c ... If microcosm selected, skip the rest of the crop code
      if (micosm .eq. 1) then
        goto 999
      endif

c ... Update flows so direct absorption will be accounted for
c ... before plant uptake.
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar

c ... Grow (growth checks crpgrw and exactly what should be done)
      call growth(tfrac, tavedly, month, crpGrossPsn)

c ... Fall of standing dead
      call falstd(pltlig, tfrac)

c ... Death of roots
      call droot(pltlig, tfrac, avgstemp)

c ... Death of shoots
      call dshoot(bgwfunc, tfrac, curday)

c ... Cultivation
      if (docult .and. (cultday .eq. curday)) then
        call cultiv(pltlig)
      endif

c ... Update state variables and accumulators and sum carbon isotopes.
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar

999   continue

      return
      end
