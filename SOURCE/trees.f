
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      subroutine trees (bgwfunc, tavewk, tfrac, curday, tavedly,
     &                  avgstemp, forGrossPsn)

      implicit none
      include 'const.inc'
      include 'dovars.inc'
      include 'fertil.inc'
      include 'param.inc'
      include 'parcp.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'timvar.inc'
      include 'zztim.inc'

c ... Argument declarations
      real             bgwfunc, tavedly, tavewk, tfrac
      real             avgstemp
      double precision forGrossPsn
      integer          curday

c ... Simulate forest production for the month.

c ... Fortran to C prototype
      INTERFACE

        SUBROUTINE flow(from, to, when, howmuch)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          REAL from
          REAL to
          REAL when
          REAL howmuch
        END SUBROUTINE flow

        SUBROUTINE flowup(time)
          !MS$ATTRIBUTES ALIAS:'_flowup' :: flowup
          REAL time
        END SUBROUTINE flowup

        SUBROUTINE flowup_double(time)
          !MS$ATTRIBUTES ALIAS:'_flowup_double' :: flowup_double
          REAL time
        END SUBROUTINE flowup_double

        SUBROUTINE flowup_double_in(time)
          !MS$ATTRIBUTES ALIAS:'_flowup_double_in' :: flowup_double_in
          REAL time
        END SUBROUTINE flowup_double_in

        SUBROUTINE flowup_double_out(time)
          !MS$ATTRIBUTES ALIAS:'_flowup_double_out' :: flowup_double_out
          REAL time
        END SUBROUTINE flowup_double_out

      END INTERFACE

c ... Local variables
      real    accum(ISOS)
      real    fraclabl, k2
      integer iel, ii

      accum(LABELD) = 0.0
      accum(UNLABL) = 0.0
      fraclabl = 0.0

c ... Option to add organic matter
      if (doomad .and. (omadday .eq. curday)) then
        call addomad(time)
      endif

c ... Update flows so direct absorption will be accounted for
c ... before plant uptake
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar

      call treegrow(tfrac, tavedly, forGrossPsn)

c ... Death of tree parts
      call wdeath(tavewk, bgwfunc, tfrac, avgstemp)

c ... Fall of standing dead tree biomass. -mdh 10/6/2018
      call wfalstd(tfrac)

c ... Update state variables and accumulators and sum carbon isotopes.
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar

      return
      end
