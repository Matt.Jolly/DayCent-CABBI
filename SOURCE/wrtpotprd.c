
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      wrtpotprd.c
**
**  FUNCTION:  void wrtpotprd()
**
**  PURPOSE:   Write out the soil carbon values. 
**
**  AUTHOR:    Melannie Hartman
**             May 10, 2019
** 
**  INPUTS:
**    cursys    - 1=crop, 2=forest, 3=grasstree
**    curday    - the day of the year (1..366)
**    daylen    - daylength (hours)
**    langleys  - incoming short wave radiation (langleys/day) 
**    prdx      - potential production parameter from crop.100, tree.100, or grasstree.100
**    gdpf      - temperature effect on potential production (0.0-1.0)
**    h2ogef    - soil moisture effect on potential production (0.0-1.0)
**    wstress   - water stress term used in h2ogef calculation (units??)
**    fldeff    - flood effect on potential production (0.0-1.0)
**    biof      - effect of the ratio of live biomass to dead biomass 
**                on the reduction of potential growth rate
**    shdmod    - shading modifier on crop/grass for savanna (0.0-1.0)
**    laiprd    - reduction in production when crop/tree is young (0.0 - 1.0)
**                This is sdlng when called from potcrp.f.
**                This is the LAI effect on production (laiprd) when called 
**                from potgrasstree.f or potfor.f (0.0-1.0)
**    co2cpr    - CO2 effect on potential production (0.5 - 2.0, approximately)
**    time      - simulation time (years)
**    tprod     - total potential production (gC/m2/day)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files               - structure containing information about output files
**    files->fp_potcrp    - file pointer to potcrp.csv output file
**    files->fp_potfor    - file pointer to potfor.csv output file
**    files->fp_potgt     - file pointer to potgt.csv output file
**    files->write_potcrp - flag to indicate if potcrp.csv output file should
**                          be created, 0 = do not create, 1 = create
**    files->write_potfor - flag to indicate if potfor.csv output file should
**                          be created, 0 = do not create, 1 = create
**    files->write_potgt  - flag to indicate if potgt.csv output file should
**                          be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**     None
**
**  CALLED BY:
**     simsom()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include "soilwater.h"

    void wrtpotprd(int *cursys, float *time, int *curday, float *daylen, 
                   float *langleys, float *prdx, float *gdpf, float *h2ogef, 
                   float *wstress, float *fldeff, float *biof, float *shdmod, float *laiprd, 
                   float *co2cpr, float *tprod)
    {
      extern FILES_SPT files;

      if (*cursys == 1)
      {
          if (!files->write_potcrp) {
            goto ex;
          }
    
          fprintf(files->fp_potcrp, "%6.2f, %3d, %10.4f, %10.4f, %10.4f, %10.4f,",
                  *time, *curday, *daylen, *langleys, *prdx, *gdpf);
          fprintf(files->fp_potcrp, "%10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f\n", 
                  *h2ogef, *wstress, *fldeff, *biof, *shdmod, *laiprd, *co2cpr, *tprod);
      }

      if (*cursys == 2)
      {
          if (!files->write_potfor) {
            goto ex;
          }
    
          fprintf(files->fp_potfor, "%6.2f, %3d, %10.4f, %10.4f, %10.4f, %10.4f,",
                  *time, *curday, *daylen, *langleys, *prdx, *gdpf);
          fprintf(files->fp_potfor, "%10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f\n", 
                  *h2ogef, *wstress, *fldeff, *biof, *shdmod, *laiprd, *co2cpr, *tprod);
      }

      if (*cursys == 3)
      {
          if (!files->write_potgt) {
            goto ex;
          }
    
          /* Here, LAI is in the position of shdmod */
          fprintf(files->fp_potgt, "%6.2f, %3d, %10.4f, %10.4f, %10.4f, %10.4f,",
                  *time, *curday, *daylen, *langleys, *prdx, *gdpf);
          fprintf(files->fp_potgt, "%10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f, %10.4f\n", 
                  *h2ogef, *wstress, *fldeff, *biof, *shdmod, *laiprd, *co2cpr, *tprod);
      }

ex:   return;
    }
