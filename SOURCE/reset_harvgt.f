
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c
c*****************************************************************************
c
c  FILE:      reset_harvgt.f
c
c  FUNCTION:  subroutine reset_harvgt()
c
c  PURPOSE:   Reset harvest variables before a TREM event (harvest) for 
c             GrassTrees. 
c
c  AUTHOR:    Melannie Hartman September 4, 2019
c 
c  Called from:  grasstreerem
c
c*****************************************************************************

      subroutine reset_harvgt

      implicit none
      include 'const.inc'
      include 'harvgt.inc'

c ... Local variables
      integer iel

c ... Reset harvest output variables
c ... From gtlivsdrem
      gtlvcrem = 0.0
      gtdlvcrem = 0.0
      gtstmcrem = 0.0
      gtdstmcrem = 0.0
      gtlv2dlvc = 0.0
      gtstm2dstmc = 0.0
      irrapp = 0.0
      omadapp = 0.0
c ... From gtkillroot
      carbostgloss = 0.0
      srfclittrj = 0.0
      srfclittrm = 0.0
      srfclittrcrt = 0.0
      soillittrj = 0.0
      soillittrm = 0.0
      soillittrcrt = 0.0
c ... From gtcutrtn
      gtlvcret = 0.0
      gtdlvcret = 0.0
      gtstmcret = 0.0
      gtdstmcret = 0.0
      do 10 iel = 1, MAXIEL
c ..... From gtlivsdrem
        gtlverem(iel) = 0.0
        gtdlverem(iel) = 0.0
        gtstmerem(iel) = 0.0
        gtdstmerem(iel) = 0.0
        gtlv2dlve(iel) = 0.0
        gtstm2dstme(iel) = 0.0
        fertapp(iel) = 0.0
        omaeapp(iel) = 0.0
c ..... From gtkillroot
        gtstgloss(iel) = 0.0
        esrfclittrj(iel) = 0.0
        esrfclittrm(iel) = 0.0
        esrfclittrcrt(iel) = 0.0
        esoillittrj(iel) = 0.0
        esoillittrm(iel) = 0.0
        esoillittrcrt(iel) = 0.0
c ..... From gtcutrtn
        gtlveret(iel) = 0.0
        gtdlveret(iel) = 0.0
        gtstmeret(iel) = 0.0
        gtdstmeret(iel) = 0.0
10    continue

      return
      end
