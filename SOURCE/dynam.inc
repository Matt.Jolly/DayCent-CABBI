
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c ..............................................................................
c ... MODIFICATIONS FOR GRASSTREE
c ... Added grasstree_cfrac(5)
c ..............................................................................

      common/dynam/ tree_cfrac(5), grasstree_cfrac(5)
  
      real tree_cfrac, grasstree_cfrac

      save /dynam/
