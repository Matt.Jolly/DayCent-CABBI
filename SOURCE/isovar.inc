
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c...............................................................................
c... MODIFICATIONS FOR GRASSTREE
c... Added cisogt
c...............................................................................

c ... c14frc tells what fraction of the carbon in new plant tissue is labeled.

      common/isovar/cisofr, cisotf, cisogt
      real cisofr, cisotf, cisogt

      save/isovar/
