c
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      drootgt.f
c
c  FUNCTION:  subroutine drootgt()
c
c  PURPOSE:   Compute death of roots (fine and coarse) for grasstree
c             when soil is dry. 
c
c  AUTHOR:    New function derived from droot.
c             Melannie Hartman Jan. 24, 2014
c
c  FUNCTION ARGUMENTS:
c    gtlig(*) 	- lignin fraction for each live grasstree part
c    tfrac	- time fraction (1/# days in current month)  
c    avgstemp	- weighted average soil temperature from the 2nd and 3rd
c
c  VARIABLES:
c    frtdsrfc - Fraction of the fine roots that are transferred into the 
c		surface litter layer (STRUCC(1) and METABC(1)) upon fine 
c		root death, the remainder of the roots will go to the soil 
c		litter layer (STRUCC(2) and METABC(2)). See grasstree.100.
c    crtdsrfc - Fraction of the fine roots that are transferred into the 
c		surface litter layer (STRUCC(1) and METABC(1)) upon fine 
c		root death, the remainder of the roots will go to the soil 
c		litter layer (STRUCC(2) and METABC(2)). See grasstree.100.
c    rootdr(1)- maximum fraction of juvenile fine roots that die per month 
c               under drought or temperature stress. See grasstree.100.
c    rootdr(2)- maximum fraction of mature fine roots that die per month 
c               under drought or temperature stress. See grasstree.100.
c    rootdr(3)- maximum fraction of coarse roots that die per month 
c               under drought or temperature stress. See grasstree.100.
c
c  LOCAL VARIABLES:
c    rdfrac   - The fraction of live fine or coarse roots that die.
c               This is a combined effect of temperature and moisture 
c               stress and the rootdr(*) parameters from grasstree.100. 
c    rdeath   - Amount of fine or coarse root biomass that dies (gC/m2)
c 
c***********************************************************************

      subroutine drootgt(gtlig, tfrac, avgstemp)

      implicit none
      include 'const.inc'
      include 'param.inc'
c     include 'parcp.inc'
      include 'pargt.inc'
      include 'parfx.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot4.inc'
      include 'zztim.inc'

c ... Argument declarations
      real gtlig(GTLIVPARTS)
      real tfrac
      real avgstemp

c ... Simulate death of roots

c ... Function declarations
      real      carctanf, gpdf, maxswpot
      external  carctanf, gpdf, maxswpot

c ... Local variables
      integer iel
      real fr14, recres(MAXIEL), rdeath, rtdh, rdfrac
      real srfclittr, soillittr, tempeff, watreff
      real accum(ISOS), cturn, eturn, temp
      real eloss(MAXIEL), frc14
      real liveCtotal, liveCremoved, liveCfrac
      real carboLoss, carboStorage

c ... Death of roots

c ... Add code to age fine roots, juvenile fine roots age to the mature
c ... fine root pool.  Modify this subroutine so that the death rate of
c ... roots is a function of soil water potential and soil temperature,
c ... cak - 06/28/2007
c ... See:  A Model of Production and Turnover of Roots in Shortgrass Prairie
c ...       Parton, Singh, and Coleman, 1978
c ...       Journal of Applied Ecology

c ... Cap the temperature effect on fine roots at -2 and +28 degrees C
      if (avgstemp .gt. 28.0) then
        temp = 28.0
      else if (avgstemp .lt. -2.0) then
        temp = -2.0
      else
        temp = avgstemp
      endif

c ... Upon coarse root death transfer carbon from the
c ... carbohydrate storage pool to the C source/sink
      liveCtotal = gtcrootc
      liveCremoved = 0.0
      liveCfrac = 0.0
      accum(LABELD) = 0.0
      accum(UNLABL) = 0.0

c ... Soil temperature effect on aging of juvenile roots
c ... Juvenile fine roots --> mature fine roots

      if (gtfrootcj .gt. 0.0) then
        tempeff = gpdf(temp, 37.0, 0.0, 3.0, 3.0)
        cturn = gtmxturn * tempeff * gtfrootcj * tfrac
        fr14 = gtfrtcisj(LABELD) / gtfrootcj
        if (isnan(cturn)) then
          write(*,*) 'Error in drootgt, cturn is NaN'
          STOP
        endif
        call csched(cturn, fr14, 1.0,
     &              gtfrtcisj(UNLABL), gtfrtcism(UNLABL),
     &              gtfrtcisj(LABELD), gtfrtcism(LABELD),
     &              1.0, accum)
        do 60 iel = 1, nelem
          eturn = cturn * (gtfrootej(iel) / gtfrootcj)
        if (isnan(eturn)) then
          write(*,*) 'Error in drootgt, eturn is NaN'
          STOP
        endif
          call flow(gtfrootej(iel), gtfrootem(iel), time, eturn)
60      continue
      endif

c ... Soil temperature effect on root death rate
      tempeff = (temp - 10.0)**2 / 4.0 * 0.00175 + 0.1
      tempeff = min(tempeff, 0.5)
c ... Soil water potential effect on root death rate
c ...   Note: maxswpot returns a maximum value of 35, so carctanf <= 0.5.
c ...   swpot is 0.0 at saturation and increases with dryness. -mdh 9/6/2019
c ... claypg was being used instead of gtlaypg! -mdh 9/6/2019
c     watreff = maxswpot(claypg)
      watreff = maxswpot(gtlaypg)
      watreff = carctanf(watreff, 35.0, 0.5, 1.0, 0.05)
c ... Root death is driven by the maximum of the soil temperature
c ... effect and the soil water potential effect on root death rate,
c ... cak - 06/28/2007
      rtdh = max(tempeff, watreff)
      rtdh = max(rtdh, 0.0)

c ... Death of juvenile fine roots
      if (gtfrootcj .gt. 0.0) then
        rdfrac = rootdr(1) * tfrac * rtdh
        if (rdfrac .gt. 0.95) then
          rdfrac = 0.95
        endif
        rdeath = rdfrac * gtfrootcj
        do 10 iel = 1, nelem
          recres(iel) = gtfrootej(iel)/gtfrootcj
10      continue
        fr14 = gtfrtcisj(LABELD)/gtfrootcj
c ..... A fraction of the dead roots are transferred to the surface
c ..... litter layer, the remainder goes to the soil litter layer
c ..... cak - 05/14/2007
        srfclittr = rdeath * frtdsrfc
        soillittr = rdeath * (1.0 - frtdsrfc)
        call partit(srfclittr,recres,SRFC,gtfrtcisj,gtfrootej,
     &              gtlig(GTFROOTJ),fr14)
        call partit(soillittr,recres,SOIL,gtfrtcisj,gtfrootej,
     &              gtlig(GTFROOTJ),fr14)
      endif

c ... Death of mature fine roots
      if (gtfrootcm .gt. 0.0) then
        rdfrac = rootdr(2) * tfrac * rtdh
        if (rdfrac .gt. 0.95) then
          rdfrac = 0.95
        endif
        rdeath = rdfrac * gtfrootcm
        do 20 iel = 1, nelem
          recres(iel) = gtfrootem(iel)/gtfrootcm
20      continue
        fr14 = gtfrtcism(LABELD)/gtfrootcm
c ..... A fraction of the dead roots are transferred to the surface
c ..... litter layer, the remainder goes to the soil litter layer
c ..... cak - 05/14/2007
        srfclittr = rdeath * frtdsrfc
        soillittr = rdeath * (1.0 - frtdsrfc)
        call partit(srfclittr,recres,SRFC,gtfrtcism,gtfrootem,
     &              gtlig(GTFROOTM),fr14)
        call partit(soillittr,recres,SOIL,gtfrtcism,gtfrootem,
     &              gtlig(GTFROOTM),fr14)
      endif


c ... Death of COARSE ROOTS
c ... Bill Parton said if coarse roots are like rhizomes, then they will
c ... get incorporated into the SURFACE litter when they die. In this
c ... case, the parameter crtdsrfc (fraction of the coarse roots that 
c ... are transferred into the surface litter layer) should be set to 
c ... 1.0 in grasstree.100.  -mdh 2/19/2014

      if (gtcrootc .gt. 0.001) then
        rdfrac = rootdr(3) * tfrac * rtdh
        if (rdfrac .gt. 0.95) then
          rdfrac = 0.95
        endif
        rdeath = rdfrac * gtcrootc
        liveCremoved = rdeath
        do 30 iel = 1, nelem
          recres(iel) = gtcroote(iel)/gtcrootc
30      continue
        frc14 = gtcrtcis(LABELD) / gtcrootc
        srfclittr = rdeath * crtdsrfc
        soillittr = rdeath * (1.0 - crtdsrfc)
        call partit(srfclittr,recres,SRFC,gtcrtcis,gtcroote,
     &              gtlig(GTCROOT),frc14)
        call partit(soillittr,recres,SOIL,gtcrtcis,gtcroote,
     &              gtlig(GTCROOT),frc14)
      endif

c ... Remove C from carbohydrate storage pool based on fraction 
c ... of coarse that died, transfer carbon from the carbohydrate 
c ... storage pool to the C source sink. 
      if (liveCtotal > 0.001) then
        liveCfrac = liveCremoved / liveCtotal
        carboStorage = carbostg(GTARYINDX,UNLABL) +
     &                 carbostg(GTARYINDX,LABELD)
        carboLoss = max(0.0, liveCfrac * carboStorage)
        if (carboLoss .gt. 0.0) then
          call csched(carboLoss,carbostg(GTARYINDX,LABELD),carboStorage,
     &                carbostg(GTARYINDX,UNLABL), csrsnk(UNLABL),
     &                carbostg(GTARYINDX,LABELD), csrsnk(LABELD),
     &                1.0, accum)
        endif
      endif

c ... Remove from elemental STORAGE pool based on fraction of 
c ... coarse roots that die.

      do 40 iel = 1, nelem
        eloss(iel) = MAX(liveCfrac * gtstg(iel), 0.0)
        if (isnan(eloss(iel))) then
          write(*,*) 'Error in drootgt, eloss(iel) is NaN'
          STOP
        endif
        call flow(gtstg(iel),esrsnk(iel),time,eloss(iel))
40    continue

      return
      end
