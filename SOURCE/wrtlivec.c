
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      wrtlivec.c
**
**  FUNCTION:  void wrtlivec()
**
**  PURPOSE:   Write out the live carbon values. 
**
**  AUTHOR:    Cindy Keough  10/02
** 
**  INPUTS:
**    aglivc  - crop/grass above ground live carbon (gC/m2)
**    bglivcj - crop/grass below ground juvenile fine root live carbon (gC/m2)
**    bglivcm - corp/grass below ground mature fine root live carbon (gC/m2)
**    crootc  - forest coarse root live carbon (gC/m2)
**    curday  - the day of the year (1..366)
**    fbrchc  - fine branch live carbon (gC/m2)
**    frootcj - forest juvenile fine root live carbon (gC/m2)
**    frootcm - forest mature fine root live carbon (gC/m2)
**    gtcrootc  - grasstree live coarse root carbon (gC/m2)
**    gtfrootcj - grasstree live junvenile fine root carbon (gC/m2)
**    gtfrootcm - grasstree live mature fine root carbon (gC/m2)
**    gtleavc - grasstree live leaf carbon (gC/m2)
**    gtstemc - grasstree live stem carbon (gC/m2)
**    rleavc  - forest leaf live carbon (gC/m2)
**    rlwodc  - forest large wood live carbon (gC/m2)
**    time      - simulation time (years)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files              - structure containing information about output files
**    files->fp_livec    - file pointer to livec.out output file
**    files->write_livec - flag to indicate if livec.out output file should
**                         be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**     None
**
**  CALLED BY:
**     simsom()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include "soilwater.h"

    void wrtlivec(float *time, int *curday, float *aglivc, float *bglivcj,
                  float *bglivcm, float *rleavc, float *frootcj,
                  float *frootcm, float *fbrchc, float *rlwodc, float *crootc,
                  float *gtleavc, float *gtstemc, float *gtfrootcj, float *gtfrootcm, float *gtcrootc)
    {
      extern FILES_SPT files;

      if (!files->write_livec) {
        goto ex;
      }

      fprintf(files->fp_livec, "%6.2f,%2d,%10.4f,%10.4f,%10.4f,%10.4f,",
              *time, *curday, *aglivc, *bglivcj, *bglivcm, *rleavc);
      fprintf(files->fp_livec, "%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,", 
              *frootcj, *frootcm, *fbrchc, *rlwodc, *crootc);
      fprintf(files->fp_livec, "%10.4f,%10.4f,%10.4f,%10.4f,%10.4f\n", 
              *gtleavc, *gtstemc, *gtfrootcj, *gtfrootcm, *gtcrootc);

ex:   return;
    }
