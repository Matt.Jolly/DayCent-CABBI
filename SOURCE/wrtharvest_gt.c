/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      wrtharvest_gt.c
**
**  FUNCTION:  void wrtharvestgt()
**
**  PURPOSE:   Write out the state of the system at a harvest event. 
**
**  AUTHOR:    Cindy Keough  08/10 - write to harvest.csv for CROP types
**             Melannie Hartman 09/2019 
**               * updated to write to harvest_gt.csv for GRAS types
** 
**  INPUTS:
**
**    carbostgloss - amount of carbohydrate storage lost when coarse roots
**                   are killed (gC/m2)
**    cgracc       - annual accumulator for carbon in harvested grain (gC/m2)
**    cgrain       - amount of carbon in harvested grain (gC/m2)
**    crpval       - numerical representation of the current crop
**    dayofyr      - current simulation day of the year (1..366)
**    egracc1      - annual accumulator of nitrogen in harvested grain and
**                   tubers (gN/m2)
**    egracc2      - annual accumulator of phosphorus in harvested grain and
**                   tubers (gP/m2)
**    egracc3      - annual accumulator of sulfur in harvested grain and
**                   tubers (gS/m2)
**    egrain1      - amount of nitrogen in harvested grain and tubers (gN/m2)
**    egrain2      - amount of phosphorus in harvested grain and tubers
**                   (gP/m2)
**    egrain3      - amount of sulfur in harvested grain and tubers (gS/m2)
**    esoillittrcrt1 - amount of dead coarse root nitrogen transferred to
**                   soil litter pool (metabe(2,1) and struce(2,1)) due to a
**                   harvest event (gN/m2)
**    esoillittrj1 - amount of dead juvenile fine root nitrogen transferred to
**                   soil litter pool (metabe(2,1) and struce(2,1)) due to a
**                   harvest event (gN/m2)
**    esoillittrj2 - amount of dead juvenile fine root phosphorus transferred
**                   to soil litter pool (metabe(2,2) and struce(2,2)) due to
**                   a harvest event (gP/m2)
**    esoillittrj3 - amount of dead juvenile fine root sulfur transferred to
**                   soil litter pool (metabe(2,3) and struce(2,3)) due to a
**                   harvest event (gS/m2)
**    esoillittrm1 - amount of dead mature fine root nitrogen transferred to
**                   soil litter pool (metabe(2,1) and struce(2,1)) due to a
**                   harvest event (gN/m2)
**    esoillittrm2 - amount of dead mature fine root phosphorus transferred to
**                   soil litter pool (metabe(2,2) and struce(2,2)) due to a
**                   harvest event (gP/m2)
**    esoillittrm3 - amount of dead mature fine root sulfur transferred to
**                   soil litter pool (metabe(2,3) and struce(2,3)) due to a
**                   harvest event (gS/m2)
**    esrfclittrcrt1 - amount of dead coarse root nitrogen transferred to
**                   surface litter pool (metabe(1,1) and struce(1,1)) due to
**                   a harvest event (gN/m2)
**    esrfclittrj1 - amount of dead juvenile fine root nitrogen transferred to
**                   surface litter pool (metabe(1,1) and struce(1,1)) due to
**                   a harvest event (gN/m2)
**    esrfclittrj2 - amount of dead juvenile fine root phosphorus transferred
**                   to surface litter pool (metabe(1,2) and struce(1,2)) due
**                   to a harvest event (gP/m2)
**    esrfclittrj3 - amount of dead juvenile fine root sulfur transferred to
**                   surface litter pool (metabe(1,3) and struce(1,3)) due to
**                   a harvest event (gS/m2)
**    esrfclittrm1 - amount of dead mature fine root nitrogen transferred to
**                   surface litter pool (metabe(1,1) and struce(1,1)) due to
**                   a harvest event (gN/m2)
**    esrfclittrm2 - amount of dead mature fine root phosphorus transferred to
**                   surface litter pool (metabe(1,2) and struce(1,2)) due to
**                   a harvest event (gP/m2)
**    esrfclittrm3 - amount of dead mature fine root sulfur transferred to
**                   surface litter pool (metabe(1,3) and struce(1,3)) due to
**                   a harvest event (gS/m2)
**    fertapp1     - amount of nitrogen fertilizer applied since pervious HARV
**                   event (gN/m2)
**    fertapp2     - amount of phosphorus fertilizer applied since pervious
**                   HARV event (gP/m2)
**    fertapp3     - amount of sulfur fertilizer applied since pervious HARV
**                   event (gS/m2)
**    gtlvacc      - growing season leaf production to date (gC/m2)
**    gtstemacc    - growing season stem production to date (gC/m2)
**    gtfrtjacc    - growing season juvenile fine root production to date (gC/m2)
**    gtfrtmacc    - growing season mature fine root production to date (gC/m2)
**    gtcrtacc     - growing season coarse root production to date (gC/m2)
**    potgtacc     - growing season potential production to date (gC/m2)
**    gtlvcrem     - amount of live leaf C removed during harvest event (gC/m2)
**    gtlverem1    - amount of live leaf N removed during harvest event (gN/m2)
**    gtlverem2    - amount of live leaf P removed during harvest event (gP/m2)
**    gtlverem3    - amount of live leaf S removed during harvest event (gS/m2)
**    gtdlvcrem    - amount of dead leaf C removed during harvest event (gC/m2)
**    gtdlverem1   - amount of dead leaf N removed during harvest event (gN/m2)
**    gtdlverem2   - amount of dead leaf P removed during harvest event (gP/m2)
**    gtdlverem3   - amount of dead leaf S removed during harvest event (gS/m2)
**    gtstmcrem    - amount of live stem C removed during harvest event (gC/m2)
**    gtstmerem1   - amount of live stem N removed during harvest event (gN/m2)
**    gtstmerem2   - amount of live stem P removed during harvest event (gP/m2)
**    gtstmerem3   - amount of live stem S removed during harvest event (gS/m2)
**    gtdstmcrem   - amount of dead stem C removed during harvest event (gC/m2)
**    gtdstmerem1  - amount of dead stem N removed during harvest event (gN/m2)
**    gtdstmerem2  - amount of dead stem P removed during harvest event (gP/m2)
**    gtdstmerem3  - amount of dead stem S removed during harvest event (gS/m2)
**    gtlv2dlvc    - amount of live leaf C transferred to dead leaf C during harvest (gC/m2)
**    gtlv2dlve1   - amount of live leaf N transferred to dead leaf N during harvest (gN/m2)
**    gtlv2dlve2   - amount of live leaf P transferred to dead leaf P during harvest (gP/m2)
**    gtlv2dlve3   - amount of live leaf S transferred to dead leaf S during harvest (gS/m2)
**    gtstm2dstmc  - amount of live stem C transferred to dead stem C during harvest (gC/m2)
**    gtstm2dstme1 - amount of live stem N transferred to dead stem N during harvest (gN/m2)
**    gtstm2dstme2 - amount of live stem P transferred to dead stem P during harvest (gP/m2)
**    gtstm2dstme3 - amount of live stem S transferred to dead stem S during harvest (gS/m2)
**    gtlvcret     - amount of live leaf C removed that is returned to surface litter (gC/m2)
**    gtlveret1    - amount of live leaf N removed that is returned to surface litter (gN/m2)
**    gtlveret2    - amount of live leaf P removed that is returned to surface litter (gP/m2)
**    gtlveret3    - amount of live leaf S removed that is returned to surface litter (gN/m2)
**    gtdlvcret    - amount of dead leaf C removed that is returned to surface litter (gC/m2)
**    gtdlveret1   - amount of dead leaf N removed that is returned to surface litter (gN/m2)
**    gtdlveret2   - amount of dead leaf P removed that is returned to surface litter (gP/m2)
**    gtdlveret3   - amount of dead leaf S removed that is returned to surface litter (gS/m2)
**    gtstmcret    - amount of live stem C removed that is returned to surface litter (gC/m2)
**    gtstmeret1   - amount of live stem N removed that is returned to surface litter (gN/m2)
**    gtstmeret2   - amount of live stem P removed that is returned to surface litter (gP/m2)
**    gtstmeret3   - amount of live stem S removed that is returned to surface litter (gS/m2)
**    gtdstmcret   - amount of dead stem C removed that is returned to surface litter (gC/m2)
**    gtdstmeret1  - amount of dead stem N removed that is returned to surface litter (gN/m2)
**    gtdstmeret2  - amount of dead stem P removed that is returned to surface litter (gP/m2)
**    gtdstmeret3  - amount of dead stem S removed that is returned to surface litter (gS/m2)
**    gtstgloss1   - amount of internal N storage lost when coarse roots
**                   are killed (gN/m2)
**    gtstgloss2   - amount of internal P storage lost when coarse roots
**                   are killed (gP/m2)
**    gtstgloss3   - amount of internal S storage lost when coarse roots
**                   are killed (gS/m2)
**    hi           - harvest index (fraction of gtleavc going to grain)
**    irrapp       - amount of irrigation applied since pervious HARV event
**                   (cm H2O)
**    omadapp      - amount of carbon added to the system through organic
**                   matter addition events since the previous HARV event
**                   (gC/m2)
**    omaeapp1     - amount of nitrogen added to the system through organic
**                   matter addition events since the previous HARV event
**                   (gN/m2)
**    omaeapp2     - amount of for phosphorus added to the system through
**                   organic matter addition events since the previous HARV
**                   event (gP/m2)
**    omaeapp3     - amount of for sulfur added to the system through organic
**                   matter addition events since the previous HARV event
**                   (gS/m2)
**    soillittrcrt - amount of dead coarse root carbon transferred to
**                   soil litter pool (metabc(2) and strucc(2)) due to a
**                   harvest event (gC/m2)
**    soillittrj   - amount of dead juvenile fine root carbon transferred to
**                   soil litter pool (metabc(2) and strucc(2)) due to a
**                   harvest event (gC/m2)
**    soillittrm   - amount of dead mature fine root carbon transferred to
**                   soil litter pool (metabc(2) and strucc(2)) due to a
**                   harvest event (gC/m2)
**    srfclittrcrt - amount of dead coarse root carbon transferred to
**                   surface litter pool (metabc(1) and strucc(1)) due to a
**                   harvest event (gC/m2)
**    srfclittrj   - amount of dead juvenile fine root carbon transferred to
**                   surface litter pool (metabc(1) and strucc(1)) due to a
**                   harvest event (gC/m2)
**    srfclittrm   - amount of dead mature fine root carbon transferred to
**                   surface litter pool (metabc(1) and strucc(1)) due to a
**                   harvest event (gC/m2)
**    strmac1      - annual accumulator for cm H2O in stream flow
**    strmac2      - annual accumulator for nitrogen from mineral leaching of
**                   stream flow (gN/m2)
**    strmac3      - annual accumulator for phosphorus from mineral leaching
**                   of stream flow (gP/m2)
**    strmac4      - annual accumulator for sulfur from mineral leaching of
**                   stream flow (gS/m2)
**    strmac5      - annual accumulator for carbon from organic leaching of
**                   stream flow (gC/m2)
**    strmac6      - annual accumulator for nitrogen from organic leaching of
**                   stream flow (gN/m2)
**    strmac7      - annual accumulator for phosphorus from organic leaching
**                   of stream flow (gP/m2)
**    strmac8      - annual accumulator for sulfur from organic leaching of
**                   stream flow (gS/m2)
**    time         - simulation time (years)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files                - structure containing information about output
**                           files
**    files->fp_harvgt     - file pointer to harvest_gt.csv output file
**    files->write_harvgt - flag to indicate if harvest_gt.csv output file
**                           should be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**     None
**
**  CALLED BY:
**     grasstreerem()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include "soilwater.h"

    void wrtharvestgt(float *time, int *dayofyr, float *crpval, 
      float *gtlvacc, float *gtstemacc, float *gtfrtjacc,
      float *gtfrtmacc, float *gtcrtacc, float *potgtacc,
      float *cgrain, float *egrain1, float *egrain2, float *egrain3, float *hi,
      float *gtlvcrem, float *gtlverem1, float *gtlverem2, float *gtlverem3,
      float *gtdlvcrem, float *gtdlverem1, float *gtdlverem2, float *gtdlverem3,
      float *gtstmcrem, float *gtstmerem1, float *gtstmerem2, float *gtstmerem3,
      float *gtdstmcrem, float *gtdstmerem1, float *gtdstmerem2, float *gtdstmerem3,
      float *gtstm2dstmc, float *gtstm2dstme1, float *gtstm2dstme2, float *gtstm2dstme3,
      float *gtlv2dlvc, float *gtlv2dlve1, float *gtlv2dlve2, float *gtlv2dlve3,
      float *gtlvcret, float *gtlveret1, float *gtlveret2, float *gtlveret3,
      float *gtdlvcret, float *gtdlveret1, float *gtdlveret2, float *gtdlveret3,
      float *gtstmcret, float *gtstmeret1, float *gtstmeret2, float *gtstmeret3,
      float *gtdstmcret, float *gtdstmeret1, float *gtdstmeret2, float *gtdstmeret3,
      float *irrapp, float *fertapp1, float *fertapp2, float *fertapp3,
      float *omadapp, float *omaeapp1, float *omaeapp2, float *omaeapp3, 
      float *strmac1, float *strmac2, float *strmac3, float *strmac4, 
      float *strmac5, float *strmac6, float *strmac7, float *strmac8,
      float *carbostgloss, float *gtstgloss1, float *gtstgloss2, float *gtstgloss3,
      float *cgracc, float *egracc1, float *egracc2, float *egracc3, 
      float *srfclittrj, float *esrfclittrj1, 
      float *esrfclittrj2, float *esrfclittrj3, 
      float *soillittrj, float *esoillittrj1, 
      float *esoillittrj2, float *esoillittrj3, 
      float *srfclittrm, float *esrfclittrm1, 
      float *esrfclittrm2, float *esrfclittrm3, 
      float *soillittrm, float *esoillittrm1, 
      float *esoillittrm2, float *esoillittrm3,
      float *srfclittrcrt, float *esrfclittrcrt1, 
      float *esrfclittrcrt2, float *esrfclittrcrt3, 
      float *soillittrcrt, float *esoillittrcrt1, 
      float *esoillittrcrt2, float *esoillittrcrt3)
    {
      extern FILES_SPT files;

      if (!files->write_harvgt) {
        goto ex;
      }

      fprintf(files->fp_harvgt, "%.2f,%d,%.4f,", *time, *dayofyr, *crpval);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtlvacc, *gtstemacc, *gtfrtjacc, *gtfrtmacc);
      fprintf(files->fp_harvgt, "%.4f,%.4f,",
              *gtcrtacc, *potgtacc);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,%.4f,",
              *cgrain, *egrain1, *egrain2, *egrain3, *hi); 

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtlvcrem,*gtlverem1,*gtlverem2,*gtlverem3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtdlvcrem,*gtdlverem1,*gtdlverem2,*gtdlverem3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtstmcrem,*gtstmerem1,*gtstmerem2,*gtstmerem3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtdstmcrem,*gtdstmerem1,*gtdstmerem2,*gtdstmerem3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtlv2dlvc,*gtlv2dlve1,*gtlv2dlve2,*gtlv2dlve3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtstm2dstmc,*gtstm2dstme1,*gtstm2dstme2,*gtstm2dstme3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtlvcret,*gtlveret1,*gtlveret2,*gtlveret3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtdlvcret,*gtdlveret1,*gtdlveret2,*gtdlveret3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtstmcret,*gtstmeret1,*gtstmeret2,*gtstmeret3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *gtdstmcret,*gtdstmeret1,*gtdstmeret2,*gtdstmeret3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *irrapp, *fertapp1, *fertapp2, *fertapp3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *omadapp, *omaeapp1, *omaeapp2, *omaeapp3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *strmac1, *strmac2, *strmac3, *strmac4);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *strmac5, *strmac6, *strmac7, *strmac8);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *carbostgloss,*gtstgloss1,*gtstgloss2,*gtstgloss3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *cgracc, *egracc1, *egracc2, *egracc3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *srfclittrj, *esrfclittrj1, *esrfclittrj2, *esrfclittrj3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *soillittrj, *esoillittrj1, *esoillittrj2, *esoillittrj3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *srfclittrm, *esrfclittrm1, *esrfclittrm2, *esrfclittrm3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *soillittrm, *esoillittrm1, *esoillittrm2, *esoillittrm3);

      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f,",
              *srfclittrcrt, *esrfclittrcrt1, *esrfclittrcrt2, *esrfclittrcrt3);
      fprintf(files->fp_harvgt, "%.4f,%.4f,%.4f,%.4f\n",
              *soillittrcrt, *esoillittrcrt1, *esoillittrcrt2, *esoillittrcrt3);



ex:   return;
    }
