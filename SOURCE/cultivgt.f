
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      cultivgt.f
c
c  FUNCTION:  subroutine cultivgt()
c
c  PURPOSE:   Implement cultivation option for grasstree.
c
c  AUTHOR:    New function derived from cultiv (crop/grass).
c             Melannie Hartman Feb. 5, 2014
c   
c  Updates:
c    Melannie Hartman September 16, 2020
c    * The elemental storage pool, gtstg, was tied to both stems and coarse 
c      roots.  It should only be tied to coarse roots. I commented out the 
c      2 sections of code that tied it to stems. 
c
c  VARIABLES:
c    frtdsrfc - Fraction of the fine roots that are transferred into the 
c		surface litter layer (STRUCC(1) and METABC(1)) upon fine 
c		root death, the remainder of the roots will go to the soil 
c		litter layer (STRUCC(2) and METABC(2)). See grasstree.100.
c    crtdsrfc - Fraction of the fine roots that are transferred into the 
c		surface litter layer (STRUCC(1) and METABC(1)) upon fine 
c		root death, the remainder of the roots will go to the soil 
c		litter layer (STRUCC(2) and METABC(2)). See grasstree.100.
c
c*****************************************************************************

      subroutine cultivgt(gtlig)

      implicit none
      include 'const.inc'
      include 'param.inc'
      include 'parcp.inc'
      include 'pargt.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot4.inc'
      include 'zztim.inc'

c ... Argument declarations
      real      gtlig(GTLIVPARTS)

c ... Local variables
      integer   iel
      real      accum(ISOS), fr14, recres(MAXIEL), tagsfc, tagsoi,
     &          tbgsoi, trans, tsdsfc, tsdsoi,
     &          srfclittr, soillittr, 
     &          mRespStorage, mrspStgLoss, eloss(MAXIEL)

      accum(LABELD) = 0.0
      accum(UNLABL) = 0.0

c ... Update flows and sum carbon isotopes
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar


c ... Some standing dead stems and leaves go into surface litter

c ... STANDING DEAD LEAVES --> surface litter
      if (gtdleavc .gt. 0.0) then
        tsdsfc = gtdleavc * cultra(4)
        do 10 iel = 1, nelem
          recres(iel) = gtdleave(iel)/gtdleavc
10      continue
        fr14 = gtdlvcis(LABELD)/gtdleavc
        call partit(tsdsfc,recres,1,gtdlvcis,gtdleave,gtlig(GTLEAF),
     &              fr14)
      endif

c ... STANDING STEMS LEAVES --> surface litter
      if (gtdstemc .gt. 0.0) then
        tsdsfc = gtdstemc * cultra(4)
        do 15 iel = 1, nelem
          recres(iel) = gtdsteme(iel)/gtdstemc
15      continue
        fr14 = gtdstmcis(LABELD)/gtdstemc
        call partit(tsdsfc,recres,1,gtdstmcis,gtdsteme,gtlig(GTSTEM),
     &              fr14)
      endif


c ... Some surface litter goes into the top soil layer.

c ... Structural Litter
      trans = strucc(SRFC) * cultra(6)

      if (trans .gt. 0.0 .and. strucc(SRFC) .gt. 0.001) then
        call csched(trans,strcis(SRFC,LABELD),strucc(SRFC),
     &              strcis(SRFC,UNLABL),strcis(SOIL,UNLABL),
     &              strcis(SRFC,LABELD),strcis(SOIL,LABELD),
     &              1.0,accum)

c ..... Recompute lignin fraction in structural soil C
        call adjlig(strucc(SOIL),strlig(SRFC),trans,strlig(SOIL))

        do 20 iel = 1, nelem
          trans = struce(SRFC,iel) * cultra(6)
          call flow(struce(SRFC,iel),struce(SOIL,iel),time,trans)
20      continue
      endif

c ... Metabolic Litter
      trans = metabc(SRFC) * cultra(6)
      if (trans .gt. 0.0 .and. metabc(SRFC) .gt. 0.001) then
        call csched(trans,metcis(SRFC,LABELD),metabc(SRFC),
     &              metcis(SRFC,UNLABL),metcis(SOIL,UNLABL),
     &              metcis(SRFC,LABELD),metcis(SOIL,LABELD),
     &              1.0,accum)
        do 30 iel = 1, nelem
          trans = metabe(SRFC,iel) * cultra(6)
          call flow(metabe(SRFC,iel),metabe(SOIL,iel),time,trans)
30      continue
      endif

c ... Surface SOM1
      trans = som1c(SRFC) * cultra(6)
      if (trans .gt. 0.0 .and. som1c(SRFC) .gt. 0.001) then
        call csched(trans,som1ci(SRFC,LABELD),som1c(SRFC),
     &              som1ci(SRFC,UNLABL),som1ci(SOIL,UNLABL),
     &              som1ci(SRFC,LABELD),som1ci(SOIL,LABELD),
     &              1.0,accum)
        do 40 iel = 1, nelem
          trans = som1e(SRFC,iel) * cultra(6)
          call flow(som1e(SRFC,iel),som1e(SOIL,iel),time,trans)
40      continue
      endif

c ... Surface SOM2
      trans = som2c(SRFC) * cultra(6)
      if (trans .gt. 0.0 .and. som2c(SRFC) .gt. 0.001) then
        call csched(trans,som2ci(SRFC,LABELD),som2c(SRFC),
     &              som2ci(SRFC,UNLABL),som2ci(SOIL,UNLABL),
     &              som2ci(SRFC,LABELD),som2ci(SOIL,LABELD),
     &              1.0,accum)
        do 45 iel = 1, nelem
          trans = som2e(SRFC,iel) * cultra(6)
          call flow(som2e(SRFC,iel),som2e(SOIL,iel),time,trans)
45      continue
      endif


c ... Some standing dead goes to the top soil layer.

c ... STANDING DEAD LEAVES --> soil litter
      if (gtdleavc .gt. 0.0) then
        tsdsoi = gtdleavc * cultra(5)
        call partit(tsdsoi,recres,2,gtdlvcis,gtdleave,gtlig(GTLEAF),
     &              fr14)
      endif

c ... STANDING DEAD STEMS --> soil litter
      if (gtdstemc .gt. 0.0) then
        tsdsoi = gtdstemc * cultra(5)
        call partit(tsdsoi,recres,2,gtdstmcis,gtdsteme,gtlig(GTSTEM),
     &              fr14)
      endif


c ... Some above ground live goes into surface litter

c ... LIVE LEAVES --> surface litter
      if (gtleavc .gt. 0.0) then
        tagsfc = gtleavc * cultra(2)
        do 50 iel = 1, nelem
          recres(iel) = gtleave(iel)/gtleavc
50      continue
        fr14 = gtlvcis(LABELD)/gtleavc
        call partit(tagsfc,recres,1,gtlvcis,gtleave,gtlig(GTLEAF),
     &              fr14)
      endif

c ... LIVE STEMS --> surface litter
      if (gtstemc .gt. 0.0) then
        tagsfc = gtstemc * cultra(2)
        do 55 iel = 1, nelem
          recres(iel) = gtsteme(iel)/gtstemc
55      continue
        fr14 = gtstmcis(LABELD)/gtstemc
        call partit(tagsfc,recres,1,gtstmcis,gtsteme,gtlig(GTSTEM),
     &              fr14)
      endif


c ... Some storage pool goes to metabolic surface pool
c     if (gtstemc .gt. 0.0) then
c       do 60 iel = 1, nelem
c         trans = gtstg(iel) * cultra(2)
c         call flow(gtstg(iel),metabe(SRFC,iel),time,trans)
c60     continue
c     endif


c ... Some above ground live goes to the top soil layer.

c ... LIVE LEAVES --> soil litter
      if (gtleavc .gt. 0.0) then
        tagsoi = gtleavc * cultra(3)
        call partit(tagsoi,recres,2,gtlvcis,gtleave,gtlig(GTLEAF),
     &              fr14)
      endif

c ... LIVE STEMS --> soil litter
      if (gtstemc .gt. 0.0) then
        tagsoi = gtstemc * cultra(3)
        call partit(tagsoi,recres,2,gtstmcis,gtsteme,gtlig(GTSTEM),
     &              fr14)
      endif

c ... Some storage pool goes to metabolic soil pool
c     if (gtstemc .gt. 0.0) then
c       do 70 iel = 1, nelem
c         trans = gtstg(iel) * cultra(3)
c         call flow(gtstg(iel),metabe(SOIL,iel),time,trans)
c70     continue
c     endif


c ... Live roots go to the top soil layer.

c ... Juvenile fine roots
      if (gtfrootcj .gt. 0.0) then
        tbgsoi = gtfrootcj * cultra(7)
        do 80 iel = 1, nelem
          recres(iel) = gtfrootej(iel)/gtfrootcj
80      continue
        fr14 = gtfrtcisj(LABELD)/gtfrootcj
c ..... A fraction of the live roots are transferred to the surface
c ..... litter layer, the remainder goes to the soil litter layer,
c ..... cak - 05/14/2007
        srfclittr = tbgsoi * frtdsrfc
        soillittr = tbgsoi * (1.0 - frtdsrfc)
        call partit(srfclittr,recres,SRFC,gtfrtcisj,gtfrootej,
     &              gtlig(GTFROOTJ),fr14)
        call partit(soillittr,recres,SOIL,gtfrtcisj,gtfrootej,
     &              gtlig(GTFROOTJ),fr14)
      endif

c ... Mature fine roots
      if (gtfrootcm .gt. 0.0) then
        tbgsoi = gtfrootcm * cultra(7)
        do 85 iel = 1, nelem
          recres(iel) = gtfrootem(iel)/gtfrootcm
85      continue
        fr14 = gtfrtcism(LABELD)/gtfrootcm
c ..... A fraction of the live roots are transferred to the surface
c ..... litter layer, the remainder goes to the soil litter layer
c ..... cak - 05/14/2007
        srfclittr = tbgsoi * frtdsrfc
        soillittr = tbgsoi * (1.0 - frtdsrfc)
        call partit(srfclittr,recres,SRFC,gtfrtcism,gtfrootem,
     &              gtlig(GTFROOTM),fr14)
        call partit(soillittr,recres,SOIL,gtfrtcism,gtfrootem,
     &              gtlig(GTFROOTM),fr14)
      endif

c ... Coarse roots
      if (gtcrootc .gt. 0.0) then
        tbgsoi = gtcrootc * cultra(7)
        do 87 iel = 1, nelem
          recres(iel) = gtcroote(iel)/gtcrootc
87      continue
        fr14 = gtcrtcis(LABELD)/gtcrootc

c ..... A fraction of the live roots are transferred to the surface
c ..... litter layer, the remainder goes to the soil litter layer
c ..... cak - 05/14/2007
c ..... Bill Parton said if coarse roots are like rhizomes, then they will
c ..... get incorporated into the SURFACE litter when they die. In this
c ..... case, the parameter crtdsrfc (fraction of the coarse roots that 
c ..... are transferred into the surface litter layer) should be set to 
c ..... 1.0 in grasstree.100.  -mdh 2/19/2014

        srfclittr = tbgsoi * crtdsrfc
        soillittr = tbgsoi * (1.0 - crtdsrfc)
        call partit(srfclittr,recres,SRFC,gtcrtcis,gtcroote,
     &              gtlig(GTCROOT),fr14)
        call partit(soillittr,recres,SOIL,gtcrtcis,gtcroote,
     &              gtlig(GTCROOT),fr14)
      endif

c ..... Remove C from carbohydrate storage pool based on fraction of
c ..... coarse roots that die.
c       if (carbostg(GTARYINDX,UNLABL) .lt. 0.0) then
c         write(*,*) 'Error in cultivgt, carbostg(3,UNLABL) < 0.0'
c         STOP
c       endif
c       if (carbostg(GTARYINDX,LABELD) .lt. 0.0) then
c         write(*,*) 'Error in cultivgt, carbostg(3,LABELD) < 0.0'
c         STOP
c       endif
        if (isnan(carbostg(GTARYINDX,UNLABL))) then
          write(*,*) 'Error in cultivgt, carbostg(3,UNLABL) is NaN'
          STOP
        endif
        if (isnan(carbostg(GTARYINDX,LABELD))) then
          write(*,*) 'Error in cultivgt, carbostg(3,LABELD) is NaN'
          STOP
        endif
        mRespStorage = carbostg(GTARYINDX,UNLABL)
     &                 + carbostg(GTARYINDX,LABELD)
        mrspStgLoss = cultra(7) * mRespStorage
        if (mRespStorage .gt. 0.001) then
          call csched(mrspStgLoss, carbostg(GTARYINDX,LABELD),
     &                mRespStorage,
     &                carbostg(GTARYINDX,UNLABL), csrsnk(UNLABL),
     &                carbostg(GTARYINDX,LABELD), csrsnk(LABELD),
     &                1.0, accum)
        endif

c ..... Remove from elemental STORAGE pool based on fraction of 
c ..... coarse roots that die. 

        do 90 iel = 1, nelem
          eloss(iel) = max(cultra(7) * gtstg(iel), 0.0)
          call flow(gtstg(iel),esrsnk(iel),time,eloss(iel))
90      continue

c ... Some above ground live goes to standing dead

c ... LIVE LEAVES -> DEAD STANDING LEAVES
      trans = gtleavc * cultra(1)
      if (trans .gt. 0.0 .and. gtleavc .gt. 0.001) then
        call csched(trans,gtlvcis(LABELD),gtleavc,
     &              gtlvcis(UNLABL),gtdlvcis(UNLABL),
     &              gtlvcis(LABELD),gtdlvcis(LABELD),
     &              1.0,accum)
        do 95 iel = 1, nelem
          trans = gtleave(iel) * cultra(1)
          call flow(gtleave(iel),gtdleave(iel),time,trans)
95      continue
      endif

c ... LIVE STEMS -> DEAD STANDING STEMS
      trans = gtstemc * cultra(1)
      if (trans .gt. 0.0 .and. gtstemc .gt. 0.001) then
        call csched(trans,gtstmcis(LABELD),gtstemc,
     &              gtstmcis(UNLABL),gtdstmcis(UNLABL),
     &              gtstmcis(LABELD),gtdstmcis(LABELD),
     &              1.0,accum)
        do 97 iel = 1, nelem
          trans = gtsteme(iel) * cultra(1)
          call flow(gtsteme(iel),gtdsteme(iel),time,trans)
97      continue
      endif

c ... State variables and accumulators
      call flowup(time)
      call flowup_double(time)
      call flowup_double_in(time)
      call flowup_double_out(time)
      call sumcar
 
c ... Check status of carbon values to make sure that everything
c ... has been reset correctly; if carbon = 0, reset elements to 0 as well

c ... LIVE LEAVES
      if (gtlvcis(UNLABL)+gtlvcis(LABELD) .lt. 1.e-05) then
        gtlvcis(UNLABL) = 0.0
        gtlvcis(LABELD) = 0.0
        do 100 iel = 1, MAXIEL
          gtleave(iel) = 0
100     continue
      endif

c ... DEAD LEAVES
      if (gtdlvcis(UNLABL)+gtdlvcis(LABELD) .lt. 1.e-05) then
        gtdlvcis(UNLABL) = 0.0
        gtdlvcis(LABELD) = 0.0
        do 105 iel = 1, MAXIEL
          gtdleave(iel) = 0
105     continue
      endif

c ... LIVE STEMS
      if (gtstmcis(UNLABL)+gtstmcis(LABELD) .lt. 1.e-05) then
        gtstmcis(UNLABL) = 0.0
        gtstmcis(LABELD) = 0.0
        do 110 iel = 1, MAXIEL
          gtsteme(iel) = 0
110     continue
      endif

c ... DEAD STEMS
      if (gtdstmcis(UNLABL)+gtdstmcis(LABELD) .lt. 1.e-05) then
        gtdstmcis(UNLABL) = 0.0
        gtdstmcis(LABELD) = 0.0
        do 115 iel = 1, MAXIEL
          gtdsteme(iel) = 0
115     continue
      endif

      do 120 iel = 1, MAXIEL
        if (gtleave(iel) .lt. 1.e-05) then
          gtleave(iel) = 0.0
        endif
        if (gtdleave(iel) .lt. 1.e-05) then
          gtdleave(iel) = 0.0
        endif
        if (gtsteme(iel) .lt. 1.e-05) then
          gtsteme(iel) = 0.0
        endif
        if (gtdsteme(iel) .lt. 1.e-05) then
          gtdsteme(iel) = 0.0
        endif
        if (gtfrootej(iel) .lt. 1.e-05) then
          gtfrootej(iel) = 0.0
        endif
        if (gtfrootem(iel) .lt. 1.e-05) then
          gtfrootem(iel) = 0.0
        endif
        if (gtcroote(iel) .lt. 1.e-05) then
          gtcroote(iel) = 0.0
        endif
120   continue
       
      return
      end
