c*****************************************************************************
c
c  FILE:      rootprimeff.f
c
c  FUNCTION:  real rootprimeff()
c
c  PURPOSE:   Compute root priming effect for CRPSYS, GRASSTREESYS,
c             FORSYS, and SAVSYS.
c
c  AUTHOR:    Melannie Hartman March 12, 2014
c             New function.  This code was originally written by Cindy Keough 
c             1/28/2014 and was formerly located at the beginning of the daily 
c             loop in simsom.  I added code for GRASSTREESYS calculation. 
c             CRPSYS and GRASSTREESYS use the same parameters.
c
c  FUNCTION ARGUMENTS:
c    dhresp - daily heterotrophic respiration
c    dsresp - daily soil respiration (root respiration + heterotrophic)
c
c*****************************************************************************

      real function rootprimeff(dhresp, dsresp)

      implicit none
      include 'const.inc'
      include 'monprd.inc'
      include 'param.inc'
      include 'plot1.inc'
      include 'plot3.inc'
      include 'seq.inc'

c ... Function arguments
      real dhresp, dsresp

c ... Function declarations
      real ramp
      external ramp

c ... Local variables
      real crpeff, trpeff, rpeff

c ..... Calculate the root priming effect on the som2c(2) decomposition
c ..... rate, CAK - 01/28/2014
        crpeff = 1.0
        trpeff = 1.0
c ..... Crop/grass root priming effect
        if (crpindx .eq. 0) then
c ....... No root priming effect on som2c(2) decomposition
          crpeff = 1.0
        elseif (crpindx .eq. 1) then
c ....... Root priming effect on som2c(2) decomposition based on total
c ....... soil respiration
          crpeff = ramp(dsresp, crpcmn, crpmnmul, crpcmx, crpmxmul)
        elseif (crpindx .eq. 2) then
c ....... Root priming effect on som2c(2) decompostion based on soil
c ....... hetertrophic respiration only
          crpeff = ramp(dhresp, crpcmn, crpmnmul, crpcmx, crpmxmul)
        elseif (crpindx .eq. 3) then
c ....... Root priming effect on som2c(2) decomposition based on fine
c ....... root production 
          if (cursys .eq. CRPSYS) then
            crpeff = ramp(mcprd(BELOWJ)+mcprd(BELOWM), crpcmn, crpmnmul,
     &                    crpcmx, crpmxmul)
          else
c ......... GRASSTREESYS
            crpeff = ramp(mgtprd(GTFROOTJ)+mgtprd(GTFROOTM), crpcmn, 
     &                    crpmnmul, crpcmx, crpmxmul)
          endif
        endif

c ..... Tree root priming effect
        if (trpindx .eq. 0) then
c ....... No root priming effect on som2c(2) decomposition
         trpeff = 1.0
        elseif (trpindx .eq. 1) then
c ....... Root priming effect on som2c(2) decomposition based on total
c .......  soil respiration
          trpeff = ramp(dsresp, trpcmn, trpmnmul, trpcmx, trpmxmul)
        elseif (trpindx .eq. 2) then
c ....... Root priming effect on som2c(2) decompostion based on soil
c ....... hetertrophic respiration only
          trpeff = ramp(dhresp, trpcmn, trpmnmul, trpcmx, trpmxmul)
        elseif (trpindx .eq. 3) then
c ....... Root priming effect on som2c(2) decomposition based on fine
c ....... root production 
          trpeff = ramp(mfprd(FROOTJ)+mfprd(FROOTM), trpcmn, trpmnmul,
     &                 trpcmx, trpmxmul)
        endif

        if (cursys .eq. CRPSYS .or. cursys .eq. GRASSTREESYS) then
          rpeff = crpeff
        elseif (cursys .eq. FORSYS) then
          rpeff = trpeff
        elseif (cursys .eq. SAVSYS) then
c ....... If there is fine root growth weight the root priming effect
c ....... based on crop/grass vs. tree fine root growth
          if (mcprd(BELOWJ)+mcprd(BELOWM) .gt. 0.0 .or.
     &        mfprd(FROOTJ)+mfprd(FROOTM) .gt. 0.0) then
            rpeff = crpeff * ((mcprd(BELOWJ) + mcprd(BELOWM)) /
     &                        (mcprd(BELOWJ) + mcprd(BELOWM) +
     &                         mfprd(FROOTJ) + mfprd(FROOTM))) +
     &              trpeff * ((mfprd(FROOTJ) + mfprd(FROOTM)) /
     &                        (mcprd(BELOWJ) + mcprd(BELOWM) +
     &                         mfprd(FROOTJ) + mfprd(FROOTM)))
          else
c ......... If there is no fine root production weight the root priming
c ......... effect based on crop/grass vs. tree fine root biomass
            rpeff = crpeff * ((bglivcj + bglivcm) /
     &                        (bglivcj + bglivcm + frootcj + frootcm))+
     &              trpeff * ((frootcj + frootcm) /
     &                        (bglivcj + bglivcm + frootcj + frootcm))
          endif
        endif

      rootprimeff = rpeff

      return
      end
