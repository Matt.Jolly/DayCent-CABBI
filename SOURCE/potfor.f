
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      subroutine potfor(tavedly, petdly, tfrac, tavemth, srad,
     &                  curday)

      implicit none
      include 'comput.inc'
      include 'const.inc'
      include 'dynam.inc'
      include 'monprd.inc'
      include 'param.inc'
      include 'parfs.inc'
      include 'parfx.inc'
      include 'plot1.inc'
      include 'plot3.inc'
      include 'potent.inc'
      include 'site.inc'
      include 'timvar.inc'
      include 'zztim.inc'

c ... Argument declarations
      integer curday
      real    tavedly, petdly, tfrac
      real    tavemth, fldeff
      double precision srad

c ... Compute monthly potential production for forest
c ...
c ... Outputs
c ...   pforc gC/m^2/time
c ...   h2ogef(2)

c ... RESPPT is an array of respiration values for the forest system
c ... production components.

c ... Added savanna model, pcropc now pforc
c ...                      tgprod now tfprod (BO)

c ... Function declarations
      real     gpdf, frespr, laprod, line
      external gpdf, frespr, laprod, line

c ... Local variables
      real     fcmax, frlive, lai, langleys, potprd
      real     laiprd, biof

c ... Estimate potential production based on temp & h2o

      if (tavedly .gt. 0.0) then

c ..... Calculate temperature effect on growth.
        potprd = gpdf(tavedly, ppdf(1,2), ppdf(2,2), ppdf(3,2), 
     &                ppdf(4,2))

c ..... Added to match version 3.0 -lh 4/93
        potprd = potprd * 0.8

c ..... Calculate moisture effect on growth
        if (petdly .ge. .01) then
c ....... Calculate potential growth based on the relative water content
c ....... of the wettest soil layer, cak - 12/06/04
          h2ogef(2) = 1.0/(1.0+exp(wscoeff(2,2)*
     &                             (wscoeff(2,1)-twstress)))
        else
          h2ogef(2) = 0.01
        endif

c ..... For large wood, calculate the percentage which is live (sapwood)
        frlive = sapk / (sapk + rlwodc)
 
c ..... Calculate LAI and then use in function for LAI reducer on
c ..... production.  -rm 5/91
c ..... Calculate theoretical maximum for LAI based on large wood biomass,
c ..... cak - 07/24/02
c ..... Include fine branch carbon as part of the woody component in the
c ..... LAI calculation, cak - 10/20/2006
        call lacalc(lai, fbrchc, rlwodc, maxlai, klai)

        laiprd = laprod(lai,laitop)

c ..... Reduction in production due to flooding. -mdh 6/19/2019
c ..... Occurs when the soil water content on the rooting zone (swcpg)
c ..... is greater than the amount of water held at field capacity (swcfcpg).
        if (swcpg .gt. swcfcpg) then
          fldeff = line(swcpg, swcfcpg, 1.0, swcsatpg, flodeff(FORSYS))
        else
          fldeff = 1.0
        endif

c ..... Use the solar radiation value as calculated by Peter Thornton's
c ..... subroutines in the calculation of potential production,
c ..... cak - 06/18/2009
c ..... Convert the solar radiation value from W/m^2 to langleys/day
        langleys = srad*((daylength(curday)*60*60)/(10000*4.184))
        fcmax = langleys * prdx(2) * tfrac * potprd * h2ogef(2) *
     &          fldeff * laiprd * co2cpr(FORSYS)
        pforc = fcmax

        if (time .ge. strplt) then
          biof = -99
          call wrtpotprd(FORSYS,time,curday,daylength(curday),
     &         langleys,prdx(FORSYS),potprd,h2ogef(FORSYS), 
     &         twstress,fldeff,biof,lai,laiprd,co2cpr(FORSYS),
     &         pforc)
        endif

c ..... Compute carbon allocation fractions for each tree part
c ..... mdh 5/11/01
c ..... Call moved from treegrow subroutine, cak - 07/01/02
        if (pforc .gt. 0.01) then
          call treeDynC(pforc, tavemth, tree_cfrac)
        else
          pforc = 0.0
        endif

      else
        pforc = 0.
      endif

      return
      end
