
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      potgrasstree.f
c
c  FUNCTION:  subroutine potgrasstree(tavedly, petdly, tfrac, srad, curday)
c
c  PURPOSE:   Compute daily production potential for a grasstree
c
c  AUTHOR:    New function derived from potcrop.
c             Melannie Hartman Jan. 16, 2014
c
c  FUNCTION ARGUMENTS:
c    cancvr     - canopy cover
c    tavedly	- average daily temperature (C)
c    petdly	- potential evapotranspiration (cm/day)
c    tfrac	- time fraction (1/# days in current month)
c    srad	- average daily incident solar radiation (W/m^2)
c    curday     - day of year (1..366)
c
c  fix.100:
c    pmxbio     - Maximum dead biomass (standing dead + 10% litter) level 
c                 for soil temperature calculation and for calculation of 
c                 the potential negative effect on plant (crop and grass) 
c                 growth of physical obstruction by standing dead and 
c                 surface litter (fix.100).
c
c*****************************************************************************


      subroutine potgrasstree(tavedly, petdly, tfrac, srad, curday)

      implicit none
      include 'comput.inc'
      include 'const.inc'
      include 'dovars.inc'
      include 'dynam.inc'
      include 'monprd.inc'
      include 'param.inc'
      include 'parfx.inc'
      include 'pargt.inc'
      include 'plot1.inc'
      include 'plot4.inc'
      include 'potent.inc'
      include 'seq.inc'
      include 'site.inc'
      include 'timvar.inc'
      include 'zztim.inc'

c ... Argument declarations
      integer curday
      real    tavedly, petdly, tfrac
      double precision srad

c ... Function declarations
      real     gpdf, laprodgt, line
      external gpdf, laprodgt, line

c ... Local variables
      real     bioc, biof, tgtprod,
     &         bioprd, langleys, potprd, ratlc, 
     &         sdlng, lai, laiprd, shdmod 
      real     temp1, temp2, temp3, lwodc, fldeff


      tgtprod = 0.0

c ... Calculate moisture effect on growth based on the relative 
c ... water content of the wettest soil layer (gtwstress)
      if (petdly .ge. 0.01) then
        h2ogef(GTARYINDX) = 1.0/(1.0+exp(wscoeff(GTARYINDX,2)
     &                      * (wscoeff(GTARYINDX,1)-gtwstress)))
      else
        h2ogef(GTARYINDX) = 0.01
      endif

c ... Estimate plant production:
      if (tavedly .gt. 0.0) then

c ..... Calculate temperature effect on growth
c ..... For grasstree, ppdf(*,1) becomes ppdf(*,GTARYINDX)

        potprd = gpdf(tavedly, ppdf(1,GTARYINDX), ppdf(2,GTARYINDX),  
     &                ppdf(3,GTARYINDX), ppdf(4,GTARYINDX))

c ..... Calculate biof
        if (gtbioflg .eq. 1) then

c ....... Calculate maximum potential effect of standing dead on plant growth
c ....... (the effect of physical obstruction of litter and standing dead)
c         bioc = stdedc + 0.1*strucc(SRFC)

          bioc = gtdleavc + gtdstemc + 0.1*strucc(SRFC)
          if (bioc .le. 0.0) then
            bioc = 0.01
          endif

          if (bioc .gt. pmxbio) then
            bioc = pmxbio
          endif
          bioprd = 1.0 - (bioc/(gtbiok5+bioc))

c ....... Calculate the effect of the ratio of live biomass to dead biomass
c ....... on the reduction of potential growth rate.  The intercept of this 
c ....... equation ( highest negative effect of dead plant biomass ) is equal
c ....... to bioprd when the ratio is zero.

          temp1 = (1.0 - bioprd)
          temp2 = temp1*0.75
          temp3 = temp1*0.25
c         ratlc = aglivc/bioc 
          ratlc = (gtleavc+gtstemc)/bioc 
          if (ratlc .le. 1.0) then
            biof = bioprd+(temp2*ratlc)
          endif
          if (ratlc .gt. 1.0 .and. ratlc .le. 2.0) then
            biof = (bioprd+temp2)+temp3*(ratlc-1.0)
          endif
          if (ratlc .gt. 2.0) then
            biof = 1.0
          endif
        else
          biof = 1.0
        endif

c ..... Restriction on seedling growth
c ..... sdlng is the fraction that prdx is reduced
c ..... ATTENTION: are both seedl and laiprd needed?
c       if (gtleavc .gt. gtfulcan) then
c         seedl = 0
c       endif
c
c       if (seedl .eq. 1) then
c         sdlng = min(1.0, gtpltmrf + gtleavc*(1-gtpltmrf) /gtfulcan)
c       else
c         sdlng = 1.0
c       endif

        sdlng = 1.0

c ..... Calculate theoretical maximum for LAI based on stem biomass
c ..... to reduce production when biofuel is getting established. 
c ..... This function is also used in potfor.f. -mdh 5/29/2019
        lwodc = 0.0
        call lacalc(lai, gtstemc, lwodc, gtmaxlai, gtklai)
        laiprd = laprodgt(lai, gtlaitop)

c ..... Reduction in production due to flooding. -mdh 6/19/2019
c ..... Occurs when the soil water content on the rooting zone (swcpg)
c ..... is greater than the amount of water held at field capacity (swcfcpg).
        if (swcpg .gt. swcfcpg) then
          fldeff = line(swcpg,swcfcpg,1.0,swcsatpg,flodeff(GTARYINDX))
        else
          fldeff = 1.0
        endif

c ..... Compute potential total production
c ..... Use the solar radiation value as calculated by Peter Thornton's
c ..... subroutines in the calculation of potential production,
c ..... cak - 06/18/2009
c ..... Convert the solar radiation value from W/m^2 to langleys/day 
c .....   langley = 41840 J/m^2, 3600 = sec/hour
c ..... prdx is the radiation use efficiency per langley 
c .....   (gC production/(m^2*month*langley))

        langleys = srad * ((daylength(curday)*3600.0)/41840.0)
        tgtprod = langleys * prdx(GTARYINDX) * potprd * 
     &            h2ogef(GTARYINDX) * fldeff * biof * laiprd *
     &            sdlng * co2cpr(GTARYINDX) * tfrac

        if (time .ge. strplt) then
c ....... Shading effect (savannas only). Currently, no shading modification
c ....... because grasstrees cannot be grown with trees in savanna mode.
          shdmod = -99
          call wrtpotprd(GTARYINDX,time,curday,daylength(curday),
     &         langleys,prdx(GTARYINDX),potprd,h2ogef(GTARYINDX), 
     &         gtwstress,fldeff,biof,lai,laiprd,co2cpr(GTARYINDX),
     &         tgtprod)
        endif

c ..... Dynamic carbon allocation routines for grasstree
c ..... Do not call the dynamic carbon allocation routine when there is no
c ..... production, cak - 09/09/02
        if (tgtprod .gt. 0.0) then
c ....... gtbiomc = (g dry biomass)/(gC) (formerly hardcoded as 2.5)
          pgrasstreec = tgtprod / gtbiomc
          call grasstreeDynC(pgrasstreec, grasstree_cfrac)
        else
          tgtprod = 0.0
          pgrasstreec = 0.0
          goto 40
        endif

      else
c ..... No production this month
        tgtprod = 0.0
        pgrasstreec = 0.0
        goto 40
      endif

c ... Update potential C production accumulator
c     write(*,*) 'pgrasstreec = ', pgrasstreec
      potgtacc =  potgtacc + pgrasstreec

40    continue

      return
      end
