
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      grasstreeDynC.f
c
c  FUNCTION:  subroutine grasstreeDynC()
c
c  PURPOSE:   Compute carbon allocation fractions for live grasstree parts 
c            (GTLEAF, GTFROOT, GTSTEM, and GTCROOT)
c
c  AUTHOR:    New function derived from treeDynC.
c             Melannie Hartman Jan. 16, 2014
c
c*****************************************************************************

      subroutine grasstreeDynC(potgrasstreec, grasstree_cfrac)

      implicit none
      include 'comput.inc'
      include 'const.inc'
c     include 'dynam.inc'
      include 'param.inc'
      include 'pargt.inc'
      include 'parfx.inc'
      include 'pheno.inc'
      include 'plot1.inc'
c     include 'plot3.inc'
      include 'plot4.inc'
      include 'potent.inc'

c ... Argument declarations
      real potgrasstreec, grasstree_cfrac(GTLIVPARTS)

c ... Function declarations
      real      froota, rtimp
      external  froota, rtimp

c ... Local variables
      integer iel, lyr
      real    availm(MAXIEL), demand, eavail(MAXIEL), fracrc, leafprod,
     &        maxNfix, rimpct, rootprod, totale, toler

      toler = 1.0E-30

c ... Use grasstree specific favail(1) value
      favail(1) = sfavail(3)

c ... Estimate the fraction of carbon going to the roots
      if (gtfrtcindx .eq. 1) then
c ..... A perennial plant 
        fracrc = (gtfrtcw(1)+gtfrtcw(2)+gtfrtcn(1)+gtfrtcn(2))/4.0
      else
c ..... An annual plant
        fracrc = (gtfrtc(1)+gtfrtc(2))/2.0
      endif

c ... Estimate the fine root and leaf production based on total
c ... potential production

      leafprod = potgrasstreec * (1.0 - fracrc) 
      rootprod = potgrasstreec * fracrc

c ... Determine nutrients available to plants for growth.

      do 20 iel = 1, nelem
        availm(iel) = 0.0
c ..... Nutrients available to grasstrees are in the top gtlaypg layers
        do 30 lyr = 1, gtlaypg
          if (minerl(lyr,iel) .gt. toler) then
            availm(iel) = availm(iel) + minerl(lyr, iel)
          endif
30      continue
20    continue

c ... Calculate impact of root biomass on available nutrients

      rimpct = rtimp(riint, rictrl, gtfrootcj+gtfrootcm)

c ... Calculate soil available nutrients, based on a maximum fraction
c ... (favail) and the impact of root biomass (rimpct), adding storage.

      do 45 iel = 1, nelem
        eavail(iel) = (availm(iel) * favail(iel) * rimpct) + 
     &                 gtstg(iel)
45    continue

c ... Compute the minimum and maximum C/E ratios
      call fltcegrasstree(nelem, gtleavc, gtstemc, co2cce)

c ... Estimate the nutrient demand assuming ?????

      do 50  iel = 1, nelem
c ..... Initialize fixation to 0
        maxNfix = 0.0
c ..... N FIXATION
        if (iel .eq. N) then
          maxNfix = snfxmx(GTARYINDX) * potgrasstreec
        endif
c ..... DEMAND based on the maximum E/C ratio (minimum C/E ratio for new growth).
        demand = 0.0
        demand = demand + leafprod * (1.0/cergtamn1(GTLEAF,iel))
        demand = demand + rootprod * (1.0/cergtbmn(GTFROOT,iel))
        totale = eavail(iel) + maxNfix

c ..... Ratio of available E to demand for E
        grasstree_a2drat(iel) = min(1.0, totale / demand)
        grasstree_a2drat(iel) = max(0.0, grasstree_a2drat(iel))
50    continue

c ... Allocate to fine roots first and then to leaves
      grasstree_cfrac(GTFROOT) =
     &    froota(grasstree_a2drat, h2ogef(GTARYINDX),GRASSTREESYS)
      grasstree_cfrac(GTLEAF) = 1.0 - grasstree_cfrac(GTFROOT)
      grasstree_cfrac(GTFROOTM) = 0.0
      grasstree_cfrac(GTSTEM) = 0.0
      grasstree_cfrac(GTCROOT) = 0.0

c     write(*,*) 'grasstreeDynC: froota =', grasstree_cfrac(GTFROOT)

      return
      end
