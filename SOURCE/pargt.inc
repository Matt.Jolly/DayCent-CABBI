
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c  This file contains a subset of grasstree.100 parameters
c 
c  Replace cerfor(3,5,3) with the following C/E ratios (grasstree.100):
c    cergtamn1(1-2,3)
c    cergtamn2(1-2,3)
c    cergtamx1(1-2,3)
c    cergtamx2(1-2,3)
c    cergtbmn(3-5,3)
c    cergtbxn(3-5,3)
c    cergtini(7,3) (removed 4/17/2014 - not needed)
c  gtmrtfrac - Fraction of fine root production that goes to mature roots
c
c  Parameters added for annual grasstrees (13). -mdh 9/5/2019
c    efgrngt(3)
c    gtfrtc(5)
c    gtrtdtmp
c    himaxgt
c    himongt(2)
c    hiwsfgt
c  Unused parameters removed (1). -mdh 9/5/2019
c    gtpltmrf

      common/pargt/cergtamn1(2,3), cergtamn2(2,3), 
     &  cergtamx1(2,3), cergtamx2(2,3),
     &  cergtbmn(5,3), cergtbmx(5,3), crtdsrfc, 
     &  efgrngt(3), frtdsrfc, gtfrtcindx, 
     &  gtfrtc(5), gtfrtcn(2), gtfrtcw(2), gtcfrac(5),
     &  grasstreegrw, gtlvbiomax, gtstmbiomax,
     &  gtbioflg, gtbiok5, gtbiomc, gtbtolai,
     &  gtklai, gtlaitop, gtmaxlai,
     &  gtfsdeth(6), gtrtf(3), gtmrtfrac, gtmxturn, 
     &  gtrtdtmp, himaxgt, himongt(2), hiwsfgt,  
     &  sdfallrt(2), rootdr(3), vlossgt

      integer grasstreegrw, gtfrtcindx, gtbioflg, himongt

      real cergtamn1, cergtamn2, 
     &  cergtamx1, cergtamx2, cergtbmn, cergtbmx, 
     &  crtdsrfc, efgrngt, frtdsrfc, 
     &  gtfrtc, gtfrtcn, gtfrtcw, gtcfrac,
     &  gtlvbiomax, gtstmbiomax,
     &  gtbiok5, gtbiomc, gtbtolai,
     &  gtklai, gtlaitop, gtmaxlai,
     &  gtfsdeth, gtrtf, gtmrtfrac, gtmxturn,
     &  gtrtdtmp, himaxgt, hiwsfgt, 
     &  sdfallrt, rootdr, vlossgt 

      save /pargt/

