
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c
c*****************************************************************************
c
c  FILE:      gtcutrtn.f
c
c  FUNCTION:  subroutine gtcutrtn()
c
c  PURPOSE:   TREM event (harvest) for GrassTrees. 
c             Elemental return from a cutting event.
c
c  AUTHOR:    New function derived from cutrtrn.
c             Melannie Hartman Feb. 5, 2014
c
c             Updated to use new definitions of TREM parameters
c             that were implemented for the snag model. -mdh 2/20/2019
c
c             Track grasstree harvest variables in harvgt.inc. -mdh 9/4/2019
c
c  Called from:  grasstreerem
c
c*****************************************************************************

      subroutine gtcutrtn()

      implicit none
      include 'const.inc'
      include 'ligvar.inc'
      include 'forrem.inc'
      include 'harvgt.inc'
      include 'param.inc'
c     include 'parfs.inc'
      include 'pargt.inc'
      include 'plot1.inc'
      include 'plot2.inc'
c     include 'plot3.inc'
      include 'plot4.inc'
      include 'zztim.inc'

c ... Argument declarations

c ... Fortran to C prototype
      INTERFACE
        SUBROUTINE flow(from, to, when, howmuch)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          REAL from
          REAL to
          REAL when
          REAL howmuch
        END SUBROUTINE flow
      END INTERFACE

c ... Local Variables
      integer   iel
      real      frc14, recres(MAXIEL)
      real      cret, eret(MAXIEL)

c ... LIVE LEAVES are returned to LITTER
      if (gtleavc .gt. 0.001) then
        cret = remf(1) * (1.0 - lv2std(1)) * retf(1,1) * gtleavc
        gtcreta = gtcreta + cret
        gtlvcret = cret
        if (cret .gt. 0.0) then
          do 10 iel = 1, nelem
            eret(iel) = remf(1) * (1.0 - lv2std(1)) * retf(1,iel+1) 
     &                  * gtleave(iel)
            gtlveret(iel) = eret(iel)
            gtereta(iel) = gtereta(iel) + eret(iel)
            recres(iel) = eret(iel) / cret
10        continue
          frc14 = gtlvcis(LABELD) / gtleavc
          call partit(cret,recres,1,csrsnk,esrsnk,gtlig(GTLEAF),frc14)
        endif
      endif

c.... DEAD ATTACHED LEAVES are returned to LITTER
      if (gtdleavc .gt. 0.001) then
        cret = remf(6) * retf(4,1) * gtdleavc
        gtdlvcret = cret
        gtcreta = gtcreta + cret
        if (cret .gt. 0.0) then
          do 15 iel = 1, nelem
            eret(iel) = remf(6) * retf(4,iel+1) * gtdleave(iel)
            gtdlveret(iel) = eret(iel)
            gtereta(iel) = gtereta(iel) + eret(iel)
            recres(iel) = eret(iel) / cret
15        continue
          frc14 = gtdlvcis(LABELD) / gtdleavc
          call partit(cret,recres,1,csrsnk,esrsnk,gtlig(GTLEAF),frc14)
        endif
      endif


c ... LIVE STEMS are returned to LITTER
      if (gtstemc .gt. 0.001) then
        cret = remf(2) * (1.0 - lv2std(2)) * retf(2,1) * gtstemc
        gtstmcret = cret
        gtcreta = gtcreta + cret
        if (cret .gt. 0.0) then
          do 20 iel = 1, nelem
            eret(iel) = remf(2) * (1.0 - lv2std(2)) * retf(2,iel+1)
     &                 * gtsteme(iel)
            gtstmeret(iel) = eret(iel)
            gtereta(iel) = gtereta(iel) + eret(iel)
            recres(iel) = eret(iel) / cret
20        continue
          frc14 = gtstmcis(LABELD) / gtstemc
          call partit(cret,recres,1,csrsnk,esrsnk,gtlig(GTSTEM),frc14)
        endif
      endif

c.... DEAD STANDING STEMS are returned to LITTER
      if (gtdstemc .gt. 0.001) then
        cret = remf(7) * retf(5,1) * gtdstemc
        gtdstmcret = cret
        gtcreta = gtcreta + cret
        if (cret .gt. 0.0) then
          do 30 iel = 1, nelem
            eret(iel) = remf(7) * retf(5,iel+1) * gtdsteme(iel)
            gtdstmeret(iel) = eret(iel)
            gtereta(iel) = gtereta(iel) + eret(iel)
            recres(iel) = eret(iel) / cret
30        continue
          frc14 = gtdstmcis(LABELD) / gtdstemc
          call partit(cret,recres,1,csrsnk,esrsnk,gtlig(GTSTEM),frc14)
        endif
      endif

      return
      end
