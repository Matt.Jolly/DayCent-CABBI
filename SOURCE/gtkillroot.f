
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c
c*****************************************************************************
c
c  FILE:      gtkillroot.f
c
c  FUNCTION:  subroutine gtkillroot()
c
c  PURPOSE:   TREM event (harvest) for GrassTrees. 
c             Death of fine and coarse roots due to cutting or fire.
c
c  AUTHOR:    New function derived from killrt.
c             Melannie Hartman Feb. 19, 2014
c
c             Track grasstree harvest variables in harvgt.inc. -mdh 9/4/2019
c
c  Called from:  grasstreerem
c
c  VARIABLES:
c    fd(1)    - Fraction of fine roots that die in TREM event
c    fd(2)    - Fraction of coarse roots that die in TREM event
c    frtdsrfc - Fraction of the fine roots that are transferred into the 
c		surface litter layer (STRUCC(1) and METABC(1)) upon fine 
c		root death, the remainder of the dead fine roots will go 
c		to the soil litter layer (STRUCC(2) and METABC(2)). 
c               See grasstree.100.
c    crtdsrfc - Fraction of the coarse roots that are transferred into the 
c		surface litter layer (STRUCC(1) and METABC(1)) upon fine 
c		root death, the remainder of the dead coarse roots will go 
c		to the soil litter layer (STRUCC(2) and METABC(2)). 
c               See grasstree.100.
c
c  LOCAL VARIABLES:
c    frd      - Amount of fine roots that die (gC/m2)
c    crd      - Amount of coarse roots that die (gC/m2)
c
c*****************************************************************************

      subroutine gtkillroot(accum)

      implicit none
      include 'const.inc'
      include 'ligvar.inc'
      include 'forrem.inc'
      include 'harvgt.inc'
      include 'param.inc'
c     include 'parfs.inc'
      include 'pargt.inc'
      include 'plot2.inc'
      include 'plot4.inc'
      include 'zztim.inc'

c ... Argument declarations
      real     accum(ISOS)

c ... Local Variables
      integer  iel
      real     frc14, crd, frd, recres(MAXIEL)
      real     mRespStorage, mRespStgLoss

c ... Death of juvenile FINE ROOTS

      if (gtfrootcj .gt. 0.001) then
        frd = gtfrootcj * fd(1)
        do 10 iel = 1, nelem
          recres(iel) = gtfrootej(iel) / gtfrootcj
10      continue

        frc14 = gtfrtcisj(LABELD) / gtfrootcj
        srfclittrj = frd * frtdsrfc
        soillittrj = frd * (1.0 - frtdsrfc)
        do 15 iel = 1, nelem
          esrfclittrj(iel) = srfclittrj * recres(iel)
          esoillittrj(iel) = soillittrj * recres(iel)
15      continue
c       write(*,*) 'gtkillroot(gtfrootcj): srfclittrj =', srfclittrj
c       write(*,*) 'gtkillroot(gtfrootcj): soillittrj =', soillittrj
        call partit(srfclittrj,recres,SRFC,gtfrtcisj,gtfrootej,
     &              gtlig(GTFROOTJ),frc14)

        call partit(soillittrj,recres,SOIL,gtfrtcisj,gtfrootej,
     &              gtlig(GTFROOTJ),frc14)
      endif

c ... Death of mature FINE ROOTS

      if (gtfrootcm .gt. 0.001) then
        frd = gtfrootcm * fd(1)
        do 20 iel = 1, nelem
          recres(iel) = gtfrootem(iel) / gtfrootcm
20      continue

        frc14 = gtfrtcism(LABELD) / gtfrootcm
        srfclittrm = frd * frtdsrfc
        soillittrm = frd * (1.0 - frtdsrfc)
        do 25 iel = 1, nelem
          esrfclittrm(iel) = srfclittrm * recres(iel)
          esoillittrm(iel) = soillittrm * recres(iel)
25      continue
c       write(*,*) 'gtkillroot(gtfrootcm): srfclittrm =', srfclittrm
c       write(*,*) 'gtkillroot(gtfrootcm): soillittrm =', soillittrm
        call partit(srfclittrm,recres,SRFC,gtfrtcism,gtfrootem,
     &              gtlig(GTFROOTM),frc14)
        call partit(soillittrm,recres,SOIL,gtfrtcism,gtfrootem,
     &              gtlig(GTFROOTM),frc14)
      endif

c ... Death of COARSE ROOTS
c ... Bill Parton said if coarse roots are like rhizomes, then they will
c ... get incorporated into the SURFACE litter when they die. In this
c ... case, the parameter crtdsrfc (fraction of the coarse roots that 
c ... are transferred into the surface litter layer) should be set to 
c ... a value close to 1.0 in grasstree.100.  -mdh 2/19/2014

      if (gtcrootc .gt. 0.001) then
        crd = gtcrootc * fd(2)
        do 30 iel = 1, nelem
          recres(iel) = gtcroote(iel) / gtcrootc
30      continue

        frc14 = gtcrtcis(LABELD) / gtcrootc
        srfclittrcrt = crd * crtdsrfc
        soillittrcrt = crd * (1.0 - crtdsrfc)
        do 35 iel = 1, nelem
          esrfclittrcrt(iel) = srfclittrcrt * recres(iel)
          esoillittrcrt(iel) = soillittrcrt * recres(iel)
35      continue
c       write(*,*) 'gtkillroot(gtcrootc): srfclittrcrt =', srfclittrcrt
c       write(*,*) 'gtkillroot(gtcrootc): soillittrcrt =', soillittrcrt
        call partit(srfclittrcrt,recres,SRFC,gtcrtcis,gtcroote,
     &              gtlig(GTCROOT),frc14)
        call partit(soillittrcrt,recres,SOIL,gtcrtcis,gtcroote,
     &              gtlig(GTCROOT),frc14)

c ..... Remove C from carbohydrate storage pool based on fraction of
c ..... live coarse roots removed
        if (carbostg(GTARYINDX,UNLABL) .lt. 0.0) then
          write(*,*) 'Error in gtkillroot, carbostg(3,UNLABL) < 0.0'
          STOP
        endif
        if (carbostg(GTARYINDX,LABELD) .lt. 0.0) then
          write(*,*) 'Error in gtkillroot, carbostg(3,LABELD) < 0.0'
          STOP
        endif
        mRespStorage = carbostg(GTARYINDX,UNLABL)
     &                 + carbostg(GTARYINDX,LABELD)
        mRespStgLoss = max(fd(2) * mRespStorage, 0.0)
        carbostgloss = mRespStgLoss
        if (mRespStorage .gt. 0.001) then
c         write(*,*) 'gtkillroot: mRespStgLoss =', mRespStgLoss
          call csched(mRespStgLoss, carbostg(GTARYINDX,LABELD),
     &                mRespStorage,
     &                carbostg(GTARYINDX,UNLABL), csrsnk(UNLABL),
     &                carbostg(GTARYINDX,LABELD), csrsnk(LABELD),
     &                1.0, accum)
        endif

c ..... Remove from elemental STORAGE pool based on fraction of 
c ..... live coarse roots removed.

c       write(*,*)
c       write(*,*) 'Subroutine gtkillroot:'
c       write(*,*) 'gtstg(1) =', gtstg(1)

        do 40 iel = 1, nelem
          gtstgloss(iel) = max(fd(2) * gtstg(iel), 0.0)
c         write(*,*) 'gtkillroot: gtstgloss =', gtstgloss
          call flow(gtstg(iel),esrsnk(iel),time,gtstgloss(iel))
40      continue
      endif

      return
      end
