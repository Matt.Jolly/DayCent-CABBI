
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      gtleafa.f
c
c  FUNCTION:  subroutine gtleafa()
c
c  PURPOSE:   Dynamic allocation for a grasstree. Compute the fraction of 
c             production going to leaves based on water and 
c             nutrient availability.
c             
c  AUTHOR:    New function derived from leafa.
c             Melannie Hartman March 7, 2014
c
c  Called from:  grasstreegrowth
c
c  FUNCTION ARGUMENTS:
c    cprodgt    - total potential C production for all grasstree parts (gC/m^2)
c    cprodLeft  - amount of carbon still available for allocation (gC/m^2)
c    gtleavc    - tree leaf carbon (gC/m^2)
c    gtstemc    - live stem carbon (gC/m^2)
c
c  LOCAL VARIABLES:
c    woodc     - tree large wood carbon (gC/m^2)
c    leafprod   - amount of leaf production required to reach optimal LAI
c
c*****************************************************************************

      real function gtleafa(gtleavc, gtstemc, cprodLeft, cprodgt)

      implicit none
      include 'pargt.inc'

c ... Argument declarations
      real      gtleavc, gtstemc
      real      cprodLeft, cprodgt

c ... Local Variables
      real lai
      real leafprod
      real gtleavc_opt
      real lgwoodc

      if (gtleavc .lt. 0.0) then
        write(*,*) 'Error in gtleafa, gtleavc < 0.0'
        write(*,*) 'gtleavc = ', gtleavc
        STOP
      endif
      if (cprodLeft .lt. 0.0) then
        write(*,*) 'Error in gtleafa, cprodLeft < 0.0'
        STOP
      endif
      if (cprodgt .le. 0.0) then
        write(*,*) 'Error in gtleafa, cprodgt <= 0.0'
        STOP
      endif
      if (isnan(gtleavc)) then
        write(*,*) 'Error in gtleafa, gtleavc is NaN'
        STOP
      endif
      if (isnan(gtstemc)) then
        write(*,*) 'Error in gtleafa, gtstemc is NaN'
        STOP
      endif

c ... Calculate theoretical maximum for LAI based on large wood biomass,
c ... cak - 07/24/02

      lgwoodc = 0
      call lacalc(lai, gtstemc, lgwoodc, gtmaxlai, gtklai)
      if (lai .lt. 0.1) then
        lai = 0.1
      endif

      if (gtbtolai .le. 0.0) then
        write(*,*) 'Error in gtleafa, gtbtolai <= 0.0' 
        write(*,*) 'Check grasstree.100'
        STOP
      endif
c ... Calculate optimal gtleavc
      gtleavc_opt = lai / (gtbiomc * gtbtolai)

      if (gtleavc_opt .gt. gtleavc) then
c ..... if possible, increase leaf biomass so that optimum is reached
        leafprod = min((gtleavc_opt-gtleavc), cprodLeft)
      else
c .....  optimum leaf area has already been acheived
        leafprod = 0.0
      endif

      gtleafa = leafprod / cprodgt

c     write(*,*) 'gtleafa: lai =', lai
c     write(*,*) 'gtleafa: gtleavc_opt =', gtleavc_opt
c     write(*,*) 'gtleafa: gtleavc =', gtleavc

      if (gtleafa .lt. 0.0) then
        write(*,*) 'Error in gtleafa, gtleafa < 0.0'
        write(*,*) 'gtleafa = ', gtleafa
        STOP
      endif
      if (gtleafa .gt. 1.0) then
        write(*,*) 'Error in gtleafa, gtleafa > 1.0'
        write(*,*) 'gtleafa = ', gtleafa
        STOP
      endif
      if (isnan(gtleafa)) then
        write(*,*) 'Error in gtleafa, gtleafa is NaN'
        STOP
      endif

      return
      end
