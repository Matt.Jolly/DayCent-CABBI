
/*              Copyright 1993 Colorado State University                    */
/*                      All Rights Reserved                                 */

/*****************************************************************************
**
**  FILE:      cmpnfrac.c
**
**  FUNCTION:  void cmpnfrac()
**
**  PURPOSE:   This subroutine computes the relative fractions of ammonium 
**             and nitrate to be added/subtracted from the Nitrogen
**             pool.
**
**  INPUTS:
**    clyr      - Century soil mineral layer (1..nlayer) to which amt 
**                has been added.  Since this function will be called from 
**                FORTRAN, subtract 1 from this index.
**    ammonium  - total ammonium in soil mineral pool (gN/m2)
**    minerl[]  - soil minerl pool, 1..nlayer (g/m2)
**                Minerl is a 2-D array in FORTRAN.  FORTRAN stores multi-
**                dimensioned arrays in column-major order.  In this routine it
**                is a 1-D array, where minerl(clyr,iel) in FORTRAN is equivalent 
**                to minerl[(iel-1)*CMXLYR+(clyr-1)] in this routine.
**    nitrate[] - total nitrate in soil mineral pool, distributed among soil
**                water model layers (gN/m2)
**    no3pref   - preference for fraction of plant N uptake that is NO3.
**                This applies to top Century layer only (clyr=1) since below 
**                that layer only nitrate exists.
**
**  GLOBAL VARIABLES:
**    CMXLYR - maximum number of Century soil layers (10)
**
**  LOCAL VARIABLES:
**    iel  - current element, set to 0 for nitrogen
**    Nsum - total nitrogen in the top Century layer (~top 10-20 cm of soil)
**
**  OUTPUTS:
**    frac_nh4 - the relative fraction of ammonium added/subtracted from the
**               nitrogen pool
**    frac_no3 - the relative fraction of nitrate to be added/subtracted from
**               the nitrogen pool
**
**  CALLED BY:
**    growth()
**    partit()
**    treegrow()
**
**  CALLS:
**    None
**
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "swconst.h"
#include "soilwater.h"

    void cmpnfrac(int *clyr, double *ammonium, double nitrate[],
                  float minerl[], double *frac_nh4, double *frac_no3,
                  float *no3pref)
    {
      int iel;    /* Nitrogen */
      int ilyr;
      double Nsum;

      extern LAYERPAR_SPT layers;
 
      /* Initialization */
      iel = 0;

      /* Total Nitrogen in top Century layer (~top 10-20cm)        */
      /* We are no longer assuming that the top Century layers is  */
      /* composed of the top 3 soils.in layers. -mdh 6/13/2019     */
      /* Nsum = *ammonium + nitrate[0] + nitrate[1] + nitrate[2];  */
      Nsum = *ammonium;
      for(ilyr = layers->ubnd[0]; ilyr <= layers->lbnd[0]; ilyr++) {
        Nsum += nitrate[ilyr];
      }

      if (*clyr != 1) {
        *frac_nh4 = 0.0;
        *frac_no3 = 1.0;
      } else {
        if (*ammonium > Nsum) {
          *frac_nh4 = 1.0; 
        } else if (*ammonium > 0.0) {
/*          *frac_nh4 = *ammonium/(minerl[(iel)*CMXLYR + (*clyr-1)]); */
          *frac_nh4 = *ammonium/Nsum;
        } else {
          *frac_nh4 = 0.0;
        }
        if (*no3pref >= 0.0) {
          /* Using no3pref to determine fraction of NO3 and NH4 */
          /* Override previous calculation of frac_nh4. -mdh 6/8/2019 */
          /* Now update_npool will need to ensure that these fractions */
          /* do not cause either N pools to go negative during removals. */
          *frac_nh4 = 1.0 - *no3pref;
        }
      }
      if ((*frac_nh4 > 1.0 + 1.0E-04) || (*frac_nh4 < 0.0 - 1.0E-04)) {
        fprintf(stderr, "Error in cmpnfrac, frac_nh4 = %12.10f\n", *frac_nh4);
        fprintf(stderr, "clyr = %1d\n", *clyr);
        fprintf(stderr, "ammonium = %12.10f\n", *ammonium);
        fprintf(stderr, "Nsum = %12.10f\n", Nsum);
        fprintf(stderr, "minerl[%1d][1] = %12.10f\n",
                *clyr, minerl[(iel)*CMXLYR+(*clyr-1)]);
        exit(1);
      }
      
      *frac_nh4 = min(1.0, *frac_nh4);
      *frac_nh4 = max(0.0, *frac_nh4);
      *frac_no3 = 1.0 - *frac_nh4;

      return;
    }
