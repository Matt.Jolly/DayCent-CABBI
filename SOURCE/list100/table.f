      block data

      include 'table.inc'

      data tvals /487, 222, 139, 154/
      data table(  1,  1) /'agcacc'/
      data table(  1,  2) /'agcmth(1)'/
      data table(  1,  3) /'agcmth(2)'/
      data table(  1,  4) /'agcmth(3)'/
      data table(  1,  5) /'agcmth(4)'/
      data table(  1,  6) /'agcmth(5)'/
      data table(  1,  7) /'agcmth(6)'/
      data table(  1,  8) /'agcmth(7)'/
      data table(  1,  9) /'agcmth(8)'/
      data table(  1, 10) /'agcmth(9)'/
      data table(  1, 11) /'agcmth(10)'/
      data table(  1, 12) /'agcmth(11)'/
      data table(  1, 13) /'agcmth(12)'/
      data table(  1, 14) /'agcprd'/
      data table(  1, 15) /'agdefac'/
      data table(  1, 16) /'aglcis(1)'/
      data table(  1, 17) /'aglcis(2)'/
      data table(  1, 18) /'aglivc'/
      data table(  1, 19) /'aglive(1)'/
      data table(  1, 20) /'aglive(2)'/
      data table(  1, 21) /'aglive(3)'/
      data table(  1, 22) /'aminrl(1)'/
      data table(  1, 23) /'aminrl(2)'/
      data table(  1, 24) /'aminrl(3)'/
      data table(  1, 25) /'amt1c2'/
      data table(  1, 26) /'amt2c2'/
      data table(  1, 27) /'anerb'/
      data table(  1, 28) /'annet'/
      data table(  1, 29) /'arspmth(1,1)'/
      data table(  1, 30) /'arspmth(2,1)'/
      data table(  1, 31) /'arspmth(3,1)'/
      data table(  1, 32) /'arspmth(1,2)'/
      data table(  1, 33) /'arspmth(2,2)'/
      data table(  1, 34) /'arspmth(3,2)'/
      data table(  1, 35) /'as11c2'/
      data table(  1, 36) /'as12c2'/
      data table(  1, 37) /'as21c2'/
      data table(  1, 38) /'as22c2'/
      data table(  1, 39) /'as3c2'/
      data table(  1, 40) /'asmos(1)'/
      data table(  1, 41) /'asmos(2)'/
      data table(  1, 42) /'asmos(3)'/
      data table(  1, 43) /'asmos(4)'/
      data table(  1, 44) /'asmos(5)'/
      data table(  1, 45) /'asmos(6)'/
      data table(  1, 46) /'asmos(7)'/
      data table(  1, 47) /'asmos(8)'/
      data table(  1, 48) /'asmos(9)'/
      data table(  1, 49) /'asmos(10)'/
      data table(  1, 50) /'ast1c2'/
      data table(  1, 51) /'ast2c2'/
      data table(  1, 52) /'ast1uvc2'/
      data table(  1, 53) /'astduvc2'/
      data table(  1, 54) /'avh2o(1)'/
      data table(  1, 55) /'avh2o(2)'/
      data table(  1, 56) /'avh2o(3)'/
      data table(  1, 57) /'bgcjacc'/
      data table(  1, 58) /'bgcmacc'/
      data table(  1, 59) /'bgcjmth(1)'/
      data table(  1, 60) /'bgcjmth(2)'/
      data table(  1, 61) /'bgcjmth(3)'/
      data table(  1, 62) /'bgcjmth(4)'/
      data table(  1, 63) /'bgcjmth(5)'/
      data table(  1, 64) /'bgcjmth(6)'/
      data table(  1, 65) /'bgcjmth(7)'/
      data table(  1, 66) /'bgcjmth(8)'/
      data table(  1, 67) /'bgcjmth(9)'/
      data table(  1, 68) /'bgcjmth(10)'/
      data table(  1, 69) /'bgcjmth(11)'/
      data table(  1, 70) /'bgcjmth(12)'/
      data table(  1, 71) /'bgcmmth(1)'/
      data table(  1, 72) /'bgcmmth(2)'/
      data table(  1, 73) /'bgcmmth(3)'/
      data table(  1, 74) /'bgcmmth(4)'/
      data table(  1, 75) /'bgcmmth(5)'/
      data table(  1, 76) /'bgcmmth(6)'/
      data table(  1, 77) /'bgcmmth(7)'/
      data table(  1, 78) /'bgcmmth(8)'/
      data table(  1, 79) /'bgcmmth(9)'/
      data table(  1, 80) /'bgcmmth(10)'/
      data table(  1, 81) /'bgcmmth(11)'/
      data table(  1, 82) /'bgcmmth(12)'/
      data table(  1, 83) /'bgcjprd'/
      data table(  1, 84) /'bgcmprd'/
      data table(  1, 85) /'bgdefac'/
      data table(  1, 86) /'bglcisj(1)'/
      data table(  1, 87) /'bglcisj(2)'/
      data table(  1, 88) /'bglcism(1)'/
      data table(  1, 89) /'bglcism(2)'/
      data table(  1, 90) /'bglivcj'/
      data table(  1, 91) /'bglivcm'/
      data table(  1, 92) /'bglivej(1)'/
      data table(  1, 93) /'bglivej(2)'/
      data table(  1, 94) /'bglivej(3)'/
      data table(  1, 95) /'bglivem(1)'/
      data table(  1, 96) /'bglivem(2)'/
      data table(  1, 97) /'bglivem(3)'/
      data table(  1, 98) /'cautoresp(1)'/
      data table(  1, 99) /'cautoresp(2)'/
      data table(  1,100) /'cgrain'/
      data table(  1,101) /'cinput'/
      data table(  1,102) /'clittr(1,1)'/
      data table(  1,103) /'clittr(2,1)'/
      data table(  1,104) /'clittr(1,2)'/
      data table(  1,105) /'clittr(2,2)'/
      data table(  1,106) /'cgrspflux(1)'/
      data table(  1,107) /'cgrspflux(2)'/
      data table(  1,108) /'cgrspflux(3)'/
      data table(  1,109) /'cmrspflux(1)'/
      data table(  1,110) /'cmrspflux(2)'/
      data table(  1,111) /'cmrspflux(3)'/
      data table(  1,112) /'co2cce(1,1,1)'/
      data table(  1,113) /'co2cce(2,1,1)'/
      data table(  1,114) /'co2cce(3,1,1)'/
      data table(  1,115) /'co2cce(1,2,1)'/
      data table(  1,116) /'co2cce(2,2,1)'/
      data table(  1,117) /'co2cce(3,2,1)'/
      data table(  1,118) /'co2cce(1,1,2)'/
      data table(  1,119) /'co2cce(2,1,2)'/
      data table(  1,120) /'co2cce(3,1,2)'/
      data table(  1,121) /'co2cce(1,2,2)'/
      data table(  1,122) /'co2cce(2,2,2)'/
      data table(  1,123) /'co2cce(3,2,2)'/
      data table(  1,124) /'co2cce(1,1,3)'/
      data table(  1,125) /'co2cce(2,1,3)'/
      data table(  1,126) /'co2cce(3,1,3)'/
      data table(  1,127) /'co2cce(1,2,3)'/
      data table(  1,128) /'co2cce(2,2,3)'/
      data table(  1,129) /'co2cce(3,2,3)'/
      data table(  1,130) /'co2crs(1)'/
      data table(  1,131) /'co2crs(2)'/
      data table(  1,132) /'co2crs(3)'/
      data table(  1,133) /'co2cpr(1)'/
      data table(  1,134) /'co2cpr(2)'/
      data table(  1,135) /'co2cpr(3)'/
      data table(  1,136) /'co2ctr(1)'/
      data table(  1,137) /'co2ctr(2)'/
      data table(  1,138) /'co2ctr(3)'/
      data table(  1,139) /'cproda'/
      data table(  1,140) /'cprodc'/
      data table(  1,141) /'cprodf'/
      data table(  1,142) /'cprodgt'/
      data table(  1,143) /'creta'/
      data table(  1,144) /'crmvst'/
      data table(  1,145) /'crpstg(1)'/
      data table(  1,146) /'crpstg(2)'/
      data table(  1,147) /'crpstg(3)'/
      data table(  1,148) /'crpval'/
      data table(  1,149) /'dsomsc'/
      data table(  1,150) /'egrain(1)'/
      data table(  1,151) /'egrain(2)'/
      data table(  1,152) /'egrain(3)'/
      data table(  1,153) /'elimit'/
      data table(  1,154) /'eprodc(1)'/
      data table(  1,155) /'eprodc(2)'/
      data table(  1,156) /'eprodc(3)'/
      data table(  1,157) /'eprodf(1)'/
      data table(  1,158) /'eprodf(2)'/
      data table(  1,159) /'eprodf(3)'/
      data table(  1,160) /'eprodgt(1)'/
      data table(  1,161) /'eprodgt(2)'/
      data table(  1,162) /'eprodgt(3)'/
      data table(  1,163) /'ermvst(1)'/
      data table(  1,164) /'ermvst(2)'/
      data table(  1,165) /'ermvst(3)'/
      data table(  1,166) /'eupacc(1)'/
      data table(  1,167) /'eupacc(2)'/
      data table(  1,168) /'eupacc(3)'/
      data table(  1,169) /'eupaga(1)'/
      data table(  1,170) /'eupaga(2)'/
      data table(  1,171) /'eupaga(3)'/
      data table(  1,172) /'eupbga(1)'/
      data table(  1,173) /'eupbga(2)'/
      data table(  1,174) /'eupbga(3)'/
      data table(  1,175) /'eupprd(1)'/
      data table(  1,176) /'eupprd(2)'/
      data table(  1,177) /'eupprd(3)'/
      data table(  1,178) /'evap'/
      data table(  1,179) /'fautoresp(1)'/
      data table(  1,180) /'fautoresp(2)'/
      data table(  1,181) /'fertac(1)'/
      data table(  1,182) /'fertac(2)'/
      data table(  1,183) /'fertac(3)'/
      data table(  1,184) /'fertmth(1,1)'/
      data table(  1,185) /'fertmth(2,1)'/
      data table(  1,186) /'fertmth(3,1)'/
      data table(  1,187) /'fertmth(4,1)'/
      data table(  1,188) /'fertmth(5,1)'/
      data table(  1,189) /'fertmth(6,1)'/
      data table(  1,190) /'fertmth(7,1)'/
      data table(  1,191) /'fertmth(8,1)'/
      data table(  1,192) /'fertmth(9,1)'/
      data table(  1,193) /'fertmth(10,1)'/
      data table(  1,194) /'fertmth(11,1)'/
      data table(  1,195) /'fertmth(12,1)'/
      data table(  1,196) /'fertmth(1,2)'/
      data table(  1,197) /'fertmth(2,2)'/
      data table(  1,198) /'fertmth(3,2)'/
      data table(  1,199) /'fertmth(4,2)'/
      data table(  1,200) /'fertmth(5,2)'/
      data table(  1,201) /'fertmth(6,2)'/
      data table(  1,202) /'fertmth(7,2)'/
      data table(  1,203) /'fertmth(8,2)'/
      data table(  1,204) /'fertmth(9,2)'/
      data table(  1,205) /'fertmth(10,2)'/
      data table(  1,206) /'fertmth(11,2)'/
      data table(  1,207) /'fertmth(12,2)'/
      data table(  1,208) /'fertmth(1,3)'/
      data table(  1,209) /'fertmth(2,3)'/
      data table(  1,210) /'fertmth(3,3)'/
      data table(  1,211) /'fertmth(4,3)'/
      data table(  1,212) /'fertmth(5,3)'/
      data table(  1,213) /'fertmth(6,3)'/
      data table(  1,214) /'fertmth(7,3)'/
      data table(  1,215) /'fertmth(8,3)'/
      data table(  1,216) /'fertmth(9,3)'/
      data table(  1,217) /'fertmth(10,3)'/
      data table(  1,218) /'fertmth(11,3)'/
      data table(  1,219) /'fertmth(12,3)'/
      data table(  1,220) /'fertot(1)'/
      data table(  1,221) /'fertot(2)'/
      data table(  1,222) /'fertot(3)'/
      data table(  1,223) /'fertprd(1)'/
      data table(  1,224) /'fertprd(2)'/
      data table(  1,225) /'fertprd(3)'/
      data table(  1,226) /'fgrspflux(1)'/
      data table(  1,227) /'fgrspflux(2)'/
      data table(  1,228) /'fgrspflux(3)'/
      data table(  1,229) /'fgrspflux(4)'/
      data table(  1,230) /'fgrspflux(5)'/
      data table(  1,231) /'fgrspflux(6)'/
      data table(  1,232) /'fmrspflux(1)'/
      data table(  1,233) /'fmrspflux(2)'/
      data table(  1,234) /'fmrspflux(3)'/
      data table(  1,235) /'fmrspflux(4)'/
      data table(  1,236) /'fmrspflux(5)'/
      data table(  1,237) /'fmrspflux(6)'/
      data table(  1,238) /'grspann(1)'/
      data table(  1,239) /'grspann(2)'/
      data table(  1,240) /'grspann(3)'/
      data table(  1,241) /'grspflux(1)'/
      data table(  1,242) /'grspflux(2)'/
      data table(  1,243) /'grspflux(3)'/
      data table(  1,244) /'grspmth(1)'/
      data table(  1,245) /'grspmth(2)'/
      data table(  1,246) /'grspmth(3)'/
      data table(  1,247) /'gtautoresp(1)'/
      data table(  1,248) /'gtautoresp(2)'/
      data table(  1,249) /'gtgrspflux(1)'/
      data table(  1,250) /'gtgrspflux(2)'/
      data table(  1,251) /'gtgrspflux(3)'/
      data table(  1,252) /'gtgrspflux(4)'/
      data table(  1,253) /'gtgrspflux(5)'/
      data table(  1,254) /'gtmrspflux(1)'/
      data table(  1,255) /'gtmrspflux(2)'/
      data table(  1,256) /'gtmrspflux(3)'/
      data table(  1,257) /'gtmrspflux(4)'/
      data table(  1,258) /'gtmrspflux(5)'/
      data table(  1,259) /'harmth'/
      data table(  1,260) /'hi'/
      data table(  1,261) /'irract'/
      data table(  1,262) /'irrtot'/
      data table(  1,263) /'metabc(1)'/
      data table(  1,264) /'metabc(2)'/
      data table(  1,265) /'metabe(1,1)'/
      data table(  1,266) /'metabe(2,1)'/
      data table(  1,267) /'metabe(1,2)'/
      data table(  1,268) /'metabe(2,2)'/
      data table(  1,269) /'metabe(1,3)'/
      data table(  1,270) /'metabe(2,3)'/
      data table(  1,271) /'metcis(1,1)'/
      data table(  1,272) /'metcis(2,1)'/
      data table(  1,273) /'metcis(1,2)'/
      data table(  1,274) /'metcis(2,2)'/
      data table(  1,275) /'minerl(1,1)'/
      data table(  1,276) /'minerl(2,1)'/
      data table(  1,277) /'minerl(3,1)'/
      data table(  1,278) /'minerl(4,1)'/
      data table(  1,279) /'minerl(5,1)'/
      data table(  1,280) /'minerl(6,1)'/
      data table(  1,281) /'minerl(7,1)'/
      data table(  1,282) /'minerl(8,1)'/
      data table(  1,283) /'minerl(9,1)'/
      data table(  1,284) /'minerl(10,1)'/
      data table(  1,285) /'minerl(1,2)'/
      data table(  1,286) /'minerl(2,2)'/
      data table(  1,287) /'minerl(3,2)'/
      data table(  1,288) /'minerl(4,2)'/
      data table(  1,289) /'minerl(5,2)'/
      data table(  1,290) /'minerl(6,2)'/
      data table(  1,291) /'minerl(7,2)'/
      data table(  1,292) /'minerl(8,2)'/
      data table(  1,293) /'minerl(9,2)'/
      data table(  1,294) /'minerl(10,2)'/
      data table(  1,295) /'minerl(1,3)'/
      data table(  1,296) /'minerl(2,3)'/
      data table(  1,297) /'minerl(3,3)'/
      data table(  1,298) /'minerl(4,3)'/
      data table(  1,299) /'minerl(5,3)'/
      data table(  1,300) /'minerl(6,3)'/
      data table(  1,301) /'minerl(7,3)'/
      data table(  1,302) /'minerl(8,3)'/
      data table(  1,303) /'minerl(9,3)'/
      data table(  1,304) /'minerl(10,3)'/
      data table(  1,305) /'mrspann(1)'/
      data table(  1,306) /'mrspann(2)'/
      data table(  1,307) /'mrspann(3)'/
      data table(  1,308) /'mrspflux(1)'/
      data table(  1,309) /'mrspflux(2)'/
      data table(  1,310) /'mrspflux(3)'/
      data table(  1,311) /'mrspmth(1)'/
      data table(  1,312) /'mrspmth(2)'/
      data table(  1,313) /'mrspmth(3)'/
      data table(  1,314) /'mt1c2(1)'/
      data table(  1,315) /'mt1c2(2)'/
      data table(  1,316) /'mt2c2(1)'/
      data table(  1,317) /'mt2c2(2)'/
      data table(  1,318) /'nfix'/
      data table(  1,319) /'nfixac'/
      data table(  1,320) /'occlud'/
      data table(  1,321) /'parent(1)'/
      data table(  1,322) /'parent(2)'/
      data table(  1,323) /'parent(3)'/
      data table(  1,324) /'pet'/
      data table(  1,325) /'petann'/
      data table(  1,326) /'plabil'/
      data table(  1,327) /'prcann'/
      data table(  1,328) /'ptagc'/
      data table(  1,329) /'ptbgc'/
      data table(  1,330) /'pttr'/
      data table(  1,331) /'rain'/
      data table(  1,332) /'relyld'/
      data table(  1,333) /'resp(1)'/
      data table(  1,334) /'resp(2)'/
      data table(  1,335) /'respmth(1)'/
      data table(  1,336) /'respmth(2)'/
      data table(  1,337) /'runoff'/
      data table(  1,338) /'rwcf(1)'/
      data table(  1,339) /'rwcf(2)'/
      data table(  1,340) /'rwcf(3)'/
      data table(  1,341) /'rwcf(4)'/
      data table(  1,342) /'rwcf(5)'/
      data table(  1,343) /'rwcf(6)'/
      data table(  1,344) /'rwcf(7)'/
      data table(  1,345) /'rwcf(8)'/
      data table(  1,346) /'rwcf(9)'/
      data table(  1,347) /'rwcf(10)'/
      data table(  1,348) /'s11c2(1)'/
      data table(  1,349) /'s11c2(2)'/
      data table(  1,350) /'s12c2(1)'/
      data table(  1,351) /'s12c2(2)'/
      data table(  1,352) /'s21c2(1)'/
      data table(  1,353) /'s21c2(2)'/
      data table(  1,354) /'s22c2(1)'/
      data table(  1,355) /'s22c2(2)'/
      data table(  1,356) /'s3c2(1)'/
      data table(  1,357) /'s3c2(2)'/
      data table(  1,358) /'satmac'/
      data table(  1,359) /'sclosa'/
      data table(  1,360) /'scloss'/
      data table(  1,361) /'sdrema'/
      data table(  1,362) /'secndy(1)'/
      data table(  1,363) /'secndy(2)'/
      data table(  1,364) /'secndy(3)'/
      data table(  1,365) /'shrema'/
      data table(  1,366) /'sirrac'/
      data table(  1,367) /'snfxac(1)'/
      data table(  1,368) /'snfxac(2)'/
      data table(  1,369) /'snfxac(3)'/
      data table(  1,370) /'snlq'/
      data table(  1,371) /'snow'/
      data table(  1,372) /'soilnm(1)'/
      data table(  1,373) /'soilnm(2)'/
      data table(  1,374) /'soilnm(3)'/
      data table(  1,375) /'somsc'/
      data table(  1,376) /'somse(1)'/
      data table(  1,377) /'somse(2)'/
      data table(  1,378) /'somse(3)'/
      data table(  1,379) /'somtc'/
      data table(  1,380) /'som1c(1)'/
      data table(  1,381) /'som1c(2)'/
      data table(  1,382) /'som1ci(1,1)'/
      data table(  1,383) /'som1ci(2,1)'/
      data table(  1,384) /'som1ci(1,2)'/
      data table(  1,385) /'som1ci(2,2)'/
      data table(  1,386) /'som1e(1,1)'/
      data table(  1,387) /'som1e(2,1)'/
      data table(  1,388) /'som1e(1,2)'/
      data table(  1,389) /'som1e(2,2)'/
      data table(  1,390) /'som1e(1,3)'/
      data table(  1,391) /'som1e(2,3)'/
      data table(  1,392) /'som2c(1)'/
      data table(  1,393) /'som2c(2)'/
      data table(  1,394) /'som2ci(1,1)'/
      data table(  1,395) /'som2ci(2,1)'/
      data table(  1,396) /'som2ci(1,2)'/
      data table(  1,397) /'som2ci(2,2)'/
      data table(  1,398) /'som2e(1,1)'/
      data table(  1,399) /'som2e(2,1)'/
      data table(  1,400) /'som2e(1,2)'/
      data table(  1,401) /'som2e(2,2)'/
      data table(  1,402) /'som2e(1,3)'/
      data table(  1,403) /'som2e(2,3)'/
      data table(  1,404) /'som3c'/
      data table(  1,405) /'som3ci(1)'/
      data table(  1,406) /'som3ci(2)'/
      data table(  1,407) /'som3e(1)'/
      data table(  1,408) /'som3e(2)'/
      data table(  1,409) /'som3e(3)'/
      data table(  1,410) /'srspann(1)'/
      data table(  1,411) /'srspann(2)'/
      data table(  1,412) /'srspann(3)'/
      data table(  1,413) /'srspmth(1)'/
      data table(  1,414) /'srspmth(2)'/
      data table(  1,415) /'srspmth(3)'/
      data table(  1,416) /'stdcis(1)'/
      data table(  1,417) /'stdcis(2)'/
      data table(  1,418) /'st1c2(1)'/
      data table(  1,419) /'st1c2(2)'/
      data table(  1,420) /'st2c2(1)'/
      data table(  1,421) /'st2c2(2)'/
      data table(  1,422) /'st1uvc2(1)'/
      data table(  1,423) /'st1uvc2(2)'/
      data table(  1,424) /'stemp'/
      data table(  1,425) /'strcis(1,1)'/
      data table(  1,426) /'strcis(2,1)'/
      data table(  1,427) /'strcis(1,2)'/
      data table(  1,428) /'strcis(2,2)'/
      data table(  1,429) /'stream(1)'/
      data table(  1,430) /'stream(2)'/
      data table(  1,431) /'stream(3)'/
      data table(  1,432) /'stream(4)'/
      data table(  1,433) /'stream(5)'/
      data table(  1,434) /'stream(6)'/
      data table(  1,435) /'stream(7)'/
      data table(  1,436) /'stream(8)'/
      data table(  1,437) /'strlig(1)'/
      data table(  1,438) /'strlig(2)'/
      data table(  1,439) /'strmac(1)'/
      data table(  1,440) /'strmac(2)'/
      data table(  1,441) /'strmac(3)'/
      data table(  1,442) /'strmac(4)'/
      data table(  1,443) /'strmac(5)'/
      data table(  1,444) /'strmac(6)'/
      data table(  1,445) /'strmac(7)'/
      data table(  1,446) /'strmac(8)'/
      data table(  1,447) /'strucc(1)'/
      data table(  1,448) /'strucc(2)'/
      data table(  1,449) /'struce(1,1)'/
      data table(  1,450) /'struce(2,1)'/
      data table(  1,451) /'struce(1,2)'/
      data table(  1,452) /'struce(2,2)'/
      data table(  1,453) /'struce(1,3)'/
      data table(  1,454) /'struce(2,3)'/
      data table(  1,455) /'sumnrs(1)'/
      data table(  1,456) /'sumnrs(2)'/
      data table(  1,457) /'sumnrs(3)'/
      data table(  1,458) /'stdedc'/
      data table(  1,459) /'stdede(1)'/
      data table(  1,460) /'stdede(2)'/
      data table(  1,461) /'stdede(3)'/
      data table(  1,462) /'stduvc2(1)'/
      data table(  1,463) /'stduvc2(2)'/
      data table(  1,464) /'tave'/
      data table(  1,465) /'tlittr(1,1)'/
      data table(  1,466) /'tlittr(2,1)'/
      data table(  1,467) /'tlittr(1,2)'/
      data table(  1,468) /'tlittr(2,2)'/
      data table(  1,469) /'tminrl(1)'/
      data table(  1,470) /'tminrl(2)'/
      data table(  1,471) /'tminrl(3)'/
      data table(  1,472) /'tnetmn(1)'/
      data table(  1,473) /'tnetmn(2)'/
      data table(  1,474) /'tnetmn(3)'/
      data table(  1,475) /'totc'/
      data table(  1,476) /'tran'/
      data table(  1,477) /'volgma'/
      data table(  1,478) /'volexa'/
      data table(  1,479) /'volpla'/
      data table(  1,480) /'wd1c2(1)'/
      data table(  1,481) /'wd1c2(2)'/
      data table(  1,482) /'wd2c2(1)'/
      data table(  1,483) /'wd2c2(2)'/
      data table(  1,484) /'wd3c2(1)'/
      data table(  1,485) /'wd3c2(2)'/
      data table(  1,486) /'wdfxaa'/
      data table(  1,487) /'wdfxas'/

      data table(  2,  1) /'aagdefac'/
      data table(  2,  2) /'abgdefac'/
      data table(  2,  3) /'accrst'/
      data table(  2,  4) /'accrste(1)'/
      data table(  2,  5) /'accrste(2)'/
      data table(  2,  6) /'accrste(3)'/
      data table(  2,  7) /'agcisa(1)'/
      data table(  2,  8) /'agcisa(2)'/
      data table(  2,  9) /'aglcn'/
      data table(  2, 10) /'bgcisja(1)'/
      data table(  2, 11) /'bgcisja(2)'/
      data table(  2, 12) /'bgcisma(1)'/
      data table(  2, 13) /'bgcisma(2)'/
      data table(  2, 14) /'bglcnj'/
      data table(  2, 15) /'bglcnm'/
      data table(  2, 16) /'carbostg(1,1)'/
      data table(  2, 17) /'carbostg(2,1)'/
      data table(  2, 18) /'carbostg(3,1)'/
      data table(  2, 19) /'carbostg(1,2)'/
      data table(  2, 20) /'carbostg(2,2)'/
      data table(  2, 21) /'carbostg(3,2)'/
      data table(  2, 22) /'cgracc'/
      data table(  2, 23) /'cisgra(1)'/
      data table(  2, 24) /'cisgra(2)'/
      data table(  2, 25) /'cltfac(1)'/
      data table(  2, 26) /'cltfac(2)'/
      data table(  2, 27) /'cltfac(3)'/
      data table(  2, 28) /'cltfac(4)'/
      data table(  2, 29) /'csrsnk(1)'/
      data table(  2, 30) /'csrsnk(2)'/
      data table(  2, 31) /'dautoresp(1)'/
      data table(  2, 32) /'dautoresp(2)'/
      data table(  2, 33) /'dautoresp(3)'/
      data table(  2, 34) /'dbglivc'/
      data table(  2, 35) /'dbglivcj'/
      data table(  2, 36) /'dbglivcm'/
      data table(  2, 37) /'dblit'/
      data table(  2, 38) /'dcarbostg(1)'/
      data table(  2, 39) /'dcarbostg(2)'/
      data table(  2, 40) /'dcarbostg(3)'/
      data table(  2, 41) /'deloe'/
      data table(  2, 42) /'deloi'/
      data table(  2, 43) /'dfrootc'/
      data table(  2, 44) /'dfrootcj'/
      data table(  2, 45) /'dfrootcm'/
      data table(  2, 46) /'dmetc(1)'/
      data table(  2, 47) /'dmetc(2)'/
      data table(  2, 48) /'dhetresp'/
      data table(  2, 49) /'dslit'/
      data table(  2, 50) /'dsoilresp'/
      data table(  2, 51) /'dsom1c(1)'/
      data table(  2, 52) /'dsom1c(2)'/
      data table(  2, 53) /'dsom2c(1)'/
      data table(  2, 54) /'dsom2c(2)'/
      data table(  2, 55) /'dsom3c'/
      data table(  2, 56) /'dsomtc'/
      data table(  2, 57) /'dstruc(1)'/
      data table(  2, 58) /'dstruc(2)'/
      data table(  2, 59) /'egracc(1)'/
      data table(  2, 60) /'egracc(2)'/
      data table(  2, 61) /'egracc(3)'/
      data table(  2, 62) /'ereta(1)'/
      data table(  2, 63) /'ereta(2)'/
      data table(  2, 64) /'ereta(3)'/
      data table(  2, 65) /'esrsnk(1)'/
      data table(  2, 66) /'esrsnk(2)'/
      data table(  2, 67) /'esrsnk(3)'/
      data table(  2, 68) /'gromin(1)'/
      data table(  2, 69) /'gromin(2)'/
      data table(  2, 70) /'gromin(3)'/
      data table(  2, 71) /'lhzcac'/
      data table(  2, 72) /'lhzeac(1)'/
      data table(  2, 73) /'lhzeac(2)'/
      data table(  2, 74) /'lhzeac(3)'/
      data table(  2, 75) /'metmnr(1,1)'/
      data table(  2, 76) /'metmnr(2,1)'/
      data table(  2, 77) /'metmnr(1,2)'/
      data table(  2, 78) /'metmnr(2,2)'/
      data table(  2, 79) /'metmnr(1,3)'/
      data table(  2, 80) /'metmnr(2,3)'/
      data table(  2, 81) /'n2oacc'/
      data table(  2, 82) /'n2omth(1)'/
      data table(  2, 83) /'n2omth(2)'/
      data table(  2, 84) /'n2omth(3)'/
      data table(  2, 85) /'n2omth(4)'/
      data table(  2, 86) /'n2omth(5)'/
      data table(  2, 87) /'n2omth(6)'/
      data table(  2, 88) /'n2omth(7)'/
      data table(  2, 89) /'n2omth(8)'/
      data table(  2, 90) /'n2omth(9)'/
      data table(  2, 91) /'n2omth(10)'/
      data table(  2, 92) /'n2omth(11)'/
      data table(  2, 93) /'n2omth(12)'/
      data table(  2, 94) /'n2oprd'/
      data table(  2, 95) /'omadac'/
      data table(  2, 96) /'omadae(1)'/
      data table(  2, 97) /'omadae(2)'/
      data table(  2, 98) /'omadae(3)'/
      data table(  2, 99) /'omadmte(1,1)'/
      data table(  2,100) /'omadmte(2,1)'/
      data table(  2,101) /'omadmte(3,1)'/
      data table(  2,102) /'omadmte(4,1)'/
      data table(  2,103) /'omadmte(5,1)'/
      data table(  2,104) /'omadmte(6,1)'/
      data table(  2,105) /'omadmte(7,1)'/
      data table(  2,106) /'omadmte(8,1)'/
      data table(  2,107) /'omadmte(9,1)'/
      data table(  2,108) /'omadmte(10,1)'/
      data table(  2,109) /'omadmte(11,1)'/
      data table(  2,110) /'omadmte(12,1)'/
      data table(  2,111) /'omadmte(1,2)'/
      data table(  2,112) /'omadmte(2,2)'/
      data table(  2,113) /'omadmte(3,2)'/
      data table(  2,114) /'omadmte(4,2)'/
      data table(  2,115) /'omadmte(5,2)'/
      data table(  2,116) /'omadmte(6,2)'/
      data table(  2,117) /'omadmte(7,2)'/
      data table(  2,118) /'omadmte(8,2)'/
      data table(  2,119) /'omadmte(9,2)'/
      data table(  2,120) /'omadmte(10,2)'/
      data table(  2,121) /'omadmte(11,2)'/
      data table(  2,122) /'omadmte(12,2)'/
      data table(  2,123) /'omadmte(1,3)'/
      data table(  2,124) /'omadmte(2,3)'/
      data table(  2,125) /'omadmte(3,3)'/
      data table(  2,126) /'omadmte(4,3)'/
      data table(  2,127) /'omadmte(5,3)'/
      data table(  2,128) /'omadmte(6,3)'/
      data table(  2,129) /'omadmte(7,3)'/
      data table(  2,130) /'omadmte(8,3)'/
      data table(  2,131) /'omadmte(9,3)'/
      data table(  2,132) /'omadmte(10,3)'/
      data table(  2,133) /'omadmte(11,3)'/
      data table(  2,134) /'omadmte(12,3)'/
      data table(  2,135) /'omadmth(1)'/
      data table(  2,136) /'omadmth(2)'/
      data table(  2,137) /'omadmth(3)'/
      data table(  2,138) /'omadmth(4)'/
      data table(  2,139) /'omadmth(5)'/
      data table(  2,140) /'omadmth(6)'/
      data table(  2,141) /'omadmth(7)'/
      data table(  2,142) /'omadmth(8)'/
      data table(  2,143) /'omadmth(9)'/
      data table(  2,144) /'omadmth(10)'/
      data table(  2,145) /'omadmth(11)'/
      data table(  2,146) /'omadmth(12)'/
      data table(  2,147) /'omadprd'/
      data table(  2,148) /'omadpre(1)'/
      data table(  2,149) /'omadpre(2)'/
      data table(  2,150) /'omadpre(3)'/
      data table(  2,151) /'omadtot'/
      data table(  2,152) /'omaetot(1)'/
      data table(  2,153) /'omaetot(2)'/
      data table(  2,154) /'omaetot(3)'/
      data table(  2,155) /'prcfal'/
      data table(  2,156) /'rnpml1'/
      data table(  2,157) /'sdrmae(1)'/
      data table(  2,158) /'sdrmae(2)'/
      data table(  2,159) /'sdrmae(3)'/
      data table(  2,160) /'sdrmai(1)'/
      data table(  2,161) /'sdrmai(2)'/
      data table(  2,162) /'shrmai(1)'/
      data table(  2,163) /'shrmai(2)'/
      data table(  2,164) /'shrmae(1)'/
      data table(  2,165) /'shrmae(2)'/
      data table(  2,166) /'shrmae(3)'/
      data table(  2,167) /'somsci(1)'/
      data table(  2,168) /'somsci(2)'/
      data table(  2,169) /'somtci(1)'/
      data table(  2,170) /'somtci(2)'/
      data table(  2,171) /'somte(1)'/
      data table(  2,172) /'somte(2)'/
      data table(  2,173) /'somte(3)'/
      data table(  2,174) /'strmnr(1,1)'/
      data table(  2,175) /'strmnr(2,1)'/
      data table(  2,176) /'strmnr(1,2)'/
      data table(  2,177) /'strmnr(2,2)'/
      data table(  2,178) /'strmnr(1,3)'/
      data table(  2,179) /'strmnr(2,3)'/
      data table(  2,180) /'s1mnr(1,1)'/
      data table(  2,181) /'s1mnr(2,1)'/
      data table(  2,182) /'s1mnr(1,2)'/
      data table(  2,183) /'s1mnr(2,2)'/
      data table(  2,184) /'s1mnr(1,3)'/
      data table(  2,185) /'s1mnr(2,3)'/
      data table(  2,186) /'s2mnr(1,1)'/
      data table(  2,187) /'s2mnr(2,1)'/
      data table(  2,188) /'s2mnr(1,2)'/
      data table(  2,189) /'s2mnr(2,2)'/
      data table(  2,190) /'s2mnr(1,3)'/
      data table(  2,191) /'s2mnr(2,3)'/
      data table(  2,192) /'s3mnr(1)'/
      data table(  2,193) /'s3mnr(2)'/
      data table(  2,194) /'s3mnr(3)'/
      data table(  2,195) /'tcerat(1)'/
      data table(  2,196) /'tcerat(2)'/
      data table(  2,197) /'tcerat(3)'/
      data table(  2,198) /'tcnpro'/
      data table(  2,199) /'tgzrte(1)'/
      data table(  2,200) /'tgzrte(2)'/
      data table(  2,201) /'tgzrte(3)'/
      data table(  2,202) /'tomres(1)'/
      data table(  2,203) /'tomres(2)'/
      data table(  2,204) /'totalc'/
      data table(  2,205) /'totale(1)'/
      data table(  2,206) /'totale(2)'/
      data table(  2,207) /'totale(3)'/
      data table(  2,208) /'totsysc'/
      data table(  2,209) /'totsyse(1)'/
      data table(  2,210) /'totsyse(2)'/
      data table(  2,211) /'totsyse(3)'/
      data table(  2,212) /'voleac'/
      data table(  2,213) /'volex'/
      data table(  2,214) /'volgac'/
      data table(  2,215) /'volgm'/
      data table(  2,216) /'volpac'/
      data table(  2,217) /'volpl'/
      data table(  2,218) /'wdfx'/
      data table(  2,219) /'wdfxa'/
      data table(  2,220) /'wdfxma'/
      data table(  2,221) /'wdfxms'/
      data table(  2,222) /'wdfxs'/
      data table(  3,  1) /'acrcis(1)'/
      data table(  3,  2) /'acrcis(2)'/
      data table(  3,  3) /'afbcis(1)'/
      data table(  3,  4) /'afbcis(2)'/
      data table(  3,  5) /'afrcisj(1)'/
      data table(  3,  6) /'afrcisj(2)'/
      data table(  3,  7) /'afrcism(1)'/
      data table(  3,  8) /'afrcism(2)'/
      data table(  3,  9) /'alvcis(1)'/
      data table(  3, 10) /'alvcis(2)'/
      data table(  3, 11) /'alwcis(1)'/
      data table(  3, 12) /'alwcis(2)'/
      data table(  3, 13) /'crootc'/
      data table(  3, 14) /'croote(1)'/
      data table(  3, 15) /'croote(2)'/
      data table(  3, 16) /'croote(3)'/
      data table(  3, 17) /'crtacc'/
      data table(  3, 18) /'crtcis(1)'/
      data table(  3, 19) /'crtcis(2)'/
      data table(  3, 20) /'crtprd'/
      data table(  3, 21) /'eupprt(1,1)'/
      data table(  3, 22) /'eupprt(2,1)'/
      data table(  3, 23) /'eupprt(3,1)'/
      data table(  3, 24) /'eupprt(4,1)'/
      data table(  3, 25) /'eupprt(5,1)'/
      data table(  3, 26) /'eupprt(1,2)'/
      data table(  3, 27) /'eupprt(2,2)'/
      data table(  3, 28) /'eupprt(3,2)'/
      data table(  3, 29) /'eupprt(4,2)'/
      data table(  3, 30) /'eupprt(5,2)'/
      data table(  3, 31) /'eupprt(1,3)'/
      data table(  3, 32) /'eupprt(2,3)'/
      data table(  3, 33) /'eupprt(3,3)'/
      data table(  3, 34) /'eupprt(4,3)'/
      data table(  3, 35) /'eupprt(5,3)'/
      data table(  3, 36) /'fbrchc'/
      data table(  3, 37) /'fbrche(1)'/
      data table(  3, 38) /'fbrche(2)'/
      data table(  3, 39) /'fbrche(3)'/
      data table(  3, 40) /'fbracc'/
      data table(  3, 41) /'fbrcis(1)'/
      data table(  3, 42) /'fbrcis(2)'/
      data table(  3, 43) /'fbrprd'/
      data table(  3, 44) /'fcacc'/
      data table(  3, 45) /'fcmth(1)'/
      data table(  3, 46) /'fcmth(2)'/
      data table(  3, 47) /'fcmth(3)'/
      data table(  3, 48) /'fcmth(4)'/
      data table(  3, 49) /'fcmth(5)'/
      data table(  3, 50) /'fcmth(6)'/
      data table(  3, 51) /'fcmth(7)'/
      data table(  3, 52) /'fcmth(8)'/
      data table(  3, 53) /'fcmth(9)'/
      data table(  3, 54) /'fcmth(10)'/
      data table(  3, 55) /'fcmth(11)'/
      data table(  3, 56) /'fcmth(12)'/
      data table(  3, 57) /'fcprd'/
      data table(  3, 58) /'forstg(1)'/
      data table(  3, 59) /'forstg(2)'/
      data table(  3, 60) /'forstg(3)'/
      data table(  3, 61) /'frootcj'/
      data table(  3, 62) /'frootcm'/
      data table(  3, 63) /'frootej(1)'/
      data table(  3, 64) /'frootej(2)'/
      data table(  3, 65) /'frootej(3)'/
      data table(  3, 66) /'frootem(1)'/
      data table(  3, 67) /'frootem(2)'/
      data table(  3, 68) /'frootem(3)'/
      data table(  3, 69) /'frtjacc'/
      data table(  3, 70) /'frtmacc'/
      data table(  3, 71) /'frtcisj(1)'/
      data table(  3, 72) /'frtcisj(2)'/
      data table(  3, 73) /'frtcism(1)'/
      data table(  3, 74) /'frtcism(2)'/
      data table(  3, 75) /'frtjprd'/
      data table(  3, 76) /'frtmprd'/
      data table(  3, 77) /'frstc'/
      data table(  3, 78) /'frste(1)'/
      data table(  3, 79) /'frste(2)'/
      data table(  3, 80) /'frste(3)'/
      data table(  3, 81) /'fsysc'/
      data table(  3, 82) /'fsyse(1)'/
      data table(  3, 83) /'fsyse(2)'/
      data table(  3, 84) /'fsyse(3)'/
      data table(  3, 85) /'rleavc'/
      data table(  3, 86) /'rleave(1)'/
      data table(  3, 87) /'rleave(2)'/
      data table(  3, 88) /'rleave(3)'/
      data table(  3, 89) /'rlvacc'/
      data table(  3, 90) /'rlvcis(1)'/
      data table(  3, 91) /'rlvcis(2)'/
      data table(  3, 92) /'rlvprd'/
      data table(  3, 93) /'rlwodc'/
      data table(  3, 94) /'rlwode(1)'/
      data table(  3, 95) /'rlwode(2)'/
      data table(  3, 96) /'rlwode(3)'/
      data table(  3, 97) /'rlwacc'/
      data table(  3, 98) /'rlwcis(1)'/
      data table(  3, 99) /'rlwcis(2)'/
      data table(  3,100) /'rlwprd'/
      data table(  3,101) /'sumrsp'/
      data table(  3,102) /'tcrem'/
      data table(  3,103) /'terem(1)'/
      data table(  3,104) /'terem(2)'/
      data table(  3,105) /'terem(3)'/
      data table(  3,106) /'w1lig'/
      data table(  3,107) /'w2lig'/
      data table(  3,108) /'w3lig'/
      data table(  3,109) /'w1mnr(1)'/
      data table(  3,110) /'w1mnr(2)'/
      data table(  3,111) /'w1mnr(3)'/
      data table(  3,112) /'w2mnr(1)'/
      data table(  3,113) /'w2mnr(2)'/
      data table(  3,114) /'w2mnr(3)'/
      data table(  3,115) /'w3mnr(1)'/
      data table(  3,116) /'w3mnr(2)'/
      data table(  3,117) /'w3mnr(3)'/
      data table(  3,118) /'wd1cis(1)'/
      data table(  3,119) /'wd1cis(2)'/
      data table(  3,120) /'wd2cis(1)'/
      data table(  3,121) /'wd2cis(2)'/
      data table(  3,122) /'wd3cis(1)'/
      data table(  3,123) /'wd3cis(2)'/
      data table(  3,124) /'wood1c'/
      data table(  3,125) /'wood2c'/
      data table(  3,126) /'wood3c'/
      data table(  3,127) /'woodc'/
      data table(  3,128) /'wood1e(1)'/
      data table(  3,129) /'wood1e(2)'/
      data table(  3,130) /'wood1e(3)'/
      data table(  3,131) /'wood2e(1)'/
      data table(  3,132) /'wood2e(2)'/
      data table(  3,133) /'wood2e(3)'/
      data table(  3,134) /'wood3e(1)'/
      data table(  3,135) /'wood3e(2)'/
      data table(  3,136) /'wood3e(3)'/
      data table(  3,137) /'woode(1)'/
      data table(  3,138) /'woode(2)'/
      data table(  3,139) /'woode(3)'/
      data table(  4,  1) /'agtcrtcis(1)'/
      data table(  4,  2) /'agtcrtcis(2)'/
      data table(  4,  3) /'agtfrcisj(1)'/
      data table(  4,  4) /'agtfrcisj(2)'/
      data table(  4,  5) /'agtfrcism(1)'/
      data table(  4,  6) /'agtfrcism(2)'/
      data table(  4,  7) /'agtlvcis(1)'/
      data table(  4,  8) /'agtlvcis(2)'/
      data table(  4,  9) /'agtstmcis(1)'/
      data table(  4, 10) /'agtstmcis(2)'/
      data table(  4, 11) /'awstduvc2'/
      data table(  4, 12) /'dfbrchc'/
      data table(  4, 13) /'dfbrche(1)'/
      data table(  4, 14) /'dfbrche(2)'/
      data table(  4, 15) /'dfbrche(3)'/
      data table(  4, 16) /'dfbrcis(1)'/
      data table(  4, 17) /'dfbrcis(2)'/
      data table(  4, 18) /'dfrstc'/
      data table(  4, 19) /'dfrste(1)'/ 
      data table(  4, 20) /'dfrste(2)'/ 
      data table(  4, 21) /'dfrste(3)'/ 
      data table(  4, 22) /'dleavc'/
      data table(  4, 23) /'dleave(1)'/
      data table(  4, 24) /'dleave(2)'/
      data table(  4, 25) /'dleave(3)'/
      data table(  4, 26) /'dlvcis(1)'/
      data table(  4, 27) /'dlvcis(2)'/
      data table(  4, 28) /'dlwodc'/
      data table(  4, 29) /'dlwode(1)'/
      data table(  4, 30) /'dlwode(2)'/
      data table(  4, 31) /'dlwode(3)'/
      data table(  4, 32) /'dlwcis(1)'/
      data table(  4, 33) /'dlwcis(2)'/
      data table(  4, 34) /'dw1mnr(1)'/
      data table(  4, 35) /'dw1mnr(2)'/
      data table(  4, 36) /'dw1mnr(3)'/
      data table(  4, 37) /'dw2mnr(1)'/
      data table(  4, 38) /'dw2mnr(2)'/
      data table(  4, 39) /'dw2mnr(3)'/
      data table(  4, 40) /'dwd1c2(1)'/
      data table(  4, 41) /'dwd1c2(2)'/
      data table(  4, 42) /'dwd2c2(1)'/
      data table(  4, 43) /'dwd2c2(2)'/
      data table(  4, 44) /'eupgtprt(1,1)'/
      data table(  4, 45) /'eupgtprt(2,1)'/
      data table(  4, 46) /'eupgtprt(3,1)'/
      data table(  4, 47) /'eupgtprt(4,1)'/
      data table(  4, 48) /'eupgtprt(5,1)'/
      data table(  4, 49) /'eupgtprt(1,2)'/
      data table(  4, 50) /'eupgtprt(2,2)'/
      data table(  4, 51) /'eupgtprt(3,2)'/
      data table(  4, 52) /'eupgtprt(4,2)'/
      data table(  4, 53) /'eupgtprt(5,2)'/
      data table(  4, 54) /'eupgtprt(1,3)'/
      data table(  4, 55) /'eupgtprt(2,3)'/
      data table(  4, 56) /'eupgtprt(3,3)'/
      data table(  4, 57) /'eupgtprt(4,3)'/
      data table(  4, 58) /'eupgtprt(5,3)'/
      data table(  4, 59) /'gtcacc'/
      data table(  4, 60) /'gtcmth(1)'/
      data table(  4, 61) /'gtcmth(2)'/
      data table(  4, 62) /'gtcmth(3)'/
      data table(  4, 63) /'gtcmth(4)'/
      data table(  4, 64) /'gtcmth(5)'/
      data table(  4, 65) /'gtcmth(6)'/
      data table(  4, 66) /'gtcmth(7)'/
      data table(  4, 67) /'gtcmth(8)'/
      data table(  4, 68) /'gtcmth(9)'/
      data table(  4, 69) /'gtcmth(10)'/
      data table(  4, 70) /'gtcmth(11)'/
      data table(  4, 71) /'gtcmth(12)'/
      data table(  4, 72) /'gtcprd'/
      data table(  4, 73) /'gtcrem'/
      data table(  4, 74) /'gtcrootc'/
      data table(  4, 75) /'gtcroote(1)'/
      data table(  4, 76) /'gtcroote(2)'/
      data table(  4, 77) /'gtcroote(3)'/
      data table(  4, 78) /'gtcrtacc'/
      data table(  4, 79) /'gtcrtcis(1)'/
      data table(  4, 80) /'gtcrtcis(2)'/
      data table(  4, 81) /'gtcrtprd'/
      data table(  4, 82) /'gtdleavc'/
      data table(  4, 83) /'gtdleave(1)'/
      data table(  4, 84) /'gtdleave(2)'/
      data table(  4, 85) /'gtdleave(3)'/
      data table(  4, 86) /'gtdlvcis(1)'/
      data table(  4, 87) /'gtdlvcis(2)'/
      data table(  4, 88) /'gtdstemc'/
      data table(  4, 89) /'gtdsteme(1)'/
      data table(  4, 90) /'gtdsteme(2)'/
      data table(  4, 91) /'gtdsteme(3)'/
      data table(  4, 92) /'gtdstmcis(1)'/
      data table(  4, 93) /'gtdstmcis(2)'/
      data table(  4, 94) /'gterem(1)'/
      data table(  4, 95) /'gterem(2)'/
      data table(  4, 96) /'gterem(3)'/
      data table(  4, 97) /'gtfrootcj'/
      data table(  4, 98) /'gtfrootcm'/
      data table(  4, 99) /'gtfrootej(1)'/
      data table(  4,100) /'gtfrootej(2)'/
      data table(  4,101) /'gtfrootej(3)'/
      data table(  4,102) /'gtfrootem(1)'/
      data table(  4,103) /'gtfrootem(2)'/
      data table(  4,104) /'gtfrootem(3)'/
      data table(  4,105) /'gtfrtcisj(1)'/
      data table(  4,106) /'gtfrtcisj(2)'/
      data table(  4,107) /'gtfrtcism(1)'/
      data table(  4,108) /'gtfrtcism(2)'/
      data table(  4,109) /'gtfrtjacc'/
      data table(  4,110) /'gtfrtjprd'/
      data table(  4,111) /'gtfrtmacc'/
      data table(  4,112) /'gtfrtmprd'/
      data table(  4,113) /'gtleavc'/
      data table(  4,114) /'gtleave(1)'/
      data table(  4,115) /'gtleave(2)'/
      data table(  4,116) /'gtleave(3)'/
      data table(  4,117) /'gtlvacc'/
      data table(  4,118) /'gtlvcis(1)'/
      data table(  4,119) /'gtlvcis(2)'/
      data table(  4,120) /'gtlvprd'/
      data table(  4,121) /'gtpltc'/
      data table(  4,122) /'gtplte(1)'/
      data table(  4,123) /'gtplte(2)'/
      data table(  4,124) /'gtplte(3)'/
      data table(  4,125) /'gtstc'/
      data table(  4,126) /'gtste(1)'/
      data table(  4,127) /'gtste(2)'/
      data table(  4,128) /'gtste(3)'/
      data table(  4,129) /'gtstemacc'/
      data table(  4,130) /'gtstemc'/
      data table(  4,131) /'gtsteme(1)'/
      data table(  4,132) /'gtsteme(2)'/
      data table(  4,133) /'gtsteme(3)'/
      data table(  4,134) /'gtstemprd'/
      data table(  4,135) /'gtstg(1)'/
      data table(  4,136) /'gtstg(2)'/
      data table(  4,137) /'gtstg(3)'/
      data table(  4,138) /'gtstmcis(1)'/
      data table(  4,139) /'gtstmcis(2)'/
      data table(  4,140) /'gtsysc'/
      data table(  4,141) /'gtsyse(1)'/
      data table(  4,142) /'gtsyse(2)'/
      data table(  4,143) /'gtsyse(3)'/
      data table(  4,144) /'potgtacc'/
      data table(  4,145) /'tcreta'/
      data table(  4,146) /'tereta(1)'/
      data table(  4,147) /'tereta(2)'/
      data table(  4,148) /'tereta(3)'/
      data table(  4,149) /'wstduvc2(1)'/
      data table(  4,150) /'wstduvc2(2)'/
      data table(  4,151) /'gtcreta'/
      data table(  4,152) /'gtereta(1)'/
      data table(  4,153) /'gtereta(2)'/
      data table(  4,154) /'gtereta(3)'/
      end
