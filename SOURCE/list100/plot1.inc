
c               Copyright 1993 Colorado State University
c                       All Rights Reserved

c ..............................................................................
c     MODIFICATIONS FOR GRASSTREE (-mdh, January 11, 2019):
c     Added 36 new variables to the 445 existing variables (451+6 total now).
c     The dimensions of these variables were increased from (2) to (3) 
c       grspann(2), grspflux(2), grspmth(2),
c       mrspann(2), mrspflux(2), mrspmth(2),
c       srspann(2), srspmth(2)
c       co2cpr(2), co2ctr(2), co2crs(2)
c       snfxac(2)
c     Increase arspmth(2,2) to arspmth(3,2)
c     Increase co2cce(2,2,3) to co2cce(3,2,3)
c     Added gtautoresp(2), gtgrspflux(5), gtmrspflux(5)
c     Added cprodgt, eprodgt(3)
c ..............................................................................

      common/plot1/ agcacc,agcmth(12),agcprd,agdefac,aglcis(2),aglivc,
     & aglive(3),aminrl(3),amt1c2,amt2c2,anerb,annet,arspmth(3,2),
     & as11c2,as12c2,as21c2,as22c2,as3c2,asmos(10),ast1c2,ast2c2,
     & ast1uvc2,astduvc2,avh2o(3),
     & bgcjacc,bgcmacc,bgcjmth(12),bgcmmth(12),bgcjprd,bgcmprd,
     & bgdefac,bglcisj(2),bglcism(2), 
     & bglivcj,bglivcm,bglivej(3),bglivem(3),
     & cautoresp(2),cgrain,cinput,clittr(2,2),
     & cgrspflux(3),cmrspflux(3),
     & co2cce(3,2,3),co2crs(3),co2cpr(3),co2ctr(3),cproda,cprodc,
     & cprodf,cprodgt,creta,crmvst,crpstg(3),crpval,dsomsc,
     & egrain(3),elimit,eprodc(3),eprodf(3),eprodgt(3),ermvst(3),
     & eupacc(3),eupaga(3),eupbga(3),eupprd(3),evap,
     & fautoresp(2),fertac(3),fertmth(12,3),fertot(3),fertprd(3),
     & fgrspflux(6),fmrspflux(6),grspann(3),grspflux(3), grspmth(3),
     & gtautoresp(2),gtgrspflux(5),gtmrspflux(5),
     & harmth,hi,irract,irrtot,metabc(2),metabe(2,3),metcis(2,2),
     & minerl(10,3),mrspann(3),mrspflux(3),mrspmth(3),
     & mt1c2(2),mt2c2(2),nfix,nfixac,
     & occlud,parent(3),pet,petann,plabil,prcann,ptagc,ptbgc,
     & pttr,rain,relyld,resp(2),respmth(2),runoff,rwcf(10),
     & s11c2(2),s12c2(2),s21c2(2),s22c2(2),s3c2(2),satmac,sclosa,
     & scloss,sdrema,secndy(3),shrema,sirrac,snfxac(3),snlq,snow,
     & soilnm(3),somsc,somse(3),somtc,som1c(2),som1ci(2,2),som1e(2,3),
     & som2c(2),som2ci(2,2),som2e(2,3),som3c,som3ci(2),som3e(3),
     & srspann(3),srspmth(3),stdcis(2),st1c2(2),st2c2(2),st1uvc2(2),
     & stemp,strcis(2,2),stream(8),strlig(2),strmac(8),strucc(2),
     & struce(2,3),sumnrs(3),stdedc,stdede(3),stduvc2(2),
     & tave,tlittr(2,2),tminrl(3),tnetmn(3),totc,tran,
     & volgma,volexa,volpla,
     & wd1c2(2),wd2c2(2),wd3c2(2),wdfxaa,wdfxas

      real agcacc,agcmth,agcprd,agdefac,aglcis,aglivc,
     &     aglive,aminrl,amt1c2,amt2c2,anerb,annet,arspmth,
     &     as11c2,as12c2,as21c2,as22c2,as3c2,asmos,ast1c2,ast2c2,
     &     ast1uvc2,astduvc2,avh2o,
     &     bgcjacc,bgcmacc,bgcjmth,bgcmmth,bgcjprd,bgcmprd,
     &     bgdefac,bglcisj,bglcism,
     &     bglivcj,bglivcm,bglivej,bglivem,
     &     cautoresp,cgrain,cinput,clittr,
     &     cgrspflux,cmrspflux,
     &     co2cce,co2crs,co2cpr,co2ctr,cproda,cprodc,
     &     cprodf,cprodgt,creta,crmvst,crpstg,crpval,dsomsc,
     &     egrain,elimit,eprodc,eprodf,eprodgt,ermvst,
     &     eupacc,eupaga,eupbga,eupprd,evap,
     &     fautoresp,fertac,fertmth,fertot,fertprd,
     &     fgrspflux,fmrspflux,grspann,grspflux,grspmth,
     &     gtautoresp,gtgrspflux,gtmrspflux,
     &     harmth,hi,irract,irrtot,metabc,metabe,metcis,
     &     minerl,mrspann,mrspflux,mrspmth,
     &     mt1c2,mt2c2,nfix,nfixac,
     &     occlud,parent,pet,petann,plabil,prcann,ptagc,ptbgc,
     &     pttr,rain,relyld,resp,respmth,runoff,rwcf,
     &     s11c2,s12c2,s21c2,s22c2,s3c2,satmac,sclosa,
     &     scloss,sdrema,secndy,shrema,sirrac,snfxac,snlq,snow,
     &     soilnm,somsc,somse,somtc,som1c,som1ci,som1e,
     &     som2c,som2ci,som2e,som3c,som3ci,som3e,
     &     srspann,srspmth,stdcis,st1c2,st2c2,st1uvc2,
     &     stemp,strcis,stream,strlig,strmac,strucc,
     &     struce,sumnrs,stdedc,stdede,stduvc2,
     &     tave,tlittr,tminrl,tnetmn,totc,tran,
     &     volgma,volexa,volpla,
     &     wd1c2,wd2c2,wd3c2,wdfxaa,wdfxas

      save /plot1/
