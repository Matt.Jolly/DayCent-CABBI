
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c...............................................................................
c ... MODIFICATIONS FOR GRASSTREE
c ... Added gtlig(5), for live grasstree parts only
c...............................................................................

c ... pltlig tells what fraction of the current year's plant residue
c ... will be lignin.  (1) = aboveground; (2) = juvenile belowground;
c ...                                     (3) = mature belowground

      common/ligvar/pltlig(3), gtlig(5)
      real pltlig, gtlig

      save /ligvar/
