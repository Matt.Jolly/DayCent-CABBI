
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


c ... bioabsorp - litter biomass at full absorption of radiation
c ...             (grams biomass)
c ... maxphoto - maximum carbon loss due to photodecomposition
c ...            (ug C/KJ srad)
c ... watrflag  0 = rain and irrigation events used to add water to the
c ...               soil potentially bringing it to saturation
c ...           1 = water added to the system automatically to bring
c ...               the soil water content to saturation
c ..............................................................................
c   MODIFICATIONS FOR GRASSTREE (-mdh, January 2019):
c ... prdx(2) increased to prdx(3)
c ... no3pref(2) increased to no3pref(3)
c ... co2ipr(2) increased to co2ipr(3)
c ... co2ice(2,2,3) increased to co2ice(3,2,3)
c ... co2irs(2) increased to co2irs(3)
c ... co2itr(2) increased to co2itr(3)
c ... ppdf(4,2) increased to ppdf(4,3)
c ... wscoeff(2,2) increased to wscoeff(3,2)
c ... ps2mrsp(2) increased to ps2mrsp(3)
c ... snfxmx(2) increased to snfxmx(3)
c ... Added gtgresp(5), counterpart to fgresp(6)
c ... Added gtkmrspmx(5), counterpart to fkmrspmx(6),
c ... Added gtmrsplai(6), counterpart to fmrsplai(6)
c ... Added gtlaypg, counterpart to claypg and tlaypg
c ... Added gtmix, counterpart to cmix and tmix
c ..............................................................................
c ... Added flodeff(3). -mdh 6/19/2019
c ... Added dofwueup. -mdh 7/19/2019
c ... Added gtlaypg_const -mdh 8/30/2019

      common/param/afiel(10),amov(10),autoresp1(2),autoresp2(2),
     &    awilt(10),basef,bioabsorp,bulkd,
     &    co2ipr(3),co2ice(3,2,3),co2irs(3),co2itr(3),co2sys,co2tm(2),
     &    crpindx,crpcmn,crpcmx,crpmnmul,crpmxmul,
     &    drain,epnfa(2),epnfs(2),falprc,fracro,
     &    hpttr(12),htran(12),ivauto,labtyp,labyr,
     &    ckmrspmx(3),cmrspnpp(6),dofwueup,flodeff(3),fkmrspmx(6),
     &    fmrsplai(6),gtkmrspmx(5),gtmrsplai(6),
     &    gtlaypg,gtlaypg_const,gtmix,
     &    cgresp(3),fgresp(6),gtgresp(5),
     &    maxphoto,maxtmp(12),mctemp,micosm,mintmp(12),
     &    nelem,newautoresp(2),Ninput,nlayer,nlaypg,no3pref(3),
     &    Nstart,OMADinput,OMADstart,
     &    ph,pHscalar(12),phstart,phsys,phtm,
     &    ppdf(4,3),prcskw(12),prcstd(12),prdx(3),
     &    ps2mrsp(3),precip(12),precro,psloss,pslsrb,
     &    rcelit(2,3),rces1(2,3),rces2(2,3),rces3(3),remwsd,rock,
     &    satmos(2),satmt,sirri,snfxmx(3),sorpmx,stamt,stormf,
     &    strm5l,strm5u,ststart,stsys,swflag,tmxbio,trbasl,
     &    trpindx,trpcmn,trpcmx,trpmnmul,trpmxmul,
     &    claypg,claypg_const,tlaypg,cmix,tmix,wscoeff(3,2),
     &    watrflag

      integer falprc,ivauto,labtyp,labyr,micosm,nelem,
     &        Ninput,nlayer,nlaypg,Nstart,OMADinput,OMADstart,phsys,
     &        phtm,swflag,claypg,claypg_const,gtlaypg,gtlaypg_const,
     &        tlaypg,crpindx,trpindx,watrflag,dofwueup

      real    afiel,amov,autoresp1,autoresp2,
     &        awilt,basef,bioabsorp,bulkd,
     &        co2ipr,co2ice,co2irs,co2itr,co2sys,co2tm,
     &        crpcmn,crpcmx,crpmnmul,crpmxmul,
     &        drain,epnfa,epnfs,fracro,
     &        hpttr,htran,ckmrspmx,cmrspnpp,flodeff,
     &        fkmrspmx,fmrsplai,gtkmrspmx,gtmrsplai,
     &        cgresp,fgresp,gtgresp,
     &        maxphoto,maxtmp,mctemp,mintmp,
     &        newautoresp,no3pref,
     &        ph,pHscalar,phstart,
     &        ppdf,prcskw,prcstd,prdx,
     &        ps2mrsp,precip,precro,psloss,pslsrb,
     &        rcelit,rces1,rces2,rces3,remwsd,rock,
     &        satmos,satmt,sirri,snfxmx,sorpmx,stamt,stormf,
     &        strm5l,strm5u,ststart,stsys,tmxbio,trbasl,
     &        trpcmn,trpcmx,trpmnmul,trpmxmul,
     &        cmix,gtmix,tmix,wscoeff

      save /param/
