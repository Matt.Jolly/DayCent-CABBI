
c               Copyright 1993 Colorado State University
c                       All Rights Reserved


      subroutine grasstreein(tomatch)

      implicit none
      include 'chrvar.inc'
      include 'const.inc'
      include 'isovar.inc'
      include 'ligvar.inc'
      include 'param.inc'
      include 'pargt.inc'
      include 'parfx.inc'
      include 'pheno.inc'
      include 'photosyn.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot4.inc'
      include 'seq.inc'

c ... Argument declarations
      character*5 tomatch

c ... Read in the new grasstree type

c ... Function declarations
      real     fcrpval
      external fcrpval

c ... Local variables
      integer   GTLNS
      integer   ii, jj
      real      del13c, temp
      character fromdat*5, name*9, string*80

c ... Number of lines to read for each crop type
c     parameter (GTLNS = 177)
c     parameter (GTLNS = 155)
c     parameter (GTLNS = 156)
      parameter (GTLNS = 168)

      open(unit=11, file='grasstree.100',status='OLD')
      rewind(11)
20    continue
      read(11, 10, end=220) fromdat
10    format(a5)
      if (tomatch .ne. fromdat) then 
        do 25 ii = 1, GTLNS-1
          read(11, *) temp, name
c         write(*,*) temp, name
25      continue
        goto 20
      else

        read(11, *) prdx(3), name
        call ckdata9('grasstreein','prdx',name)

        do 30 ii = 1, 4
          read(11, *) ppdf(ii,3), name
          call ckdata9('grasstreein','ppdf',name)
30      continue

        read(11, *) temp, name
        gtbioflg = int(temp)
        call ckdata9('grasstreein','gtbioflg',name)
        read(11, *) gtbiok5, name
        call ckdata9('grasstreein','gtbiok5',name)
c       read(11, *) gtpltmrf, name
c       call ckdata9('grasstreein','gtpltmrf',name)
        read(11, *) gtbiomc, name
        call ckdata9('grasstreein','gtbiomc',name)

        read(11, *) temp, name
        call ckdata9('grasstreein','gtfrtcind',name)
        gtfrtcindx = int(temp)
c ..... day implementation
        if (gtfrtcindx .ne. 1 .and. gtfrtcindx .ne. 2) then
          write(*,*) 'Error in grasstreein.  gtfrtcindx must = 1 or 2'
          write(*,*) 'Check grasstree.100 file.'
          write(*,*) 'gtfrtcindx = ', gtfrtcindx
          STOP
        endif
c ..... gtfrtc(1)-(5) annual grasstree parameters. -mdh 9/5/2019
        read(11, *) gtfrtc(1), name
        call ckdata9('grasstreein','gtfrtc',name)
        read(11, *) gtfrtc(2), name
        call ckdata9('grasstreein','gtfrtc',name)
        read(11, *) gtfrtc(3), name
        call ckdata9('grasstreein','gtfrtc',name)
        read(11, *) gtfrtc(4), name
        call ckdata9('grasstreein','gtfrtc',name)
        read(11, *) gtfrtc(5), name
        call ckdata9('grasstreein','gtfrtc',name)

c ..... gtfrtcn(*) and gtfrtcw(*) for calculating water and nutrient
c ..... stress using unique parameter values.
        read(11, *) gtfrtcn(1), name
        call ckdata9('grasstreein','gtfrtcn',name)
        read(11, *) gtfrtcn(2), name
        call ckdata9('grasstreein','gtfrtcn',name)
        read(11, *) gtfrtcw(1), name
        call ckdata9('grasstreein','gtfrtcw',name)
        read(11, *) gtfrtcw(2), name
        call ckdata9('grasstreein','gtfrtcw',name)
        if ((gtfrtcindx .eq. 1) .or. (gtfrtcindx .eq. 3)) then
          if ((gtfrtcn(1) .le. 0.0) .or. (gtfrtcn(2) .le. 0.0) .or.
     &        (gtfrtcw(1) .le. 0.0) .or. (gtfrtcw(2) .le. 0.0)) then
            write(*,*)'grasstree.100: gtfrtcn(*),gtfrtcw(*) must be >0'
            write(*,*)'gtfrtcn(1) = ', gtfrtcn(1)
            write(*,*)'gtfrtcn(2) = ', gtfrtcn(2)
            write(*,*)'gtfrtcw(1) = ', gtfrtcw(1)
            write(*,*)'gtfrtcw(2) = ', gtfrtcw(2)
            STOP
          endif
          if ((gtfrtcn(1) .gt. 1.0) .or. (gtfrtcn(2) .gt. 1.0) .or.
     &        (gtfrtcw(1) .gt. 1.0) .or. (gtfrtcw(2) .gt. 1.0)) then
            write(*,*)'grasstree.100: gtfrtcn(*),gtfrtcw(*) must be >=0'
            write(*,*)'gtfrtcn(1) = ', gtfrtcn(1)
            write(*,*)'gtfrtcn(2) = ', gtfrtcn(2)
            write(*,*)'gtfrtcw(1) = ', gtfrtcw(1)
            write(*,*)'gtfrtcw(2) = ', gtfrtcw(2)
            STOP
          endif
          if ((gtfrtcn(2) .gt. gtfrtcn(1))) then
            write(*,*) 'grasstree.100: gtfrtcn(2) must be <= gtfrtcn(1)'
            write(*,*) 'gtfrtcn(1) = ', gtfrtcn(1)
            write(*,*) 'gtfrtcn(2) = ', gtfrtcn(2)
            STOP
          endif
          if ((gtfrtcw(2) .gt. gtfrtcw(1))) then
            write(*,*) 'grasstree.100: gtfrtcw(2) must be <= gtfrtcw(1)'
            write(*,*) 'gtfrtcw(1) = ', gtfrtcw(1)
            write(*,*) 'gtfrtcw(2) = ', gtfrtcw(2)
            STOP
          endif
        endif

        read(11, *) gtlvbiomax, name
        call ckdata9('grasstreein','gtlvbioma',name)

        read(11, *) gtstmbiomax, name
        call ckdata9('grasstreein','gtstmbiom',name)

        read(11, *) gtbtolai, name
        call ckdata9('grasstreein','gtbtolai',name)

        read(11, *) gtklai, name
        call ckdata9('grasstreein','gtklai',name)

        read(11, *) gtlaitop, name
        call ckdata9('grasstreein','gtlaitop',name)

        read(11, *) gtmaxlai, name
        call ckdata9('grasstreein','gtmaxlai',name)

c       read(11, *) gtcfrac(1), name
c       call ckdata9('grasstreein','gtcfrac',name)

        read(11, *) gtcfrac(2), name
        call ckdata9('grasstreein','gtcfrac',name)

c       read(11, *) gtcfrac(3), name
c       call ckdata9('grasstreein','gtcfrac',name)

        read(11, *) gtcfrac(4), name
        call ckdata9('grasstreein','gtcfrac',name)

c       read(11, *) gtcfrac(5), name
c       call ckdata9('grasstreein','gtcfrac',name)


        do 60 ii = GTLEAF, GTSTEM
          do 50 jj = 1, MAXIEL
            read(11, *) cergtamn1(ii,jj), name
            call ckdata9('grasstreein','cergtamn1',name)
50        continue
60      continue

        do 65 ii = GTLEAF, GTSTEM
          do 55 jj = 1, MAXIEL
            read(11, *) cergtamn2(ii,jj), name
            call ckdata9('grasstreein','cergtamn2',name)
55        continue
65      continue


        do 80 ii = GTLEAF, GTSTEM
          do 70 jj = 1, MAXIEL
            read(11, *) cergtamx1(ii,jj), name
            call ckdata9('grasstreein','cergtamx1',name)
70        continue
80      continue

        do 85 ii = GTLEAF, GTSTEM
          do 75 jj = 1, MAXIEL
            read(11, *) cergtamx2(ii,jj), name
            call ckdata9('grasstreein','cergtamx2',name)
75        continue
85      continue

c       do 100 ii = GTFROOTJ, GTCROOT
        do 100 ii = GTFROOTJ, GTLIVPARTS
          do 90 jj = 1, MAXIEL
            read(11, *) cergtbmn(ii,jj), name
            call ckdata9('grasstreein','cergtbmn',name)
90        continue
100     continue

c       do 105 ii = GTFROOTJ, GTCROOT
        do 105 ii = GTFROOTJ, GTLIVPARTS
          do 95 jj = 1, MAXIEL
            read(11, *) cergtbmx(ii,jj), name
            call ckdata9('grasstreein','cergtbmx',name)
95        continue
105     continue


c       do 120 ii = 1, GTPARTS
c         do 110 jj = 1, MAXIEL
c           read(11, *) cergtini(ii,jj), name
c           call ckdata9('grasstreein','cergtini',name)
c110       continue
c120     continue

        do 140 ii = 1, GTLIVPARTS
          read(11, *) gtlig(ii), name
          call ckdata9('grasstreein','gtlig',name)
140     continue

c ..... New parameters for annual grasstree grain harvest. -mdh 9/5/2019
        read(11, *) himaxgt, name
        call ckdata9('grasstreein','himaxgt',name)
        read(11, *) hiwsfgt, name
        call ckdata9('grasstreein','hiwsfgt',name)
        read(11, *) temp, name
        himongt(1) = int(temp)
        call ckdata9('grasstreein','himongt',name)
        read(11, *) temp, name
        himongt(2) = int(temp)
        call ckdata9('grasstreein','himongt',name)
        read(11, *) efgrngt(1), name
        call ckdata9('grasstreein','efgrngt',name)
        read(11, *) efgrngt(2), name
        call ckdata9('grasstteein','efgrngt',name)
        read(11, *) efgrngt(3), name
        call ckdata9('grasstreein','efgrngt',name)

        read(11, *) vlossgt, name
        call ckdata9('grasstreein','vlossgt',name)

        do 160 ii = 1, 6
          read(11, *) gtfsdeth(ii), name
          call ckdata9('grasstreein','gtfsdeth',name)
160     continue

        read(11, *) sdfallrt(1), name
        call ckdata9('grasstreein','sdfallrt',name)
        read(11, *) sdfallrt(2), name
        call ckdata9('grasstreein','sdfallrt',name)

        read(11, *) rootdr(1), name
        call ckdata9('grasstreein','rootdr',name)
        read(11, *) rootdr(2), name
        call ckdata9('grasstreein','rootdr',name)
        read(11, *) rootdr(3), name
        call ckdata9('grasstreein','rootdr',name)

        read(11, *) frtdsrfc, name
        call ckdata9('grasstreein','frtdsrfc',name)
        read(11, *) crtdsrfc, name
        call ckdata9('grasstreein','crtdsrfc',name)

c ..... New parameter for annual grasstrees. -mdh 9/5/2019
        read(11, *) gtrtdtmp, name
        call ckdata9('grasstreein','gtrtdtmp',name)

        do 170 ii = 1, MAXIEL
          read(11, *) gtrtf(ii), name
          call ckdata9('grasstreein','gtrtf',name)
170     continue

        read(11, *) gtmrtfrac, name
        call ckdata9('grasstreein','gtmrtfrac',name)

        read(11, *) snfxmx(GTARYINDX), name
        call ckdata9('grasstreein','snfxmx',name)
        read(11, *) del13c, name
        call ckdata9('grasstreein','del13c',name)
        read(11, *) co2ipr(GTARYINDX), name
        call ckdata9('grasstreein','co2ipr',name)
        read(11, *) co2itr(GTARYINDX), name
        call ckdata9('grasstreein','co2itr',name)

        do 190 ii = IMIN, IMAX
          do 180 jj = 1, MAXIEL
            read(11, *) co2ice(GTARYINDX,ii,jj), name
            call ckdata9('grasstreein','co2ice',name)
180       continue
190     continue

        read(11, *) co2irs(GTARYINDX), name
        call ckdata9('grasstreein','co2irs',name)

        do 195 ii = 1, GTLIVPARTS
          read(11, *) gtkmrspmx(ii), name
          call ckdata9('grasstreein','gtkmrspmx',name)
195     continue

c ..... Parameters for controlling the linear decrease in
c ....  maintenance respiration as the amount of carbohydrate stored
c ..... in the carbohydrate storage pool gets smaller.
        read(11, *) gtmrsplai(1), name
        call ckdata9('grasstreein','gtmrsplai',name)
        read(11, *) gtmrsplai(2), name
        call ckdata9('grasstreein','gtmrsplai',name)
        read(11, *) gtmrsplai(3), name
        call ckdata9('grasstreein','gtmrsplai',name)
        read(11, *) gtmrsplai(4), name
        call ckdata9('grasstreein','gtmrsplai',name)
        read(11, *) gtmrsplai(5), name
        call ckdata9('grasstreein','gtmrsplai',name)
        read(11, *) gtmrsplai(6), name
        call ckdata9('grasstreein','gtmrsplai',name)

c ..... gtgresp parameters for the growth respiration code,
        do 198 ii = 1, GTLIVPARTS
          read(11, *) gtgresp(ii), name
          call ckdata9('grasstreein','gtgresp',name)
198     continue

        read(11, *) no3pref(GTARYINDX), name
        call ckdata9('grasstreein','no3pref',name)
        if ((no3pref(GTARYINDX) .lt. -1.0) .or.
     &      (no3pref(GTARYINDX) .gt. 1.0)) then
          write(*,*) 'no3pref out of range grasstree.100.'
          write(*,*) 'Must be >= -1.0 and <= 1.0'
          write(*,*) 'no3pref = ', no3pref
          STOP
        endif

        read(11, *) temp, name
        gtlaypg_const = int(temp)
        call ckdata9('grasstreein','gtlaypg',name)
        if (gtlaypg_const .gt. nlayer) then
          write(*,*) 'gtlaypg out of range in grasstree.100.'
          write(*,*) 'Must be <= nlayer '
          write(*,*) 'gtlaypg = ', gtlaypg_const
          write(*,*) 'nlayer = ', nlayer
          write(*,*) 'Resetting gtlaypg to equal nlayer.'
          gtlaypg_const = nlayer
        endif
c ..... For an annual plant initialize the rooting depth to 1
        if (gtfrtcindx .eq. 2) then
          gtlaypg = 1
        else
          gtlaypg = gtlaypg_const
        endif

        read(11, *) gtmix, name
        call ckdata9('grasstreein','gtmix',name)

        read(11, *) tmpgerm, name
        call ckdata9('grasstreein','tmpgerm',name)
        read(11, *) ddbase, name
        call ckdata9('grasstreein','ddbase',name)
        read(11, *) tmpkill, name
        call ckdata9('grasstreein','tmpkill',name)

        read(11, *) basetemp(1), name
        call ckdata9('grasstreein','basetemp',name)
        read(11, *) basetemp(2), name
        call ckdata9('grasstreein','basetemp',name)
        read(11, *) temp, name
        mxdysene = int(temp)
        call ckdata9('grasstreein','mxdysene',name)

c ..... Not implementing late season growth restriction for grasstree
c       read(11, *) temp, name
c       gturgdys = int(temp)
c       call ckdata9('grasstreein','gturgdys',name)
c       read(11, *) gtlsgres, name
c       call ckdata9('grasstreein','gtlsgres',name)

        read(11, *) gtmxturn, name
        call ckdata9('grasstreein','gtmxturn',name)

c ..... Water stress equation coefficents
        read(11,*) wscoeff(3,1), name
        call ckdata9('grasstreein', 'wscoeff', name)
        read(11,*) wscoeff(3,2), name
        call ckdata9('grasstreein', 'wscoeff', name)

        read(11, *) ps2mrsp(GTARYINDX), name
        call ckdata9('grasstreein','ps2mrsp',name)

c ..... Added sfavail(3) (favail(1) moved from fix.100)
        read(11, *) sfavail(3), name
        call ckdata9('grasstreein','sfavail',name)

c ..... Parameters to control the root priming effect on the
c ..... som2(2) decomposition rate
        read(11, *) temp, name
        crpindx = int(temp)
        if (crpindx .lt. 0 .or. crpindx .gt. 3) then
          write(*,*) 'Error in grasstreein.  crpindx out of range.'
          write(*,*) 'Check grasstree.100 file.'
          write(*,*) 'crpindx = ', crpindx
          STOP
        endif
        call ckdata9('grasstreein','crpindx',name)
        read(11, *) crpcmn, name
        call ckdata9('grasstreein','crpcmn',name)
        read(11, *) crpcmx, name
        call ckdata9('grasstreein','crpcmx',name)
        read(11, *) crpmnmul, name
        call ckdata9('grasstreein','crpmnmul',name)
        read(11, *) crpmxmul, name
        call ckdata9('grasstreein','crpmxmul',name)

c ..... Add the parameters for the photosynthesis submodel
        read(11, *) carbostg(GTARYINDX,UNLABL), name
        call ckdata9('grasstreein','carbostg',name)
        read(11, *) carbostg(GTARYINDX,LABELD), name
        call ckdata9('grasstreein','carbostg',name)
        read(11, *) aMax(GTARYINDX), name
        call ckdata9('grasstreein','amax',name)
        read(11, *) aMaxFrac(GTARYINDX), name
        call ckdata9('grasstreein','amaxfrac',name)
        read(11, *) aMaxScalar1(GTARYINDX), name
        call ckdata9('grasstreein','amaxscala',name)
        read(11, *) aMaxScalar2(GTARYINDX), name
        call ckdata9('grasstreein','amaxscala',name)
        read(11, *) aMaxScalar3(GTARYINDX), name
        call ckdata9('grasstreein','amaxscala',name)
        read(11, *) aMaxScalar4(GTARYINDX), name
        call ckdata9('grasstreein','amaxscala',name)
        read(11, *) attenuation(GTARYINDX), name
        call ckdata9('grasstreein','attenuati',name)
        read(11, *) baseFolRespFrac(GTARYINDX), name
        call ckdata9('grasstreein','basefolre',name)
        read(11, *) cFracLeaf(GTARYINDX), name
        call ckdata9('grasstreein','cfracleaf',name)
        read(11, *) dVpdExp(GTARYINDX), name
        call ckdata9('grasstreein','dvpdexp',name)
        read(11, *) dVpdSlope(GTARYINDX), name
        call ckdata9('grasstreein','dvpdslope',name)
        read(11, *) growthDays1(GTARYINDX), name
        call ckdata9('grasstreein','growthday',name)
        read(11, *) growthDays2(GTARYINDX), name
        call ckdata9('grasstreein','growthday',name)
        read(11, *) growthDays3(GTARYINDX), name
        call ckdata9('grasstreein','growthday',name)
        read(11, *) growthDays4(GTARYINDX), name
        call ckdata9('grasstreein','growthday',name)
        read(11, *) halfSatPar(GTARYINDX), name
        call ckdata9('grasstreein','halfsatpa',name)
        read(11, *) leafCSpWt(GTARYINDX), name
        call ckdata9('grasstreein','leafcspwt',name)
        read(11, *) psnTMin(GTARYINDX), name
        call ckdata9('grasstreein','psntmin',name)
        read(11, *) psnTOpt(GTARYINDX), name
        call ckdata9('grasstreein','psntopt',name)
c ..... FLOOD EFFECT. -mdh 6/19/2019
        read(11, *) flodeff(GTARYINDX), name
        call ckdata('grasstreein','flodeff',name)

c ..... Close the file
        close(11)

c ..... Hold on to the current crop just read in
        curgtre = tomatch
      endif

c ... Determine the 'numerical value' of the curgtre, 
c ... for use as an output variable
c     crpval = 0
c     do 200 ii = 1, 5
c       if (curgtre(ii:ii) .ne. ' ') then
c         if(curgtre(ii:ii) .ge. '0' .and. curgtre(ii:ii) .le. '9')then
c           crpval = crpval +
c    &               ((ichar(curgtre(ii:ii)) - ichar('0')) / 10.0)
c         else
c           crpval = crpval + (ichar(curgtre(ii:ii)) - ichar('A')) + 1
c         endif
c       endif
c200  continue

      crpval = fcrpval(curgtre)

c ... Calculate cisofr as 13C if 13C labeling
      if (labtyp .eq. 2) then
        cisofr = del13c * PEEDEE * 1.0e-03 + PEEDEE
        cisofr = 1 / (1/cisofr + 1)
      endif

      return

220   call message('Error reading values from the grasstree.100 file.')
      string = '  Looking for grasstree type: ' // tomatch
      call message(string)
      STOP

      end
