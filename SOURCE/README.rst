#############
DAYCENT Model
#############

Updated to replace the legacy Makefile build system with a unified Meson build file.

To build the DayCent model, an implementation of the Meson build system is required.
This can be installed through a system package manager or:

- using pip:

  .. code-block:: console

    bloggsj@some-hpc-system$ pip install meson ninja

- ``muon``, the Meson C implementation.

Building the Model
===================

Configuration
-------------

.. Tip::

    The ``CC``, ``CFLAGS``, ``FC``, and ``FFLAGS`` only need to be set if the defaults are not appropriate, just run the ``meson setup`` portion if that's the case.

To configure with the defaults:

.. code-block:: console

    bloggsj@some-hpc-system$ meson setup build

To configure with the intel compilers:

.. code-block:: console

    bloggsj@some-hpc-system$ export CC=icc
    bloggsj@some-hpc-system$ export CFLAGS="-xHost"
    bloggsj@some-hpc-system$ export FC=ifort
    bloggsj@some-hpc-system$ export FFLAGS="-xHost"
    bloggsj@some-hpc-system$ meson setup build -Doptimization=3 -Ddebug=false -Dwith_list100=false

Compilation
-----------

To build:

.. code-block:: console

    bloggsj@some-hpc-system$ ninja -C build

Build options:

- ``-Dwith_list100``: Build the ``list100_DayCent_CABBI`` binary.

Running the Model
==================

.. code-block:: console

    bloggsj@some-hpc-system$ ./build/DayCent_CABBI
    bloggsj@some-hpc-system$ ./build/list100_DayCent_CABBI

Legacy changelog below!

The code for DayCent-CABBI was copied from the directory below on May 11, 2020
/data/parton/melannie/DAYCENT_SOURCE/MUVP/DailyDayCent_muvps_gt/SOURCE_11.14.2019_Annual_gt
Below are development notes starting from Sept. 2017.
====================================================================================================

I converted the original Windows 32-bit code in PC_Orig_Dec_2014/ to code that can be
compiled for both Linux and Windows-64 bit.  I have not tested this code to see how
the results compare to the Windows-32 bit version. -mdh 9/15/2017
====================================================================================================

To build the DailyDayCent_muvp executable for Linux using gfortran:

make -f Makefile.txt DailyDayCent_muvp


To build the DailyDayCent_muvp.exe executable for Windows 64-bit:
You will need update makefile_Win64.txt with the path to the ming32-gfortran compile installed on 
your system; at NREL on rubel it is /data/paustian/Century/bin/MinGW/bin/x86_64-w64-mingw32-gfortran 

make -f Makefile_Win64.txt DailyDayCent_muvp.exe

====================================================================================================
Code updates:
Sept. 15, 2017

All *.c *.f *.h *.inc files must begin with small case letters to be consistent with other
DayCent Linux versions.

----------------------------------------------------------------------------------------------------
The soil surface temperature function (surftemp.c) was replaced since the previous version 
(surftemp_old.c) had a bug when calculating soil surface temperature under the snow. -mdh 9/15/2017

----------------------------------------------------------------------------------------------------
The file catanf.f was renamed to carctanf.f.  Also, calls to catanf wre renamed to carctanf in the 
following files:

carctanf.f
droot.f
litdec.f
prelim.f
pschem.f
somdec.f
tcalc.f
wdeath.f
woodec.f

We discovered a couple years ago that catanf function is an intrinisic gfortran compiler function
of some sort, and there is a conflict between this internal compiler function and DayCent's catanf
function.  Without this name substitution DayCent will generate randomly large numbers when the
function is called. -mdh 9/15/2017

====================================================================================================

Nov. 7, 2017

The indices in the agcmth, bgcjmth, and bgcmmth accumulators in csa_main.f were going
out of bounds when month=1.  I trapped for this condition.

Fixed all floating point comparisons to 0.0.  See file flteq0.txt.

Within calcdfac.c, tcalc (a FORTRAN function) was being called as a real function.
This worked for the old MSDOS FORTRAN compilier, but was causing a segmentation
violation in the executable compiled with gfortran.  I changed tcalc to a 
FORTRAN subroutine.

====================================================================================================

Nov. 8, 2017

There seemed to be an underflow problem with rleavc in leafa.f.  When rleavc went
slightly negative, DayCent put out and error message and quit.  This stopped the 
model from ever recovering from this underrflow condition. I trapped for the 
(rleavc < 0.0) condtion in treegrow, right before the call to leafa, and 
reset rleavc and its isotope components (rlvcis(*) to 0.0.

====================================================================================================

September 13, 2018

I have been updating the muvp and grasstree code so they are more consistent. 
Mostly the grasstree code has been updated.
The muvp code produces identical results compared to the code in SOURCE_Nov_2017_Elena.

====================================================================================================

Sept 14, 2018

Updated firrtn.f so that it returns C in burnt live tree parts as charcoal.
Charcoal gets transferred to the passive pool (som3c).
This may greatly increase the amount of som3c, depending on the values
of retf(1,1), retf(2,1), and retf(3,1) for burn events in trem.100.  
If passive C accumulation is too high, reduce these parameters.

====================================================================================================
====================================================================================================

BEGIN GRASSTREE CODE MODIFICATIONS:

January 9, 2018

I started with code from this folder: DAYCENT_SOURCE/DailyDayCent_muvp/DailyDayCent_muvp_snag

I am planning to insert the grass-tree code into the DayCent_muvp_snag code.  This will
create one model that has all the following features:

muvp that Cindy created: Methane, UV litter degredation, photosynthesis
Cindy's updates to the growing degree day model (GDD).
The standing dead tree snap model that I developed for Tara Hudiburg.
The grasstree system type (in addition to the traditional crop, tree, savanna).

These are new files added to the muvp_snag code for grasstree:
cultivgt.f
drootgt.f
dshootgt.f
falstdgt.f
fltdegt.f
grasstreeDynC.f  
grasstreegrow.f  
grasstreein.f  
grasstreerem.f  
grasstrees.f

Note: irrigt.f is not a "grasstree" file. 

The first version of grasstree:
  * output .csv files instead of .out files
  * had an option to read CO2 concentrations from a file
  * tracked "notill" Rh and output it to summary.csv

====================================================================================================
  
January 21, 2019

  * Code updates mainly to .inc files.
  * No detectable difference in the output with these updates.

====================================================================================================

February 20-23, 2019
  * N imbalance led me to discover that gtcroote(N) was going negative.  After much searching, 
    I found the problem in drootgt, a copy/paste/modify error. gtroot was using the C:N of
    mature fine roots.
  * I switched the order of GTCROOT and GTFROOTM to be consistent with the order for forests.
    GTCROOT=4 and GTFROOTM=5. 
  * There was an extra (uninitialized) parameter in pargt.inc, gtrprtf(MAXIEL) that was being 
    used where gtrtf(MAXIEL) should have been used. GTRTF was in grasstree.100 but gtrprtf
    was not.  Deleted gtrprtf because I could not find it's purpose. Before this fix, gtstg(N)
    was always zero. 
  * TO DO: update gtfirrtn to use the new trem.100 parameters.

====================================================================================================

April 9, 2019

Added pH effects on Denitrification to the grasstree version.  Here are the comments from
the muvp version where these updates were originally implemented:

Notes from February 3, 2019

I talked with Steve Del Grosso on Thursday.  He said low pH reduces complete denitrification to N2. 
He suggested I create a 0.0-1.0 multiplier on the N2:N2O ratio in the denitrification subroutine 
which is 0.0 at low pH and 1.0 at high pH.  I started with a linear function that is 0.0 at pH 1.0 
and below and 1.0 at pH 9.0 and above.  If the resulting ratio is too high or too low, one can use 
the N2:N2O ratio adjustment multiplier in sitepar.in to decrease or increase the ratio.

In the denitrification subroutine, the amount of N2 and N2O is calculated by layer.  Previously 
there was no pH effect on denitrification. The denitrification routine now uses the pH by layer 
from the soils.in file to calculate the effect on the N2:N2O ratio for each soil layer. 
The pH effect on nitrification now uses pH from the top 10cm instead of pH from the top layer only.
This is now consistent with pH used for the pH effect on decomposition.

I added 3 columns to nflux.out to give us feedback on this modification.  

Rn2n2o_1 - Ratio of N2:N2O in topmost layer where denitrification occurs before adjustment for pH effect 

Rn2n2opH - Ratio of N2:N2O in topmost layer where denitrification occurs adjusted for pH effect 
pH - soil pH in top 3 daycent layers, adjusted by pH scaling factor (if used). 
This is the pH value used for pH effect on decomposition and nitrification.

Internally the ratio of N2:N2O can not be below 0.1, but the output file may show values less than that.

====================================================================================================

Notes from February 7, 2019

I abandoned the linear pH effect for fRpH.  Implemented the one by Wagena et al. 2017 instead.
Also added their pH effect on total denitrification.

Added two variables to the end of sitepar.in to turn on and off these effect.

1        / dofDpH: compute pH effect on gross denitrification (1=yes, 0=no)
1        / dofRpH: compute pH effect on Ratio N2:N2O during denitrification (1=yes, 0=no)

From denitrify.c:

        /*----------------------------------------------------------------------------*/
        /* Affect of pH on total denitrification flux. -mdh 2/7/2019                  */
        /* Expontential fDpH from Wagena_2017: Development of a nitrous oxide routine */
        /* for the SWAT model to assess greenhouse gas emissions from agroecosystems  */

        if (layers->pH[ilyr]*(*pHscale) <= 3.5) {
          fDpH = 0.001;
        } else if (layers->pH[ilyr]*(*pHscale) >= 6.5) {
          fDpH = 1.0;
        } else {
          fDpH = (layers->pH[ilyr]*(*pHscale) - 3.5) / 3.0;
        }
        if (sitepar-> dofDpH != 1) {
          fDpH = 1.0;
        }
        Dtotflux *= fDpH;
        if (debug) {
          fprintf(stdout, "denitrify: dofDpH = %1d\n", sitepar->dofDpH); 
          fprintf(stdout, "lyr %1d: pH = %10.4f\n", ilyr, layers->pH[ilyr]);
          fprintf(stdout, "lyr %1d: fDpH = %10.4f\n", ilyr, fDpH);
        }
        /*----------------------------------------------------------------------------*/


        /*----------------------------------------------------------------------------*/
        /* Added pH effect on the ratio of N2:N2O. -mdh 2/7/2019                      */
        /* fRpH = (y2-y1)/(x2-x1)*(layers->pH[ilyr]*(*pHscale) - x1) + y1; */
        /* Expontential fRpH from Wagena_2017: Development of a nitrous oxide routine */
        /* for the SWAT model to assess greenhouse gas emissions from agroecosystems  */
        fRpH = 1/(1470*exp(-1.1*layers->pH[ilyr]*(*pHscale)));
        if (debug) {
          fprintf(stdout,"denitrify: dofRpH = %1d\n", sitepar->dofRpH); 
          fprintf(stdout, "lyr %1d: pH = %10.4f\n", ilyr, layers->pH[ilyr]);
          fprintf(stdout, "lyr %1d: fRpH = %10.4f\n", ilyr, fRpH);
        }
        if (sitepar-> dofRpH != 1) {
          fRpH = 1.0;
        }
        /* Save the values of Rn2n2o in topmost layer where denitrification occurs */
        if (*Rn2n2o_1 <= 0.0001) {
          *Rn2n2o_1 = Rn2n2o;
          *Rn2n2opH = Rn2n2o * fRpH;
        }
        Rn2n2o *= fRpH;
        if (Rn2n2o < 0.1) { Rn2n2o = 0.1; }
        /*----------------------------------------------------------------------------*/


====================================================================================================
April 9, 2019

Changed format of sitepar.in to include the variable names next to the values.  The function
initsw.f now checks for valid variable names in the file.
  To Do: Set defaults in case some variables are left out. Alternative, check that all variables
         are included. (done 5/10/2019).

====================================================================================================
May 10, 2019

Allow output of potential production from crops, forests, and grasstrees.  The output of files
potcrp.csv, potfor.csv, and potgt.csv is turned on or off in outfiles.in. This requires 3 extra
lines at the bottom of outfiles.in.

New file: wrtpotprd.c
Updated files: potcrp.f, potfor.f, potgrasstree.f, initsite_tg.c, soilwater.h

Note: potcrp.f, potfor.f, and potgrasstree.f pass curday as an argument instead of daylenght(curday).

------
Updated initsite_tg.c to use default parameter values if they are not initialized by the user.
====================================================================================================

May 31, 2019

* Use laiprd scalar on grasstree potential production, similar to that found in potfor subroutine.
* Updated wrtpotprd.c because it was only writing the file for grasstrees.  I was missing some code.
* Updated closefiles to include potential production output files.
* Added write statements to several functions in attempt to understand how crpstg(1) gains and 
  losses N.  No errors found. Note: NOx is absorbed by canopy in tgmodel.c, and that was
  the source of N that I was searching for.  
* Update to is_saturated calculation in tgmodel.c:

      /* This calculation compares the average layer volumetric water content */
      /* to the depth weighted average soil water at field capacity. Not equivalent. */
      /* If the volumetric soil water content in the top 3 soil layers is */
      /* greater than field capacity consider the soil to be saturated */
      /* avg_vswc = (float)(layers->swc[0] / layers->width[0] + */
      /*                   layers->swc[1] / layers->width[1] + */
      /*                   layers->swc[2] / layers->width[2]) / 3.0f; */
      /* avg_fc = (layers->fieldc[0] * layers->width[0]  + */
      /*          layers->fieldc[1] * layers->width[1] + */
      /*          layers->fieldc[2] * layers->width[2]) / */
      /*         (layers->width[0] + layers->width[1] + layers->width[2]); */
      /* if (avg_vswc <= avg_fc) { */
      /*  is_saturated = 0; */
      /*} else { */
      /*  is_saturated = 1; */
      /* } */

      /* Kendrick redefined that test in the trunk to just compare the 
      /* surface water to the the water held at field capacity. 5/23/2019 */
      is_saturated = (layers->swc[0] + layers->swc[1] + layers->swc[2]) >
          (layers->fieldc[0] * layers->width[0] +
           layers->fieldc[1] * layers->width[1] +
           layers->fieldc[2] * layers->width[2])? 1: 0;

--------------------------------------------------------------------------------
6/7/2019
update to dailymoist.f
transpiration from TREES was near zero since aggreenc, used to calculate total LAI, 
did not include rleavc

c ... Include tree and grasstree leaf area in aggreenc for transpiration calculations. -mdh 6/7/2019
c     aggreenc = aglivc * scenfrac
      aggreenc = aglivc * scenfrac + rleavc + gtleavc

================================================================================
6/13/2019

Allow the first Century layer (ADEP(1) in fix.100) to be a flexible number 
of soils.in layers, so that it can be deeper than 10cm.
We are no longer assuming that that the top Century layer or the ammonium 
layer is comprised of the first 3 soils.in layers.
The code in the following files has been updated using 
layers->ubnd[0] and layers->lbnd[0] to specify the indices of the 
soils.in layers inside of the top Century layer.

**    layers->lbnd[] - the index of the lower soil water model layer which
**                     corresponds to clyr in Century (0 based)
**    layers->ubnd[] - the index of the upper soil water model layer which 
**                     corresponds to layer clyr in Century (0 based)

Instead of assuming that the top century layer is composed of the top 3
soils.in layers (index=0,1,2), we should use ilyr = layers->lbnd[0] .. layers->ubnd[0].

    for(ilyr = layers->ubnd[0]; ilyr <= layers->lbnd[0]; ilyr++) 
    {
    }

................................................................................
calcdefac.c - this code was updated:

      /* avgwfps is passed to the trace_gas_model, calculate this value */
      /* every time calcdefac is called, cak - 09/22/03 */
      wfps(layers);
      *avgwfps = (layers->wfps[0]*layers->width[0] +
                 layers->wfps[1]*layers->width[1] +
                 layers->wfps[2]*layers->width[2]) /
                 (layers->width[0] + layers->width[1] + layers->width[2]);

      switch (*idef) {
          /* Compute water effect for soil decomposition using a weighted */
          /* averaging of the 2nd, and 3rd soil layers, cak - 08/01/04 */
          for (ilyr = 1; ilyr < 3; ilyr ++) {
            rel_wc[ilyr] = ((float)layers->swc[ilyr]/(layers->width[ilyr]) -
                            layers->swclimit[ilyr]) /
                            (layers->fieldc[ilyr] - layers->swclimit[ilyr]);

      /* Compute the soil temperature effect on decomposition */
      /* Compute the temperature effect on decomposition */
      /* using an arctangent function.  CAK - 03/16/01   */
      /* Use a weighted average of the average soil temperature */
      /* in the second and third soil layer when calculating tfunc, */
      /* cak - 07/30/04 */
      *avgstemp = (soil->soiltavg[1] * layers->width[1] + 
                   soil->soiltavg[2] * layers->width[2]) /
                  (layers->width[1] + layers->width[2]);
      tcalc(avgstemp, teff, tfunc);

................................................................................
cmpnfrac.c - this code was updated:

      /* Total Nitrogen in top 10 cm */
      Nsum = *ammonium + nitrate[0] + nitrate[1] + nitrate[2];

................................................................................
nitrify.c - this code was updated:

      /* Convert ammonium (g/m2) to nh4_conc (ppm) */
      /* Assume all ammonium occurs in the top 15 cm */

      grams_soil = (layers->bulkd[0]*layers->width[0] +
                    layers->bulkd[1]*layers->width[1] +
                    layers->bulkd[2]*layers->width[2])*100*100;
      nh4_conc = (float)*ammonium/grams_soil*1.0E6f;



      /* Compute relative water content in the 2nd and 3rd soil layers, */
      /* cak - 08/19/04 */
      for (ilyr = 1; ilyr < 3; ilyr ++) {
        rel_wc[ilyr] = ((float)layers->swc[ilyr]/(layers->width[ilyr]) -
                        layers->swclimit[ilyr]) /
                        (layers->fieldc[ilyr] - layers->swclimit[ilyr]);
        if (rel_wc[ilyr] < 0.0) {
          rel_wc[ilyr] = 0.0f;
/*        } else if (rel_wc[ilyr] > 1.0) {
          rel_wc[ilyr] = 1.0f; */
        }
        rel_wc[ilyr] *= layers->width[ilyr];
      }
      avg_rel_wc = (rel_wc[1] + rel_wc[2]) /
                   (layers->width[1] + layers->width[2]);



        /* Compute average water filled pore space in 2nd and 3rd soil */
        /* layers, cak - 08/19/04 */
        wfps(layers);
        avgwfps = (layers->wfps[1]*layers->width[1] +
                   layers->wfps[2]*layers->width[2]) /
                  (layers->width[1] + layers->width[2]);
        if (debug > 1) {
          fprintf(stdout, "avgwfps = %6.2f\n", avgwfps);
        }
        avgfc = (layers->fieldc[1]*layers->width[1] +
                 layers->fieldc[2]*layers->width[2]) /
      }

      /* Rates of nitrification were too low at low soil temperatures, */
      /* shift the curve so that the nitrification rates are effectively */
      /* higher for cooler sites, this change does not affect sites with */
      /* hot temperatures, cak - 11/25/03 */
      avgstemp = (soil->soiltavg[1] * layers->width[1] + 
                  soil->soiltavg[2] * layers->width[2]) /
                 (layers->width[1] + layers->width[2]);

................................................................................
tgmodel.c - this code was updated:

  * calculation of is_saturated
      /* Kendrick redefined that test in the trunk to just compare the 
      /* surface water to the the water held at field capacity. 5/23/2019 */
      is_saturated = (layers->swc[0] + layers->swc[1] + layers->swc[2]) >
          (layers->fieldc[0] * layers->width[0] +
           layers->fieldc[1] * layers->width[1] +
           layers->fieldc[2] * layers->width[2])? 1: 0;

................................................................................
updateN.c - this code was updated:

      if (*frac_no3 > 0.0) {
        if ((*clyr == 1) && (no3amt >= 0.0)) {
          /* ADD nitrate to top 15 cm */
          nitrate[0] += no3amt*0.07;  /* assumes 1 cm */
          nitrate[1] += no3amt*0.20;  /* assumes 3 cm */
          nitrate[2] += no3amt*0.73;  /* assumes 11 cm */

................................................................................
wrtsoiln.c - this code was updated:

      /* Ammonium is in the top 15 cm */
      grams_soil = (layers->bulkd[0]*layers->width[0] +
                    layers->bulkd[1]*layers->width[1] +
                    layers->bulkd[2]*layers->width[2])*(100*100);

      NH4_ppm = *ammonium/grams_soil * 1.0E6;

================================================================================

Added Luxury N uptake option for annual plants.
June 19, 2019

................................................................................
parcp.inc
  Added variables  luxeupf(3), cstgeupf(3), cstgdys, and cstga2drat

................................................................................
cropin.f

c LUXURY N UPTAKE MODIFICATIONS:
c Also CROPLNS at the top of the file was increased by 8,
c and these variables were added to parcp.inc.
c ..... Parameters controlling Luxury N uptake. -mdh 6/19/2019
        read(11, *) luxeupf(N), name
        call ckdata('cropin','luxeupf',name)
        read(11, *) luxeupf(P), name
        call ckdata('cropin','luxeupf',name)
        read(11, *) luxeupf(S), name
        call ckdata('cropin','luxeupf',name)
        read(11, *) cstgeupf(N), name
        call ckdata('cropin','cstgeupf',name)
        read(11, *) cstgeupf(P), name
        call ckdata('cropin','cstgeupf',name)
        read(11, *) cstgeupf(S), name
        call ckdata('cropin','cstgeupf',name)
        read(11, *) temp, name
        cstgdys = int(temp)
        call ckdata('cropin','cstgdys',name)
        read(11, *) cstga2drat, name
        call ckdata('cropin','cstga2dr',name)

................................................................................
growth.f

c LUXURY N UPTAKE MODIFICATIONS:
c ..... New variables to compute luxury N uptake. -mdh 6/19/2019
c ..... crpstgin = amount of internal N storage available for uptake,
c .....   passed to the restrp subroutine.
c ..... lueavail = amount of N available in a layer for luxury N
c .....   uptake.  Calculated after conventional N uptake has occurred.
        crpstgin(iel) = 0.0
        luxeavail(iel) = 0.0

c LUXURY N UPTAKE MODIFICATIONS:
c ..... For annual plants doing luxury N uptake, calculate the amount
c ..... of internal N storage available for uptake. -mdh 6/19/2019

        if (((frtcindx .eq. 2) .or. (frtcindx .ge. 4)) 
     &       .and. (luxeupf(N) .gt. 0.0) .and. (cgrwdys .lt. cstgdys)
     &       .and. (crop_a2drat(N) .ge. cstga2drat)) then

c ....... Under these conditions, annual plants will not use crpstg to 
c ....... fullfill E demand. Here, cgrwdys is the number of days in the 
c ....... current growing season, excluding today. The cstgdys variable 
c ....... is a crop.100 parameter. Also, the ratio of available E to 
c ....... the demand for E (a2drat) is high. The value of crop_a2drat
c ....... was initialized in cropDynC, and is updated by the call to restrp. 

          do 72 iel=1,nelem
            crpstgin(iel) = 0.0
72        continue
        else 
          if (((frtcindx .eq. 2) .or. (frtcindx .ge. 4)) .and. 
     &         (luxeupf(N) .gt. 0.0)) then
CCCCC&         (luxeupf(N) .gt. 0.0) .and. (cgrwdys .ge. cstgdys)) then

c ......... Under these conditions, luxury N uptake is allowed, but
c ......... either crop_a2drat(N) < cstga2drat or cgrwdys >= cstgdys. 
c ......... Annual plants can use a fraction of crpstg to fullfill E demand.

            do 74 iel=1,nelem
              crpstgin(iel) = max(0.0, cstgeupf(iel)*crpstg(iel))
74          continue
          else
c ......... All of crpstg is available for uptake.
            do 76 iel = 1, nelem
              crpstgin(iel) = crpstg(iel)
76          continue
          endif
        endif

c ..... Determine actual production values, restricting the C/E ratios.
c       call restrp(elimit, nelem, availm, cercrp, CPARTS-1, cfrac,
c    &    pcropc, rimpct, crpstg, snfxmx(CRPSYS), cprodcdy,
c    &    eprodcdy, uptake, crop_a2drat, cropNfix, relyld)

        call restrp(elimit, nelem, availm, cercrp, CPARTS-1, cfrac,
     &    pcropc, rimpct, crpstgin, snfxmx(CRPSYS), cprodcdy,
     &    eprodcdy, uptake, crop_a2drat, cropNfix, relyld)

c LUXURY N UPTAKE MODIFICATIONS:
c ... Determine of there is luxury N uptake to crpstg(N). 
c ... There has been no flowup event in this subroutine, so the amount 
c ... of N uptake must be considered when computing the soil N available.
c ... Only consider N for now. Once we finalize the algorithm it
c ... will be generalized for all elements. -mdh 6/17/2019
      esum = 0.0
      do 100 lyr = 1, claypg
        esum = esum + minerl(lyr,N)*favail(N)*rimpct*gnfrac
100   continue
      esum = esum - uptake(ESOIL,N)
      if (esum .gt. 1e-6 .and. luxeupf(N) .gt. 0.0 .and. 
     &    cgrwdys .lt. cstgdys) then
        do 101 lyr = 1, claypg
          luxeavail(N) = minerl(lyr,N)*favail(N)*rimpct*gnfrac
     &                   - uptake(ESOIL,N)*(minerl(lyr,N)/esum)
          if (luxeavail(N) .gt. 0.0) then
c ......... I am concerned that taking up the full luxeavail(N) 
c ......... instead of a fraction of it might cause N balance errors. 
c ......... -mdh 6/19/2019
            amt = min(luxeavail(N),luxeupf(N)*uptake(ESOIL,N))
c           write(*,*) 'time =', time
c           write(*,*) 'Potential luxury N to crpstg for lyr=', lyr, 
c    &                  ': ', luxeupf(N)*uptake(ESOIL,N)
c           write(*,*) 'Actual luxury N for lyr=', lyr, ' is ', amt
c           write(*,*) 'luxeupf(N) =', luxeupf(N)
c           write(*,*) 'cstgdys =', cstgdys
c           write(*,*) 'cgrwdys =', cgrwdys
            namt = -amt
            call cmpnfrac(lyr,ammonium,nitrate,minerl,
     &                  frac_nh4,frac_no3,no3pref(CRPSYS))
            call update_npool(lyr, namt, frac_nh4_fert, 
     &                  frac_no3_fert, ammonium, nitrate,
     &                  subname)
            call flow(minerl(lyr,N),crpstg(N),time,amt)
          endif
101     continue
      endif

================================================================================

Added flooding effect on potential production for crops, trees, grasstrees.
June 19, 2019

................................................................................
param.inc

c ... Added flodeff(3). -mdh 6/19/2019

................................................................................
3 new variables in potent.inc

swcpg    - amount of water in the rooting zone (1..nlaypg) (cm H2O)
swcfcpg  - amount of water when at field capacity in the rooting zone (1..nlaypg) (cm H2O)
swcsatpg - amount of water when at saturation in the rooting zone (1..nlaypg) (cm H2O)

................................................................................
cropin.f

      parameter (CROPLNS = 145)

c ..... FLOOD EFFECT. -mdh 6/19/2019
        read(11, *) flodeff(CRPSYS), name
        call ckdata('cropin','flodeff',name)
................................................................................
treein.f

      parameter (TREELNS = 179)

c ..... FLOOD EFFECT. -mdh 6/19/2019
        read(11, *) flodeff(FORSYS), name
        call ckdata('treein','flodeff',name)

................................................................................
grasstreein.f

      parameter (GTLNS = 156)

c ..... FLOOD EFFECT. -mdh 6/19/2019
        read(11, *) flodeff(GTARYINDX), name
        call ckdata('grasstreein','flodeff',name)
................................................................................
potcrp.f
................................................................................
potfor.f
................................................................................
potgrasstree.f
................................................................................
dailymoist.f

3 new arguments to watrflow
          REAL             swcpg
          REAL             swcfcpg
          REAL             swcsatpg

................................................................................
watrflow.c

3 new arguments to watrflow

**    swcpg      - amount of water in the rooting zone (1..nlaypg) (cm H2O)
**    swcfcpg    - amount of water when at field capacity in the rooting zone (1..nlaypg) (cm H2O)
**    swcsatpg   - amount of water when at saturation in the rooting zone (1..nlaypg) (cm H2O)

                  float *h2ogef, float *stcrlai, double *srad, float *swcpg,
                  FLOAT *swcfcpg, float *swcsatpg)

      /* Compute total rooting zone swc, swc at field capacity, and swc at saturation */
      /* Used for flood effect on potential production.-mdh 6/19/2019 */
      *swcpg = 0.0;
      *swcfcpg = 0.0;
      *swcsatpg = 0.0;
      for(ilyr=0; ilyr <= layers->lbnd[*nlaypg]; ilyr++) {
        *swcpg += layers->swc[ilyr];
        *swcfcpg += layers->fieldc[ilyr] * layers->width[ilyr];
        *swcsatpg += (1.0f - layers->bulkd[ilyr] / (float)PARTDENS) *
                     layers->width[ilyr];
      }
     

================================================================================
July 18, 2019
Code copied from here:
N:\Research\Parton\melannie\DAYCENT_SOURCE\MUVP\DailyDayCent_muvps_gt\SOURCE_FLDEFF 

................................................................................
harvest.f
E for egrain comes from crpstg as well as aglive.

        if (flghrv .eq. 1) then
c ....... Include internal E storage in egrain uptake. -mdh 7/18/2019
c         egrain(iel) = efrgrn(iel) * aglive(iel) * (1.0 - aglrem) *
c    &                  sqrt(hi/himax)
          egrain(iel) = efrgrn(iel) * (aglive(iel)+max(0.0,crpstg(iel))) 
     &                  * (1.0 - aglrem) * sqrt(hi/himax)
c ....... The flow of E to egrain is split proportionally between aglive 
c ....... and crpstg. -mdh 7/18/2019
c         call flow(aglive(iel),esrsnk(iel),time,egrain(iel))
          amt1 = egrain(iel) * aglive(iel) / 
     &           (aglive(iel)+max(0.0,crpstg(iel)))
          amt2 = egrain(iel) * max(0.0,crpstg(iel)) /
     &           (aglive(iel)+max(0.0,crpstg(iel)))
          call flow(aglive(iel),esrsnk(iel),time,amt1)
          call flow(crpstg(iel),esrsnk(iel),time,amt2)

================================================================================

July 19-August 27 2019

Code updates needed to implement mineral E uptake that is based on fraction
of water use (transpriation) in each soil layer or fraction of roots in each layer.  

................................................................................
Add dofwueup parameter to the end of the sitepar.in file.  

................................................................................
initsite_tg.c

Read dofwueup from sitepar.in. Search for "dofwueup" for required updates.

................................................................................
initsw.c

Pass dofwueup back to subroutine detiv where it will be initialized as
one of the param.inc variables so it can be accessed from the FORTRAN 
subroutines.

          void initsw(float *sitlat, float swcinit[MXSWLYR], int *usexdrvrs,
                int *numlyrs, int *texture, float daylength[NDAY],
                float sradadj[NMONTH], float *tminslope, float *tminintercept,
                float *maxphoto, float *bioabsorp, int *dofwueup, int *ext_flag,
                float swcextend[MXSWLYR])int dofDpH, dofRpH, dofwueup;

Retrieve the value of dofwueup from sitepar.in.  Also set sitepar->dofwueup
so the value of this variable can be accessed fromt hthe C subroutines.

      initsite(FNSITE, sitepar, layers, flags, sradadj, tminslope,
               tminintercept, maxphoto, bioabsorp, &dofDpH, &dofRpH, dofwueup);
      latitude = *sitlat;
      if (dofDpH != 1) { dofDpH = 0; }
      if (dofRpH != 1) { dofRpH = 0; }
      if (*dofwueup != 1) { *dofwueup = 0; }
      sitepar->dofDpH = dofDpH;
      sitepar->dofRpH = dofRpH;
      sitepar->dofwueup = *dofwueup;

................................................................................
soilwater.h

Add new parameter to SITEPAR_S:

  int    dofwueup;
} SITEPAR_S, *SITEPAR_SPT;


Add new variable to LAYERPAR_S:

  float  ftransp[MXSWLYR];
  :
} LAYERPAR_S, *LAYERPAR_SPT;


Add new argument to initsite function prototype:

void initsite(char *sitename, SITEPAR_SPT sitepar, LAYERPAR_SPT layers,
              FLAG_SPT flags, float sradadj[NMONTH], float *tminslope,
              float *tminintercept, float *maxphoto, float *bioabsorp,
              int *dofDpH, int *dofRpH, int *dofwueup);

................................................................................
soiltransp.c

Replace local variable ftransp with layers->ftransp[ilyr]

      /* float ftransp; */

        /* ftransp = swpfrac[ilyr] / sumswpf; */
        layers->ftransp[ilyr] = swpfrac[ilyr] / sumswpf;

        /* transp[ilyr] = ftransp * bstrate; */
        transp[ilyr] = layers->ftransp[ilyr] * bstrate;

................................................................................
param.inc

add "dofwueup" to parameter list

      common/param/afiel(10),amov(10),autoresp1(2),autoresp2(2),
     &    awilt(10),basef,bioabsorp,bulkd,
     &    co2ipr(3),co2ice(3,2,3),co2irs(3),co2itr(3),co2sys,co2tm(2),
     &    crpindx,crpcmn,crpcmx,crpmnmul,crpmxmul,
     &    drain,epnfa(2),epnfs(2),falprc,fracro,
     &    hpttr(12),htran(12),ivauto,labtyp,labyr,
     &    ckmrspmx(3),cmrspnpp(6),dofwueup,flodeff(3),fkmrspmx(6),

      integer falprc,ivauto,labtyp,labyr,micosm,nelem,
     &        Ninput,nlayer,nlaypg,Nstart,OMADinput,OMADstart,phsys,
     &        phtm,swflag,claypg,claypg_const,gtlaypg,tlaypg,
     &        crpindx,trpindx,watrflag,dofwueup

................................................................................
default.f

Under the section for param

      dofwueup = 0

These accumulators were not initialized. -mdh 9/5/2019
1364d1363
<       gtcreta = 0.0
1405d1403
<         gtereta(ii) = 0.0
................................................................................
csa_detiv.f

add "dofwueup" to initsw function call

        SUBROUTINE initsw(sitlat,swcinit,usexdrvrs,numlyrs,texture,
     &                  daylength,sradadj,tminslope,tminintercept,
     &                  maxphoto,bioabsorp,dofwueup,ext_flag,swcextend)
          !MS$ATTRIBUTES ALIAS:'_initsw' :: initsw
          REAL             sitlat
          REAL             swcinit(*)
          INTEGER          usexdrvrs
          INTEGER          numlyrs
          INTEGER          texture
          REAL             daylength(*)
          REAL             sradadj(*)
          REAL             tminslope
          REAL             tminintercept
          REAL             maxphoto
          REAL             bioabsorp
          INTEGER          dofwueup
          INTEGER          ext_flag
          REAL             swcextend(*)
        END SUBROUTINE initsw


      call initsw(sitlat, swcinit, usexdrvrs, numlyrs, texture,
     &            daylength, sradadj, tminslope, tminintercept,
     &            maxphoto, bioabsorp, dofwueup, ext_flag, swcextend)

................................................................................
New source code: setfwueup.c. This was also added to the Makefile.

  Subroutine setfwueup computes fwueup[nlayer] (Century soil layers) from 
  layers->ftransp[numlyrs] or layers->tcoeff[numlyrs] (DayCent soil layers).

  This will be called from subroutine growth in order to calculate mineral E
  uptake by crops/grasses. Later it may also be used by subroutine treegrow 
  and subroutine grasstreegrow.

................................................................................
New source code: adjfeup.f. This was also added to the Makefile.

  When doing mineral E uptake based on fwueup, this subroutine will be called 
  from growth (and later treegrow) to adjust the fwueup(lyr) fractions if 
  one or more of these fractions would cause overextraction of E from a soil layer.

................................................................................

growth.f

New local variables:

      real             esum, fwueup(CMXLYR), frootdens(CMXLYR)
      real             feup(CMXLYR)

New subroutine interface:

        SUBROUTINE setfwueup(nlayer, dofwueup, fwueup)
          !MS$ATTRIBUTES ALIAS:'_flow' :: flow
          INTEGER nlayer
          INTEGER dofwueup
          REAL fwueup(*)
        END SUBROUTINE setfwueup



c ......... Take up nutrients from soil
c ......... Nutrients for uptake are available in the top claypg layers,
c ......... cak 01/29/03
c ......... If we have reached the late growing season flow nutrients to
c ......... the crop/grass storage pool rather than to the component
c ......... nutrient pools based on the crop/grass late season growth
c ......... restriction parameter value, cak - 03/11/2010

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
c Update for fwueup. -mdh 7/25/2019, 8/6/2019
c ......... uptake(ESOIL,iel) = total E uptake from the soil;
c .........   this needs to be distributed among layers.
c ......... calcup is the uptake from the soil layer (lyr)
c .........   based on the relative amount of E in that layer.
c ......... fwueup(lyr) = fraction of transpiration that occurred from 
c .........   the soil layer (lyr)
c ......... frootdens(lyr) = fraction of roots in each soil layer (lyr)
c ......... feup(lyr) = Fraction of mineral E uptake that occurs from the 
c .........   soil layer. Initially, this is the fraction of mineral E in the 
c .........   layer relative to the total abundance E within the rooting zone,
c .........   which is the default fraction.

            do 25 lyr = 1, claypg
              if (minerl(lyr,iel) .gt. toler) then
                fsol = 1.0
                if (iel .eq. P) then
                  fsol = fsfunc(minerl(SRFC,P), pslsrb, sorpmx)
                endif
                feup(lyr) = minerl(lyr,iel) * fsol / soilm(iel)
              endif
25          continue

            if (dofwueup .ge. 1) then
              write(*,*)
              write(*,*) 'Fraction E uptake based on N distribution: '
              do 26 lyr = 1, claypg
                write(*,'(a5,i1,a4,f7.4)') 'feup(',lyr,') = ',feup(lyr)
26            continue
            endif

            if (dofwueup .eq. 1) then

c ........... Retrieve fwueup(*), the fraction of transpiration from each layer.
              call setfwueup(claypg, dofwueup, fwueup)

              write(*,*) 'Fraction E uptake based on transpiration: '
              do 27 lyr = 1, claypg
                write(*,'(a7,i1,a4,f7.4)') 'fwueup(',lyr,') = ',
     &                                     fwueup(lyr)
27            continue

c ........... If using fwueup(*), reset feup(*) to fwueup(*) when this 
c ........... would not result in overextraction of E from any layer.
c ........... Otherwise, feup(*) will be based on N distribution.
c ........... Make sure gnfrac (fraction of N to crops/grasses) is set
c ........... to 1.0 for non-savannas.
              call adjfeup(claypg, iel, time, uptake(ESOIL,iel), 
     &                     gnfrac, rimpct, fwueup, feup)
            endif

            if (dofwueup .eq. 2) then

c ........... Retrieve frootdens(*), the fraction of of roots in each layer.
              call setfwueup(claypg, dofwueup, frootdens)

              write(*,*) 'Fraction E uptake based on root density: '
              do 28 lyr = 1, claypg
                write(*,'(a10,i1,a4,f7.4)') 'frootdens(',lyr,') = ',
     &                                     frootdens(lyr)
28            continue

c ........... If using frootdens(*), reset feup(*) to frootdens(*) then
c ........... adjust feup(*) so there no everextraction of N from any layer.
c ........... Make sure gnfrac (fraction of N to crops/grasses) is set
c ........... to 1.0 for non-savannas.
              call adjfeup(claypg, iel, time, uptake(ESOIL,iel), 
     &                     gnfrac, rimpct, frootdens, feup)
            endif
c --------------------------------------------------------------------------------
c --------------------------------------------------------------------------------

            do 30 lyr = 1, claypg
              if (minerl(lyr,iel) .gt. toler) then

c               fsol = 1.0
c               if (iel .eq. P) then
c                 fsol = fsfunc(minerl(SRFC,P), pslsrb, sorpmx)
c               endif

                call cmpnfrac(lyr,ammonium,nitrate,minerl,
     &                        frac_nh4,frac_no3,no3pref(CRPSYS))
c ............. Changed availm(iel) to soilm(iel) to get the correct
c ............. normalization, KLK - 03/27/2009
c                calcup = uptake(ESOIL,iel) *
c     &                   minerl(lyr,iel) * fsol / availm(iel)
c               calcup = uptake(ESOIL,iel) *
c    &                   minerl(lyr,iel) * fsol / soilm(iel)
                calcup = uptake(ESOIL,iel) * feup(lyr)
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

================================================================================
August 30, 2019

Begin updates for annual grasstree

................................................................................
froota.f

Add case (gtfrtcindx .eq. 2)

        if (gtfrtcindx .eq. 1) then

c ....... A perennial grasstree
c ....... Effect of water limitation
          h2oeff = line(h2ogef, 0.0, gtfrtcw(1), 1.0, gtfrtcw(2))
c ....... Effect of nutrient limitation
          ntreff = 0.0
          do 40 iel = 1, nelem
            temp = line(a2drat(iel), 0.0, gtfrtcn(1), 1.0, gtfrtcn(2))
            ntreff = max(temp, ntreff)
40        continue
c ....... Compute fraction of C to go to roots
          froota = max(h2oeff, ntreff)
          froota = min(froota, 0.99)

        elseif (gtfrtcindx .eq. 2) then

c ....... An annual grasstree
c ....... Compute fraction of C to the roots when there is no 
c ....... nutrient or water stress
          froota = ramp(real(plntcnt), 0.0, gtfrtc(1), gtfrtc(3), gtfrtc(2))

c ....... Effect of water limitation
c ....... Amount to increase fraction of C going to roots due to
c ....... water stress
          h2oeff = line(h2ogef, 0.0, gtfrtc(4), 1.0, 0.0)

c ....... Effect of nutrient limitation
c ....... Amount to increase fraction of C going to roots due to
c ....... nutrient stress
          ntreff = 0.0
          do 30 iel = 1, nelem
            temp = line(a2drat(iel), 0.0, gtfrtc(5), 1.0, 0.0)
            ntreff = max(temp, ntreff)
30        continue

c ....... Compute fraction of C to go to roots
c ....... Fraction of C to go to roots is adjusted due to nutrient or
c ....... water stress
          temp = max(h2oeff, ntreff)
          temp = min(temp, 0.99)
          froota = min(froota + temp, 0.99)

        else
          write(*,*) 'Invalid value of gtfrtcindx in froota. '
          write(*,*) 'gtfrtcindx = ', gtfrtcindx
          STOP
        endif
................................................................................

potgrasstree.f

c ..... Restriction on seedling growth
c ..... sdlng is the fraction that prdx is reduced
c ..... ATTENTION: are both seedl and laiprd needed?
c       if (gtleavc .gt. gtfulcan) then
c         seedl = 0
c       endif
c
c       if (seedl .eq. 1) then
c         sdlng = min(1.0, gtpltmrf + gtleavc*(1-gtpltmrf) /gtfulcan)
c       else
c         sdlng = 1.0
c       endif

        sdlng = 1.0
................................................................................

savarp.f

c ... Do not include the crop/grass carbohydrate storage pool values
c ... in the summation for total system carbon when simulating an
c ... annual plant, CAK - 12/04/2014
      if ((frtcindx .eq. 2) .or. (frtcindx .ge. 4) 
     &     .or. gtfrtcindx .eq. 2) then
        totsysc = fsysc + aglivc + bglivcj + bglivcm + stdedc +
     &            gtpltc + 
     &            carbostg(FORSYS,UNLABL) + carbostg(FORSYS,LABELD)
      else
        totsysc = fsysc + aglivc + bglivcj + bglivcm + stdedc +
     &          gtpltc +
     &          carbostg(CRPSYS,UNLABL) + carbostg(CRPSYS,LABELD) +
     &          carbostg(FORSYS,UNLABL) + carbostg(FORSYS,LABELD) +
     &          carbostg(GTARYINDX,UNLABL) + carbostg(GTARYINDX,LABELD)
      endif
................................................................................

simsom.f

c......................................................................
c ..... Code added for grasstree. -mdh 1/24/2019
        if (dogtlast .and. (gtlastday .eq. curday)) then
c         write(*,*) 'dogtlast=true at time and day', time, curday
          grasstreegrw = 0
          gtgrwdys = 0
          gtpsndys = 0
          plntcnt = 0
          call inprac(GRASSTREESYS)
c ....... For an annual grasstree reset the rooting depth back to 1 on a
c ....... GLST event
          if (gtfrtcindx .eq. 2) then
            gtlaypg = 1
c ......... If this is an annual plant flow the carbohydrate storage pool
c ......... to the csrsnk
            csrsnk(UNLABL) = csrsnk(UNLABL) + carbostg(GTARYINDX,UNLABL)
            csrsnk(LABELD) = csrsnk(LABELD) + carbostg(GTARYINDX,LABELD)
            carbostg(GTARYINDX,UNLABL) = 0.0
            carbostg(GTARYINDX,LABELD) = 0.0
        endif

................................................................................

dailymoist.f

c ... If the grasstree plant functional type is annual calculate a
c ... dynamic value for gtlaypg from 1 to gtlaypg_const based on the
c ... number of days since planting and GTFRTC(3), gtlaypg_const is the
c ... value of gtlaypg as read for the current GRAS option
c ... -mdh 8/30/2019
      if (cursys .eq. GRASSTREESYS) then
        if (gtfrtcindx .eq. 2) then
          gtclaypg = nint(ramp(real(plntcnt), 0.0, 1.0, gtfrtc(3),
     &                         real(gtclaypg_const)))
        endif
      endif

................................................................................

grasstreein.f

Save gtlaypg_const (param.inc)

        read(11, *) temp, name
        gtlaypg_const = int(temp)
        call ckdata9('grasstreein','gtlaypg',name)
        if (gtlaypg_const .gt. nlayer) then
          write(*,*) 'gtlaypg out of range in grasstree.100.'
          write(*,*) 'Must be <= nlayer '
          write(*,*) 'gtlaypg = ', gtlaypg_const
          write(*,*) 'nlayer = ', nlayer
          write(*,*) 'Resetting gtlaypg to equal nlayer.'
          gtlaypg_const = nlayer
        endif
c ..... For an annual plant initialize the rooting depth to 1
        if (gtfrtcindx .eq. 2) then
          gtlaypg = 1
        else
          gtlaypg = gtlaypg_const
        endif

................................................................................
param.inc

Added gtlaypg_const

................................................................................

drootgt.f

Updated the code for removing carbohydrate storage when coarse roots
die to make it cleaner.  Include annuals and perennials.

c ... Remove C from carbohydrate storage pool based on fraction 
c ... of coarse that died, transfer carbon from the carbohydrate 
c ... storage pool to the C source sink. 
      if (liveCtotal > 0.001) then
        liveCfrac = liveCremoved / liveCtotal
        carboStorage = carbostg(GTARYINDX,UNLABL) +
     &                 carbostg(GTARYINDX,LABELD)
        carboLoss = max(0.0, liveCfrac * carboStorage)
        call csched(carboLoss,carbostg(GTARYINDX,LABELD),carboStorage,
     &              carbostg(GTARYINDX,UNLABL), csrsnk(UNLABL),
     &              carbostg(GTARYINDX,LABELD), csrsnk(LABELD),
     &              1.0, accum)
      endif

c ... Remove from elemental STORAGE pool based on fraction of 
c ... coarse roots that die.

      do 40 iel = 1, nelem
        eloss(iel) = MAX(liveCfrac * gtstg(iel), 0.0)
        if (isnan(eloss(iel))) then
          write(*,*) 'Error in drootgt, eloss(iel) is NaN'
          STOP
        endif
        call flow(gtstg(iel),esrsnk(iel),time,eloss(iel))
40    continue


claypg was being used instead of gtlaypg -mdh 9/6/2019

c ... Soil temperature effect on root death rate
      tempeff = (temp - 10.0)**2 / 4.0 * 0.00175 + 0.1
      tempeff = min(tempeff, 0.5)
c ... Soil water potential effect on root death rate
c ...   Note: maxswpot returns a maximum value of 35, so carctanf <= 0.5.
c ...   swpot is 0.0 at saturation and increases with dryness. -mdh 9/6/2019
c ... claypg was being used instead of gtlaypg! -mdh 9/6/2019
c     watreff = maxswpot(claypg)
      watreff = maxswpot(gtlaypg)
      watreff = carctanf(watreff, 35.0, 0.5, 1.0, 0.05)
c ... Root death is driven by the maximum of the soil temperature
c ... effect and the soil water potential effect on root death rate,
c ... cak - 06/28/2007
      rtdh = max(tempeff, watreff)
      rtdh = max(rtdh, 0.0)
................................................................................

dshootgt.f

TO DO: extend senescence to 30 days?


................................................................................

grasstreeDynC.f

c ... Estimate the fraction of carbon going to the roots
      if (gtfrtcindx .eq. 1) then
c ..... A perennial plant 
        fracrc = (gtfrtcw(1)+gtfrtcw(2)+gtfrtcn(1)+gtfrtcn(2))/4.0
      else
c ..... An annual plant
        fracrc = (gtfrtc(1)+gtfrtc(2))/2.0
      endif

================================================================================
November 6, 2019

The value of cursys was not set correctly when CROP and GRAS are alternated 
in the same block. Check for this condition when CROP, PLTM, FRST, GRAS, or
GFST are encountered in the schedule file.

................................................................................
schedl.f

c ... Look for events in timary that match the current time
c ... If found, handle the event
      if ((timary(evtptr,1) .eq. crtyr) .and.
     &     timary(evtptr,2) .eq. curday) then

        if (cmdary(evtptr) .eq. 'CROP') then
c ....... Since CROP and GRAS can be alternated in the same block.
c ....... If cursys is a grasstree, switch it to a crop. -mdh 11/6/2019
          if (cursys .eq. GRASSTREESYS) then
            cursys = CRPSYS
          endif 
          if (curcrp .ne. typary(evtptr)) then
            call cropin(typary(evtptr))
            call co2eff(time)
          endif

        elseif (cmdary(evtptr) .eq. 'PLTM') then
c ....... Since CROP and GRAS can be alternated in the same block.
c ....... If cursys is a grasstree, switch it to a crop. -mdh 11/6/2019
          if (cursys .eq. GRASSTREESYS) then
            cursys = CRPSYS
          endif 
          doplnt = .true.
          plntday = timary(evtptr, 2)

        elseif (cmdary(evtptr) .eq. 'HARV') then
          dohrvt = .true.
          if (curharv .ne. typary(evtptr)) then
            call harvin(typary(evtptr),curharv)
          endif 
          hrvtday = timary(evtptr, 2)

        elseif (cmdary(evtptr) .eq. 'FRST') then
c ....... Since CROP and GRAS can be alternated in the same block.
c ....... If cursys is a grasstree, switch it to a crop. -mdh 11/6/2019
          if (cursys .eq. GRASSTREESYS) then
            cursys = CRPSYS
          endif 
          dofrst = .true.
          frstday = timary(evtptr, 2)


        :
        :
        :

        elseif (cmdary(evtptr) .eq. 'GRAS') then
c ....... Since CROP and GRAS can be alternated in the same block.
c ....... If cursys is a crop, switch it to a grasstree. -mdh 11/6/2019
          if (cursys .eq. CRPSYS) then
            cursys = GRASSTREESYS
          endif 
          if (curgtre .ne. typary(evtptr)) then
            call grasstreein(typary(evtptr))
            call co2eff(time)
          endif

        elseif (cmdary(evtptr) .eq. 'GFST') then
c ....... Since CROP and GRAS can be alternated in the same block.
c ....... If cursys is a crop, switch it to a grasstree. -mdh 11/6/2019
          if (cursys .eq. CRPSYS) then
            cursys = GRASSTREESYS
          endif 
          dogtfrst = .true.
          gtfrstday = timary(evtptr, 2)
          savegtfrstday = gtfrstday
................................................................................

Growing season accumulators gfert, gomad, gomaeapp, and girrapp need separate
accumulators for grasstrees when CROP and GRAS are alternated 
in the same block.  

................................................................................
fertil.inc

c ... Added separate growing season accumulators for grasstree 
c ... (-mdh 11/6/2019): gtfertot, gtirrtot, gtomadtot, gtomaetot

      common/fertil/aufert, feramt(3), gfertot(3), girrtot, gomadtot,
     &              gomaetot(3), gtfertot(3), gtirrtot, gtomadtot,
     &              gtomaetot(3), ninhib, ninhtm,
     &              Nscalar(12), nreduce, OMADscalar(12)
      real aufert, feramt, gfertot, girrtot, gomadtot, gomaetot,
     &     gtfertot, gtirrtot, gtomadtot, gtomaetot,
     &     ninhib, Nscalar, nreduce, OMADscalar

................................................................................
default.f

................................................................................
dailymoist.f

................................................................................
addomad.f

................................................................................
irrigt.f


================================================================================
November 8, 2019
When CROP and GRAS are alternated, crpval is not set correctly.
To fix this I created a real function in fcrpval.f that can
be called to compute crpval.  The call to this function is in four places.

cropin.f (the call to the function replaces the calculation within this subroutine).
grasstreein.f (the call to the function replaces the calculation within this subroutine).
schedl.f - 2 calls added so crpval gets reset every time keyword "CROP" or "GRAS"
  is found in the block.

================================================================================
TO DO:
November 14, 2019
When REMF(1) == 1.0 (trem.100), get this error:
Setting REMF(1) = 0.99 eliminates the probelm

    Model is running...
  Using only air temp for PET calc
SnowFlag = 1;  snow insulation with basetemp max; Parton paper
 Error in gtleafa, gtleavc < 0.0
 gtleavc =   -7.62939453E-06
Note: The following floating-point exceptions are signalling: IEEE_DIVIDE_BY_ZERO IEEE_UNDERFLOW_FLAG IEEE_DENORMAL
................................................................................
................................................................................
................................................................................


================================================================================
July 17, 2020
Add daily srfc and soil net N mineralization and NO3- leaching output to nflux.csv

initsw.f

      if (files->write_nflux) {
        files->fp_nflux = fopen("nflux.csv", "w"); 
        fprintf(files->fp_nflux, "%8s,%8s,%16s,%17s,%16s,%11s,",
                "time", "dayofyr", "nit_N2O-N(gN/ha)", "dnit_N2O-N(gN/ha)", 
                "dnit_N2-N(gN/ha)","NO-N(gN/ha)");
        fprintf(files->fp_nflux, "%14s,%13s,%15s,%15s,%18s,%8s,%9s,%2s\n",
                "CUM-N2O(gN/ha)", "CUM-NO(gN/ha)",
                "netNmin1(gN/m2)", "netNmin2(gN/m2)", "NO3-N-leach(gN/m2)",
                "Rn2n2o_1", "Rn2n2o_pH", "pH");
      }

................................................................................
wrtnflux.c

    void wrtnflux(float *time, int *curday, double *Nn2oflux,
                  double *Dn2oflux, double *Dn2flux, double *NOflux,
                  double *nflux_sum1, double *nflux_sum2, 
                  double *nmin_sum1, double *nmin_sum2, double *inorglch,
                  double *Rn2n2o_1, double *Rn2n2o_pH, float *pH)
    {

      fprintf(files->fp_nflux, "%8.2f,%8d,", *time, *curday);
      fprintf(files->fp_nflux, "%10.4f,", *Nn2oflux);
      fprintf(files->fp_nflux, "%10.4f,", *Dn2oflux);
      fprintf(files->fp_nflux, "%10.4f,", *Dn2flux);
      fprintf(files->fp_nflux, "%10.4f,", *NOflux);
      fprintf(files->fp_nflux, "%14.4f,", *nflux_sum1);
      fprintf(files->fp_nflux, "%13.4f,", *nflux_sum2);
      fprintf(files->fp_nflux, "%10.6f,", *nmin_sum1);
      fprintf(files->fp_nflux, "%10.6f,", *nmin_sum2);
      fprintf(files->fp_nflux, "%10.6f,", *inorglch);
      fprintf(files->fp_nflux, "%10.4f,", *Rn2n2o_1);
      fprintf(files->fp_nflux, "%10.4f,", *Rn2n2o_pH);
      fprintf(files->fp_nflux, "%10.4f",  *pH);
      fprintf(files->fp_nflux, "\n");


................................................................................
dailymoist.f

Pass inorglch to wrtnflux

      double precision nmin_sum1, nmin_sum2

c ... Store sum of N mineralization accumulators at the beginning of the day (-mdh 8/19/2015)
        nmin_sum1 = metmnr(SRFC,N) + strmnr(SRFC,N) +
     &              s1mnr(SRFC,N)  + s2mnr(SRFC,N)  +
     &              w1mnr(N) + w2mnr(N)

        nmin_sum2 = metmnr(SOIL,N) + strmnr(SOIL,N) +
     &              s1mnr(SOIL,N)  + s2mnr(SOIL,N)  +
     &              s3mnr(N)       + w3mnr(N)



c ... Daily above and below ground N mineralization to nflux.out (-mdh 7/17/2020).
c ... Set nmin_sum1 and nmin_sum2 to daily values by subtracting accumulators
c ... (end of day - beginning of day) (-mdh 8/19/2015).
        nmin_sum1 = metmnr(SRFC,N) + strmnr(SRFC,N) +
     &              s1mnr(SRFC,N)  + s2mnr(SRFC,N)  +
     &              w1mnr(N) + w2mnr(N) - nmin_sum1

        nmin_sum2 = metmnr(SOIL,N) + strmnr(SOIL,N) +
     &              s1mnr(SOIL,N)  + s2mnr(SOIL,N)  +
     &              s3mnr(N) + w3mnr(N) - nmin_sum2

        call wrtnflux(time, curday, Nn2oflux*SCALE, Dn2oflux*SCALE,
     &            Dn2flux*SCALE, NOflux*SCALE, nflux_sum1,
     &            nflux_sum2, nmin_sum1, nmin_sum2, inorglch, 
     &            Rn2n2o_1, Rn2n2opH, ph)




================================================================================
September 16, 2020
cultivgt.f

The elemental storage pool, gtstg, was tied to both stems and coarse 
roots.  This caused gtstg(1) to go negative after a cultivation event
that killed the entire plant. gtstg should only be tied to coarse roots. 
I commented out the 2 sections of code that tied it to stems. 

c ... Some storage pool goes to metabolic surface pool
c     if (gtstemc .gt. 0.0) then
c       do 60 iel = 1, nelem
c         trans = gtstg(iel) * cultra(2)
c         call flow(gtstg(iel),metabe(SRFC,iel),time,trans)
c60     continue
c     endif

c ... Some storage pool goes to metabolic soil pool
c     if (gtstemc .gt. 0.0) then
c       do 70 iel = 1, nelem
c         trans = gtstg(iel) * cultra(3)
c         call flow(gtstg(iel),metabe(SOIL,iel),time,trans)
c70     continue
c     endif

================================================================================
October 28, 2020

Soil temperature with depth is not stored in the binary file, therefore it
can not be set from the previous simulation's value when doing an extend.
Furthermore, was being set to 0.0 at the start of every simulation.
Improve the way soil temperature is initialized. 

Note: extended site file now contains the values of soil->stmtemp, but 
it still needs additional variables added before it can be used.

................................................................................

initsw.c:

      /* Initialize soil layers using sitepar parameters. -mdh 10/21/2020 */
      /* These will be reinitialized from the finer soil layers the first time */
      /* function soiltemp() is called. */
      /* tbot = temp at the bottom of the soil profile (C) */
      /* dTemp = temperature change in each soil layer (C) */

      soil->soiltmin[0] = *stemp;
      soil->soiltmax[0] = *stemp;
      tbot = 0.5*(sitepar->tbotmn + sitepar->tbotmx); 
      dTemp = (tbot - *stemp)/(layers->numlyrs - 1);
      for (ilyr=0; ilyr<MXSWLYR; ilyr++) 
      {
        soil->soiltmin[ilyr] = soil->soiltmin[ilyr-1] + dTemp;
        soil->soiltmax[ilyr] = soil->soiltmax[ilyr-1] + dTemp;
        if (ilyr >= layers->numlyrs-1)
        {
          /* deep soil layers are initialized with sitepar.in parameters */
          soil->soiltmin[ilyr] = tbot;
          soil->soiltmax[ilyr] = tbot;
        }
        soil->soiltavg[ilyr] = 0.5*(soil->soiltmin[ilyr] + soil->soiltmax[ilyr]);
      }

      /* Allow finer layers in soil temperature profile to be initialized wtih non-zero values. */
      /* -mdh 10/28/2020 */

      if (*ext_flag == 0) 
      {
        /* ibot = # of finer 2-cm layers in numlyrs (soils.in layers) */
        ibot = (int)(0.5 + layers->dpthmx[layers->numlyrs-1] / 2.0f);
        soil->stmtemp[0] = *stemp; 
        /* dTemp = temperature change in each 2-cm soil layer (C) */
        dTemp = (tbot - *stemp)/(ibot-1);

        /*
        printf("\ninitsw: dpthmx = %f\n", layers->dpthmx[layers->numlyrs-1]);
        printf("initsw: ibot = %d\n", ibot);
        printf("initsw: tbot = %f\n", tbot);
        printf("initsw: stemp = %f\n", *stemp);
        printf("initsw: dTemp = %f\n", dTemp);
        */

        for (ilyr=1; ilyr<MAXSTLYR; ilyr++) 
        {
          if (ilyr < ibot) {
            soil->stmtemp[ilyr] = soil->stmtemp[ilyr-1] + dTemp;
          } else {
            soil->stmtemp[ilyr] = tbot;
          }
        }
      } 
      else   
      {
        /* Use soil temperature values from the extended site.100 file */
        for (ilyr=0; ilyr<MAXSTLYR; ilyr++) 
        {
          soil->stmtemp[ilyr] = stmtempextend[ilyr];
        }
      }

................................................................................

wrtextsite.f

c ... Save soil temperature values (2cm layers). -mdh 10/21/2020
      call getstmtemp(stmtempextend, MAXSTLYR)

      write(100,25) '*** Soil temperature extend parameters'
      call leftjust(stmtempextend(1), charval)
      write(100,45) charval, "'STMTEMPEXTEND(1)'"
      call leftjust(stmtempextend(2), charval)
      write(100,45) charval, "'STMTEMPEXTEND(2)'"
      :
      call leftjust(stmtempextend(199), charval)
      write(100,45) charval, "'STMTEMPEXTEND(199)'"
      call leftjust(stmtempextend(200), charval)
      write(100,45) charval, "'STMTEMPEXTEND(200)'"

................................................................................

sitein_ext.f

c ... Soil temperature extend parameters. -mdh 10/21/2020
      read(7,*)
      do 370 ii = 1, MAXSTLYR
        read(7,*) stmtempextend(ii), name
        call ckdata('sitein_ext','stmtem',name)
370   continue

================================================================================
November 9, 2020

Save above- and below-ground litter inputs to the cflows.csv and year_cflows.csv
files.  

................................................................................

cflows.inc

These variables were added to the common block:

c ... ainputmetbc(1) - annual accumulator for C inputs to surface metabolic 
c ...                  pool (gC/m2/yr)
c ... ainputmetbc(2) - annual accumulator for C inputs to soil metabolic 
c ...                  pool (gC/m2/yr)
c ... ainputstrucc(1)- annual accumulator for C inputs to surface structural 
c ...                  pool (gC/m2/yr)
c ... ainputstrucc(2)- annual accumulator for C inputs to soil structural 
c ...                  pool (gC/m2/yr)
c ... ainputmetbn(1) - annual accumulator for N inputs to surface metabolic 
c ...                  pool (gN/m2/yr)
c ... ainputmetbn(2) - annual accumulator for N inputs to soil metabolic 
c ...                  pool (gN/m2/yr)
c ... ainputstrucn(1)- annual accumulator for N inputs to surface structural 
c ...                  pool (gN/m2/yr)
c ... ainputstrucn(2)- annual accumulator for N inputs to soil structural 
c ...                  pool (gN/m2/yr)

c ... inputmetabc(1) - C inputs to surface metabolic pool (gC/m2/day)
c ... inputmetabc(2) - C inputs to soil metabolic pool (gC/m2/day)
c ... inputstrucc(1) - C inputs to surface structural pool (gC/m2/day)
c ... inputstrucc(2) - C inputs to soil structural pool (gC/m2/day)
c ... inputmetabn(1) - N inputs to surface metabolic pool (gN/m2/day)
c ... inputmetabn(2) - N inputs to soil metabolic pool (gN/m2/day)
c ... inputstrucn(1) - N inputs to surface structural pool (gN/m2/day)
c ... inputstrucn(2) - N inputs to soil structural pool (gN/m2/day)

................................................................................
annacc.f

      ainputmetabc(1) = 0.0
      ainputmetabc(2) = 0.0
      ainputmetabn(1) = 0.0
      ainputmetabn(2) = 0.0
      ainputstrucc(1) = 0.0
      ainputstrucc(2) = 0.0
      ainputstrucn(1) = 0.0
      ainputstrucn(2) = 0.0

................................................................................
csa_main.f

        call wrtyrcflows(time, asom11tosom21, asom12tosom22,
     &                   asom12tosom3, asom21tosom11, asom21tosom22,
     &                   asom22tosom12, asom22tosom3, asom3tosom12,
     &                   ametc1tosom11, ametc2tosom12, astruc1tosom11,
     &                   astruc1tosom21, astruc2tosom12,
     &                   astruc2tosom22, awood1tosom11, awood1tosom21,
     &                   awood2tosom11, awood2tosom21, awood3tosom12,
     &                   awood3tosom22, adwood1tosom11, adwood1tosom21,
     &                   adwood2tosom11, adwood2tosom21,
     &                   ainputmetabc(1), ainputmetabc(2),
     &                   ainputstrucc(1), ainputstrucc(2),
     &                   ainputmetabn(1), ainputmetabn(2),
     &                   ainputstrucn(1), ainputstrucn(2))

................................................................................
default.f

      ainputmetabc(1) = 0.0
      ainputmetabc(2) = 0.0
      ainputmetabn(1) = 0.0
      ainputmetabn(2) = 0.0
      ainputstrucc(1) = 0.0
      ainputstrucc(2) = 0.0
      ainputstrucn(1) = 0.0
      ainputstrucn(2) = 0.0

      inputmetabc(1) = 0.0
      inputmetabc(2) = 0.0
      inputmetabn(1) = 0.0
      inputmetabn(2) = 0.0
      inputstrucc(1) = 0.0
      inputstrucc(2) = 0.0
      inputstrucn(1) = 0.0
      inputstrucn(2) = 0.0

................................................................................
dailymoist.f

        call wrtcflows(time, curday, som11tosom21, som12tosom22,
     &                 som12tosom3, som21tosom11, som21tosom22,
     &                 som22tosom12, som22tosom3, som3tosom12,
     &                 metc1tosom11, metc2tosom12, struc1tosom11,
     &                 struc1tosom21, struc2tosom12, struc2tosom22,
     &                 wood1tosom11, wood1tosom21, wood2tosom11,
     &                 wood2tosom21, wood3tosom12, wood3tosom22,
     &                 dwood1tosom11, dwood1tosom21, dwood2tosom11,
     &                 dwood2tosom21,
     &                 inputmetabc(1), inputmetabc(2),
     &                 inputstrucc(1), inputstrucc(2),
     &                 inputmetabn(1), inputmetabn(2),
     &                 inputstrucn(1), inputstrucn(2)) 

      endif
................................................................................
partit.f

Where ever cinput is updated, the daily and annual C litter input variables are 
also. The litter N input varaibles are updated at the bottom of the subroutine. 

        cinput = cinput + accum(UNLABL) + accum(LABELD)
        inputmetabc(lyr) = inputmetabc(lyr) + caddm
        ainputmetabc(lyr) = ainputmetabc(lyr) + caddm

        cinput = cinput + accum(UNLABL) + accum(LABELD)
        inputstrucc(lyr) = inputstrucc(lyr) + cadds
        ainputstrucc(lyr) = ainputstrucc(lyr) + cadds

        cinput = cinput + c13struc
        inputstrucc(lyr) = inputstrucc(lyr) + c13struc
        ainputstrucc(lyr) = ainputstrucc(lyr) + c13struc

        cinput = cinput + c12struc
        inputstrucc(lyr) = inputstrucc(lyr) + c12struc
        ainputstrucc(lyr) = ainputstrucc(lyr) + c12struc

        cinput = cinput + c13met
        inputmetabc(lyr) = inputmetabc(lyr) + c13met
        ainputmetabc(lyr) = ainputmetabc(lyr) + c13met

        cinput = cinput + c12met
        inputmetabc(lyr) = inputmetabc(lyr) + c12met
        ainputmetabc(lyr) = ainputmetabc(lyr) + c12met

c ... Partition mineral elements into structural and metabolic
      do 20 iel = 1, nelem

c ..... Flow into structural
        eadds = cadds/rcestr(iel)
        if(isnan(eadds)) then
          write(*,*) 'partit eadds is NaN'
          STOP
        endif
        call flow(edonor(iel),struce(lyr,iel),time,eadds)
c ..... Flow into metabolic
        eaddm = epart(iel)+dirabs(iel)-eadds
        if(isnan(eaddm)) then
          write(*,*) 'partit eaddm is NaN'
          STOP
        endif
        call flow(edonor(iel),metabe(lyr,iel),time,eaddm)
        if (iel .eq. N) then
          inputstrucn(lyr) = inputstrucn(lyr) + eadds
          ainputstrucn(lyr) = ainputstrucn(lyr) + eadds
          inputmetabn(lyr) = inputmetabn(lyr) + eaddm
          ainputmetabn(lyr) = ainputmetabn(lyr) + eaddm
        endif
20    continue



................................................................................
simsom.f

        call dailymoist(curday, agdefacsum, bgdefacsum, bgwfunc,
     &                  frlech, co2val, saccum, evapdly, intrcpt, melt,
     &                  outflow, petdly, runoffdly, sublim, trandly,
     &                  avgstemp, scenfrac, rpeff, watr2sat,
     &                  prev_mcprd2 + prev_mcprd3, Com, avgst_10cm, TI,
     &                  SI, Cr, Eh, Feh, CH4_prod, CH4_Ep, CH4_Ebl,
     &                  CH4_oxid, aetdly)


c ..... Reset daily litter input accumulators here instead of beginning 
c ..... of the day. These variables are written to cflows.csv in 
c ..... dailymoist, but most calls to partit.f, where these variables 
c ..... are set, happen after the call to dailymoist. -mdh 11/9/2020
        inputmetabc(1) = 0.0
        inputmetabc(2) = 0.0
        inputmetabn(1) = 0.0
        inputmetabn(2) = 0.0
        inputstrucc(1) = 0.0
        inputstrucc(2) = 0.0
        inputstrucn(1) = 0.0
        inputstrucn(2) = 0.0
................................................................................
wrtcflows.c

    void wrtcflows(float *time, int *curday, float *som11tosom21,
                   float *som12tosom22, float *som12tosom3,
                   float *som21tosom11, float *som21tosom22,
                   float *som22tosom12, float *som22tosom3,
                   float *som3tosom12, float *metc1tosom11,
                   float *metc2tosom12, float *struc1tosom11,
                   float *struc1tosom21, float *struc2tosom12,
                   float *struc2tosom22, float *wood1tosom11,
                   float *wood1tosom21, float *wood2tosom11,
                   float *wood2tosom21, float *wood3tosom12,
                   float *wood3tosom22, float *dwood1tosom11,
                   float *dwood1tosom21, float *dwood2tosom11,
                   float *dwood2tosom21,
                   float *inputmetabc1, float *inputmetabc2,
                   float *inputstrucc1, float *inputstrucc2,
                   float *inputmetabn1, float *inputmetabn2,
                   float *inputstrucn1, float *inputstrucn2)

      fprintf(files->fp_cflows, "%12.8f,", *inputmetabc1);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabc2);
      fprintf(files->fp_cflows, "%12.8f,", *inputstrucc1);
      fprintf(files->fp_cflows, "%12.8f,", *inputstrucc2);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabn1);
      fprintf(files->fp_cflows, "%12.8f,", *inputmetabn2);
      fprintf(files->fp_cflows, "%12.8f,", *inputstrucn1);
      fprintf(files->fp_cflows, "%12.8f",  *inputstrucn2);
      fprintf(files->fp_cflows, "\n");
................................................................................
wrtyrcflows.c

    void wrtyrcflows(float *time, float *asom11tosom21, float *asom12tosom22,
                     float *asom12tosom3, float *asom21tosom11,
                     float *asom21tosom22, float *asom22tosom12,
                     float *asom22tosom3, float *asom3tosom12,
                     float *ametc1tosom11, float *ametc2tosom12,
                     float *astruc1tosom11, float *astruc1tosom21,
                     float *astruc2tosom12, float *astruc2tosom22,
                     float *awood1tosom11, float *awood1tosom21,
                     float *awood2tosom11, float *awood2tosom21,
                     float *awood3tosom12, float *awood3tosom22,
                     float *adwood1tosom11, float *adwood1tosom21,
                     float *adwood2tosom11, float *adwood2tosom21,
                     float *ainputmetabc1, float *ainputmetabc2,
                     float *ainputstrucc1, float *ainputstrucc2,
                     float *ainputmetabn1, float *ainputmetabn2,
                     float *ainputstrucn1, float *ainputstrucn2

      fprintf(files->fp_yrcflows, "%12.6f,", *ainputmetabc1);
      fprintf(files->fp_yrcflows, "%12.6f,", *ainputmetabc2);
      fprintf(files->fp_yrcflows, "%12.6f,", *ainputstrucc1);
      fprintf(files->fp_yrcflows, "%12.6f,", *ainputstrucc2);
      fprintf(files->fp_yrcflows, "%12.6f,", *ainputmetabn1);
      fprintf(files->fp_yrcflows, "%12.6f,", *ainputmetabn2);
      fprintf(files->fp_yrcflows, "%12.6f,", *ainputstrucn1);
      fprintf(files->fp_yrcflows, "%12.6f",  *ainputstrucn2);
      fprintf(files->fp_yrcflows, "\n");
................................................................................

================================================================================

TO DO:
Allow more than 20 chars for weather file names in the schedule file.
Update wet deposition calculations based on daily precipitation.


