
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      dshootgt.f
c
c  FUNCTION:  subroutine dshootgt()
c
c  PURPOSE:   Compute death of leaves and stems in a grasstree.
c
c  AUTHOR:    New function derived from dshoot.
c             Melannie Hartman Jan. 24, 2014
c
c  FUNCTION ARGUMENTS:
c    bgwfunc 	- soil moisture effect on decomposition (0-1) (See calcdefac) 
c    tfrac	- time fraction (1/# days in current month)  
c    curday	- current day of year (1-366)
c 
c  grasstree.100 parameters:
c    gtfsdeth(1) - Maximum leaf death rate at very dry soil conditions 
c		   (fraction/month); to get the monthly shoot death rate, 
c		   this fraction is multiplied by a reduction factor 
c		   depending on the soil water status. Live leaves that die 
c		   are transferred to dead attached leaf pool.
c
c    gtfsdeth(2) - Maximum stem death rate at very dry soil conditions 
c		   (fraction/month); to get the monthly shoot death rate, 
c		   this fraction is multiplied by a reduction factor 
c		   depending on the soil water status. Live stems that die 
c		   are transferred to standing dead stem pool.
c
c    gtfsdeth(3) - Fraction of leaves which die during senescence month; 
c		   must be >= 0.4. Live leaves that die are transferred 
c		   to dead attached leaf pool.
c
c    gtfsdeth(4) - Fraction of stems which die during senescence month; 
c		   must be >= 0.4. Live stems that die are transferred 
c		   to standing dead stem pool.
c
c    gtfsdeth(5) - Additional fraction of leaves which die when live leaf C 
c		   is greater than gtfsdeth(6). Live stems that die are  
c		   transferred to dead attached leaf pool.
c
c    gtfsdeth(6) - Level of live leaf C which shading occurs and shoot  
c		   senescence increases.
c 
c    gtrtf(3)    - Fraction of N,P,S retranslocated from grasstree 
c                  leaves at death.
c
c***********************************************************************

      subroutine dshootgt(curday, bgwfunc, tfrac)

      implicit none
      include 'const.inc'
      include 'dovars.inc'
      include 'param.inc'
c     include 'parcp.inc'
      include 'pargt.inc'
      include 'pheno.inc'
      include 'plot1.inc'
      include 'plot2.inc'
      include 'plot4.inc'
      include 'zztim.inc'

c ... Argument declarations
      real    bgwfunc
      real    tfrac
      integer curday

c ... Simulate death of leaves and stems. 

c ... Local variables
      integer   iel, indexlv, indexstm
      real      accum(ISOS), dthppt, tostore
      real      fdeth(2), sdethc(2), sdethe(2) 
      real      death_volpl(2)

c ... Death of leaves and stems.  Most leaves and stems die during 
c ... at senescence. During other times, only a small fraction of 
c ... leaves and stems die.

      if ((gtleavc + gtstemc) .le. 0) return

      accum(UNLABL) = 0.0
      accum(LABELD) = 0.0
      fdeth(GTLEAF) = 0.0
      fdeth(GTSTEM) = 0.0
      sdethc(GTLEAF) = 0.0
      sdethc(GTSTEM) = 0.0

      if (isnan(bgwfunc)) then
        write(*,*) 'Error in dshootgt, bgwfunc is NaN'
        STOP
      endif

      indexlv = 1
      indexstm = 2
      dthppt = 1.0 - bgwfunc

      if (dosene .and. (seneday .eq. curday)) then

c ..... Death due to frost kill or senescence
        indexlv = 3
        indexstm = 4

        dthppt = 1.0
        fdeth(GTLEAF) = gtfsdeth(indexlv) * dthppt
        fdeth(GTSTEM) = gtfsdeth(indexstm) * dthppt

        if (isnan(fdeth(GTLEAF))) then
          write(*,*) 'Error in dshootgt 1, fdeth(GTLEAF) is NaN'
          STOP
        endif
        if (isnan(fdeth(GTSTEM))) then
          write(*,*) 'Error in dshootgt 1, fdeth(GTSTEM) is NaN'
          STOP
        endif

c ..... It looks like senescence happens all in one day, even 
c ..... though the senecnt is incremented in simsom for 30 days.
        senecnt = 1
        thermunits = 0.0
        accumdd = .false.
        senmschd = .false.

      else
c ..... Death due to dry conditions, but not frost kill or senescence
        fdeth(GTLEAF) = gtfsdeth(indexlv) * tfrac * dthppt
        fdeth(GTSTEM) = gtfsdeth(indexstm) * tfrac * dthppt

        if (isnan(gtfsdeth(indexlv))) then
          write(*,*) 'Error in dshootgt 2, gtfsdeth(indexlv) is NaN'
          STOP
        endif
        if (isnan(gtfsdeth(indexstm))) then
          write(*,*) 'Error in dshootgt 2, gtfsdeth(indexstm) is NaN'
          STOP
        endif
        if (isnan(fdeth(GTLEAF))) then
          write(*,*) 'Error in dshootgt 2, fdeth(GTLEAF) is NaN'
          STOP
        endif
        if (isnan(fdeth(GTSTEM))) then
          write(*,*) 'Error in dshootgt 2, fdeth(GTSTEM) is NaN'
          STOP
        endif

      endif

c ... Increase the death rate of leaves to account for effect of shading.
c ... This is not done during senescence (when the death rate is greater
c ... than or equal to 0.4)

      if ((gtfsdeth(indexlv) .lt. 0.4) .and.
     &    (gtleavc .gt. gtfsdeth(6))) then
        fdeth(GTLEAF) = fdeth(GTLEAF) + gtfsdeth(5) * tfrac
      endif

c ....Constrain the fraction
c ... Allow senescence to kill 100% of the leaves and stems
      fdeth(GTLEAF) = min(fdeth(GTLEAF), 1.0)
      fdeth(GTSTEM) = min(fdeth(GTSTEM), 1.0)

c ... Calculate the amounts and flow

c ... sdethc(*) are the amounts of leaves and stems that die
      sdethc(GTLEAF) = gtleavc * fdeth(GTLEAF)
      sdethc(GTSTEM) = gtstemc * fdeth(GTSTEM)

c ... Flow of C from live leaves to dead attached leaves
      if (isnan(sdethc(GTLEAF))) then
        write(*,*) 'Error in dshootgt, sdethc(GTLEAF) is NaN'
        STOP
      endif
      if (gtleavc .gt. 0.0) then
c       write(*,*) 'dshootgt: sdethc(GTLEAF) =', sdethc(GTLEAF)
        call csched(sdethc(GTLEAF),gtlvcis(LABELD),gtleavc,
     &              gtlvcis(UNLABL),gtdlvcis(UNLABL),
     &              gtlvcis(LABELD),gtdlvcis(LABELD),
     &              1.0,accum)
      endif
 
c ... Flow of C from live stems to dead standing stems
      if (isnan(sdethc(GTSTEM))) then
        write(*,*) 'Error in dshootgt, sdethc(GTSTEM) is NaN'
        STOP
      endif

      if (gtstemc .gt. 0.0) then
c       write(*,*) 'dshootgt: sdethc(GTSTEM) =', sdethc(GTSTEM)
        call csched(sdethc(GTSTEM),gtstmcis(LABELD),gtstemc,
     &              gtstmcis(UNLABL),gtdstmcis(UNLABL),
     &              gtstmcis(LABELD),gtdstmcis(LABELD),
     &              1.0,accum)
      endif

      do 10 iel = 1, nelem
        sdethe(GTLEAF) = fdeth(GTLEAF) * gtleave(iel)
        sdethe(GTSTEM) = fdeth(GTSTEM) * gtsteme(iel)

c ..... Use the local variable death_volpl so that volatilization that
c ..... occurs at harvest and senescence can both be tracked, see harvst,
c ..... cak - 01/02

        if (iel .eq. N) then
          death_volpl(GTLEAF) = vlossgt * sdethe(GTLEAF)
          death_volpl(GTSTEM) = vlossgt * sdethe(GTSTEM)

          if (isnan(death_volpl(GTLEAF))) then
            write(*,*) 'Error in dshootgt, death_volpl(GTLEAF) is NaN'
            STOP
          endif
          if (isnan(death_volpl(GTSTEM))) then
            write(*,*) 'Error in dshootgt, death_volpl(GTSTEM) is NaN'
            STOP
          endif

          call flow(gtleave(iel),esrsnk(iel),time,death_volpl(GTLEAF))
          call flow(gtsteme(iel),esrsnk(iel),time,death_volpl(GTSTEM))

          volpl = volpl + death_volpl(GTLEAF) + death_volpl(GTSTEM)
          volpla = volpla + death_volpl(GTLEAF) + death_volpl(GTSTEM)
          volpac = volpac + death_volpl(GTLEAF) + death_volpl(GTSTEM)

          sdethe(GTLEAF) = sdethe(GTLEAF) - death_volpl(GTLEAF) 
          sdethe(GTSTEM) = sdethe(GTSTEM) - death_volpl(GTSTEM) 
        endif

c ..... Retranslocation of E happens only from leaves, not stems
c ..... Partition sdethe(GTLEAF) between storage and dead attached leaves

        tostore = sdethe(GTLEAF) * gtrtf(iel)
        if (isnan(tostore)) then
          write(*,*) 'Error in dshootgt, tostore is NaN'
          STOP
        endif
        call flow(gtleave(iel),gtstg(iel),time,tostore)

        sdethe(GTLEAF) = sdethe(GTLEAF) * (1 - gtrtf(iel))
        if (isnan(sdethe(GTLEAF))) then
          write(*,*) 'Error in dshootgt, sdethe(GTLEAF) is NaN'
          STOP
        endif
        call flow(gtleave(iel),gtdleave(iel),time,sdethe(GTLEAF))

c ..... Transfer sdethe(GTSTEM) to dead standing stems
        if (isnan(sdethe(GTSTEM))) then
          write(*,*) 'Error in dshootgt, sdethe(GTSTEM) is NaN'
          STOP
        endif
        call flow(gtsteme(iel),gtdsteme(iel),time,sdethe(GTSTEM))

10    continue

      return
      end
