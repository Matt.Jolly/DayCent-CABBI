/*****************************************************************************
**
**  FILE:      wrtnflux.c
**
**  FUNCTION:  void wrtnflux()
**
**  PURPOSE:   This function writes daily values for trace gas fluxes.
**
**  INPUTS:
**    time       - current simulation time (years)
**    curday     - the day of the year (1..366)
**    Nn2oflux   - nitrous oxide nitrification (gN/ha/day)
**    Dn2oflux   - nitrous oxide denitrification (gN/ha/day)
**    Dn2flux    - elemental inert nitrogen gas denitrification (gN/ha/day)
**    NOflux     - nitric oxide (gN/ha/day)
**    nflux_sum1 - annual accumulator for nitrous oxide (gN/ha)
**    nflux_sum2 - annual accumulator for nitric oxide (gN/ha)
**    nmin_sum1  - above ground net mineralization (gN/m2/day) (added 7/17/2020 -mdh)
**    nmin_sum2  - below ground net mineralization (gN/m2/day) (added 7/17/2020 -mdh)
**    inorglch   - NO3-N leaching (gN/m2/day) (added 7/16/2020 -mdh)
**    Rn2n2o_1   - Ratio of N2:N2O in topmost layer where denitrification occurs  
**                 before adjustment for pH effect (added -mdh 2/3/2019)
**    Rn2n2opH   - Ratio of N2:N2O inin topmost layer where denitrification occurs  
**                 adjusted for pH effect (added -mdh 2/3/2019)
**    pHadj      - soil pH in top 3 soils.in layers, initialized in function 
**                 setlyrs() and adjusted by pH scaling factor (if used). This 
**                 is the pH value used for pH effect on decomposition and pH
**                 effect nitrification (added -mdh 2/3/2019)
**
**  GLOBAL VARIABLES:
**    None
**
**  EXTERNAL VARIABLES:
**    files->fp_nflux    - file pointer to nflux.out output file
**    files->write_nflux - flag to indicate if nflux.out output file should
**                         be created, 0 = do not create, 1 = create
**
**  LOCAL VARIABLES:
**    None
**
**  OUTPUTS:
**    None
**
**  CALLED BY:
**    dailymoist()
**
*****************************************************************************/

#include <math.h>
#include <stdio.h>
#include "soilwater.h"

    void wrtnflux(float *time, int *curday, double *Nn2oflux,
                  double *Dn2oflux, double *Dn2flux, double *NOflux,
                  double *nflux_sum1, double *nflux_sum2, 
                  double *nmin_sum1, double *nmin_sum2, double *inorglch,
                  double *Rn2n2o_1, double *Rn2n2o_pH, float *pH)
    {

      extern FILES_SPT files;

      if (!files->write_nflux) {
        return;
      }

      fprintf(files->fp_nflux, "%8.2f,%8d,", *time, *curday);
      fprintf(files->fp_nflux, "%10.4f,", *Nn2oflux);
      fprintf(files->fp_nflux, "%10.4f,", *Dn2oflux);
      fprintf(files->fp_nflux, "%10.4f,", *Dn2flux);
      fprintf(files->fp_nflux, "%10.4f,", *NOflux);
      fprintf(files->fp_nflux, "%14.4f,", *nflux_sum1);
      fprintf(files->fp_nflux, "%13.4f,", *nflux_sum2);
      fprintf(files->fp_nflux, "%10.6f,", *nmin_sum1);
      fprintf(files->fp_nflux, "%10.6f,", *nmin_sum2);
      fprintf(files->fp_nflux, "%10.6f,", *inorglch);
      fprintf(files->fp_nflux, "%10.4f,", *Rn2n2o_1);
      fprintf(files->fp_nflux, "%10.4f,", *Rn2n2o_pH);
      fprintf(files->fp_nflux, "%10.4f",  *pH);
      fprintf(files->fp_nflux, "\n");

      return;
    }
