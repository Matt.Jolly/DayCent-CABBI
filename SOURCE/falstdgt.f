
c               Copyright 1993 Colorado State University
c                       All Rights Reserved
c
c*****************************************************************************
c
c  FILE:      falstdgt.f
c
c  FUNCTION:  subroutine falstdgt()
c
c  PURPOSE:   Compute drop of dead leaves and fall of dead stems for grasstree.
c
c  AUTHOR:    New function derived from falstd (crop/grass).
c             Melannie Hartman Feb. 19, 2014
c
c  FUNCTION ARGUMENTS:
c    gtlig(*) 	- lignin fraction for each live grasstree part
c    tfrac	- time fraction (1/# days in current month)  
c
c  COMMENTS:
c    Use gtlig(*) for live parts only. 
c    There is no gtlig(GTDLEAF) or gtlig(GTDSTEM).
c
c*****************************************************************************

      subroutine falstdgt(gtlig,tfrac)

      implicit none
      include 'const.inc'
      include 'param.inc'
c     include 'parcp.inc'
      include 'pargt.inc'
      include 'plot4.inc'

c ... Argument declarations
      real gtlig(GTLIVPARTS)
      real tfrac

c ... Simulate fall of standing dead leaves and stems.

c ... Local variables
      integer   iel
      real      fr14, fsdc, recres(MAXIEL)

c ... Drop dead leaves....
      if (gtdleavc .gt. 0) then
        fsdc = gtdleavc * sdfallrt(1) * tfrac
        do 10 iel = 1, nelem
          recres(iel) = gtdleave(iel)/gtdleavc
10      continue
        fr14 = gtdlvcis(LABELD)/gtdleavc
        call partit(fsdc,recres,1,gtdlvcis,gtdleave,gtlig(GTLEAF),fr14)
      endif

c ... Fall of dead stems....
      if (gtdstemc .gt. 0) then
        fsdc = gtdstemc * sdfallrt(2) * tfrac
        do 20 iel = 1, nelem
          recres(iel) = gtdsteme(iel)/gtdstemc
20      continue
        fr14 = gtdstmcis(LABELD)/gtdstemc
        call partit(fsdc,recres,1,gtdstmcis,gtdsteme,gtlig(GTSTEM),fr14)
      endif

      return
      end
